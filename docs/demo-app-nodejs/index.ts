/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as Framework from 'kephats';

const app = Framework.app();


console.log('In your NodeJs app');
console.log(app.kernel().version());
