'use strict';

/**
 *  This file is part of the KephaJS project.
 *
 *  (c) Grégoire OLIVEIRA SILVA
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

import * as Framework from 'kephats';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const server = Framework.server(require(process.cwd() + '/config/config.json'));

// Init du server
server.init = (): void => {

    // Forms
    server.setFormBuilders({
        message: require('./Form/MessageFormBuilder').MessageFormBuilder
    });
};

// On lance le server
server.run();
