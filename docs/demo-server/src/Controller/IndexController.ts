/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Controller } from '../../../../src/index';

export class IndexController extends Controller {

    public foo = '';
    public okAsync = false;

    setViews (): Record<string, string | null> {

        return {
            blank: null,
            alias: 'index.html'
        };
    }

    indexAction (): void {

        this.foo = this.$user.get('foo');
        this.$view.title = 'Hello world';
        this.$view.message = 'Salut';
        this.$view.foobar = 'test';
    }

    blankAction (): void { return; }

    aliasAction (): void { return; }

    testAction (): void { return; }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    slotAction (vars: Record<string, any>): void {

        this.$view.$setLayout(false);

        if (vars.a)
            this.$view.a = vars.a;

        this.get('$config').set('testRequest', this.$request, true);
        this.get('$config').set('testResponse', this.$response, true);
        this.get('$config').set('testUser', this.$user, true);
    }

    asyncAction (): Promise<void> {

        return new Promise(resolve => {
            this.okAsync = true;
            resolve();
        });
    }
}
