/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Controller } from 'kephats';

export class MessageController extends Controller {

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    indexAction (vars: Record<string, any>): void {

        const form = this.get('$forms').build('message');
        form.setAction('./message.html', 'post');

        this.$view.title = 'Messages';
        this.$view.form = form;

        if (this.$request.method == "POST") {

            const ret = form.process(vars);

            if (ret) {
                this.$view.message = ret.auteur + ' a envoyé : <br><b>'
                    + ret.titre + '</b><br>' + ret.message;
            }
        }
    }
}
