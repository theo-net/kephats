/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Controller } from '../../../../src/index';

export class ErrorController extends Controller {

    e403Action (): void {

        return;
    }

    e404Action (): void {

        return;
    }

    e500Action (): void {

        return;
    }
}
