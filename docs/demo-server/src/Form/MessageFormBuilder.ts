/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { FormBuilder } from '../../../../src/index';

export class MessageFormBuilder extends FormBuilder {

    public a = false;

    build (): void {

        this.a = true;

        this.getForm()
            .add('string', {
                label: 'Titre du message',
                name: 'titre',
                required: true
            })
            .add('string', {
                label: 'E-Mail de l\'auteur',
                name: 'auteur'
            }, 'mail')
            .add('text', {
                label: 'Message',
                name: 'message',
                rows: 8,
                cols: 60,
                required: true
            })
            .add('submit', {
                name: 'submit',
                value: 'Envoyer'
            });
    }
}
