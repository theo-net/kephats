Utilisation de `Table`
======================

## Usage simple

Par défaut les headers sont en rouge et les bordures en gris.

```javascript
      const table = new Table({ head: ['a', 'b'] });

      table.push(['c', 'd']);

```


## Couleurs désactivées

    ┌──────┬─────────────────────┬─────────────────────────┬─────────────────┐
    │ Rel  │ Change              │ By                      │ When            │
    ├──────┼─────────────────────┼─────────────────────────┼─────────────────┤
    │ v0.1 │ Testing something … │ rauchg@gmail.com        │ 7 minutes ago   │
    ├──────┼─────────────────────┼─────────────────────────┼─────────────────┤
    │ v0.1 │ Testing something … │ rauchg@gmail.com        │ 8 minutes ago   │
    └──────┴─────────────────────┴─────────────────────────┴─────────────────┘

```javascript
    const table = new Table({
        head: ['Rel', 'Change', 'By', 'When'],
        style: {
            head: [],   // désactivation des couleurs
            border: []  // désactivation des couleurs
        },
        colWidths: [6, 21, 25, 17]  // définit la largeur des colones (optionel)
    });

    table.push(
        ['v0.1', 'Un test', 'foobar', 'il y a 7 minutes']
        ['v0.1', 'Un test', 'foobar', 'il y a 8 minutes']
    );

```


## Créé un tableau vertical avec un objet associant des clés et des valeurs

    ┌────┬──────┐
    │v0.1│Foobar│
    ├────┼──────┤
    │v0.1│Foobar│
    └────┴──────┘

```javascript
    const table = new Table({ style: { 'padding-left': 0, 'padding-right': 0, head: [], border: [] } });

      table.push(
          {'v0.1': 'Foobar'},
          {'v0.1': 'Foobar'}
      );

```


## Un autre type de tableau

    ┌────────┬────────┬────────┐
    │        │Header 1│Header 2│
    ├────────┼────────┼────────┤
    │Header 3│v0.1    │Foobar  │
    ├────────┼────────┼────────┤
    │Header 4│v0.1    │Foobar  │
    └────────┴────────┴────────┘

```javascript
    const table = new Table({ head: ["", "Header 1", "Header 2"], style: { 'padding-left': 0, 'padding-right': 0, head: [], border: [] } });

    table.push(
        {"Header 3": ['v0.1', 'Foobar'] },
        {"Header 4": ['v0.1', 'Foobar'] }
    );

```


## Stilisation des bordures

    ╔══════╤═════╤══════╗
    ║ foo  │ bar │ baz  ║
    ╟──────┼─────┼──────╢
    ║ frob │ bar │ quuz ║
    ╚══════╧═════╧══════╝

```javascript
    const table = new Table({
        chars: {
            'top': '═',
            'top-mid': '╤',
            'top-left': '╔',
            'top-right': '╗',
            'bottom': '═',
            'bottom-mid': '╧',
            'bottom-left': '╚',
            'bottom-right': '╝',
            'left': '║',
            'left-mid': '╟',
            'right': '║',
            'right-mid': '╢'
        },
        style: {
            head: [],
            border: []
        }
    });

    table.push(
        ['foo', 'bar', 'baz'],
        ['frob', 'bar', 'quuz']
    );

```


## Utilise la coloration pour coloridser des cellules (plusieurs lignes dans une case)

```javascript
    const table = new Table({ style: {border: [], header: [] } });
    const color = kernel.get('$color');

    table.push([
        colors.red('Hello\nhow\nare\nyou?'),
        colors.blue('I\nam\nfine\nthanks!')
    ]);

```


### Définit `wordWrap` à `true` pour permettre le retour à la ligne automatique

    ┌───────┬─────────┐
    │ Hello │ I am    │
    │ how   │ fine    │
    │ are   │ thanks! │
    │ you?  │         │
    └───────┴─────────┘

```javascript
    const table = new Table({
        style: { border: [], header: [] },
        colWidths: [7, 9],
        wordWrap: true
    });

    table.push([
        'Hello how are you?',
        'I am fine thanks!'
    ]);

```


## colSpan

    ┌───────────────┐
    │ greetings     │
    ├───────────────┤
    │ greetings     │
    ├───────┬───────┤
    │ hello │ howdy │
    └───────┴───────┘

```javascript
    const table = new Table({ style: { head: [], border: [] } });

    table.push(
        [{ colSpan: 2, content: 'greetings' }],
        [{ colSpan: 2, content: 'greetings' }],
        ['hello', 'howdy']
    );

```


## Autre colSpan

    ┌───────┬───────┐
    │ hello │ howdy │
    ├───────┴───────┤
    │ greetings     │
    ├───────────────┤
    │ greetings     │
    └───────────────┘

```javascript
    const table = new Table({ style: { head: [], border: [] } });

    table.push(
        ['hello', 'howdy'],
        [{ colSpan: 2, content: 'greetings' }],
        [{ colSpan: 2, content: 'greetings' }]
    );

```


## rowSpan

    ┌───────────┬───────────┬───────┐
    │ greetings │           │ hello │
    │           │ greetings ├───────┤
    │           │           │ howdy │
    └───────────┴───────────┴───────┘

```javascript
    const table = new Table({ style: { head: [], border: [] } });

    table.push(
        [{ rowSpan: 2, content: 'greetings' }, { rowSpan: 2, content: 'greetings', vAlign: 'center' }, 'hello'],
        ['howdy']
    );

```


## Autre rowSpan

    ┌───────┬───────────┬───────────┐
    │ hello │ greetings │           │
    ├───────┤           │           │
    │ howdy │           │ greetings │
    └───────┴───────────┴───────────┘

```javascript
    const table = new Table({ style: { head: [], border: [] } });

    table.push(
        ['hello', {rowSpan: 2, content: 'greetings' }, { rowSpan: 2, content: 'greetings', vAlign: 'bottom' }],
        ['howdy']
    );

```


## Mix de colSpan et rowSpan

    ┌───────┬─────┬────┐
    │ hello │ sup │ hi │
    ├───────┤     │    │
    │ howdy │     │    │
    ├───┬───┼──┬──┤    │
    │ o │ k │  │  │    │
    └───┴───┴──┴──┴────┘

```javascript
    const table = new Table({ style: { head: [], border: [] } });

    table.push(
        [{ content: 'hello', colSpan: 2 }, { rowSpan: 2, colSpan: 2, content: 'sup' }, { rowSpan: 3, content: 'hi' }],
        [{ content: 'howdy', colSpan: 2 }],
        ['o', 'k', '', '']
    );

```


## Contenu multi-ligne dans des cellules rowSpan

    ┌───────┬───────────┬───────────┐
    │ hello │ greetings │ greetings │
    ├───────┤ friends   │ friends   │
    │ howdy │           │           │
    └───────┴───────────┴───────────┘

```javascript
    const table = new Table({ style: { head: [], border: [] } });

    table.push(
        ['hello', { rowSpan: 2, content: 'greetings\nfriends' }, { rowSpan: 2, content: 'greetings\nfriends' }],
        ['howdy']
    );

```


## Autre démo de multi-ligne

    ┌───────┬─────┬────┐
    │ hello │ sup │ hi │
    ├───────┤ man │ yo │
    │ howdy │ hey │    │
    ├───┬───┼──┬──┤    │
    │ o │ k │  │  │    │
    └───┴───┴──┴──┴────┘

```javascript
    const table = new Table({ style: { head: [], border: [] } });

    table.push(
        [{ content: 'hello', colSpan: 2 }, { rowSpan: 2, colSpan: 2, content: 'sup\nman\nhey' }, { rowSpan: 3, content: 'hi\nyo' }],
        [{ content: 'howdy', colSpan: 2 }],
        ['o', 'k', '', '']
    );

```


## rowSpan pour espacer des cellules

    ┌───┬───┐
    │ a │ b │
    │   ├───┤
    │   │ c │
    ├───┤   │
    │ d │   │
    └───┴───┘

```javascript
    const table = new Table({ style: { head: [], border: [] } });

    table.push(
        [{ content: 'a', rowSpan: 2 }, 'b'],
        [{ content: 'c', rowSpan: 2 }],
        ['d']
    );

```


## Si besoin, le Layout manager créé automatiquement les cellules vides nécessaires

    ┌───┬───┬──┐
    │ a │ b │  │
    │   ├───┤  │
    │   │   │  │
    │   ├───┴──┤
    │   │ c    │
    ├───┤      │
    │   │      │
    └───┴──────┘

```javascript
    const table = new Table({ style: { head: [], border: [] } });

    // Nous ne créons ici que 3 cellules, mais 6 seront générées.
    table.push(
        [{ content: 'a', rowSpan: 3, colSpan: 2 }, 'b'],
        [],
        [{ content: 'c', rowSpan: 2, colSpan: 2 }],
        []
    );
```


## Utilise l'option `rowHeights` pour fixer la hauteurs des cellules. Le symboles de troncation sera inséré à la fin

    ┌───────┐
    │ hello │
    │ hi…   │
    └───────┘

```javascript
    const table = new Table({ rowHeights: [2], style: { head: [], border: [] } });

    table.push(['hello\nhi\nsup']);

```


## Si `colWidths` n'est pas spécifié, le layout manager ajuste le contenu pour aligner les cellules

    ┌─────────────┐
    │ hello there │
    ├──────┬──────┤
    │ hi   │ hi   │
    └──────┴──────┘

```javascript
    const table = new Table({ style: { head: [], border: [] } });

    table.push(
        [{ colSpan: 2, content: 'hello there' }],
        ['hi', 'hi']
    );

```


## Vous pouvez la largeur d'une cellule pour la première lignes, les autres seront ajustées automatiquement content
    ┌─────────────┐
    │ hello there │
    ├────┬────────┤
    │ hi │   hi   │
    └────┴────────┘
```javascript
    const table = new Table({ colWidths: [4], style: { head: [], border: [] } });

    table.push(
        [{ colSpan: 2, content: 'hello there' }],
        ['hi', { hAlign: 'center', content: 'hi' }]
    );

```


## Une colonne avec une taille `null` sera ajustée pour aligenr le contenu

    ┌─────────────┐
    │ hello there │
    ├────────┬────┤
    │     hi │ hi │
    └────────┴────┘

```javascript
    const table = new Table({ colWidths: [null, 4], style: { head: [], border: [] } });

    table.push(
        [{ colSpan: 2, content: 'hello there' }],
        [{ hAlign: 'right', content: 'hi' }, 'hi']
    );

```


## Même en utilisant des couleurs, les tailles seron bien calculées

```javascript
    const table = new Table({ colWidths: [5], style: { head: [], border: [] } });

    table.push([colors.red('hello')]);

```
