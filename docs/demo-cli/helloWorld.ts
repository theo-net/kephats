/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as Framework from 'kephats';
import { Cli } from 'kephats/build/cli/Cli';

const cli = Framework.cli();

(cli
    .version('1.0.0') as Cli)
    .command('hello', 'Affihce un Hello World!')
    .alias('salut')
    .argument('<nom>', 'Nom', 'string')
    .option('-e, --excla', 'Avec `!`')
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    .action((args: Record<string, any>, options: Record<string, any>) => {
        console.log('Hello ' + args.nom + (options.excla ? '!' : '.'));
    });
cli.run();

