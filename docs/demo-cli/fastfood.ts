/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as Framework from 'kephats';
import { Cli } from 'kephats/build/cli/Cli';

const cli = Framework.cli();

(cli
    .version('1.0.0') as Cli)

    // Commande `order`
    .command('burger', 'Commander un burger')
    .alias('jaifaim')

    .argument('<type>', 'Type du burger', ['cheese', 'double-cheese', 'fish'])
    .argument('<from>', 'À quelle enseigne commander', 'string')
    .argument('<account>', 'Avec quel compte commander', 'integer')

    .option('-n, --number <num>', 'Nombre de burger', { integer: { min: 0 } }, 1)
    .option('-d, --discount <amount>', 'Remise offerte', { float: { min: 0 } })
    .option('-p, --pay-by <mean>', 'Payer avec', ['esp', 'cb', 'chq'])
    .option('-l <test>', 'Version sans sauce du burger')

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    .action((args: Record<string, any>, options: Record<string, any>) => {

        console.log('Commande \'burger\' appellée avec :');
        console.log('arguments: %j', args);
        console.log('options: %j', options);
    })

    // La commande "return"
    .command('return', 'Retourne une commande')

    .argument('<order-id>', 'Order id', 'integer')
    .argument('<to-store>', 'Store id', 'integer')

    .option('--ask-change <other-kind-burger>',
        'Demande pour un autre type de burger',
        ['cheese', 'double-cheese', 'fish'])
    .option('--say-something <something>',
        'Dire quelque-chose au manager', 'string')

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    .action((args: Record<string, any>, options: Record<string, any>) => {
        return Promise.resolve('wooooo').then(ret => {
            console.log('Commande \'return\' appellée avec :');
            console.log('arguments: %j', args);
            console.log('options: %j', options);
            console.log('promise succeed with: %s', ret);
        });
    });

cli.run();
