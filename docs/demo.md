Démonstrations
==============

Des démonstrations sont disponibles, elles vous donneront une première base pour débuter avec notre framework.


## Application NodeJs

`demo-app-nodejs` est l'exemple d'une application utilisant le framework.

Pour l'exécuter, vous devez lancer en ligne de commande :

    npm install
    npm run build
    npm run exec

La première commande ne s'exécute que la première fois. La seconde lorsque vous modifiez les sources et la dernière à chaque fois.


## Application CLI

`demo-cli` vous montrera la puissance du framework avec des outils spécifiques pour la ligne de commande.

Pour l'exécuter, vous devez lancer en ligne de commande :

    npm install
    npm run build
    npm run demo1
    npm run demo2

La première commande ne s'exécute que la première fois. La seconde lorsque vous modifiez les sources et les deux dernières lance des démos.


## Application serveur

`demo-server` vous montre l'exemple d'un serveur HTTP mettant à disposition un site web. C'est l'exemple à suivre pour développer vos sites avec notre framework.

Pour l'exécuter, vous devez lancer en ligne de commande :

    npm install
    npm run build
    npm run start

La première commande ne s'exécute que la première fois. La seconde lorsque vous modifiez les sources
et la dernière pour lancer le serveur.

## Application browser

`demo-browser` vous montre une application développée pour la partie cliente, elle s'exécutera dans le navigateur.

Pour l'exécuter, vous devez lancer en ligne de commande :

    npm install
    npm run build:dev

La première commande ne s'exécute que la première fois. La seconde lorsque vous modifiez les sources.

Vous pouvez également remplacer `build:dev` par `build:prod` pour générer les fichiers pour votre environnement de production.

Ensuite, ouvrez le fichier `demo.html` dans votre navigateur.
