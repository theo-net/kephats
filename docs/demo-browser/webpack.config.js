module.exports = {
    entry: __dirname + '/app.ts',
    devtool: 'inline-source-map',
    output: {
        filename: "app.js",
        path: __dirname + '/dist'
    },
    module: {
        rules: [{
            test: /\.ts$/,
            use: "ts-loader",
            //exclude: /node_modules/
        }]
    },
    resolve: {
        alias: {
            'kephats/src/browser': 'kephats/src/browser.ts'
        }
    }
};
