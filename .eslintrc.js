module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint',
  ],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  'env': {
    'browser': true,
    'node': true
  },
  rules: {
    'indent': 'error',
    'curly': ['error', 'multi-or-nest'],
    'indent': ['error', 4, {
      'SwitchCase': 1,
      'VariableDeclarator': 'first',
      'CallExpression': {
        'arguments': 'off'
      },
      'ignoredNodes': ['ConditionalExpression'],
      'MemberExpression': 'off'
    }],
    'max-len': ['error', 100, 4,
      {'ignoreUrls': true, 'ignoreComments': true}
    ],
    'linebreak-style': ['error', 'unix'],
    'operator-linebreak': ['error', 'after', {
      'overrides': {'+': 'before', ':': 'before'}
    }],
    'array-bracket-spacing': 'error',
    'no-spaced-func': 'error',
    'space-infix-ops': 'error',
    'space-in-parens': ['error', 'never'],
    'space-before-blocks': 'error',
    'block-spacing': 'error',
    'space-before-function-paren': [2, 'always'],
    'semi': ['error', 'always'],
    'semi-spacing': ['error', {'before': false, 'after': true}],
    'space-unary-ops': ['error', {
      'words': true,
      'nonwords': false,
    }],
    'key-spacing': ['error', {
      'afterColon': true,
      'mode': 'minimum'
    }],
    'one-var-declaration-per-line': ['error', 'initializations'],
  }
};
