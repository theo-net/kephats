/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Application } from '../core/Application';
import { Kernel } from '../core/Kernel';
import { Render } from './Render';
import { VElement } from './vdom';


/**
 * Point d'entrée du Framework chargé dans le navigateur
 */
export * from '../core/Framework';
export * from './Utils';


/**
 * Créé une nouvelle application
 * @param envConfig Variables d'environnement
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function app (envConfig?: Record<string, any>): Application {

    const kernel = new Kernel();

    const app = new Application(kernel, envConfig);
    kernel.container().register('$application', app);
    return app;
}


/**
 * Exportation du vdom
 */
export * from './vdom';


/**
 * Met à disposition une fonction de rendu pour afficher un élément virtuel
 */
export function render (element: VElement, container: HTMLElement): Render {

    return new Render(element, container);
}

/**
 * Exportation des hooks
 */
export * from './hooks';
