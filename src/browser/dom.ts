/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Fiber } from './Render';

const _isEvent = (k: string): boolean => k.startsWith('on');
const _eventName = (k: string): string => k.toLowerCase().substring(2);
const _isProperty = (k: string): boolean => k !== 'children';

/**
 * Met à jour le DOM d'un élément
 * @param dom DOM à mettre à jour
 * @param prevProps Anciennes propriétés
 * @param nextProps Nouvelles propriétés
 */
export function updateDom (
    dom: HTMLElement | Text,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    prevProps: Record<string, any>, nextProps: Record<string, any>
): void {

    // supprime les anciennes propriétés
    Object.keys(prevProps)
        .filter(_isProperty)
        .forEach(name => {
            if (!(name in nextProps)) {
                if (_isEvent(name))
                    dom.removeEventListener(_eventName(name), prevProps[name]);
                else
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    (dom as { [key: string]: any })[name] = '';
            }
        });

    // ajoute ou modifie les nouvelles propriétés
    Object.keys(nextProps)
        .filter(_isProperty)
        .forEach(name => {
            if (prevProps[name] !== nextProps[name]) {
                if (_isEvent(name)) {
                    if (prevProps[name])
                        dom.removeEventListener(_eventName(name), prevProps[name]);
                    dom.addEventListener(_eventName(name), nextProps[name]);
                }
                else
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    (dom as { [key: string]: any })[name] = nextProps[name];
            }
        });
}


/**
 * Créé le DOM d'un élément
 * @param fiber Élément dont on veut générer le DOM
 */
export function createDom (fiber: Fiber): HTMLElement | Text {

    const dom = fiber.type === 'TEXT_ELEMENT' ?
        document.createTextNode(fiber.props.nodeValue as string)
        : document.createElement(fiber.type as string);

    updateDom(dom, {}, fiber.props);

    return dom;
}
