/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { createDom, updateDom } from './dom';
import { Fiber } from './Render';
import { createElement } from './vdom';

describe.only('browser/dom', () => {

    describe('createDom', () => {

        it('adds the good attributes', () => {

            const elem = createElement('h1', { id: 'foo'}) as Fiber;
            const dom = createDom(elem) as HTMLElement;
            expect(dom.getAttribute('id')).to.be.equal('foo');
        });

        it('creates also text element', () => {

            const elem = createElement('h1', null, 'salut') as Fiber;
            const dom = createDom(elem.props.children[0] as Fiber) as Text;
            expect(dom.nodeValue).to.be.equal('salut');
        });

        // add event listerner if needed ?
    });

    describe('updateDom', () => {

        it('update a prop', () => {

            const elem = createElement('h1', { id: 'foo' }, 'salut') as Fiber;
            const dom = createDom(elem as Fiber) as HTMLElement;
            updateDom(dom, { id: 'foo' }, { id: 'bar' });
            expect(dom.getAttribute('id')).to.be.equal('bar');
        });

        it('remove a prop', () => {

            const elem = createElement('h1', { id: 'foo' }, 'salut') as Fiber;
            const dom = createDom(elem as Fiber) as HTMLElement;
            updateDom(dom, { id: 'foo' }, {});
            expect(dom.getAttribute('id')).to.be.equal('');
        });

        it('add a prop', () => {

            const elem = createElement('h1', {}, 'salut') as Fiber;
            const dom = createDom(elem as Fiber) as HTMLElement;
            updateDom(dom, {}, { id: 'bar' });
            expect(dom.getAttribute('id')).to.be.equal('bar');
        });

        it.only('add/remove eventListener if needed', () => {

            const fct1 = (): void => { return; },
                  fct2 = (): void => { return; };
            const elem = createElement('h1', { onClick: fct1 }, 'salut') as Fiber;
            const dom = createDom(elem as Fiber) as HTMLElement;

            expect(dom.onclick).to.be.equal(fct1);
            updateDom(dom, { onClick: fct1 }, { onFocus: fct1 });
            expect(dom.onclick).to.be.equal(null);
            expect(dom.onfocus).to.be.equal(fct1);
            updateDom(dom, { onFocus: fct1 }, { onFocus: fct2 });
            expect(dom.onfocus).to.be.equal(fct2);
        });
    });
});
