/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export * from '../core/Utils';

/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * Ajoute des outils spécifique aux scripts s'exécutant dans un navigateur
 */

/**
 * Test si `value` est un objet `Blob`
 * @category Lang
 * @param value valeur à tester
 */
export function isBlob (value: any): boolean {

    return value.toString() === '[object Blob]';
}


/**
 * Test si `value` est un objet `File`
 * @category Lang
 * @param value valeur à tester
 */
export function isFile (value: any): boolean {

    return value.toString() === '[object File]';
}


/**
 * Test si `value` un objet `FormData`
 * @category Lang
 * @param value valeur à tester
 */
export function isFormData (value: any): boolean {

    return value.toString() === '[object FormData]';
}


/**
 * Créé du DOM à partir d'un String
 * @param html HTML à transformer
 */
export function str2DomElement (html: string): NodeListOf<ChildNode> {

    const wrapMap: { [keys: string]: Array<number | any> } = {
        option: [1, '<select multiple="multiple">', '</select>'],
        legend: [1, '<fieldset>', '</fieldset>'],
        area: [1, '<map>', '</map>'],
        param: [1, '<object>', '</object>'],
        thread: [1, '<table>', '</table>'],
        tr: [2, '<table><tbody>', '</tbody></table>'],
        col: [2, '<table><tbody></tbody><colgroup>', '</colgroup></table>'],
        td: [3, '<table><tbody><tr>', '</tr></tbody></table>'],
        body: [0, '', ''],
        default: [1, '<div>', '</div>']
    };

    wrapMap.optgroup = wrapMap.option;
    wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption =
        wrapMap.thead;
    wrapMap.th = wrapMap.td;

    const match = /<\s*\w.*?>/g.exec(html);
    const element: HTMLElement = document.createElement('div');

    // Ne contient pas de balise
    if (match != null) {

        const tag = match[0].replace(/</g, '').replace(/>/g, '').split(' ')[0];

        /*// On demande un body
        if (tag.toLowerCase() === 'body') {

            const body = document.createElement('body');

            // On garde les attribus
            element.innerHTML = html.replace(/<body>/g, '<div>').replace(/<\/body>/g, '</div>');
            const attrs = ((element.firstChild as HTMLElement).attributes) as unknown as Attr[];

            // On complète le body
            body.innerHTML = html;
            attrs.forEach(attr => {
                body.setAttribute(attr.name, attr.value);
            });

            return body;
        }
        else {*/

        let child = element as ChildNode;
        const map = wrapMap[tag] || wrapMap.default;
        html = map[1] + html + map[2];
        element.innerHTML = html;
        let j = map[0];
        while (j--)
            child = child.lastChild as ChildNode;
        return child.childNodes;
        //}
    }
    else {
        element.innerHTML = html;
        return element.childNodes;
    }
}


/**
 * Permet de déclencher l'appel à une fonction après un certains délai et
 * réinitialise le timer si la fonction est appellée avant que le délai soit
 * dépassé.
 *
 * exemple d'appel:
 *
 *      search.addEventListener('keyup', debounce(function (e) {
 *          // Le code ici sera exécuté au bout de 350 ms
 *          // mais si l'utilisateur tape une nouvelle fois durant cet intervalle
 *          // de temps, le timer sera réinitialisé.
 *      }, 350))
 *
 * @param {Function} callback La fonction
 * @param {Number} delay Le délai en ms
 */
export function debounce (callback: Function, delay: number): Function {

    let timer: ReturnType<typeof setTimeout>;
    return (...args: any[]): void => {

        clearTimeout(timer);
        timer = setTimeout(() => {
            callback(...args);
        }, delay);
    };
}


/**
 * Permet d'éviter des appels consécutifs en introduisant un délai.
 *
 * exemple :
 *
 *      window.addEventListener('scroll', throttle(function (e) {
 *          // Le code ici ne pourra être exécuté que toutes les 50 ms (20 appels max
 *          // par secondes)
 *      }, 50))
 * @param callback La fonction
 * @param delay Le délai en ms
 */
export function throttle (callback: Function, delay: number): Function {

    let last: number;
    let timer: ReturnType<typeof setTimeout>;
    return function (...args: any[]): void {

        const now = +new Date();

        if (last && now < last + delay) {

            // le délai n'est pas écoulé, on reset le timer
            clearTimeout(timer);
            timer = setTimeout(() => {
                last = now;
                callback(...args);
            }, delay);
        } else {
            last = now;
            callback(...args);
        }
    };
}


/**
 * Retourne le chemin absolu d'un href
 *
 * @param href `href` dont on veut le chemin absolu
 */
export function absolutePath (href: string): string {

    const link = document.createElement('a');
    link.href = href;
    return (link.protocol + '//' + link.host + link.pathname + link.search + link.hash);
}
