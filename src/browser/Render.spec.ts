/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Render } from './Render';
import { createElement, VElement } from './vdom';

describe.only('browser/Render', () => {

    let rootDom: HTMLElement;
    let ricSave: Function;

    const waitWip = (render: Render, done: Mocha.Done): void => {

        setTimeout((): void => {
            if (render.hasWip())
                waitWip(render, done);
            else
                done();
        }, 500);
    };

    before(() => {

        ricSave = window.requestIdleCallback;
    });

    after(() => {

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        window.requestIdleCallback = ricSave as any;
    });

    beforeEach(() => {

        rootDom = document.createElement('div');
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        window.requestIdleCallback = (handler: Function): any => {

            const startTime = Date.now();

            return setTimeout((): void => {
                handler({
                    didTimeout: false,
                    timeRemaining: (): number => {
                        return Math.max(0, 50.0 - (Date.now() - startTime));
                    }
                });
            }, 1);
        };
    });

    afterEach(() => {

        window.requestIdleCallback = (): void  => {
            return;
        };
    });

    describe('render', () => {

        it('creates DOM Element and insert it in the container', (done) => {

            const elem = createElement('h1');
            const render = new Render(elem, rootDom);
            waitWip(render, () => {
                expect(rootDom.children[0].nodeName).to.be.equal('H1');
                done();
            });
        });

        it('children are also created', (done) => {

            const elem = createElement(
                'section',
                null,
                createElement('h1', null, createElement('a')),
                createElement('p')
            );
            const render = new Render(elem, rootDom);
            waitWip(render, () => {
                expect(rootDom.children[0].nodeName).to.be.equal('SECTION');
                expect(rootDom.children[0].children[1].nodeName).to.be.equal('P');
                expect(rootDom.children[0].children[0].children[0].nodeName).to.be.equal('A');
                done();
            });
        });

        it('update the dom', (done) => {

            let elem = createElement('p', null, 'a');
            let render = new Render(elem, rootDom);
            elem = createElement('p', null, 'a');
            render = new Render(elem, rootDom);
            waitWip(render, () => {
                expect(rootDom.children.length).to.be.equal(1);
                done();
            });
        });

        // Problème : quand je génère un nouvel élément, actuellement il n'y a plus de trace de ce
        // que j'ai généré, donc ça ne sert à rien... (tout est regénéré from scratch)
        // Tester en ajoutant un attribut au dom et en regardant si après un update il ne l'a pas
        // recréé (ie qua l'attribut ajouté manuellement est tjs présent)

        // Tester que c'est une vraie modif : on change l'intérieur du texte, mais les attributs 'nont pas changés

        // Système de clé qui permet d'éviter de regénérer un élément s'il a juste changé de position

        it('support function component', (done) => {

            function MyElem ({name}: {name: string}): VElement {
                return createElement('h1', null, name);
            }
            const elem = createElement(MyElem, { name: 'Foobar' });
            const render = new Render(elem, rootDom);
            waitWip(render, () => {
                expect(rootDom.children[0].nodeName).to.be.equal('H1');
                done();
            });
        });

        // Gère la suppression de MyElem ?

        // si on a :
        // <h1/><p/> et que ça devient <h2/><p/> mais que le deuxième n'a pas changé, est ce que l'ordre
        // est respecté (ie on obtient pas <p/><h2/>)
    });
});
