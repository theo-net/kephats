/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Props, VElement } from "./vdom";
import { createDom, updateDom } from "./dom";

/**
 * `RequestIdleCallbackHandle` est une méthode expérimentale, utiliser un polyfill si le navigateur
 * cible ne suporte pas cette fonctionnalité.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type RequestIdleCallbackHandle = any;
type RequestIdleCallbackOptions = {
  timeout: number;
};
type RequestIdleCallbackDeadline = {
  readonly didTimeout: boolean;
  timeRemaining: (() => number);
};

declare global {
  interface Window {
    requestIdleCallback: ((
      callback: ((deadline: RequestIdleCallbackDeadline) => void),
      opts?: RequestIdleCallbackOptions,
    ) => RequestIdleCallbackHandle);
    cancelIdleCallback: ((handle: RequestIdleCallbackHandle) => void);
  }
}

/**
 * Pour le rendu on utilise un *Fiber Tree* : chaque élément peut avoir un enfant (*child*), un
 * parent (*parent*) ou un voisin (*sibling*). On ne stocke que l'enfant et le voisin direct. En
 * parcourant l'arbre, on sera capable de reconstituer la structure.
 *
 *     root
 *
 *      ↓↑
 *
 *     <div>
 *
 *      ↓↑     ↖
 *
 *     <h1>    →    <h2>
 *
 *      ↓↑     ↖
 *
 *     <p>     →    <a>
 */
export type Fiber = {
    type?: string | Function;
    dom: HTMLElement | Text | null;
    parent?: Fiber;
    child?: Fiber;
    sibling?: Fiber;
    props: Props;
    alternate?: Fiber | null;           // référence à l'arbre précédent
    effectTag?: 'UPDATE' | 'PLACEMENT' | 'DELETION';
};


/**
 * Gère le rendu des éléments virtuels
 */
export class Render {

    protected _currentRoot: Fiber | null = null;
    protected _deletions: Fiber[] = [];
    protected _nextUnitOfWork: Fiber | null = null;
    protected _wipRoot: Fiber | null = null;

    /**
     * Lance le rendu d'un élément virtuel en gérant toute la pile
     * @param element Élément virtuel dont on veux le rendu
     * @param container Conteneur qui recevra l'élément
     */
    constructor (element: VElement, container: HTMLElement) {

        window.requestIdleCallback(this._workLoop.bind(this));
        this._render(element, container);
    }


    /**
     * Indique s'il y a encore des éléments dont on doit lancer le rendu
     */
    hasWip (): boolean {

        return this._nextUnitOfWork !== null || this._wipRoot !== null;
    }


    /**
     * Création des élément de DOM et insertion dans un conteneur
     * @param element Élément virtuel dont on veux le rendu
     * @param container Conteneur qui recevra l'élément
     */
    protected _render (element: VElement, container: HTMLElement): void {

        this._wipRoot = {
            dom: container,
            props: {
                children: [element]
            },
            alternate: this._currentRoot
        };
        this._nextUnitOfWork = this._wipRoot;
        this._deletions = [];
    }


    /**
     * Boucle centrale
     */
    protected _workLoop (deadline: RequestIdleCallbackHandle): void {

        let shouldYield = false;

        while (this._nextUnitOfWork && !shouldYield) {
            this._nextUnitOfWork = this._performUnitOfWork(this._nextUnitOfWork);
            shouldYield = deadline.timeRemaining() < 1;
        }

        if (!this._nextUnitOfWork && this._wipRoot)
            this._commitRoot();

        window.requestIdleCallback(this._workLoop.bind(this));
    }


    /**
     * Performe une tâche et renvoie la tâche suivante à effectuer
     */
    protected _performUnitOfWork (fiber: Fiber): Fiber | null {

        if (fiber.type instanceof Function)
            this._updateFunctionComponent(fiber);
        else
            this._updateHostComponent(fiber);

        // On parcourt l'arbre : d'abord l'enfant, si pas d'enfant, le voisin, si pas de voisin, le parent
        if (fiber.child)
            return fiber.child;
        let nextFiber = fiber;
        while (nextFiber) {
            if (nextFiber.sibling)
                return nextFiber.sibling;
            nextFiber = nextFiber.parent as Fiber;
        }

        return null;
    }


    /**
     *
     * @param fiber
     */
    protected _updateFunctionComponent (fiber: Fiber): void {

        const children = [(fiber.type as Function)(fiber.props)];
        this._reconcileChildren(fiber, children);
    }


    /**
     * Pour les composants natifs
     * @param fiber
     */
    protected _updateHostComponent (fiber: Fiber): void {

        if (!fiber.dom)
            fiber.dom = createDom(fiber);

        // construit le Fiber Tree
        const elements = fiber.props.children;
        this._reconcileChildren(fiber, elements);
    }


    /**
     * S'occupe des enfants
     * @param wipFiber Fibre sur laquelle nous travaillons
     * @param elements Les éléments enfant de la fibre courante
     */
    protected _reconcileChildren (wipFiber: Fiber, elements: VElement[]): void {

        let index = 0;
        let oldFiber = wipFiber.alternate && wipFiber.alternate.child;
        let prevSibling = null;
        let newFiber = null;

        while (index < elements.length || oldFiber !== null) {

            const element = elements[index];
            const sameType = oldFiber && element && element.type === oldFiber.type;

            if (sameType) {
                newFiber =  {
                    type: element.type,
                    props: element.props,
                    parent: wipFiber,
                    dom: oldFiber?.dom,
                    alternate: oldFiber,
                    effectTag: 'UPDATE'
                };
            }
            if (element && !sameType) {
                newFiber =  {
                    type: element.type,
                    props: element.props,
                    parent: wipFiber,
                    dom: null,
                    alternate: null,
                    effectTag: 'PLACEMENT'
                };
            }
            if (oldFiber && !sameType) {
                oldFiber.effectTag = 'DELETION';
                this._deletions.push(oldFiber);
            }

            if (oldFiber)
                oldFiber = oldFiber.sibling ? oldFiber.sibling : null;

            if (index === 0)
                wipFiber.child = newFiber as Fiber;
            else if (element)
                (prevSibling as Fiber).sibling = newFiber as Fiber;
            prevSibling = newFiber;
            index++;
        }
    }

    /**
     * Ajoute la racine de travail en cours au niveau du DOM
     */
    protected _commitRoot (): void {

        this._deletions.forEach(this._commitWork);
        this._commitWork(this._wipRoot?.child);
        this._currentRoot = this._wipRoot;
        this._wipRoot = null;
    }


    /**
     * Ajoute une fibre dans le DOM
     * @param fiber Fibre à commiter
     */
    protected _commitWork (fiber: Fiber | null | undefined): void {

        // à la fin de l'abre, fiber sera null car il n'y a pas d'enfant
        if (!fiber)
            return;

        let domParentFiber = fiber.parent as Fiber;
        while (!domParentFiber.dom)
            domParentFiber = fiber.parent as Fiber;
        const domParent = domParentFiber.dom;

        if (fiber.effectTag === 'PLACEMENT' && fiber.dom !== null)
            domParent?.appendChild(fiber.dom as Node);
        else if (fiber.effectTag === 'DELETION') {
            this._commitDeletion(fiber, domParent);
            return;  // Pas d'enfants puisque supprimé, on va s'occuper des voisins
        }
        else if (fiber.effectTag === 'UPDATE' && fiber.dom !== null) {
            updateDom(fiber.dom, fiber.alternate?.props as Props, fiber.props);
            domParent?.appendChild(fiber.dom as Node);
        }

        this._commitWork(fiber.child);
        this._commitWork(fiber.sibling);
    }


    /**
     * Retire une fibre du DOM
     * @param fiber Fibre à retirer
     * @param domParent DOM du parent de la fibre
     */
    protected _commitDeletion (fiber: Fiber, domParent: HTMLElement | Text): void {

        if (fiber.dom)
            domParent?.removeChild(fiber.dom as Node);
        else
            this._commitDeletion(fiber.child as Fiber, domParent);
    }
}
