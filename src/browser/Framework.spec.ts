/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { assert, expect } from 'chai';

import { Application } from '../core/Application';
import * as CoreFramework from '../core/Framework';
import * as Framework from './Framework';
import * as hooks from './hooks';
import { Render } from './Render';
import * as Utils from './Utils';
import * as vdom from './vdom';

describe.only('browser/Framework', () => {

    it('extends `cli/Utils`', () => {

        assert.containsAllKeys(Framework, Utils);
    });

    it('extends `core/Framework`', () => {

        assert.containsAllKeys(Framework, CoreFramework);
    });

    describe('app', () => {

        it('app() retourne une nouvelle instance d\'Application', () => {

            expect(Framework.app()).to.be.instanceOf(Application);
            expect(Framework.app()).to.be.not.equal(Framework.app());
        });

        it('app() transmet le kernel à l\'application', () => {

            expect(Framework.app().kernel()).to.be.equal(Framework.kernel());
        });

        it('app() enregistre l\'applocation comme service', () => {

            const app = Framework.app();
            expect(app.kernel().get('$application')).to.be.equal(app);
        });

        it('app() accepte des variables d\'environnement', () => {

            const app = Framework.app({FOO: 'bar'});
            expect(app.kernel().get('$config').get('FOO')).to.be.equal('bar');
        });
    });

    describe('jsx', () => {

        it('the framework export createElement function', () => {

            expect(Framework.createElement).to.be.equal(vdom.createElement);
        });

        it('the framework give a render function', () => {

            expect(typeof Framework.render).to.be.equal('function');
        });

        it('render return an Render instance', () => {

            expect(Framework.render(Framework.createElement('div'), document.createElement('div')))
                .to.be.instanceOf(Render);
        });

        it('all method of hooks are available', () => {

            assert.containsAllKeys(Framework, hooks);
        });
    });
});
