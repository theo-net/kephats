/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Les propriétés d'un élément
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Props = Record<string, any> & { children: VElement[] };

/**
 * Un élément virtuel
 */
export interface VElement {
    type: string | Function;
    props: Props;
}


/**
 * Créé un élément textuel
 * @param text Texte de l'élément
 */
function _createTextElement (text: string): VElement {
    return {
        type: 'TEXT_ELEMENT',
        props: {
            nodeValue: text,
            children: []
        }
    };
}

/**
 * Créé un élément virtuel
 * @param type Le type de l'élément (`h1`, `a`, ...)
 * @param props Propriétés de l'élément (attributs comme `href`, `id`, ...)
 * @param children Enfants de l'élément
 */
export function createElement (
    type: string | Function,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    props: Record<string, any> | null = null,
    ...children: Array<string | VElement>
): VElement {

    return {
        type,
        props: {
            ...props,
            children: children.map((child): VElement => {
                return (typeof child === 'object' ? child : _createTextElement(child)) as VElement;
            })
        }
    };
}
