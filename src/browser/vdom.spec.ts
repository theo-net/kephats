/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { createElement } from './vdom';

describe.only('browser/vdom', () => {

    describe('createElement', () => {

        it('create a virtual element', () => {

            expect(createElement('div')).to.be.deep.equal({
                type: 'div',
                props: {
                    children: []
                }
            });
            expect(createElement('h1')).to.be.deep.equal({
                type: 'h1',
                props: {
                    children: []
                }
            });
        });

        it('can pass properties', () => {

            expect(createElement('div', {foo: 'bar', bar: 'foo'})).to.be.deep.equal({
                type: 'div',
                props: {
                    foo: 'bar',
                    bar: 'foo',
                    children: []
                }
            });
        });

        it('can pass one children', () => {

            const elem1 = createElement('div');
            expect(createElement('div', null, elem1)).to.be.deep.equal({
                type: 'div',
                props: {
                    children: [elem1]
                }
            });
        });

        it('can pass children', () => {

            const elem1 = createElement('div'),
                  elem2 = createElement('div');
            expect(createElement('div', null, elem1, elem2)).to.be.deep.equal({
                type: 'div',
                props: {
                    children: [elem1, elem2]
                }
            });
        });

        it('child can be text element', () => {

            expect(createElement('div', null, 'Foobar')).to.be.deep.equal({
                type: 'div',
                props: {
                    children: [{
                        type: 'TEXT_ELEMENT',
                        props: {
                            nodeValue: 'Foobar',
                            children: []
                        }
                    }]
                }
            });
        });
    });
});
