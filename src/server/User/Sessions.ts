/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as Utils from '../../cli/Utils';

const NBPURGE = 200,     // seuil à partir duquel on lance le garbage collector
      EXPSESSION = 1440; // s, soit 24 min (durée de vie d'une session)

/* eslint-disable @typescript-eslint/no-explicit-any */

export interface SessionsStorageInterface {
    access (id: string): void;
    count (): number;
    delete (id: string, name: string): void;
    garbage (maxAge: number): void;
    get (id: string, name: string): any;
    has (id: string): boolean;
    hasElement (id: string, name: string): boolean;
    init (id: string): void;
    isTooOld (id: string, maxAge: number): boolean;
    remove (id: string): void;
    set (id: string, name: string, value: any): void;
}

/**
 * Service donnant accès à toutes les sessions enregistrées sur le serveur
 */
export class Sessions {

    protected _storage: SessionsStorageInterface;

    /**
     * Initialise le service avec un objet gérant le stockage des sessions
     * @param storage Objet gérant le stockage des sessions
     */
    constructor (storage: SessionsStorageInterface) {

        this._storage = storage;
    }


    /**
     * Initialise une nouvelle session, vérifiera que l'identifiant est unique
     * @return Identifiant de la nouvelle session
     */
    init (): string {

        let id = Utils.uuid();
        while (this._storage.has(id))
            id = Utils.uuid();

        this._storage.init(id);

        return id;
    }


    /**
     * Supprime une session
     * @param id Identifiant de la session à supprimer
     */
    remove (id: string): void {

        this._storage.remove(id);
    }


    /**
     * Indique si une session existe ou pas. Retournera également `false` si elle a expiré.
     * @param id Identifiant de la session
     */
    has (id: string): boolean {

        if (!this._storage.has(id))
            return false;

        return !this._storage.isTooOld(id, EXPSESSION);
    }


    /**
     * Définit un élément dans une session
     * @param id Identifiant de la session
     * @param name Nom de la variable
     * @param value Valeur
     */
    set (id: string, name: string, value: any): void {

        this._storage.set(id, name, value);
    }


    /**
     * Indique si la session possède ou non un élément
     * @param id Identifiant de la session
     * @param name Nom de la valeur
     */
    hasElement (id: string, name: string): boolean {

        return this._storage.hasElement(id, name);
    }


    /**
     * Récupère un élément de la session
     * @param id Identifiant de la session
     * @param name Nom de la valeur
     */
    get (id: string, name: string): any {

        return this._storage.get(id, name);
    }


    /**
     * Supprime un élément de la sessions
     * @param id Identifiant de la session
     * @param name Nom de la valeur
     */
    delete (id: string, name: string): void {

        this._storage.delete(id, name);
    }


    /**
     * Indique un accès demandé à la session. Lance le garbage collector s'il y a plus de 200 sessions
     * en mémoire. Les sessions ont une durée de vie de 1440 s soit 24 min.
     * @param {String} id Identifiant de la session
     */
    hello (id: string): void {

        this._storage.access(id);

        if (this._storage.count() > NBPURGE)
            this._storage.garbage(EXPSESSION);
    }
}
