/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SessionsStorageInterface } from './Sessions';
import * as Utils from '../../cli/Utils';

const MILLI2SEC = 1000;

/**
 * Stockage des sessions en mémoire
 */
export class MemoryStorage implements SessionsStorageInterface {

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected _sessions: Record<string, {content: Record<string, any>; lastAccess: number }> = {};

    /**
     * Initialise une nouvelle session. Si l'identifiant est déjà pris, la nouvelle session écrasera
     * l'ancienne
     * @param id Identifiant de la session
     */
    init (id: string): void {

        this._sessions[id] = {
            content: {},
            lastAccess: Date.now()
        };
    }


    /**
     * Retourne le nombre de session stockées en mémoire
     */
    count (): number {

        return Object.keys(this._sessions).length;
    }


    /**
     * Indique si une session est enregistrée
     * @param id Identifiant de la session
     */
    has (id: string): boolean {

        return Object.keys(this._sessions).indexOf(id) > -1;
    }


    /**
     * Détruit une session
     * @param id Identifiant de la session
     */
    remove (id: string): void {

        delete this._sessions[id];
    }


    /**
     * Enregistre/met à jour un élément dans une session
     * @param id Identifiant de la session
     * @param name Nom de la variable
     * @param value Valeur à stocker
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    set (id: string, name: string, value: any): void {

        this._sessions[id].content[name] = value;
    }


    /**
     * Indique si un élément est contenu dans la session
     * @param id Identifiant de la session
     * @param name Nom de la variable
     */
    hasElement (id: string, name: string): boolean {

        return Object.keys(this._sessions[id].content).indexOf(name) > -1;
    }

    /**
     * Récupère un élément d'une session
     * @param id Identifiant de la session
     * @param name Nom de la variable
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    get (id: string, name: string): any {

        return this._sessions[id].content[name];
    }


    /**
     * Récupère tout le contenu d'une session et son dernier accès
     * @param id Identifiant de la session
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    getRaw (id: string): {content: Record<string, any>; lastAccess: number } {

        return this._sessions[id];
    }


    /**
     * Supprime un élément d'une sessions
     * @param id Identifiant de la session
     * @param name Nom de l variable
     */
    delete (id: string, name: string): void {

        delete this._sessions[id].content[name];
    }


    /**
     * Met à jour le timestamp de la session en indiquant qu'un accès est demandé
     * @param id Identifiant de la session
     */
    access (id: string): void {

        this._sessions[id].lastAccess = Date.now();
    }


    /**
     * Indique si une session est trop vieille
     * @param {String} id Identifiant de la session
     * @param {Integer} maxAge Âge maximum des sessions
     * @returns {Boolean}
     */
    isTooOld (id: string, maxAge: number): boolean {

        return Date.now() - this._sessions[id].lastAccess > maxAge * MILLI2SEC;
    }

    /**
     * Supprime toutes les sessions trop âgées
     * @param maxAge Âge maximum des sessions
     */
    garbage (maxAge: number): void {

        Utils.forEach(this._sessions, (session, id) => {
            if (this.isTooOld(id, maxAge))
                this.remove(id);
        });
    }
}
