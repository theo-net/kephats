/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as sinon from 'sinon';

import { MemoryStorage } from './MemoryStorage';
import { Sessions } from './Sessions';


describe('src/server/User/Sessions', () => {

    let sessions: Sessions, storage: MemoryStorage;

    beforeEach(function () {

        storage = new MemoryStorage();
        sessions = new Sessions(storage);
    });


    it('init initialise une session, retourne un id', () => {

        const id = sessions.init();

        expect(storage.has(id)).to.be.true;
    });

    it('init chaque session a un identifiant différent', () => {

        const id1 = sessions.init(),
              id2 = sessions.init();

        expect(id1).to.be.not.equal(id2);
    });

    it('init l\'id est un uuid v4', () => {

        const reg = RegExp(
            '^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$',
            'i'
        );
        expect(reg.test(sessions.init())).to.be.true;
    });

    it('remove détruit une session', () => {

        const id = sessions.init();
        sessions.remove(id);

        expect(storage.has(id)).to.be.false;
    });

    it('has indique si une session existe', () => {

        const id = sessions.init();

        expect(sessions.has(id)).to.be.equal(true);
        expect(sessions.has('a')).to.be.equal(false);
    });

    it('has returns false if session is too old', () => {

        const id = sessions.init();

        expect(sessions.has(id)).to.be.equal(true);

        storage.getRaw(id).lastAccess = 0;
        expect(sessions.has(id)).to.be.equal(false);
    });

    it('set enregistre un élément dans la session', () => {

        const id = sessions.init();
        sessions.set(id, 'foo', 'bar');

        expect(storage.get(id, 'foo')).to.be.equal('bar');
    });

    it('hasElement indique si une session possède un élément', () => {

        const id = sessions.init();
        sessions.set(id, 'foo', 'bar');

        expect(sessions.hasElement(id, 'foo')).to.be.equal(true);
        expect(sessions.hasElement(id, 'bar')).to.be.equal(false);
    });

    it('get récupère un élément dans la session', () => {

        const id = sessions.init();
        sessions.set(id, 'foo', 'bar');

        expect(sessions.get(id, 'foo')).to.be.equal('bar');
    });

    it('delete supprime un élément dans la session', () => {

        const id = sessions.init();
        sessions.set(id, 'foo', 'bar');
        sessions.delete(id, 'foo');

        expect(sessions.get(id, 'foo')).to.be.undefined;
    });

    describe('hello', () => {

        it('met à jour le timestamp', () => {

            const spy = sinon.spy(storage, 'access');

            const id = sessions.init();
            sessions.hello(id);

            expect(spy.calledWith(id)).to.be.true;
        });

        it('lance un garbage si plus de 200 sessions enregistrée', () => {

            const spy = sinon.spy(storage, 'garbage');

            const id = sessions.init();

            let tmp;

            sessions.hello(id);
            expect(storage.count()).to.be.equal(1);
            expect(spy.called).to.be.false;

            for (let i = 0; i < 205; i++) {
                tmp = sessions.init();
                storage.getRaw(tmp).lastAccess = 100;
            }
            expect(storage.count()).to.be.equal(206);

            sessions.hello(id);
            expect(storage.count()).to.be.equal(1);
            expect(spy.calledWith(1440)).to.be.true;
        });
    });
});
