/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as http from 'http';
import * as sinon from 'sinon';
import { Socket } from 'net';

import { MemoryStorage } from './MemoryStorage';
import { Sessions } from './Sessions';
import { User } from './User';
import { Resp } from '../Resp';

describe('src/server/User/User', () => {

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let user: User, userProto: any,
        req: http.IncomingMessage, resp: Resp, sessions: Sessions;

    beforeEach(() => {

        req = new http.IncomingMessage(new Socket);
        resp = new Resp(new http.ServerResponse(req), req);
        sessions = new Sessions(new MemoryStorage());
        user = new User(req, resp, sessions);
        userProto = Object.getPrototypeOf(user);
    });


    it('on peut spécifier une configuration', () => {

        user = new User(req, resp, sessions, { sessionCookieName: 'foobar' });

        expect(user.getConfig().sessionCookieName).to.be.equal('foobar');
    });


    it('si `sessionCookieDomain` est un `Array`, prend le meilleur domaine', () => {

        req.headers.host = 'www.domain.com';
        user = new User(req, resp, sessions, { sessionCookieDomain: 'foobar.com' });
        expect(user.getSessionCookieDomain()).to.be.equal('foobar.com');

        user = new User(req, resp, sessions, {
            sessionCookieDomain: [
                'aaa.domain.com',
                'www.domain.com',
                'domain.com',
                'foobar.com'
            ]
        });
        expect(user.getSessionCookieDomain()).to.be.equal('www.domain.com');

        user = new User(req, resp, sessions, {
            sessionCookieDomain: [
                'aaa.domain.com',
                'domain.com',
                'foobar.com'
            ]
        });
        expect(user.getSessionCookieDomain()).to.be.equal('domain.com');
    });


    it('fonctionne également si un port est transmis', () => {

        req.headers.host = 'www.domain.com:8089';
        user = new User(req, resp, sessions, { sessionCookieDomain: 'foobar.com' });
        expect(user.getSessionCookieDomain()).to.be.equal('foobar.com');

        user = new User(req, resp, sessions, {
            sessionCookieDomain: [
                'aaa.domain.com',
                'domain.com',
                'foobar.com'
            ]
        });
        expect(user.getSessionCookieDomain()).to.be.equal('domain.com');
    });


    it('si aucun domaine trouvé, prend le premier de la liste', () => {

        req.headers.host = 'www.other.com';
        user = new User(req, resp, sessions, {
            sessionCookieDomain: [
                'aaa.domain.com',
                'domain.com',
                'foobar.com'
            ]
        });
        expect(user.getSessionCookieDomain()).to.be.equal('aaa.domain.com');
    });


    describe('cookies', () => {

        it('enregistre un cookie', () => {

            user.setCookie('foo', 'bar');
            user.getResponse().end();

            expect(resp.getHeader('Set-Cookie')).to.be.deep.equal(
                ['kts_session_id=' + user.getSessionId() + ';HttpOnly', 'foo=bar']
            );
        });

        it('on peut spécifier un expires et/ou un max-age', () => {

            user.setCookie('foo', 'bar', 100, 200);
            user.setCookie('fo1', 'bar', null, 200);
            user.setCookie('fo2', 'bar', 100);
            user.getResponse().end();

            expect(resp.getHeader('Set-Cookie'))
                .to.be.deep.equal([
                    'kts_session_id=' + user.getSessionId() + ';HttpOnly',
                    'foo=bar;Expires=100;Max-Age=200',
                    'fo1=bar;Max-Age=200',
                    'fo2=bar;Expires=100'
                ]);
        });

        it('on peut sprécifier un domain et/ou un path', () => {


            user.setCookie('foo', 'bar', null, null, 'theo-net.org');
            user.setCookie('fo1', 'bar', null, null, null, '/hello');
            user.setCookie('fo2', 'bar', null, null, 'theo-net.org', '/hello');
            user.getResponse().end();
            expect(resp.getHeader('Set-Cookie'))
                .to.be.deep.equal([
                    'kts_session_id=' + user.getSessionId() + ';HttpOnly',
                    'foo=bar;Domain=theo-net.org',
                    'fo1=bar;Path=/hello',
                    'fo2=bar;Domain=theo-net.org;Path=/hello'
                ]);
        });

        it('on peut spécfier si le cookie est Secure ou HttpOnly', () => {

            user.setCookie('foo', 'bar', null, null, null, null, true);
            user.setCookie('fo1', 'bar', null, null, null, null, false, true);
            user.setCookie('fo2', 'bar', null, null, null, null, true, true);
            user.getResponse().end();

            expect(resp.getHeader('Set-Cookie'))
                .to.be.deep.equal([
                    'kts_session_id=' + user.getSessionId() + ';HttpOnly',
                    'foo=bar;Secure',
                    'fo1=bar;HttpOnly',
                    'fo2=bar;Secure;HttpOnly'
                ]);
        });

        it('envoit une erreur si les headers ont déjà été envoyés', () => {

            user.getResponse().end();
            expect(user.setCookie.bind(user, 'foo', 'bar'))
                .to.throw(Error);
        });

        it('récupère la valeur d\'un cookie', () => {

            req.headers.cookie = 'theo=net;kepha=js';
            userProto._loadCookies.call(user);
            user.setCookie('foo', 'bar');

            expect(user.getCookie('theo')).to.be.equal('net');
            expect(user.getCookie('kepha')).to.be.equal('js');
            expect(user.getCookie('foo')).to.be.equal('bar');
        });

        it('récupère la valeur d\'un cookie, même si whitespace', () => {

            req.headers.cookie = 'theo=net; kepha=js';
            userProto._loadCookies.call(user);
            user.setCookie('foo', 'bar');

            expect(user.getCookie('theo')).to.be.equal('net');
            expect(user.getCookie('kepha')).to.be.equal('js');
            expect(user.getCookie('foo')).to.be.equal('bar');
        });

        it('indique si un cookie est enregistré', () => {

            req.headers.cookie = 'theo=net';
            userProto._loadCookies.call(user);
            user.setCookie('foo', 'bar');

            expect(user.hasCookie('theo')).to.be.equal(true);
            expect(user.hasCookie('kepha')).to.be.equal(false);
            expect(user.hasCookie('foo')).to.be.equal(true);
        });

        it('supprime un cookie', () => {

            user.setCookie('foo', 'bar');
            user.deleteCookie('foo');
            user.getResponse().end();

            expect(user.hasCookie('foo')).to.be.false;
            expect(resp.getHeader('Set-Cookie'))
                .to.be.deep.equal([
                    'kts_session_id=' + user.getSessionId() + ';HttpOnly',
                    'foo=bar',
                    'foo=;Expires=0;Max-Age=-1'
                ]);
        });

        it('transmet les autres params', () => {

            user.setCookie('foo', 'bar', null, null, 'truc.org', '/', true, true);
            user.deleteCookie('foo', 'truc.org', '/', true, true);
            user.getResponse().end();

            expect(user.hasCookie('foo')).to.be.false;
            expect(resp.getHeader('Set-Cookie'))
                .to.be.deep.equal([
                    'kts_session_id=' + user.getSessionId() + ';HttpOnly',
                    'foo=bar;Domain=truc.org;Path=/;Secure;HttpOnly',
                    'foo=;Expires=0;Max-Age=-1;Domain=truc.org;Path=/;Secure;HttpOnly'
                ]);
        });

        it('supprime tous les cookies', () => {

            req.headers.cookie = 'theo=net;kepha=js';
            userProto._loadCookies.call(user);
            user.setCookie('foo', 'bar');
            user.resetCookies();
            user.getResponse().end();

            expect(user.hasCookie('theo')).to.be.false;
            expect(user.hasCookie('kepha')).to.be.false;
            expect(user.hasCookie('foo')).to.be.false;
            expect(resp.getHeader('Set-Cookie'))
                .to.be.deep.equal([
                    'kts_session_id=' + user.getSessionId() + ';HttpOnly',
                    'foo=bar',
                    'theo=;Expires=0;Max-Age=-1',
                    'kepha=;Expires=0;Max-Age=-1',
                    'foo=;Expires=0;Max-Age=-1'
                ]);
        });
    });


    describe('session', () => {

        it('créé une nouvelle session', () => {

            const user2 = new User(req, new Resp(new http.ServerResponse(req), req), sessions);

            expect(user.getSessionId()).to.be.not.equal(user2.getSessionId());
        });

        it('créé une nouvelle session configurée', () => {

            const config = {
                sessionCookieName: 'foobar',
                sessionCookieDomain: 'localhost',
                sessionCookiePath: '/hello',
                sessionCookieSecure: true,
                sessionCookieHttpOnly: true
            };
            const user2 = new User(
                req,
                new Resp(new http.ServerResponse(req), req),
                sessions, config
            );
            const spy = sinon.spy(user2, 'setCookie');
            user2.reset();

            expect(spy.calledWith(
                'foobar',
                user2.getSessionId(),
                null, null,
                user2.getSessionCookieDomain(),
                '/hello',
                true, true))
                .to.be.true;
        });

        it('envoit l\'identifiant de la session au client', () => {

            user.getResponse().end();
            expect(resp.getHeader('Set-Cookie')).to.be.deep.equal(
                ['kts_session_id=' + user.getSessionId() + ';HttpOnly']
            );
        });

        it('récupère la session du client et fait coucou', () => {

            const spy = sinon.spy(sessions, 'hello');

            const id = sessions.init();
            sessions.set(id, '_browser', user.browserInfo().signature);
            req.headers.cookie = 'kts_session_id=' + id;
            user = new User(req, resp, sessions);

            expect(user.getSessionId()).to.be.equal(id);
            expect(spy.calledWith(id)).to.be.true;
        });

        it('réinitialise la session et détruit l\'ancienne', () => {

            const id = sessions.init();
            sessions.set(id, '_browser', user.browserInfo().signature);
            req.headers.cookie = 'kts_session_id=' + id;
            user = new User(req, resp, sessions);
            expect(user.getSessionId()).to.be.equal(id);
            user.reset();

            expect(user.getSessionId()).to.not.be.equal(id);
            expect(sessions.has(id)).to.be.false;
        });

        it('défini un élément dans la session', () => {

            user.set('foo', 'bar');
            expect(sessions.get(user.getSessionId(), 'foo')).to.be.equal('bar');
        });

        it('indique si la session possède un élément', () => {

            sessions.set(user.getSessionId(), 'foo', 'bar');

            expect(user.has('foo')).to.be.equal(true);
            expect(user.has('theo')).to.be.equal(false);
        });

        it('récupère la valeur d\'un élément', () => {

            sessions.set(user.getSessionId(), 'foo', 'bar');

            expect(user.get('foo')).to.be.equal('bar');
        });

        it('si élement existe pas, retourne la valeur par défaut', () => {

            expect(user.get('foo', 'bar')).to.be.equal('bar');
        });

        it('si default est une fonction, retourne son résultat', () => {

            expect(user.get('foo', () => { return 'bar'; })).to.be.equal('bar');
        });

        it('supprime un élément de la session', () => {

            sessions.set(user.getSessionId(), 'foo', 'bar');
            user.delete('foo');

            expect(user.has('foo')).to.be.equal(false);
        });

        it('si le navigateur n\'est plus le même, reset', () => {

            req.headers['user-agent'] = 'foobar';
            req.headers.accept = 'text/html, application/xhtml+xml';
            req.headers['accept-language'] = 'fr-FR,fr, en-Us';
            req.headers['accept-encoding'] = 'gzip, deflate';
            user = new User(req, resp, sessions);
            const idOld = user.getSessionId();

            req.headers.cookie = 'kts_session_id=' + idOld;
            req.headers['user-agent'] = 'foobar2';
            req.headers.accept = 'text/html';
            req.headers['accept-language'] = 'en-Us';
            req.headers['accept-encoding'] = 'deflate';
            user = new User(req, resp, sessions);

            expect(user.getSessionId()).to.be.not.equal(idOld);
        });

        it('On peut switcher vers une autres session', () => {

            const user2 = new User(req, resp, sessions);

            user2.switchSession(user.getSessionId());

            expect(user2.getSessionId()).to.be.equal(user.getSessionId());
        });

        it('Lors d\'un switch, fait coucou avec le bon id', () => {

            const user2 = new User(req, resp, sessions),
                  spy = sinon.spy(sessions, 'hello');

            user2.switchSession(user.getSessionId());

            expect(spy.calledWith(user.getSessionId())).to.be.true;
        });

        it('Lors d\'un switch, cookie envoyé avec l\'id', () => {

            const user2 = new User(req, resp, sessions);
            const oldId = user2.getSessionId();
            user2.switchSession(user.getSessionId());
            user2.getResponse().end();

            expect(resp.getHeader('Set-Cookie')).to.be.deep.equal([
                'kts_session_id=' + user.getSessionId() + ';HttpOnly',
                'kts_session_id=' + oldId + ';HttpOnly',
                'kts_session_id=' + user.getSessionId() + ';HttpOnly'
            ]);
        });

        it('Lors d\'un switch, l\'ancienne session delete', () => {

            const user2 = new User(req, resp, sessions);
            const oldId = user2.getSessionId();
            user2.switchSession(user.getSessionId());

            expect(sessions.has(oldId)).to.be.false;
        });

        it('erreur quand id ne correspond à aucune session', () => {

            const user2 = new User(req, resp, sessions);

            expect(user2.switchSession.bind(user2, 'foobar')).to.throw(Error, 'unknown id');
        });
    });


    describe('flash message', () => {

        it('On peut enregistrer un message flash', () => {

            user.setFlash('msg', 'info');

            expect(user.get('flash'))
                .to.be.deep.equal([{ type: 'info', content: 'msg' }]);
        });

        it('On peut en enregister plusieurs', () => {

            user.setFlash('msg1', 'info');
            user.setFlash('msg2', 'success');

            expect(user.get('flash')).to.be.deep.equal([
                { type: 'info', content: 'msg1' },
                { type: 'success', content: 'msg2' }
            ]);
        });

        it('indique si un/des message(s) sont enregistré(s)', () => {

            expect(user.hasFlash()).to.be.equal(false);
            user.setFlash('msg', 'info');
            expect(user.hasFlash()).to.be.equal(true);
        });

        it('retourne la liste des messages', () => {

            user.setFlash('msg1', 'info');
            user.setFlash('msg2', 'success');

            expect(user.getFlash()).to.be.deep.equal([
                { type: 'info', content: 'msg1' },
                { type: 'success', content: 'msg2' }
            ]);
        });

        it('quand on récupère la liste, vide celle-ci', () => {

            user.setFlash('msg1', 'info');
            user.setFlash('msg2', 'success');
            user.getFlash();

            expect(user.getFlash()).to.be.deep.equal([]);
            expect(user.hasFlash()).to.be.false;
        });
    });


    describe('browserInfo', () => {

        it('retourne un object avec certains champs obligatoires', () => {

            expect(user.browserInfo()).to.be.deep.equal({
                'user-agent': '',
                'accept-language': [],
                signature: ''
            });
        });

        it('retourne l\'user agent', () => {

            req.headers['user-agent'] = 'foobar';
            expect(user.browserInfo()['user-agent']).to.be.equal('foobar');
            expect(user.browserInfo().signature).to.be.equal('foobar');
        });

        it('retourne les langages acceptés', () => {

            req.headers['accept-language'] = 'fr-FR,fr, en-Us';
            expect(user.browserInfo()['accept-language']).to.be.deep.equal([
                'fr-FR',
                'fr',
                'en-Us'
            ]);
            expect(user.browserInfo().signature).to.be.equal('fr-FR,fr, en-Us');
        });

        it('retourne une signature du navigateur', () => {

            req.headers['user-agent'] = 'foobar';
            req.headers.accept = 'text/html, application/xhtml+xml';
            req.headers['accept-language'] = 'fr-FR,fr, en-Us';

            expect(user.browserInfo().signature).to.be.equal(
                'foobarfr-FR,fr, en-Us'
            );
        });
    });
});
