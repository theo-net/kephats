/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as http from 'http';

import { KernelAccess } from '../../core/KernelAccess';
import { MemoryStorage } from './MemoryStorage';
import { Resp } from '../Resp';
import { Sessions } from './Sessions';
import * as Utils from '../../cli/Utils';

/* eslint-disable @typescript-eslint/no-explicit-any */

const CONFIG_DEFAULT = {
    sessionCookieName: 'kts_session_id',
    sessionCookieDomain: null,
    sessionCookiePath: null,
    sessionCookieSecure: false,
    sessionCookieHttpOnly: true
};

/**
 * Cette classe gère les données d'un client, un `user`. C'est à dire notamment ses cookies et sa
 * session.
 */
export class User extends KernelAccess {

    protected _config: Record<string, any>;
    protected _cookies: Record<string, any>;
    protected _request: http.IncomingMessage;
    protected _response: Resp;
    protected _sessionCookieDomain = '';
    protected _sessionId = '';
    protected _sessions: Sessions;

    /**
     * Initialise l'objet avec la requête et la réponse.
     *
     *     sessionCookieName:     'kjs_session_id',
     *     sessionCookieDomain:   null,
     *     sessionCookiePath:     null,
     *     sessionCookieSecure:   false,
     *     sessionCookieHttpOnly: false
     *
     * Si `sessionCookieDomain` est un `Array`, on parcourera la liste des domaines pour déterminer
     * celui qui correspond à la requête. Il faut donc les fournir du plus précis au moins précis
     * (s'arrête dès qu'une occurence est trouvée :
     *
     *    - `a.domain.com`
     *    - `b.domain.com`
     *    - `domain.com`
     *    - `other-domain.org`
     *    - ...
     *
     * Si aucun domaine n'est trouvé, on prend le premier de la liste.
     * @param request La requête
     * @param response La réponse
     * @param sessions Service gérant les sessions
     * @param config Configuration
     */
    constructor (
        request: http.IncomingMessage, response: Resp,
        sessions: Sessions, config: Record<string, any> = {}
    ) {

        super();

        this._request = request;
        this._response = response;

        // Configuration
        this._config = Utils.extend({}, CONFIG_DEFAULT, config);
        if (Array.isArray(this._config.sessionCookieDomain)) {

            this._sessionCookieDomain = this._config.sessionCookieDomain[0];
            const host = (request.headers.host as string).split(':')[0];

            Utils.forEach(this._config.sessionCookieDomain, domain => {

                if (host.search(new RegExp(domain + '$')) > -1) {

                    this._sessionCookieDomain = domain;
                    return false;
                }
            });
        }
        else
            this._sessionCookieDomain = this._config.sessionCookieDomain;

        // Si aucun service n'est associé, on en créé un local
        if (!sessions)
            this._sessions = new Sessions(new MemoryStorage);
        else
            this._sessions = sessions;

        // on récupère les cookies
        this._cookies = {};
        this._loadCookies();

        // On initialise ou on récupère la session
        if (!this._getSession())
            this._newSession();
    }


    /**
     * Retourne la configuration
     */
    getConfig (): Record<string, any> {

        return this._config;
    }


    /**
     * Retourne les domaines sur lesquels est associé le cookie de session
     */
    getSessionCookieDomain (): string {

        return this._sessionCookieDomain;
    }


    /**
     * Retourne l'objet `Resp`, réponse de la requête
     */
    getResponse (): Resp {

        return this._response;
    }


    /**
     * Retourne l'id de la session
     */
    getSessionId (): string {

        return this._sessionId;
    }


    /**
     * À étendre si besoin. On peut lui faire retourner une promise, ainsi, le controller ne sera
     * exécuté que lorsque cet objet sera complètement initialisé.
     */
    init (): void | Promise<any> { return; }


    /**
     * Enregistre un cookie. Si `expires` et `maxAge` sont `undefined` ou `null` alors la durée de
     * vie du cookie est celle de la session du navigateur.
     *
     * Attention, les navigateurs ont tendance à restaurer ces cookies.
     *
     * Si `domain` n'est pas défini, alors le cookie s'appliquera au domaine courant, mais pas aux
     * sous-domaines.
     * @param name Nom du cookie
     * @param value Valeur du cookie
     * @param expires Date d'expiration (timestamp)
     * @param maxAge Durée de vie du cookie
     * @param domain Inclue les sous-domaine
     * @param path Chemin pour lesquels s'applique le cookie
     * @param secure Le cookie ne sera transmis qu'en HTTPS
     * @param httpOnly Le cookie ne sera disponible que dans les requête, pas dans la page à travers
     *        les méthodes `Document.cookie`, `XMLHttpRequest` ou `Request`
     * @throws Si les headers ont déjà été envoyés
     */
    setCookie (
        name: string, value: any,
        expires?: number | null, maxAge?: number | null,
        domain?: string | null, path?: string | null,
        secure = false, httpOnly = false
    ): void {

        if (this._response && this._response.headersSent &&
            this._response.headersSent())
            throw new Error('Le cookie ne peut être créé : headers déjà evoyés');

        let cookie = name + '=' + value;

        if (expires != undefined && expires != null)
            cookie += ';Expires=' + expires;
        if (maxAge != undefined && maxAge != null)
            cookie += ';Max-Age=' + maxAge;
        if (domain != undefined && domain != null)
            cookie += ';Domain=' + domain;
        if (path != undefined && path != null)
            cookie += ';Path=' + path;
        if (secure)
            cookie += ';Secure';
        if (httpOnly)
            cookie += ';HttpOnly';

        if (this._response && this._response._cookies)
            this._response._cookies.push(cookie);

        this._cookies[name] = value;
    }


    /**
     * Retourne la valeur d'un cookie
     * @param name Nom du cookie
     */
    getCookie (name: string): any {

        return this._cookies[name];
    }


    /**
     * Indique si un cookie existe ou pas
     * @param name Nom du cookie
     */
    hasCookie (name: string): boolean {

        return Object.keys(this._cookies).indexOf(name) > -1;
    }


    /**
     * Supprime un cookie. Attention, on doit passer les mêmes options pour supprimer le cookie que
     * celles envoyées lors de sa création
     * @param name Cookie à supprimer
     * @param domain Inclue les sous-domaine
     * @param path Chemin pour lesquels s'applique le cookie
     * @param secure Le cookie ne sera transmis qu'en HTTPS
     * @param httpOnly Le cookie ne sera disponible que dans les requête, pas dans la page à travers
     *                 les méthodes `Document.cookie`, `XMLHttpRequest` ou `Request`
     */
    deleteCookie (
        name: string,
        domain?: string | null, path?: string | null,
        secure = false, httpOnly = false
    ): void {

        this.setCookie(name, '', 0, -1, domain, path, secure, httpOnly);
        delete this._cookies[name];
    }


    /**
     * Supprime tous les cookies, sauf celui de la session
     */
    resetCookies (): void {

        Utils.forEach(this._cookies, (value, cookie) => {
            if (cookie != this._config.sessionCookieName)
                this.deleteCookie(cookie);
        });
    }


    /**
     * Définie un élément dans la session
     * @param name Nom de la variable
     * @param value Valeur
     */
    set (name: string, value: any): void {

        this._sessions.set(this._sessionId, name, value);
    }


    /**
     * Indique si un élément de session est défini
     * @param name Nom de la variable
     */
    has (name: string): boolean {

        return this._sessions.hasElement(this._sessionId, name);
    }


    /**
     * Retourne la valeur d'un élément de session ou la valeur par défaut si celle-ci est spécifiée
     * et que l'élément n'existe pas.
     * @param name Nom de la variable
     * @param dflt Valeur par défaut
     */
    get (name: string, dflt: any = null): any {

        return this._sessions.hasElement(this._sessionId, name) ?
            this._sessions.get(this._sessionId, name)
            : (Utils.isFunction(dflt) ? dflt() : dflt);
    }


    /**
     * Supprime un élément de session
     * @param name Nom de la variable
     */
    delete (name: string): void {

        this._sessions.delete(this._sessionId, name);
    }


    /**
     * Réinitialise la session et détruit l'ancienne
     */
    reset (): void {

        const old = this._sessionId;
        this._newSession();
        this._sessions.remove(old);
    }


    /**
     * Enregistre un message flash
     * @param content Le message
     * @param type Type (`success`, `info`, `warning`, `danger`)
     */
    setFlash (content: string, type: string): void {

        if (!this.has('flash'))
            this.set('flash', []);

        const flash = this.get('flash');
        flash.push({ type, content });

        this.set('flash', flash);
    }


    /**
     * Indique si des messages flash sont enregistrés en session
     */
    hasFlash (): boolean {

        if (!this.has('flash'))
            return false;
        else
            return this.get('flash').length > 0;
    }


    /**
     * Retourne la liste des messages flash qui seront ensuites effacés
     * @returns Tableau contenant des objets de la forme {type, content}
     */
    getFlash (): {rtype: string; content: string}[] {

        if (this.has('flash')) {

            const flash = this.get('flash');
            this.delete('flash');
            return flash;
        }
        else
            return [];
    }


    /**
     * Retourne quelques infos sur le navigateur dans un objet ayant les propriétés
     * suivantes :
     *
     *   - `user-agent {String}` : User agent du navigateur
     *   - `accept {Array}` : liste des content-types acceptés
     *   - `accept-language {Array}` : liste des langages acceptés
     */
    browserInfo (): Record<string, any> {

        let headers: http.IncomingHttpHeaders = {};
        if (this._request && this._request.headers)
            headers = this._request.headers;

        const info: Record<string, string | string[]> = {},
              userAgent = headers['user-agent'] ? headers['user-agent'] : '',
              acceptLanguage = headers['accept-language'] ?
                  headers['accept-language'] : '';

        info['user-agent'] = userAgent;
        info['accept-language'] = acceptLanguage != '' ?
            acceptLanguage.split(',').map(str => str.trim()) : [];
        info.signature = userAgent + acceptLanguage;

        return info;
    }


    /**
     * Change la session de l'utilisateur vers une session donnée. Cela permet de forcer l'association
     * d'un utilisateur à une session donnée sans utiliser de cookies.
     *
     * Attention, aucune vérification n'est effectuée ! Une erreur est levée juste si l'id transmis ne
     * correspond à aucune session.
     * @param id Identifiant de la session
     */
    switchSession (id: string): void {

        const old = this._sessionId;

        if (!this._sessions.has(id))
            throw new Error('switchSession: unknown id [' + id + ']');

        this._sessionId = id;
        this._sessions.hello(id);
        this._setCookieSession(this._sessionId);

        this._sessions.remove(old);
    }


    /**
     * Charge les cookies à partir de la requête
     */
    protected _loadCookies (): void {

        if (typeof this._request !== 'undefined' &&
            typeof this._request.headers !== 'undefined' &&
            typeof this._request.headers.cookie !== 'undefined') {

            const cookies = this._request.headers.cookie.split(';');

            cookies.forEach(cookie => {
                const cookieArray = cookie.split('=');
                if (cookieArray.length > 1)
                    this._cookies[cookieArray[0].trim()] = cookieArray[1].trim();
            });
        }
    }


    /**
     * Initialise une nouvelle session
     */
    protected _newSession (): void {

        this._sessionId = this._sessions.init();
        this.set('_browser', this.browserInfo().signature);

        this._setCookieSession(this._sessionId);
    }


    /**
     * Récupère une session, met à jour l'id courant de l'user. On fera également un coucou au
     * gestionnaire pour mettre à jour le dernier accès
     * @returns identifiant de la session ou `false` si aucune session trouvée (pas de cookie ou
     *          pas en mémoire)
     */
    protected _getSession (): string | false {

        let id: string | false = false;
        const signature = this.browserInfo().signature;

        if (this.hasCookie(this._config.sessionCookieName)) {

            id = this.getCookie(this._config.sessionCookieName);

            // session en mémoire ?
            if (!this._sessions.has(id as string))
                id = false;
            // même navigateur ?
            else if (signature != this._sessions.get(id as string, '_browser'))
                id = false;
            // Ok, on retourne la session
            else {
                this._sessionId = id as string;
                this._sessions.hello(id as string);
            }
        }

        return id;
    }


    /**
     * Créé le cookie permettant de transmettre l'identifiant de session.
     * @param id Idenfifiant de la session
     */
    protected _setCookieSession (id: string): void {

        this.setCookie(
            this._config.sessionCookieName,
            id,
            null, null,
            this._sessionCookieDomain,
            this._config.sessionCookiePath,
            this._config.sessionCookieSecure,
            this._config.sessionCookieHttpOnly
        );
    }
}
