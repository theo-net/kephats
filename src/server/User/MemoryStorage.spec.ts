/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { MemoryStorage } from './MemoryStorage';

describe('server/User/MemoryStorage', () => {

    let storage: MemoryStorage;

    beforeEach(() => {

        storage = new MemoryStorage();
    });


    it('init, initialise une session', () => {

        storage.init('hello');

        expect(storage.getRaw('hello')).to.not.be.undefined;
        expect(storage.getRaw('hello').content).to.not.be.undefined;
        expect(storage.getRaw('hello').lastAccess).to.not.be.undefined;
        expect(typeof storage.getRaw('hello').lastAccess).to.be.equal('number');
    });

    it('has, retourne `true` si la session existe', () => {

        storage.init('hello');

        expect(storage.has('hello')).to.be.equal(true);
    });

    it('has, retourne `false` sinon', () => {

        storage.init('hello');

        expect(storage.has('foobar')).to.be.equal(false);
    });

    it('remove détruit un élément', () => {

        storage.init('hello');
        storage.remove('hello');

        expect(storage.has('hello')).to.be.false;
    });

    it('set enregistre un élément dans une session', () => {

        storage.init('hello');
        storage.set('hello', 'foo', 'bar');

        expect(storage.getRaw('hello').content.foo).to.be.equal('bar');
    });

    it('hasElement indique si une session possède un élément', () => {

        storage.init('hello');
        storage.set('hello', 'foo', 'bar');

        expect(storage.hasElement('hello', 'foo')).to.be.equal(true);
        expect(storage.hasElement('hello', 'bar')).to.be.equal(false);
    });

    it('get récupère un élément de la session', () => {

        storage.init('hello');
        storage.set('hello', 'foo', 'bar');

        expect(storage.get('hello', 'foo')).to.be.equal('bar');
    });

    it('delete supprime un élément de la session', () => {

        storage.init('hello');
        storage.set('hello', 'foo', 'bar');
        storage.delete('hello', 'foo');

        expect(storage.getRaw('hello').content.foo).to.be.undefined;
        expect(storage.get('hello', 'foo')).to.be.undefined;
    });

    it('access met à jour le timestamp', () => {

        storage.init('hello');
        const timestamp = storage.getRaw('hello').lastAccess;
        storage.getRaw('hello').lastAccess = 100;
        storage.access('hello');

        expect(storage.getRaw('hello').lastAccess).to.be.equal(timestamp);
    });

    it('count retourne le nombre de sessions enregistrées', () => {

        storage.init('foo');
        storage.init('bar');

        expect(storage.count()).to.be.equal(2);
    });

    it('isTooOld indique si une sessions est trop vieille', () => {

        storage.init('foo');
        expect(storage.isTooOld('foo', Date.now() + 10)).to.be.equal(false);

        storage.getRaw('foo').lastAccess = 0;
        expect(storage.isTooOld('foo', 200)).to.be.equal(true);
    });

    it('garbage supprime toutes les sessions trop anciennes', () => {

        storage.init('hello');
        storage.init('foo');
        storage.init('bar');

        expect(storage.count()).to.be.equal(3);

        storage.getRaw('foo').lastAccess = 100;
        storage.getRaw('bar').lastAccess = 100;

        storage.garbage(200);

        expect(storage.count()).to.be.equal(1);
    });
});
