/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as http from 'http';
import { Stream } from 'stream';

import { Slot } from './Slot';
import { User } from './User/User';
import { View } from './View';

/**
 * Objet réponse
 */
export class Resp {

    public $user: User | null = null;
    public _cookies: string[] = [];

    protected _view: View | null = null;

    protected _request: http.IncomingMessage;
    protected _response: http.ServerResponse;

    /**
     * Initialise l'objet avec la `response` du serveur
     * @param response Réponse du serveur
     * @param request Requête reçue
     */
    constructor (response: http.ServerResponse, request: http.IncomingMessage) {

        this._response = response;
        this._request = request;
    }


    /**
     * Alias vers `response.end()`
     * @param data Données à renvoyer
     * @param encoding Encodage
     * @param callback Callback à exécuter après la réponse
     */
    end (data?: string | Buffer, encoding?: BufferEncoding, callback?: (() => void)): void {

        this.setHeader('Set-Cookie', this._cookies);

        this._response.end(data, encoding as BufferEncoding, callback);
    }


    /**
     * Indique si la réponse a été envoyée ou non
     */
    finished (): boolean {

        return this._response.finished;
    }


    /**
     * Envoit le rendu d'une vue
     * @param view Vue à envoyer
     * @param viewRenderCallback Juste pour passer les tests
     */
    send (view?: View | null, viewRenderCallback?: (() => void)): void {

        if (view)
            this._view = view;
        this.done(undefined, viewRenderCallback);
    }


    /**
     * Rendu fini, on envoit la réponse. Attention, la réponse sera forcément envoyée ! Donc, ne pas
     * utiliser dans le controller `Error` sauf si vous précisez manuellement le code de retour
     * (500, 404, ...)
     * @param data Données à renvoyer (`null` envoit directement la réponse)
     * @param viewRenderCallback Juste pour passer un test
     */
    done (data?: null | string | Buffer, viewRenderCallback = (): void => { return; }): void {

        if (!this.finished()) {

            if (data === null)
                this.end();
            else if (this._view) {
                this.setContentType(this._view.$getMimeType());
                if (data)
                    this.end(data);
                else {
                    this._view.$render().then(render => {
                        this.end(render);
                        viewRenderCallback();
                    });
                }
            }
            else if (data)
                this.end(data);
        }
    }


    /**
     * Pipe un flux vers la réponse. Attention, quand le flux est entièrement lu, la réponse est
     * envoyée complètement. On ne peut plus envoyer d'autres contenus, comme le rendu d'une vue
     * par exemple.
     *
     * C'est le moyen idéal pour envoyer de gros contenus.
     * @param flux Flux à envoyer
     */
    pipeFrom (flux: Stream): void {

        flux.pipe(this._response);
    }


    /**
     * Alias vers response.setHeader()
     * @param name Nom de l'header
     * @param value Valeur de l'header
     */
    setHeader (name: string, value: string | string[] | number): void {

        this._response.setHeader(name, value);
    }


    /**
     * Alias vers `response.getHeader()`
     * @param name Nom de l'header que l'on veut lire
     */
    getHeader (name: string): string | string [] | number | undefined {

        return this._response.getHeader(name);
    }


    /**
     * Alias vers `response.getHeaders()`
     */
    getHeaders (): http.OutgoingHttpHeaders {

        return this._response.getHeaders();
    }


    /**
     * alias vers `response.headersSent()`
     */
    headersSent (): boolean {

        return this._response.headersSent;
    }


    /**
     * Définie le Content-Type de la réponse
     * @param type Content-Type
     */
    setContentType (type: string): void {

        this._response.setHeader('Content-Type', type);
    }


    /**
     * Lance une redirection HTTP
     * @param url URL de redirection
     */
    redirect (url: string): void {

        this._response.statusCode = 302;
        this._response.setHeader('Location', url);
        this.end();
    }


    /**
     * Définit le code retour de la requête HTTP
     * @param code Code à renvoyer
     */
    setStatusCode (code: number): void {

        this._response.statusCode = code;
    }


    /**
     * Retourne le code retour de la requête HTTP
     */
    getStatusCode (): number {

        return this._response.statusCode;
    }


    /**
     * Envoit une erreur 403
     */
    sendError403 (): Promise<number> {

        return this._sendError(403);
    }


    /**
     * Envoit une erreur 404
     */
    sendError404 (): Promise<number> {

        return this._sendError(404);
    }


    /**
     * Envoit une erreur 500
     */
    sendError500 (): Promise<number> {

        return this._sendError(500);
    }


    /**
     * Envoit un slot
     * @param controller Nom du controller
     * @param action Nom de l'action
     * @param vars Variables à transmettre au slot
     */
    async sendSlot (
        controller: string,
        action: string,
        vars: Record<string ,any> | null = {}    // eslint-disable-line @typescript-eslint/no-explicit-any
    ): Promise<number> {

        const slot = new Slot(
            controller, action, vars,
            this._request, this, this.$user as User
        );

        return slot.execute().then(() => {
            this.send(slot.getView());
            return Promise.resolve(this._response.statusCode);
        });
    }


    /**
     * Retourne l'objet réponse interne à NodeJs
     */
    getInternalResponse (): http.ServerResponse {

        return this._response;
    }

    /**
     * Envoit une erreur
     * @param code Code erreur HTTP
     */
    protected _sendError (code: number): Promise<number> {

        // Évite une boucle infinie (si le slot envoit déjà la même erreur)
        if (this._response.statusCode < code) {

            this._response.statusCode = code;
            return this.sendSlot('error', 'e' + code, null);
        }
        return Promise.resolve(code);
    }
}
