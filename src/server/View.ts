/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as fs from 'fs';
import * as path from 'path';

import { ViewError } from './ViewError';
import { KernelAccess } from '../core/KernelAccess';
import * as Utils from '../cli/Utils';

const MIME: Record<string, string> = {
    'html': 'text/html',
    'js': 'text/javascript',
    'json': 'application/json',
    'map': 'application/octet-stream',
    'css': 'text/css',
    'ico': 'image/x-icon',
    'png': 'image/png',
    'jpg': 'image/jpeg',
    'gif': 'image/gif',
    'wav': 'audio/wav',
    'mp3': 'audio/mpeg',
    'svg': 'image/svg+xml',
    'pdf': 'application/pdf',
    'doc': 'application/msword',
    'eot': 'appliaction/vnd.ms-fontobject',
    'ttf': 'application/font-sfnt',
    'txt': 'text/plain',
    'woff': 'application/font-woff',
    'woff2': 'application/font-woff'
};

/**
 * Génère une page ressource qui sera envoyée au client.
 *
 * Peut être du html, mais aussi du css, json, ...
 */
export class View extends KernelAccess {

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [k: string]: any;

    protected _async = false;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    protected _content = function (data?: Record<string, any>): string { return ''; }; // eslint-disable-line @typescript-eslint/no-explicit-any
    protected _contentFile: string | null;
    protected _contentSource = '';
    protected _layout: false | Function = false;
    protected _mimeType = '';
    protected _rootDir = '';


    /**
     * Créé une nouvelle vue. Si `contentFile` est `null`, une vue vide sera chargée
     * @param contentFile Fichier contenant la vue
     * @param mime Mime type (`html` par défaut)
     */
    constructor (contentFile: string | null, mime = 'html') {

        super();

        this._async = false;
        this._layout = false;
        this._contentFile = contentFile;
        this._contentSource = '';

        // On charge la vue si elle n'est pas vide
        if (contentFile !== null) {

            this._rootDir = path.dirname(contentFile);

            try {
                this.$setContent(fs.readFileSync(contentFile).toString(), contentFile);
            } catch {
                throw new ViewError('Impossible de charger la vue ' + contentFile
                    + ': accès au fichier impossible');
            }
        }

        // On définie le type MIME
        this.$setMimeType(mime);
    }


    /**
     * Génère le rendu de la vue et renvoit le contenu.
     *
     * Tout le contenu de `$view.helpers` défini dans le service `$config` sera disponible dans la
     * vue. Cela permet de créer un système d'helpers.
     * @returns {Promise}
     */
    $render (): Promise<string> {

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const data: Record<string, any> = Utils.extend({},
            this, {
                '$getSlot': this.$getSlot,
                '$getRoute': this.$getRoute,
            },
            this.kernel().get('$config').get('$view', { helpers: {} }).helpers,
        );

        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const self = this;
        async function render (): Promise<string> {

            try {

                // Rendu du slot
                data.content = await self._content(data);
                // Ajout du layout
                if (self._layout)
                    data.content = await self._layout(data);

                return data.content;
            }
            catch (e) {

                console.error(self.kernel().get('$color').red(
                    'Error view render (' + self._contentFile + ') ' + e
                ));
                console.error(e);
                self.$response.sendError500();
            }
            return '';
        }

        return render();
    }


    /**
     * Définit le type Mime
     * @param mime Type Mime
     */
    $setMimeType (mime: string): void {

        this._mimeType = MIME[mime];
        if (this._mimeType == undefined) {
            throw new ViewError('Impossible de charger la vue ' + this._contentFile
                + ': MIME type `' + mime + '` non supporté');
        }
    }


    /**
     * Retourne le type MIME
     */
    $getMimeType (): string {

        return this._mimeType;
    }


    /**
     * Défini un layout en chargeant le fichier `layout`. Si ce paramètre vaut `false`, alors aucun
     * layout ne sera chargé.
     * @param layout Chemin vers le layout
     * @throws `ViewError` Si la vue n'arrive pas à charger le fichier
     */
    $setLayout (layout: string | false): void {

        if (layout) {
            try {

                this._layoutSource = this._includeSubFiles(
                    fs.readFileSync(layout).toString(), layout
                );
                this._layout = this.kernel().get('$interpolate')(this._layoutSource);
            } catch (e) {

                throw new ViewError(
                    'Impossible de charger le layout ' + layout + ' ' + e
                );
            }
        }
        else
            this._layout = false;
    }


    /**
     * Définie le contenu de la vue
     * @param content Contenu
     * @param contentFile Fichier contenant la vue
     */
    $setContent (content: string, contentFile?: string): void {

        // On charge les fichiers inclus
        this._contentSource = this._includeSubFiles(content, contentFile);

        // On analyse le template
        try {
            this._content = this.kernel().get('$interpolate')(this._contentSource);
        } catch (e) {

            throw new ViewError('Impossible de charger la vue ' + contentFile
                + ': template invalide... ' + e);
        }
    }


    /**
     * Helper : retourne le contenu d'un autre Slot
     * @param controller Controlleur
     * @param action Action
     * @param vars Variables à transmettre
     * @returns {Promise}
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    $getSlot (controller: string, action: string, vars: Record<string, any> = {}): Promise<string> {

        const slot = new (require('./Slot').Slot)(
            controller, action, vars,
            this.$request, this.$response,
            this.$user
        );

        return new Promise(resolve => {

            slot.execute().then(() => {
                return slot.getView().$render().then((render: string) => { resolve(render); });
            });
        });
    }


    /**
     * Helper : retourne une URL construite à partir du routeur. Https si le paramètre `forceHttps`
     * du `$config` vaut `true`.
     * @param id Identifiant de la route
     * @param vars Paramètres
     */
    $getRoute (id: string, vars: Record<string, any>): string {    // eslint-disable-line @typescript-eslint/no-explicit-any

        const kernel = new (require('../core/Kernel').Kernel)();

        return kernel.get('$router').unroute(
            id, vars, this.$request.headers.host,
            kernel.get('$config').get('forceHttps', false)
        );
    }


    /**
     * Retourne la source de la view
     */
    $getContentSource (): string {

        return this._contentSource;
    }


    /**
     * Retourne le layout
     */
    $getLayout (): false | Function {

        return this._layout;
    }


    /**
     * Retourne la source du layout
     */
    $getLayoutSource (): string {

        return this._layoutSource;
    }


    /**
     * Inclus un fichier dans une view (pour $setContent et $setLayout). Cette inclusion sera
     * relative au fichier courant. Si aucun n'est précisé, ce sera relatif au basepath.
     * @param content Contenu à traité
     * @param contentFile Chemin vers le fichier contenant l'inclusion
     * @returns Contenu traité
     */
    _includeSubFiles (content: string, contentFile?: string): string {

        let rootDir = this._rootDir;
        if (contentFile) rootDir = path.dirname(contentFile);

        // On charge les fichiers inclus
        content = content.replace(
            /\{\{\s*\$include\(\s*'(.*)'\s*\)\s*\}\}/g,
            (match, file) => {

                file = rootDir + path.sep + file;
                try {
                    return fs.readFileSync(file).toString();
                } catch (e) {
                    throw new ViewError('#include dans ' + contentFile
                        + ' Impossible de charger le bloc ' + file
                        + ': accès au fichier impossible');
                }
            }
        );

        return content;
    }
}
