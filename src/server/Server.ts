/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as http from 'http';
import * as path from 'path';
import * as querystring from 'querystring';

import { Cli } from '../cli/Cli';
import { Config } from '../core/Services/Config';
import { FormBuilder } from './Form/FormBuilder';
import { Forms } from './Services/Forms';
import { Kernel } from '../core/Kernel';
import { Kfmd } from './Services/Kfmd';
import { MemoryStorage } from './User/MemoryStorage';
import { Models } from './Services/Models';
import { Resp } from './Resp';
import { Router } from './Services/Router';
import { RouteError } from './Services/RouteError';
import { Sessions } from './User/Sessions';
import { Slot } from './Slot';
import * as Utils from '../cli/Utils';

const DEFAULT_PORT = 8089,
      POST_BODY_MAX_SIZE = 1e6,
      HTTP_PAYLOAD_TOO_LARGE = 413;

/**
 * Point d'entrée de notre application. KephaJs est disponible partout à travers l'instance du
 * serveur. Vous pourrez y accéder directement avec kephajs
 *
 * Il y a trois éléments importants pour la config :
 *
 *   - `basepath` racine vers l'application (doit se terminer par `/`)
 *   - `routes` chemin à partir de basepath vers le fichier contenant les routes de l'application
 *   - `staticDir` chemin à partir de basepath vers les fichiers statiques
 */
export class Server extends Cli {

    /**
     * Cette propriété publique contient la fonction qui initialisera votre serveur : utilisez la
     * pour configurer votre application.
     * @param $config Service `$config`
     * @param kernel Noyau du framework
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public init = ($config: any, kernel: Kernel): void => { // eslint-disable-line @typescript-eslint/no-explicit-any
        return;
    };

    /**
     * Initialise le serveur et le démarre.
     * @param config Configuration du serveur
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    constructor (kernel: Kernel, config: Record<string, any> = {}) {

        super(kernel);

        const $config = this.get('$config');

        // On intialise les canaux d'évènements
        this.kernel().events().register('Router');
        this.kernel().events().register('Slot');

        // On initialise les logs
        this.kernel().events().attach('Slot', 'log', (data: string) => {
            this.kernel().get('$logs').log(data, 'debug');
        });

        // On initialise quelques services
        this.kernel().container().register('$models', new Models());
        this.kernel().container().register('$sessions', new Sessions(new MemoryStorage()));
        this.kernel().container().register('$kfmd', new Kfmd());
        this.kernel().container().register('$forms', new Forms(this.kernel().get('$validate')));
        this.kernel().container().register('$staticCache', new Config());


        // Commande principale
        this.command('start', 'Starts the server')
            .option('-c, --config', 'Fichier de configuration', 'string')
            .option('-p, --port', 'Port of server', { integer: { min: 0 } }, DEFAULT_PORT)
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            .action((args: Record<string, any>, options: Record<string, any>) => {

                // On charge la configuration
                $config.add(config);
                let basepath = config.basepath ? config.basepath : process.cwd() + path.sep;
                if (options.config) {
                    basepath = process.cwd() + path.sep;
                    // eslint-disable-next-line @typescript-eslint/no-var-requires
                    $config.add(require(basepath + options.config));
                }
                basepath = path.normalize(basepath);
                $config.set('basepath', basepath);

                // On initialise le routing
                this.kernel().container().register('$router', new Router(basepath));
                if ($config.get('routes'))
                    this.kernel().get('$router').load($config.get('routes'));

                // On lance l'init
                this.init($config, this.kernel());

                // On démarre le serveur et on sauve son instance
                $config.set('port', options.port);
                $config.set('$server', this.startServer(options.port));
            });
    }


    /**
     * Démarre un serveur
     * @param port Port sur lequel écoute le serveur
     * @returns Serveur créé
     */
    startServer (port = DEFAULT_PORT): http.Server {

        const logLevel = this.get('$config').get('logLevel');


        if (logLevel != 'warn') {

            console.log(this.color().red('Démarage du serveur sur le port ')
                + port + this.color().red('...'));
        }

        const server = http.createServer(this._requestListener.bind(this));
        server.on('clientError', (err, socket) => {
            socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
        });
        server.listen(port);

        return server;
    }


    /**
     * Alias vers this.kernel().get()
     * @param {String} service Nom du service
     * @returns {Object}
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    get (service: string): any {

        return this.kernel().get(service);
    }


    /**
     * Définis les modèles disponibles dans l'application.
     * @param {Object} models Modèles : nom => instance
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    setModels (models: Record<string, Record<string, any>>): void {

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        Utils.forEach(models, (model: any, name: string) => {
            this.get('$models').register(name, model);
        });
    }


    /**
     * Définis les formsBuilders dispobibles dans l'application.
     * @param formBuilders formBuilders : `nom => class`
     */
    setFormBuilders (formBuilders: Record<string, typeof FormBuilder>): void {

        Utils.forEach(formBuilders, (formBuilder, id) => {
            this.get('$forms').register(id, formBuilder);
        });
    }


    /**
     * Exécute la requête reçue.
     * @param request Requête reçue
     * @param res Réponse du serveur
     */
    private _requestListener (request: http.IncomingMessage, res: http.ServerResponse): void {

        if (this.kernel().get('$config').get('logLevel') == 'debug') {
            console.log(this.color().green('Requête reçue ') + request.method + ' '
                + 'http://' + request.headers.host + request.url);
        }

        // On passe par notre propre objet réponse
        const response = new Resp(res, request);

        // Dispatcher : traite la requête et envoit le bon slot
        try {
            const route = this.kernel().get('$router').getRoute(
                request.method, request.url, request.headers.host
            );
            const slot = new Slot(
                route.route._callable[0], route.route._callable[1],
                route.vars, request, response
            );

            // On traite les data POST
            if (request.method == 'POST') {

                let body = '';
                request.on('data', data => {

                    body += data;

                    if (body.length > POST_BODY_MAX_SIZE) {
                        body = '';
                        res.writeHead(
                            HTTP_PAYLOAD_TOO_LARGE, { 'Content-Type': 'text/plain' }
                        ).end();
                        request.connection.destroy();
                    }
                });

                request.on('end', () => {

                    slot.execute(querystring.parse(body))
                        .then(() => { response.send(slot.getView()); })
                        .catch(() => { response.send(); });
                });
            }
            else {
                slot.execute()
                    .then(() => { response.send(slot.getView()); })
                    .catch(() => { response.send(); });
            }
        }
        catch (e) {

            if (this.kernel().get('$config').get('logLevel') == 'debug')
                console.error(e);

            if (e instanceof RouteError)
                response.sendError404();
            else
                response.sendError500();
        }
    }


    /**
     * Capture les signaux pour quitter correctement l'application
     * @param signal Signal capturé
     */
    protected _handleExit (signal: string): void {

        console.log('\nReceived ' + signal + '. Close the server...');

        const server = this.get('$config').get('$server');
        if (server.close) {
            server.close(() => {
                console.log('...server closed');
                process.exit(0);
            });
        }
        else {
            console.log('...nothing to close');
            process.exit(0);
        }
    }
}
