/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as fs from 'fs';
import * as path from 'path';

import { Config } from '../core/Services/Config';
import { Controller } from './Controller';
import * as Utils from '../cli/Utils';

/**
 * Controlleur affichant les ressources statiques
 *
 * On y fait appel avec '@static' dans les routes. On lui transmet deux
 * variables :
 *   - `file` chemin vers le fichier à charger
 *   - `staticDir` répertoire à partir duquel on utilise `file`. S'il est non
 *      défini, on prendra la valeur se trouvant dans le service $config.
 * Un système de cache est intégré : le navigateur pourra stocker ces fichiers.
 * Le système est remis à zéro à chaque redémarage du serveur, le navigateur
 * récupérera à ce moment-là une nouvelle version du fichier. Vous pouvez
 * forcer la remise à zéro d'un fichier à l'aide du service `$staticCache` :
 *
 *     kernel.get('$staticCache').delete('chemin absolu vers le fichier') ;
 */
export class StaticController extends Controller {

    protected _staticDir = '';
    protected _staticCache: Config | undefined;


    setViews (): Record<string, string | null> {

        return {
            slot: null
        };
    }


    init (): void {

        this._staticDir = this.kernel().get('$config').get('staticDir', './');
        this._staticCache = this.kernel().get('$staticCache');
    }


    /**
     * Action principale du controller
     * @param vars Variables transmises
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    slotAction (vars: Record<string, any> = {}): Promise<unknown> {

        this.$view.$setLayout(false);

        if (vars.staticDir)
            this._staticDir = vars.staticDir;

        let pathname = this.kernel().get('$config').get('basepath', './')
                     + this._staticDir + vars.file;


        // On récupère les headers concernant le cache
        const date = new Date(this.$request.headers['if-modified-since'] as string),
              etag = this.$request.headers['if-none-match'] as string;

        // Fichier en cache, on vérifie le cache et on transmet les infos
        if ((this._staticCache as Config).has(pathname) &&
         (this._staticCache as Config).get(pathname).lastModified.getTime() <= date.getTime() &&
            (this._staticCache as Config).get(pathname).etag == etag) {

            // Header avec infos cache
            this.$response.setHeader(
                'last-modified', this.$request.headers['if-modified-since'] as string
            );
            this.$response.setHeader('etag', etag);
            this.$response.setStatusCode(304);
            this.$response.end();

            return Promise.resolve(null);
        }
        // Fichier non chargé ou plus récent
        else {
            return new Promise((resolve, reject) => {
                fs.exists(pathname, exist => {
                    // si le fichier n'existe pas, renvoie 404
                    if (!exist)
                        return reject(404);

                    // s'il s'agit d'un répertoire, on tente d'y trouver un fichier
                    // `index.html`
                    if (fs.statSync(pathname).isDirectory()) {

                        if (!/\/$/.test(pathname))
                            pathname += '/';
                        pathname += 'index.html';

                        // On charge ce fichier s'il existe
                        fs.exists(pathname, exist => {

                            if (!exist)
                                return reject(404);
                            else
                                this._staticFile(pathname, resolve);
                        });
                    }
                    else
                        this._staticFile(pathname, resolve);
                });
            });
        }
    }


    /**
     * Retourne le chemin vers le répertoire où se trouvent les fichiers statiques
     */
    getStaticDir (): string {

        return this._staticDir;
    }


    /**
     * Rend un fichier statique
     * @param file Chemin vers le fichier
     * @param resolve Référence vers le resolve de la Promise
     */
    private _staticFile (file: string, resolve: Function): void {

        // On met à jour le cache
        const cache = {
            lastModified: new Date(),
            etag: Utils.uuid()
        };
        cache.lastModified.setMilliseconds(0); // on enlève un peu de précision
        (this._staticCache as Config).set(file, cache);
        // Et on transmet les infos
        this.$response.setHeader('last-modified', cache.lastModified.toUTCString());
        this.$response.setHeader('etag', cache.etag);

        // extraction du suffixe de fichier selon le chemin basé sur l'URL
        // fournie. ex. .js, .doc, ... et détermination du content-type
        this.$view.$setMimeType(path.parse(file).ext.substring(1));
        this.$response.setContentType(this.$view.$getMimeType());

        // On envoit les cookies
        this.$response.setHeader('Set-Cookie', this.$response._cookies);

        this.$response.pipeFrom(fs.createReadStream(file));
        resolve(null);
    }
}
