/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as http from 'http';
import * as path from 'path';

import { Controller } from './Controller';
import { KernelAccess } from '../core/KernelAccess';
import { Resp } from './Resp';
import { SlotError } from './SlotError';
import { User } from './User/User';
import * as Utils from '../cli/Utils';
import { View  } from './View';

/**
 * Un slot est une action routée par le routeur que l'on peut exécuter
 *
 * Dans le service `$config`, si `$userObj` est défini on utilisera sa valeur comme l'objet à partir
 * duquel on instance l'objet `User`.
 *
 * Dans le service `$config` on peut préciser le chemin vers les controlleurs et vers les vues :
 *
 *   - `controllerDir` (défaut : `src/controller/`)
 *   - `templatesDir` (défaut : `templates/`)
 */
export class Slot extends KernelAccess {

    protected _action: string;
    protected _controller: Controller;
    protected _controllerName: string;
    protected _request: http.IncomingMessage;
    protected _response: Resp;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected _userInit: Promise<any>;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected _vars: Record<string, any>;
    protected _view: View | null;

    /**
     * Initialise le Slot. Lui associera sa vue et définira le layout
     * @param controller Controller à exécuter. Peut être un objet à initialiser
     * @param action Action à exécuter
     * @param vars Variables transmises
     * @param request Requête reçue
     * @param response Réponse du serveur
     * @param user Objet user à transmettre (si non défini, en crééra un nouveau)
     */
    constructor (
        controller: string | Function, action: string,
        vars: Record<string, any> | null, // eslint-disable-line @typescript-eslint/no-explicit-any
        request: http.IncomingMessage, response: Resp,
        user?: User
    ) {

        super();

        this._controllerName = Utils.ucfirst(controller);
        this._action = action;
        this._vars = vars ? vars : {};
        this._request = request;
        this._response = response;

        // On initialise le controller
        //   Un objet
        if (Utils.isFunction(controller))
            this._controllerName = '[object]';
        //   Un String
        else {
            let controllerName = this._controllerName + 'Controller';
            if (this._controllerName.substr(0, 1) == '@') {
                controllerName = Utils.ucfirst(controllerName.substr(1));
                controller = __dirname + path.sep
                    + controllerName;
                if (!action)
                    this._action = action = 'slot';
            }
            else {
                controller = this.kernel().get('$config').get('basepath')
                    + this.kernel().get('$config').get('controllerDir', 'src/Controller/')
                    + controllerName;
            }

            try {
                // eslint-disable-next-line @typescript-eslint/no-var-requires
                const controllerReq = require(controller);
                if (!controllerReq[controllerName])
                    throw new Error();
                else
                    controller = controllerReq[controllerName];
            }
            catch (e) {
                throw new SlotError(
                    'Controller `' + this._controllerName + '` not found. (' + e + ')'
                );
            }
        }

        // On créé une instance
        this._controller = new (controller as typeof Controller)(
            this.kernel(), request, response, user
        );
        this._userInit = this._controller.getUserInit();

        // On vérifie que l'action existe
        if (!Utils.isFunction(Reflect.get(this._controller, action + 'Action'))) {
            throw new SlotError(
                'Action `' + action + '` of controller `'
                + this._controllerName + '` not found'
            );
        }

        // On initialise la vue
        let view: null | string = '';
        const views = this._controller.setViews();

        if (views[this._action] === null)
            view = null;
        else if (views[this._action])
            view = views[this._action];
        else
            view = this._action + '.html';

        this._view = new View(view == null ? null
            : this.kernel().get('$config').get('basepath')
            + this.kernel().get('$config').get('templatesDir', 'templates' + path.sep)
            + Utils.kebabCase(this._controllerName) + path.sep + view
        );

        let layout = this.kernel().get('$config').get('layout.html', false);
        if (layout)
            layout = this.kernel().get('$config').get('basepath') + layout;
        this._view.$setLayout(layout);

        // On transmet certaines choses au controller et à la vue
        this._controller.$view = this._view;
        this._view.$request = request;
        this._view.$response = response;

        // Transmet l'User à la vue et à la réponse
        this._view.$user = this._controller.$user;
        if (this._response)
            this._response.$user = this._controller.$user;

        // On notifie le canal Slot
        this.kernel().events().notify('Slot',
            'Slot ' + this._controllerName + ':' + this._action + ' initialisé'
        );
    }


    /**
     * Exécute le slot. Gère les actions asynchrones, et même une initialisation
     * du controleur asynchrone. Pour cela, l'`init` ou l'action doivent
     * retourner une `Promise`. Le reject de cette Promise peut contenir un code
     * erreur (403, 404, 500, ...) qui renverra l'erreur page correspondante. Si
     * un message est retourné, ce sera une erreur 500, si une exception est
     * attrapée, son message sera affiché.
     * Une action dont la promise est résolue avec la valeur `null`, ne vera pas
     * son exécution résolue : utile si la requête est terminée par elle-même
     * (ex : envoit d'un fichier lourd via un buffer).
     * @param data Données pouvant être issues d'une requête
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    execute (data: Record<string, any> = {}): Promise<unknown> {

        return new Promise((resolve, reject) => {

            Utils.extend(this._vars, data);

            this._userInit.then(() => {

                return this._controller.init(this._vars);

            }).then(() => {

                return Reflect.get(this._controller, this._action + 'Action')
                    .call(this._controller, this._vars);
            }).then((code) => {

                if (code !== null)
                    resolve();
            }).catch((code) => {

                if (code && code.stack) {

                    console.error(this.kernel().get('$color').red(code.stack));
                    code = 500;
                }
                if (!Utils.isFunction(Reflect.get(this._response, 'sendError' + code)))
                    code = 500;

                Reflect.get(this._response, 'sendError' + code).call(this._response)
                    .then((code: number) => { reject(code); });
            });
        });
    }


    /**
     * Retourne la vue associée au slot
     * @returns {View|null}
     */
    getView (): View | null {

        return this._view;
    }

    /**
     * Retourne l'action
     */
    getAction (): string {

        return this._action;
    }

    /**
     * Retourne le controller
     */
    getController (): Controller {

        return this._controller;
    }

    /**
     * Retourne le nom du controller
     */
    getControllerName (): string {

        return this._controllerName;
    }

    /**
     * Retourne la requête
     */
    getRequest (): http.IncomingMessage {

        return this._request;
    }

    /**
     * Retourne la réponse
     */
    getResponse (): Resp {

        return this._response;
    }

    /**
     * Retourne les variables
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    getVars (): Record<string, any> {

        return this._vars;
    }
}
