/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as sinon from 'sinon';

import { Route } from './Route';
import { Router } from './Router';
import { RouteError } from './RouteError';

describe('server/Services/Router', () => {

    let router: Router;

    beforeEach(() => {

        router = new Router('../../../docs/demo-server/');
    });

    describe('load', () => {

        it('charge des routes à partir d\'un objet', () => {

            router.load({
                test: {
                    method: 'get',
                    pattern: 'test',
                    controller: 'index',
                    action: 'index',
                    defaults: {
                        a: 1, b: 'c'
                    },
                    requirements: {
                        a: '\\d', b: '\\w'
                    },
                    domain: 'test.com'
                }
            });

            expect(router.getRoutes().test.getMethod()).to.be.equal('get');
            expect(router.getRoutes().test.getPattern()).to.be.equal('test');
            expect(router.getRoutes().test.getDefaults()).to.be.deep.equal({ a: 1, b: 'c' });
            expect(router.getRoutes().test.getRequirements())
                .to.be.deep.equal({ a: '\\d', b: '\\w' });
            expect(router.getRoutes().test.getCallable())
                .to.be.deep.equal(['index', 'index']);
            expect(router.getRoutes().test.getDomain()).to.be.equal('test.com');
        });

        it('préfixe toutes les routes si demandé', () => {

            router.load({
                test: {
                    method: 'get',
                    pattern: 'test',
                    controller: 'index',
                    action: 'index',
                }
            }, 'un_', 'le');

            expect(router.getRoutes().leTest.getPattern()).to.be.equal('un_test');
        });

        it('détermine un domaine par défaut si demandé', () => {

            router.load({
                test1: {
                    method: 'get',
                    pattern: 'test',
                    controller: 'index',
                    action: 'index',
                    domain: 'test.com'
                },
                test2: {
                    method: 'get',
                    pattern: 'test',
                    controller: 'index',
                    action: 'index',
                }
            }, '', '', 'domain.com');

            expect(router.getRoutes().test1.getDomain()).to.be.equal('test.com');
            expect(router.getRoutes().test2.getDomain()).to.be.equal('domain.com');
        });

        it('charge des routes à partir d\'un fichier', () => {

            router.load('config/routes.json');

            expect(router.getRoutes().index).to.be.instanceof(Route);
            expect(router.getRoutes().styles).to.be.instanceof(Route);
            expect(router.getRoutes().index.getPattern()).to.be.equal('/');
            expect(router.getRoutes().styles.getPattern()).to.be.equal('/{file}');
        });

        it('gère l\'inclusion de fichiers', () => {

            router.load('config/routes.json');

            expect(router.getRoutes().adminIndex).to.be.instanceof(Route);
            expect(router.getRoutes().adminIndex.getPattern()).to.be.equal('/admin/');
        });

        it('lors de l\'inclusion on peut spécifier le prefix du pattern', () => {

            router.load({
                admin: {
                    ressource: 'config/routesAdmin.json',
                    prefix: 'admin',
                    pattern: ''
                },
                admin2: {
                    ressource: 'config/routesAdmin.json',
                    prefix: 'admin',
                    pattern: 'ok'
                }
            });

            expect(router.getRoutes().adminIndex).to.be.instanceof(Route);
            expect(router.getRoutes().adminIndex.getPattern()).to.be.equal('/');
            expect(router.getRoutes().admin2Index).to.be.instanceof(Route);
            expect(router.getRoutes().admin2Index.getPattern()).to.be.equal('ok/');
        });

        it('par l\'inclusion, on peut définir des vars par défaut', () => {

            router.load({
                admin: {
                    ressource: 'config/routesAdmin.json',
                    prefix: 'admin',
                    defaults: {
                        foo: 'bar',
                        bar: 'foo'
                    }
                },
                admin2: {
                    ressource: 'config/routesAdmin.json',
                    prefix: 'admin',
                }
            });

            expect(router.getRoutes().admin2Index.getDefaults()).to.be.deep.equal({});
            expect(router.getRoutes().admin2Test.getDefaults()).to.be.deep.equal({
                foo: 'bar2'
            });
            expect(router.getRoutes().adminIndex.getDefaults()).to.be.deep.equal({
                foo: 'bar',
                bar: 'foo'
            });
            expect(router.getRoutes().adminTest.getDefaults()).to.be.deep.equal({
                foo: 'bar2',
                bar: 'foo'
            });
        });

        it('construit une ressource controller/action', () => {

            router.load({
                test: {
                    method: 'get',
                    pattern: 'test',
                    controller: 'ctrl',
                    action: 'act'
                }
            });

            expect(router.getRoutes().test.getCallable())
                .to.be.deep.equal(['ctrl', 'act']);
        });

        it('construit une ressource @*', () => {

            router.load({
                test: {
                    method: 'get',
                    pattern: 'test',
                    controller: '@ctrl'
                }
            });

            expect(router.getRoutes().test.getCallable())
                .to.be.deep.equal(['@ctrl']);
        });
    });


    describe('getRoute', () => {

        it('parse une URL pour retourner la bonne route', () => {

            router.load({
                route1: {
                    method: 'get',
                    pattern: '/index.html'
                },
                route2: {
                    method: 'get',
                    pattern: '/foobar',
                    domain: 'forum.domain..*'
                },
                route3: {
                    method: ['get', 'post'],
                    pattern: '/index.html'
                }
            });

            expect(router.getRoute('get', '/index.html', 'lol.fr').route)
                .to.be.equal(router.getRoutes().route1);
            expect(router.getRoute('gEt', '/index.html', 'lol.fr').route)
                .to.be.equal(router.getRoutes().route1);
            expect(router.getRoute('get', '/foobar', 'forum.domain.fr').route)
                .to.be.equal(router.getRoutes().route2);
            expect(router.getRoute('gEt', '/index.html', 'lol.fr').route)
                .to.be.equal(router.getRoutes().route1);
            expect(router.getRoute('post', '/index.html', 'lol.fr').route)
                .to.be.equal(router.getRoutes().route3);
        });

        it('lève une exception si aucune route trouvée', () => {

            router.load({
                route1: {
                    method: 'get',
                    pattern: '/index.html'
                },
                route2: {
                    method: 'get',
                    pattern: '/foobar',
                    domain: /forum\.domain\..*/
                },
                route3: {
                    method: ['get', 'post'],
                    pattern: '/index.html'
                }
            });

            expect(router.getRoute.bind(router, 'get', '/inde.html', 'lol.fr'))
                .to.throw(RouteError);
            expect(router.getRoute.bind(router, 'get', '/foobar', 'lol.fr'))
                .to.throw(RouteError);
        });

        it('retourne un objet avec la route et les vars trouvées', () => {

            router.load({
                route: {
                    method: 'get',
                    pattern: '/index-{var1}{?var2}.html',
                    requirements: {
                        var1: '\\w+',
                        var2: '-(\\w+)'
                    },
                    defaults: {
                        var1: 'foo',
                        var2: 'bar',
                        var3: 'lol'
                    }
                }
            });

            expect(router.getRoute.bind(router, 'get', '/index.html', 'lol.fr'))
                .to.throw(RouteError);
            const match1 = router.getRoute('get', '/index-test-shalom.html', 'lol.fr');
            const match2 = router.getRoute('get', '/index-test.html', 'lol.fr');

            expect(match1.route).to.be.equal(router.getRoutes().route);
            expect(match2.route).to.be.equal(router.getRoutes().route);
            expect(match1.vars).to.be.deep.equal({
                var1: 'test',
                var2: 'shalom',
                var3: 'lol'
            });
            expect(match2.vars).to.be.deep.equal({
                var1: 'test',
                var2: 'bar',
                var3: 'lol'
            });
        });

        it('notifie le canal `Router` lorsqu\'une route est trouvée', () => {

            router.load({
                route1: {
                    method: 'get',
                    pattern: '/index.html'
                }
            });

            router.kernel().events().register('Router');
            const spy = sinon.spy();
            router.kernel().events().attach('Router', 'spy', spy);
            router.getRoute('get', '/index.html', 'lol.fr');

            expect(spy.getCall(0).args[0])
                .to.be.equal('Route pour `GET:lol.fr/index.html` trouvée');
        });
    });


    describe('unroute', () => {

        it('reconstruit une URL à partir d\'une route', () => {

            router.load({
                route: {
                    method: 'get',
                    pattern: '/index-{var1}{?var2}.html',
                    requirements: {
                        var1: '\\w+',
                        var2: '-(\\w+)'
                    },
                    defaults: {
                        var1: 'foo',
                        var2: 'bar',
                        var3: 'lol'
                    }
                }
            });

            expect(router.unroute('route', { var1: 'theo', var2: 'net' }))
                .to.be.equal('/index-theo-net.html');
        });

        it('Lance une exeption si la route demandée n\'existe pas', () => {

            router.load({
                route: {
                    method: 'get',
                    pattern: '/index-{var1}{?var2}.html',
                    requirements: {
                        var1: '\\w+',
                        var2: '-(\\w+)'
                    }
                }
            });

            expect(router.unroute.bind(router, 'inconnu', {}))
                .to.throw(RouteError);
        });

        it('Renvoit une URL absolue si besoin (domaine différent)', () => {

            router.load({
                route1: {
                    method: 'get',
                    pattern: '/foobar.html',
                },
                route2: {
                    method: 'get',
                    pattern: '/truc.html',
                    domain: 'admin.theo-net.*'
                }
            });

            expect(router.unroute('route2', {})).to.be.equal('/truc.html');
            expect(router.unroute('route2', {}, 'admin.theo-net.org'))
                .to.be.equal('/truc.html');
            expect(router.unroute('route2', {}, 'www.theo-net.org'))
                .to.be.equal('http://admin.theo-net.org/truc.html');
        });

        it('Renvoit une URL (domaine localhost)', () => {

            router.load({
                route1: {
                    method: 'get',
                    pattern: '/foobar.html',
                    domain: 'admin.theo-net.org'
                },
                route2: {
                    method: 'get',
                    pattern: '/truc.html',
                    domain: 'admin.theo-net.*'
                }
            });

            expect(router.unroute('route1', {}, 'localhost'))
                .to.be.equal('http://admin.theo-net.org/foobar.html');
            expect(router.unroute('route2', {}, 'localhost'))
                .to.be.equal('http://admin.theo-net/truc.html');
        });

        it('http ou https selon la requête', () => {

            router.load({
                route: {
                    method: 'get',
                    pattern: '/truc.html',
                    domain: 'admin.theo-net.*'
                }
            });

            expect(router.unroute('route', {}, 'admin.theo-net.org', true))
                .to.be.equal('/truc.html');
            expect(router.unroute('route', {}, 'www.theo-net.org', false))
                .to.be.equal('http://admin.theo-net.org/truc.html');
            expect(router.unroute('route', {}, 'www.theo-net.org', true))
                .to.be.equal('https://admin.theo-net.org/truc.html');
        });

        it('fonctionne aussi si le port est spécifique', () => {

            router.load({
                route: {
                    method: 'get',
                    pattern: '/truc.html',
                    domain: 'admin.theo-net.*'
                }
            });

            expect(router.unroute('route', {}, 'admin.theo-net.local:8089', false))
                .to.be.equal('/truc.html');
            expect(router.unroute('route', {}, 'www.theo-net.local:8089', false))
                .to.be.equal('http://admin.theo-net.local:8089/truc.html');
        });
    });


    describe('getFiles', () => {

        it('retourne la liste des fichiers chargés par le router', () => {

            router.load('config/routes.json');
            expect(router.getFiles()).to.be.deep.equal([
                {
                    file: '../../../docs/demo-server/config/routes.json',
                    prefix: '',
                    basename: ''
                },
                {
                    file: '../../../docs/demo-server/config/routesAdmin.json',
                    prefix: '/admin',
                    basename: 'admin'
                },
            ]);
        });
    });
});

