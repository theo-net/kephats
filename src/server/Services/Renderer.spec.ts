/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Renderer } from './Renderer';

describe('server/Services/renderer', () => {

    const renderer = new Renderer();

    it('code', () => {

        expect(renderer.code('test é < >', 'a'))
            .to.be.equal('<!-- a --><pre><code>\ntest é < >\n</code></pre>\n');
    });

    it('blockquote', () => {

        expect(renderer.blockquote('test'))
            .to.be.equal('<blockquote>\ntest</blockquote>\n');
    });

    it('html', () => {

        expect(renderer.html('test'))
            .to.be.equal('test');
    });

    it('heading', () => {

        expect(renderer.heading('test', 1, 'Ok1!'))
            .to.be.equal('<h1 id="ok1-">test</h1>\n');
        expect(renderer.heading('test', 3, 'Ok1!'))
            .to.be.equal('<h3 id="ok1-">test</h3>\n');
    });

    it('list', () => {

        expect(renderer.list('test', false))
            .to.be.equal('<ul>\ntest</ul>\n');
        expect(renderer.list('test', true))
            .to.be.equal('<ol>\ntest</ol>\n');
    });

    it('listItem', () => {

        expect(renderer.listItem('test'))
            .to.be.equal('  <li>test</li>\n');
    });

    it('taskList', () => {

        expect(renderer.taskList('test', false))
            .to.be.equal('<ul>\ntest</ul>\n');
        expect(renderer.taskList('test', true))
            .to.be.equal('<ol>\ntest</ol>\n');
    });

    it('taskListItem', () => {

        expect(renderer.taskListItem('test', false))
            .to.be.equal('  <li><input type="checkbox" disabled>test</li>\n');
        expect(renderer.taskListItem('test', true)).to.be.equal(
            '  <li><input type="checkbox" disabled checked>test</li>\n');
    });

    it('paragraph', () => {

        expect(renderer.paragraph('test'))
            .to.be.equal('<p>test</p>\n');
    });

    it('table', () => {

        expect(renderer.table('test', 'foobar')).to.be.equal(
            '<table>\n<thead>\ntest</thead>\n<tbody>\nfoobar</tbody>\n</table>\n');
    });

    it('tablerow', () => {

        expect(renderer.tablerow('test'))
            .to.be.equal('  <tr>\ntest  </tr>\n');
    });

    it('tablecell', () => {

        expect(renderer.tablecell('test', { header: true, align: 'right' }))
            .to.be.equal('    <th class="txtright">test</th>\n');
        expect(renderer.tablecell('test', { align: 'center' }))
            .to.be.equal('    <td class="txtcenter">test</td>\n');
        expect(renderer.tablecell('test', {}))
            .to.be.equal('    <td>test</td>\n');
    });

    it('strong', () => {

        expect(renderer.strong('test'))
            .to.be.equal('<strong>test</strong>');
    });

    it('em', () => {

        expect(renderer.em('test'))
            .to.be.equal('<em>test</em>');
    });

    it('codespan', () => {

        expect(renderer.codespan('test'))
            .to.be.equal('<code>test</code>');
    });

    it('br', () => {

        expect(renderer.br())
            .to.be.equal('<br>\n');
    });

    it('del', () => {

        expect(renderer.del('test'))
            .to.be.equal('<del>test</del>');
    });

    it('link', () => {

        expect(renderer.link('test', 'foobar', 'ok'))
            .to.be.equal('<a href="test" title="foobar">ok</a>');
        expect(renderer.link('test', '', 'ok'))
            .to.be.equal('<a href="test">ok</a>');
    });

    it('text', () => {

        expect(renderer.text('test'))
            .to.be.equal('test');
    });

    it('footNote', () => {

        expect(renderer.footNote(2))
            .to.be.equal('<a href="#note-2"><sup>2</sup></a>');
    });
});
