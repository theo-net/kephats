/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { ButtonField } from '../Form/ButtonField';
import { CheckboxField } from '../Form/CheckboxField';
import { ColorField } from '../Form/ColorField';
import { DateField } from '../Form/DateField';
import { EmailField } from '../Form/EmailField';
import { HiddenField } from '../Form/HiddenField';
import { HtmlField } from '../Form/HtmlField';
import { NumberField } from '../Form/NumberField';
import { PasswordField } from '../Form/PasswordField';
import { RadioField } from '../Form/RadioField';
import { RangeField } from '../Form/RangeField';
import { ResetField } from '../Form/ResetField';
import { SearchField } from '../Form/SearchField';
import { SelectField } from '../Form/SelectField';
import { StringField } from '../Form/StringField';
import { SubmitField } from '../Form/SubmitField';
import { TelField } from '../Form/TelField';
import { TextField } from '../Form/TextField';
import { TimeField } from '../Form/TimeField';
import { UrlField } from '../Form/UrlField';

import { Entity } from '../../core/Entity';
import { Field } from '../Form/Field';
import { Form } from '../Form/Form';
import { FormBuilder } from '../Form/FormBuilder';
import { Forms } from './Forms';
import { Validate as ValidateS} from '../../core/Services/Validate';


describe('server/Services/Forms', () => {

    let forms: Forms;
    let a = false;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let b: Record<string, any>;

    class TestFormBuilder extends FormBuilder {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        build (config: Record<string, any>): void {
            a = true;
            b = config;
            this.getForm().add('string', { name: 'test', required: true }).addFieldset();
        }
    }

    beforeEach(() => {

        forms = new Forms(new ValidateS());
    });

    it('on peut lui transmettre le service $validate', () => {

        const validate = new ValidateS();
        forms = new Forms(validate);
        expect(forms.$validate).to.be.equal(validate);
    });


    describe('register', () => {

        it('enregistre un form builder', () => {

            forms.register('test', TestFormBuilder);
            expect(forms.getAllFormBuilders()['test']).to.be.equal(TestFormBuilder);
        });

        it('retourne le service (pratique pour chaînage)', () => {

            expect(forms.register('test', TestFormBuilder)).to.be.equal(forms);
        });
    });


    describe('build', () => {

        it('retourne un form initialisé par le formBuilder', () => {

            forms.register('test', TestFormBuilder);
            const form = forms.build('test');

            expect(form).to.be.instanceOf(Form);
            expect(a).to.be.true;
        });

        it('peut transmettre une entité', () => {

            forms.register('test', TestFormBuilder);
            class MyEntity extends Entity {
                _setProperties (): void { return; }
            }
            const entity = new MyEntity();
            const form = forms.build('test', entity);

            expect(form.getEntity()).to.be.equal(entity);
        });

        it('le formulaire possède une référence au service', () => {

            forms.register('test', TestFormBuilder);
            const form = forms.build('test');

            expect(form.$forms).to.be.equal(forms);
        });

        it('on peut spécifier des classes à ajouter pour tous les formulaires créés', () => {

            forms.register('test', TestFormBuilder);
            expect(forms.addClass({
                field: 'a',
                label: 'b',
                group: 'c',
                groupSuccess: 'd',
                groupError: 'e',
                msg: 'f',
                msgHelp: 'g',
                msgError: 'h',
                fieldset: 'i'
            })).to.be.equal(forms);
            const form = forms.build('test');

            expect((form.getAllFields()[0] as Field).getClass()).to.be.equal('a');
            expect((form.getAllFields()[0] as Field).getLabelClass()).to.be.equal('b');
            expect((form.getAllFields()[0] as Field).getGroupClass()).to.be.equal('c');
            expect((form.getAllFields()[0] as Field).getMsgClass()).to.be.equal('f g');
            expect((form.getAllFields()[0] as Field).getMsgClass('help')).to.be.equal('f g');
            expect((form.getAllFields()[0] as Field).getMsgClass('error')).to.be.equal('f h');
            expect((form.getAllFields()[1] as Field).getClass()).to.be.equal('i');

            (form.getAllFields()[0] as Field).isValid();
            expect((form.getAllFields()[0] as Field).getGroupClass()).to.be.equal('c e');
            (form.getAllFields()[0] as Field).setValue('ok');
            (form.getAllFields()[0] as Field).isValid();
            expect((form.getAllFields()[0] as Field).getGroupClass()).to.be.equal('c d');
        });

        it('on peut transmettre des paramètres', () => {

            const config = {};
            forms.register('test', TestFormBuilder);
            forms.build('test', null, config);

            expect(b).to.be.equal(config);
        });
    });


    describe('has', () => {

        it('returns true if the formBuilder is registered', () => {

            forms.register('test', TestFormBuilder);
            expect(forms.has('test')).to.be.equal(true);
        });

        it('returns false if the formBuilder isn\'t registred', () => {

            forms.register('test', TestFormBuilder);
            expect(forms.has('test1')).to.be.equal(false);
        });
    });


    describe('delete', () => {

        it('delete a formBuilder', () => {

            forms.register('test', TestFormBuilder);
            expect(forms.has('test')).to.be.equal(true);
            forms.delete('test');
            expect(forms.has('test')).to.be.equal(false);
        });
    });


    describe('fields', () => {

        class MyField extends Field {}

        it('enregistre un field disponible', () => {

            forms.registerField('test', MyField);
            expect(forms.hasField('test')).to.be.true;
            expect(forms.getField('test')).to.be.equal(MyField);
        });

        it('indique si un field est disponible', () => {

            forms.registerField('test', MyField);
            expect(forms.hasField('test')).to.be.true;
            expect(forms.hasField('test2')).to.be.false;
        });

        it('supprime un field', () => {

            forms.registerField('test', MyField);
            expect(forms.hasField('test')).to.be.true;
            forms.deleteField('test');
            expect(forms.hasField('test')).to.be.false;
        });

        it('envoit une erreur si un field demandé n\'existe pas', () => {

            expect(forms.getField.bind(forms, 'test')).to.throw(Error);
        });

        it('fields disponibles par défaut', () => {

            expect(forms.getField('string')).to.be.equal(StringField);
            expect(forms.getField('email')).to.be.equal(EmailField);
            expect(forms.getField('password')).to.be.equal(PasswordField);
            expect(forms.getField('search')).to.be.equal(SearchField);
            expect(forms.getField('tel')).to.be.equal(TelField);
            expect(forms.getField('url')).to.be.equal(UrlField);
            expect(forms.getField('number')).to.be.equal(NumberField);
            expect(forms.getField('range')).to.be.equal(RangeField);
            expect(forms.getField('color')).to.be.equal(ColorField);
            expect(forms.getField('date')).to.be.equal(DateField);
            expect(forms.getField('time')).to.be.equal(TimeField);
            expect(forms.getField('hidden')).to.be.equal(HiddenField);
            expect(forms.getField('submit')).to.be.equal(SubmitField);
            expect(forms.getField('button')).to.be.equal(ButtonField);
            expect(forms.getField('reset')).to.be.equal(ResetField);
            expect(forms.getField('radio')).to.be.equal(RadioField);
            expect(forms.getField('checkbox')).to.be.equal(CheckboxField);
            expect(forms.getField('text')).to.be.equal(TextField);
            expect(forms.getField('select')).to.be.equal(SelectField);
            expect(forms.getField('html')).to.be.equal(HtmlField);
        });
    });
});
