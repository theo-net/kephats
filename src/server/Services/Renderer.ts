/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export interface RendererInterface {
    code (code: string, lang: string): string;
    blockquote (quote: string): string;
    html (html: string): string;
    heading (text: string, level: number, raw: string): string;
    list (body: string, ordered: boolean): string;
    listItem (text: string): string;
    taskList (body: string, ordered: boolean): string;
    taskListItem (text: string, checked: boolean): string;
    paragraph (text: string): string;
    table (header: string, body: string): string;
    tablerow (content: string): string;
    tablecell (content: string, flags: {header?: boolean; align?: string }): string;
    strong (text: string): string;
    em (text: string): string;
    codespan (text: string): string;
    br (): string;
    del (text: string): string;
    link (href: string, title: string | null, text: string): string;
    text (text: string): string;
    footNote (nb: number): string;
}

/**
 * Il ne s'agit pas d'un service, mais de la classe permettant d'instancier un objet pour le rendu
 * utilisé par `$kfmd`.
 */
export class Renderer {

    /**
     * Portion de code
     * @param code Portion de code
     * @param lang Langage du code
     */
    code (code: string, lang: string): string {

        lang = lang ? '<!-- ' + lang + ' -->' : '';
        return lang + '<pre><code>\n' + code + '\n</code></pre>\n';
    }


    /**
     * Citation
     * @param quote Contenu de la citation
     */
    blockquote (quote: string): string {

        return '<blockquote>\n' + quote + '</blockquote>\n';
    }


    /**
     * Code HTML
     * @param html Code qui sera retourné tel quel
     */
    html (html: string): string { return html; }


    /**
     * Titre
     * @param text Texte du titre
     * @param level Niveau (entre 1 et 6)
     * @param raw Texte du titre sans transformation
     */
    heading (text: string, level: number, raw: string): string {

        return '<h' + level + ' id="'
            + raw.toLowerCase().replace(/[^\w]+/g, '-')
            + '">' + text + '</h' + level + '>\n';
    }


    /**
     * Liste
     * @param body Contenu de la liste
     * @param ordered Liste ordonnée ou non
     */
    list (body: string, ordered: boolean): string {

        const type = ordered ? 'ol' : 'ul';
        return '<' + type + '>\n' + body + '</' + type + '>\n';
    }


    /**
     * Item d'une liste
     * @param text Texte de l'item
     */
    listItem (text: string): string { return '  <li>' + text + '</li>\n'; }


    /**
     * Liste de tâche
     * @param body Contenu de la liste
     * @param ordered Liste ordonnée ou non
     */
    taskList (body: string, ordered: boolean): string {

        const type = ordered ? 'ol' : 'ul';
        return '<' + type + '>\n' + body + '</' + type + '>\n';
    }


    /**
     * Item d'une liste de tâches
     * @param text Texte de l'item
     * @param checked Tâche effectuée ou non
     */
    taskListItem (text: string, checked: boolean): string {

        return '  <li><input type="checkbox" disabled'
            + (checked ? ' checked' : '')
            + '>' + text + '</li>\n';
    }


    /**
     * Un paragraphe
     * @param text Texte
     */
    paragraph (text: string): string { return '<p>' + text + '</p>\n'; }


    /**
     * Tableau
     * @param header Contenu du header (`<thead>`)
     * @param body Contenu du corps (`<tbody>`)
     */
    table (header: string, body: string): string {

        return '<table>\n<thead>\n' + header + '</thead>\n<tbody>\n'
            + body + '</tbody>\n</table>\n';
    }


    /**
     * Ligne d'un tableau
     * @param content Contenu de la ligne
     */
    tablerow (content: string): string { return '  <tr>\n' + content + '  </tr>\n'; }


    /**
     * Cellule d'un tableau
     * @param content Contenu de la cellule
     * @param flags Si la propriété `header` est définie, il s'agit d'une cellule d'un header (`th`),
     *              la proprité `align` (facultative) définie l'alignement
     */
    tablecell (content: string, flags: {header?: boolean; align?: string }): string {

        const type = flags.header ? 'th' : 'td';
        const tag = flags.align ?
            '    <' + type + ' class="txt' + flags.align + '">'
            : '    <' + type + '>';
        return tag + content + '</' + type + '>\n';
    }


    /**
     * Emphase forte
     * @param text Texte à mettre en emphase
     */
    strong (text: string): string { return '<strong>' + text + '</strong>'; }


    /**
     * Emphase faible
     * @param text Texte à mettre en emphase
     */
    em (text: string): string { return '<em>' + text + '</em>'; }


    /**
     * Code inline
     * @param text Code
     */
    codespan (text: string): string { return '<code>' + text + '</code>'; }


    /**
     * Retour à la ligne dans un paragraphe
     */
    br (): string { return '<br>\n'; }


    /**
     * Barré (supprimé)
     * @param text Texte
     */
    del (text: string): string { return '<del>' + text + '</del>'; }


    /**
     * Lien
     * @param href URL du lien
     * @param title Attribut title du lien (peut être vide)
     * @param text Texte du lien
     */
    link (href: string, title: string | null, text: string): string {

        let out = '<a href="' + href + '"';
        if (title)
            out += ' title="' + title + '"';
        out += '>' + text + '</a>';
        return out;
    }


    /**
     * Simple texte
     * @param text Le texte
     */
    text (text: string): string { return text; }


    /**
     * footNote
     * @param nb Numéro de la note
     */
    footNote (nb: number): string {

        return '<a href="#note-' + nb + '"><sup>' + nb + '</sup></a>';
    }
}
