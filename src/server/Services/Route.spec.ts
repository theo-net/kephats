/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Route, tRouteMatch } from './Route';

describe('server/Services/Route', () => {

    it('créé une route à partir des paramètres', () => {

        const route = new Route(
            'get', '/', ['a', 'b'],
            { a: '\\d', b: '\\w' }, { a: 1, b: 'a' },
            'www.theo-net.org'
        );

        expect(route.getMethod()).to.be.equal('get');
        expect(route.getPattern()).to.be.equal('/');
        expect(route.getCallable()).to.be.deep.equal(['a', 'b']);
        expect(route.getDefaults().a).to.be.equal(1);
        expect(route.getDefaults().b).to.be.equal('a');
        expect(route.getRequirements().a).to.be.equal('\\d');
        expect(route.getRequirements().b).to.be.equal('\\w');
        expect(route.getDomain()).to.be.equal('www.theo-net.org');
    });

    it('si `domain` est undefined, créé une valeur par défaut', () => {

        const route = new Route('get', '/', ['a', 'b']);
        expect(route.getDomain()).to.be.equal('.*');
    });

    it('si `defaults` est undefined, créé une valeur par défaut', () => {

        const route = new Route('get', '/', ['a', 'b']);
        expect(route.getDefaults()).to.be.deep.equal({});
    });

    it('si `requirements` est undefined, créé une valeur par défaut', () => {

        const route = new Route('get', '/', ['a', 'b']);
        expect(route.getRequirements()).to.be.deep.equal({});
    });

    it('dans `requirements` remplace `[uuid]`', () => {

        const route = new Route(
            'get', '/{a}{b}{c}', ['a', 'b'],
            { a: '\\d', b: '[uuid]', c: '\\d+_[uuid]_ok' }
        );
        const uuid = '[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}';
        expect(route.getPatternRegExp()).to.be.deep.equal(RegExp(
            '^/(\\d)(' + uuid + ')(\\d+_' + uuid + '_ok)$'
        ));
    });


    describe('match', () => {

        it(
            'parse uri, domain, method: if match, return object with route, false if not',
        () => {

            const route1 = new Route('get', '/index.html', ['a', 'b']);
            const route2 = new Route('get', '/foobar', ['a', 'b'], {}, {}, 'forum.domain..*');
            const route3 = new Route(['get', 'post'], '/index.html', ['a', 'b']);

            expect((route1.match('get', '/index.html', 'lol.fr') as tRouteMatch).route)
                .to.be.equal(route1);
            expect((route1.match('gEt', '/index.html', 'lol.fr') as tRouteMatch).route)
                .to.be.equal(route1);
            expect(route1.match('post', '/index.html', 'lol.fr'))
                .to.be.equal(false);
            expect(route1.match('get', '/inde.html', 'lol.fr'))
                .to.be.equal(false);
            expect((route2.match('get', '/foobar', 'forum.domain.fr') as tRouteMatch).route)
                .to.be.equal(route2);
            expect(route2.match('get', '/foobar', 'lol.fr'))
                .to.be.equal(false);
            expect((route3.match('get', '/index.html', 'lol.fr') as tRouteMatch).route)
                .to.be.equal(route3);
            expect((route3.match('gEt', '/index.html', 'lol.fr') as tRouteMatch).route)
                .to.be.equal(route3);
            expect((route3.match('post', '/index.html', 'lol.fr') as tRouteMatch).route)
                .to.be.equal(route3);
        });

        it('retourne un objet avec la route et les vars trouvées', () => {

            const route = new Route(
                'get',
                '/index-{var1}{?var2}.html', ['a', 'b'],
                {
                    var1: '\\w+',
                    var2: '-(\\w+)'
                }, {
                    var1: 'foo',
                    var2: 'bar',
                    var3: 'lol'
                }
            );

            const match1 = route.match('get', '/index.html', 'lol.fr') as tRouteMatch;
            const match2 = route.match('get', '/index-test-shalom.html', 'lol.fr') as tRouteMatch;
            const match3 = route.match('get', '/index-test.html', 'lol.fr') as tRouteMatch;

            expect(match1).to.be.false;
            expect(match2.route).to.be.equal(route);
            expect(match3.route).to.be.equal(route);
            expect(match2.vars).to.be.deep.equal({
                var1: 'test',
                var2: 'shalom',
                var3: 'lol'
            });
            expect(match3.vars).to.be.deep.equal({
                var1: 'test',
                var2: 'bar',
                var3: 'lol'
            });
        });
    });


    describe('unroute', () => {

        it('reconstruit une URI à partir des params', () => {

            const route = new Route('get', '/index-{var1}{?var2}.html', ['a', 'b'], {
                var1: '\\w+',
                var2: '-(\\w+)'
            }, {
                var1: 'foo',
                var2: 'bar',
                var3: 'lol'
            });

            expect(route.unroute({ var1: 'test', var2: 'shalom' }))
                .to.be.equal('/index-test-shalom.html');
        });

        it('utilise si besoin les valeurs par défaut', () => {

            const route = new Route('get', '/index-{var1}{?var2}.html', ['a', 'b'], {
                var1: '\\w+',
                var2: '-(\\w+)'
            }, {
                var1: 'foo',
                var2: 'bar',
                var3: 'lol'
            });

            expect(route.unroute({ var1: 'test' }))
                .to.be.equal('/index-test-bar.html');
        });
    });
});

