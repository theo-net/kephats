/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Models } from './Models';

describe('server/Services/Models', () => {

    let models: Models;
    const model = {};

    beforeEach(() => {

        models = new Models();
    });

    describe('register and get', () => {

        it('enregistre un modèle', () => {

            models.register('test', model);
            expect(models.get('test')).to.be.equal(model);
        });

        it('retourne null si le modèle est inexistant', () => {

            models.register('model', model);
            expect(models.get('model2')).to.be.equal(null);
        });
    });


    describe('has', () => {

        it('returns true if models have the key', () => {

            models.register('model', model);
            expect(models.has('model')).to.be.equal(true);
        });

        it('returns false if models haven\'t the key', () => {

            models.register('model', model);
            expect(models.has('model1')).to.be.equal(false);
        });
    });


    describe('delete', () => {

        it('delete an element', () => {

            models.register('model', model);
            expect(models.has('model')).to.be.equal(true);
            models.delete('model');
            expect(models.has('model')).to.be.equal(false);
        });
    });
});

