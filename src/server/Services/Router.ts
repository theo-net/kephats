/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { KernelAccess } from '../../core/KernelAccess';
import { Route, tRouteMatch } from './Route';
import { RouteError } from './RouteError';
import * as Utils from '../../cli/Utils';

/**
 * Routeur du serveur : il est capable de parcourir une série de routes afin de déterminer quelles
 * ressources servir à partir de la requête. Il est également capable de reconstruire une URL.
 */
export class Router extends KernelAccess {

    protected _basepath: string;
    protected _files: {
        file: string;
        prefix: string;
        basename: string;
    }[] = [];
    protected _routes: Record<string, Route> = {};

    /**
     * Initialise le service.
     *
     * @param basepath Chemin de base pour toutes les ressources chargées
     */
    constructor (basepath = './') {

        super();
        this._basepath = basepath;
    }


    /**
     * Parse un objet de configuration ou un fichier JSON.
     *
     * L'objet doit avoir la structure suivante :
     *
     *     {
     *         "leNom": {
     *             "ressource": "path",
     *             "prefix": "le préfixe",
     *             "domain": "un domain"
     *         }
     *         "unAutre": {
     *             "method": "get",
     *             "pattern": "un pattern",
     *             "domain": "le domaine à parser",
     *             "controller": "un controller",
     *             "action": "une action",
     *             "defaults": {
     *                 "nom de la variable": "valeur"
     *             },
     *             "requirements": {
     *                 'nom de la variable": "un pattern"
     *             }
     *         }
     *     }
     *
     * `method` peut aussi contenir une série de méthode :
     *
     *     "method": ["get", "post"]
     *
     * Chaque route doit avoir un identifiant unique.
     *
     * Les routes de ressources permettent d'inclure les routes d'un autre fichier. Dans ce cas le
     * nom des routes sera préfixé par le nom de la ressource dont la première lettre sera forcée
     * en majuscule (ici `leNom`, donc si une route de la ressource s'appelle `foobar`, son nom
     * deviendra `leNomFoobar`) et leur pattern par la valeur de `prefix` sauf si celui-ci est
     * précisé lors du chargement de la ressource, auquel cas il servira de préfixe. Enfin, si
     * `domain` est précisé, toutes les routes inclues auront ce pattern de domaine par défaut. Le
     * chemin de la ressource doit toujours être donné relativement au `basepath` défini lors de
     * l'initialisation du service. On peut également spécifier des variables par défauts qui seront
     * associées à chaque route incluse.
     *
     * Pour plus de détails, regardez l'objet Route.
     *
     * Attention, Les classes de caractères, `\d \w \s ...` doivent être échappées : `\\d \\w \\s ...`
     *
     * Attention, les routes seront examinées dans l'ordre du fichier et dans l'ordre d'inclusion !
     *
     * @param routes Objet contenant les routes ou chemin du fichier (donné par rapport u absepath)
     * @param prefix Préfixe qui sera ajouté au pattern de toutes les routes
     * @param basename Préfixe qui sera ajouté à l'identifiant de toutes les routes
     * @param domain Pattern de domaine par défaut
     * @param defaults Valeur par défaut des variables (chaque route étend cette liste
     */
    load (
        routes: string | Record<string, any>,       // eslint-disable-line @typescript-eslint/no-explicit-any
        prefix = '', basename = '', domain = '',
        defaults: Record<string, any> = {},         // eslint-disable-line @typescript-eslint/no-explicit-any
    ): void {

        // C'est une chaîne de caractères : on charge le fichier et on retransmet
        // le contenu à la méthode.
        if (Utils.isString(routes)) {

            this._files.push({
                file: this._basepath + routes,
                prefix: prefix,
                basename: basename
            });

            return this.load(
                require(this._basepath + routes),   // eslint-disable-line @typescript-eslint/no-var-requires
                prefix, basename, domain, defaults
            );
        }

        // Sinon on traite l'objet
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        Utils.forEach(routes as Record<string, any>, (route, name) => {

            name = basename ?
                basename + name.toUpperCase().charAt(0) + name.substring(1)
                : name;

            if (Object.keys(this._routes).indexOf(name) < 0) {

                // Ressource
                if (route.ressource) {

                    let pref = prefix;

                    if (route.pattern || route.pattern == '')
                        pref += route.pattern;
                    else
                        pref += route.prefix ? route.prefix : '';

                    this.load(route.ressource, pref, name,
                        route.domain ? route.domain : domain, route.defaults);
                }
                // Router
                else {

                    route.defaults = route.defaults ? route.defaults : {};

                    this._routes[name] = new Route(
                        route.method,
                        (prefix ? prefix : '') + route.pattern,
                        route.action ? [route.controller, route.action]
                            : [route.controller],
                        route.requirements,
                        Utils.extend({}, defaults, route.defaults),
                        route.domain ? route.domain : domain
                    );
                }
            }
        });
    }


    /**
     * Cherche la bonne route
     *
     * @param method Méthode de la requête
     * @param uri URI de la requête
     * @param domain Domaine de la requête
     * @returns Contenant deux éléments : `routes` et `vars` avec la route trouvée et les variables
     *          extraites de la requête.
     * @throws `RouteError` Si aucune route n'est trouvée
     */
    getRoute (method: string, uri: string, domain: string): tRouteMatch {

        let goodRoute = false;

        Utils.forEach(this._routes, route => {

            if ((route = route.match(method, uri, domain))) {

                this.kernel().events().notify('Router', 'Route pour `'
                    + method.toUpperCase() + ':' + domain + uri + '` trouvée'
                );

                goodRoute = route;
                return false; // On sort de la boucle
            }
        });

        if (!goodRoute)
            throw new RouteError('Aucune route trouvée');

        return goodRoute;
    }


    /**
     * Retourne toutes les routes enregistrées
     */
    getRoutes (): Record<string, Route> {

        return this._routes;
    }


    /**
     * Reconstruit une route à partir de paramètres
     * @param id Identifiant de la route
     * @param params Valeur des variables
     * @param domain Domaine courant (permettra de déterminer s'il y a besoin d'une URL absolue ou non
     * @param https L'URL absolue est-elle en HTTPS ? (`false` par défaut)
     */
    unroute (id: string, params = {}, domain = '', https = false): string {

        if (Object.keys(this._routes).indexOf(id) < 0)
            throw new RouteError('La route ' + id + ' n\'existe pas');

        return this._routes[id].unroute(params, domain, https);
    }


    /**
     * Retourne la liste des fichiers de routes chargés
     */
    getFiles (): {
        file: string;
        prefix: string;
        basename: string;
    }[] {

        return this._files;
    }
}
