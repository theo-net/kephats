/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * Service permettant d'accéder aux modèles de l'application
 *
 *     const models = new Models() ;
 *     models.add({
 *         'model1': model1,
 *         'model2': model2
 *     }) ;
 *     const model = models.get('model2') ;
 */
export class Models {

    protected _models: Record<string, any> = {};


    /**
     * Enregistre un modèle
     *
     * @param id Identifiant du modèle
     * @param model Modèle à enregistrer.
     */
    register (id: string, model: Record<string, any>): void {

        this._models[id] = model;
    }


    /**
     * Retourne un modèle ou null si le modèle n'existe pas
     *
     * @param id Identifiant
     */
    get (id: string): Record<string, any> | null {

        return this.has(id) ? this._models[id] : null;
    }


    /**
     * Indique si un modèle est enregistré ou non
     * @param id Identifiant de l'élément à tester
     */
    has (id: string): boolean {

        return Object.keys(this._models).indexOf(id) > -1;
    }


    /**
     * Supprime un élément
     * @param id Identifiant de l'élément à supprimer
     */
    delete (id: string): void {

        delete this._models[id];
    }
}
