/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as Utils from '../../cli/Utils';

export type tRouteMatch = { route: Route; vars: Record<string, string> };

/**
 * Une route utilisable par le routeur
 */
export class Route {

    protected _callable: string[];
    protected _defaults: Record<string, any>; // eslint-disable-line @typescript-eslint/no-explicit-any
    protected _domain: string;
    protected _domainRegExp: RegExp;
    protected _method: string | string[];
    protected _methodRegExp: RegExp;
    protected _pattern: string;
    protected _patternRegExp: RegExp;
    protected _requirements: Record<string, string>;
    protected _varsName: string[] = [];


    /**
     * Construit une route à partir de différents éléments.
     *
     * `method` défini la ou les méthodes pour lesquelles cette route est valide (`get`, `post`,
     * `delete`, ...). Un `string` suffit pour une méthode, utilisez un `string[]` pour plusieurs.
     * Elles sont insensibles à la case.
     *
     * Le `pattern` de la route est une chaînes permettant de matcher la route :
     *
     *     '/index.html'  // ne traitera que celle-ci
     *     '/{style}.css' // tous ce qui se termine par `.css` et définie une var
     *
     * On nottera que dans le `pattern`, les `"."` sont systématiquement échapés.
     *
     * Les variables seront traitées et leur valeur retournées après le parsage. Vous pouvez, avec
     * des objets `nomDeLaVariable: valeur` définir certaintes choses :
     *
     *   - un objet `defaults` pour les valeurs par défaut (facultatif)
     *   - un objet `requirements` pour la RegExp correspondant à la valeur
     *
     * Une variable facultative commence par un `?`, ex : `{?facultatif}`
     *
     * On peut spécifier quelle partie de la variable récupérer (très pratique pour certains patterns.
     * On prend le pattern `/news-{var1}{var2}.html` et on aura en `requirements` : `var1: \w+` et
     * `var2: -(\d+)` ainsi les URI `/news-a.html` et `/news-a-2.html` matcheront, mais `var2` aura
     * comme valeur `2` et non `-2`. Attention, il ne faut qu'une paire de parenthèses.
     *
     * Mettre un élément dans `defaults` qui n'est pas présent dans le pattern et ni dans le
     * `requirements` est un bon moyen de forcer le passage d'une valeur.
     *
     * `domain` est traité comme une RegExp.
     *
     * Échappez toutes les classes de caractères comme par exemple `\w` en mettant `\\w`.
     *
     * Dans les `requirements`, `[uuid]` sera remplacé par la RegExp permettant de tester si une
     * chaîne est un UUID v4.
     *
     * @param method Méthode(s) de la route
     * @param pattern Pattern de la route
     * @param {Array} callable Tableau ['controller', 'action'] ou [object] avec
     *   `object` l'objet dont on appelera la méthode exec()
     * @param requirements Patterns des variables
     * @param defaults Valeur par défaut des variables (nomVar: val)
     * @param domain Pattern du domain
     */
    constructor (
        method: string | string [],
        pattern: string,
        callable: string[],
        requirements?: Record<string, string>,
        defaults?: Record<string, any>, // eslint-disable-line @typescript-eslint/no-explicit-any
        domain = '.*'
    ) {

        this._method = method;
        this._pattern = pattern;
        this._callable = callable;
        this._requirements = requirements ? requirements : {};
        this._defaults = defaults ? defaults : {};
        this._domain = domain ? domain : '.*';

        if (Array.isArray(method))
            method = '(' + method.join(')|(') + ')';
        this._methodRegExp = new RegExp('^' + method + '$', 'i');
        this._domainRegExp = new RegExp('^' + this._domain + '$');

        // On échappe le pattern qu'on enregistre
        pattern = pattern.replace('.', '\\.');
        this._patternRegExp = new RegExp('^' + pattern + '$');

        // On traite les variables
        let vars: RegExpExecArray | null;
        const varsRegExp = /\{(\??\w+)\}/g;
        while ((vars = varsRegExp.exec(pattern)) !== null) {

            if (vars[1].substr(0, 1) === '?')
                this._insertVar(vars[1].substr(1), true, vars[0]);
            else
                this._insertVar(vars[1], false, vars[0]);
        }
    }


    /**
     * Retourne le callable
     */
    getCallable (): string[] {

        return this._callable;
    }


    /**
     * Retourne
     */
    getDefaults (): Record<string, any> {    // eslint-disable-line @typescript-eslint/no-explicit-any

        return this._defaults;
    }


    /**
     * Retourne
     */
    getDomain (): string {

        return this._domain;
    }


    /**
     * Retourne
     */
    getMethod (): string | string[] {

        return this._method;
    }


    /**
     * Retourne
     */
    getPattern (): string {

        return this._pattern;
    }


    /**
     * Retourne
     */
    getPatternRegExp (): RegExp {

        return this._patternRegExp;
    }


    /**
     * Retourne
     */
    getRequirements (): Record<string, string> {

        return this._requirements;
    }


    /**
     * Parse une méthode, une URI et un domaine pour déterminer si la route match
     * @param method Méthode
     * @param uri URI
     * @param domain Domaine
     * @returns `false` si ne match pas, un objet sinon : `{ route: this, vars: variables }`
     */
    match (
        method: string, uri: string, domain: string
    ): false | tRouteMatch {

        let match;

        if (this._methodRegExp.test(method) &&
            (match = this._patternRegExp.exec(uri)) &&
            this._domainRegExp.test(domain)) {

            match.shift();
            const vars = Utils.clone(this._defaults);
            match.forEach((val, index) => {
                if (val !== undefined)
                    vars[this._varsName[index]] = val;
            });

            return { route: this, vars: vars };
        }

        return false;
    }


    /**
     * Reconstruit une URL à partir de paramètres
     *
     * @param params Paramètres
     * @param domain Domaine courant (permettra de déterminer s'il y a besoin d'une URL absolue ou non
     * @param https L'URL absolue est-elle en HTTPS ?
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    unroute (params: Record<string, any>, domain = '', https = false): string {


        let url = this._pattern;

        // Corps de l'URL
        params = Utils.extend({}, this._defaults, params);
        Utils.forOwn(params, (val, name) => {

            if (/\(/.test(this._requirements[name]))
                val = this._requirements[name].replace(/\([^()]+\)/, val);

            url = url.replace('{?' + name + '}', val);
            url = url.replace('{' + name + '}', val);
        });

        // URL absolue ?
        if (domain && !this._domainRegExp.exec(domain)) {

            const extMatch = /\.(\w*)(:\d+)?$/.exec(domain);

            const ext = extMatch ? '.' + extMatch[1] + (extMatch[2] ? extMatch[2] : '') : '';

            domain = 'http' + (https ? 's' : '') + '://' + this._domain.replace('.*', ext);
        }
        else
            domain = '';


        return domain + url;
    }


    /**
     * Insert la RegExp d'une variable
     * @param name Nom de la variable
     * @param facultatif Facultatif ou non comme variable
     * @param source Source
     */
    protected _insertVar (name: string, facultatif: boolean, source: string): void {

        this._varsName.push(name);

        const requirement = this._requirements[name].replace(
            '[uuid]',
            Utils.uuidRegExp().toString().slice(2, -3)
        );

        this._patternRegExp = new RegExp(this._patternRegExp.toString().slice(1, -1).replace(
            source,
            (/\(/.test(requirement) ? '(?:' : '(')
            + requirement + (facultatif ? ')?' : ')'))
        );
    }
}
