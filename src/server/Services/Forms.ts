/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as Utils from '../../cli/Utils';

import { ButtonField } from '../Form/ButtonField';
import { CheckboxField } from '../Form/CheckboxField';
import { ColorField } from '../Form/ColorField';
import { DateField } from '../Form/DateField';
import { EmailField } from '../Form/EmailField';
import { HiddenField } from '../Form/HiddenField';
import { HtmlField } from '../Form/HtmlField';
import { NumberField } from '../Form/NumberField';
import { PasswordField } from '../Form/PasswordField';
import { RadioField } from '../Form/RadioField';
import { RangeField } from '../Form/RangeField';
import { ResetField } from '../Form/ResetField';
import { SearchField } from '../Form/SearchField';
import { SelectField } from '../Form/SelectField';
import { StringField } from '../Form/StringField';
import { SubmitField } from '../Form/SubmitField';
import { TelField } from '../Form/TelField';
import { TextField } from '../Form/TextField';
import { TimeField } from '../Form/TimeField';
import { UrlField } from '../Form/UrlField';

import { Entity } from '../../core/Entity';
import { Field } from '../Form/Field';
import { Form } from '../Form/Form';
import { FormBuilder } from '../Form/FormBuilder';
import { Validate } from '../../core/Services/Validate';

/**
 * Gère les formBuilder : ce service permet d'initialiser et de traiter facilement un formulaire
 */
export class Forms {

    public $validate: Validate;

    protected _class: Record<string, string[]> = {
        field: [],
        label: [],
        group: [],
        groupSuccess: [],
        groupError: [],
        msg: [],
        msgHelp: [],
        msgError: [],
        fieldset: []
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected _fields: Record<string, any> = {};
    protected _formBuilders: Record<string, typeof FormBuilder> = {};


    /**
     * Initialise le service
     * @param validate Service $validate
     */
    constructor (validate: Validate) {

        this.$validate = validate;

        this.registerField('button', ButtonField);
        this.registerField('checkbox', CheckboxField);
        this.registerField('color', ColorField);
        this.registerField('date', DateField);
        this.registerField('email', EmailField);
        this.registerField('hidden', HiddenField);
        this.registerField('html', HtmlField);
        this.registerField('number', NumberField);
        this.registerField('password', PasswordField);
        this.registerField('radio', RadioField);
        this.registerField('range', RangeField);
        this.registerField('reset', ResetField);
        this.registerField('search', SearchField);
        this.registerField('select', SelectField);
        this.registerField('string', StringField);
        this.registerField('submit', SubmitField);
        this.registerField('tel', TelField);
        this.registerField('text', TextField);
        this.registerField('time', TimeField);
        this.registerField('url', UrlField);
    }


    /**
     * Enregistre un formBuilder (écrasera un précédemment définit)
     * @param id Identifiant du FormBuilder
     * @param formBuilder FormBuilder à enregistrer
     * @returns Pour le chaînage
     */
    register (id: string, formBuilder: typeof FormBuilder): this {

        this._formBuilders[id] = formBuilder;
        return this;
    }


    /**
     * Retourne tous les FormBuilders
     */
    getAllFormBuilders (): Record<string, typeof FormBuilder>  {

        return this._formBuilders;
    }


    /**
     * Construit et retourne un formulaire
     * @param id Identifiant du formBuilder à utiliser
     * @param entity Entité à transmettre
     * @param config Configuration que l'on peut transmettre au form
     */
    build (
        id: string,
        entity: Entity | null = null,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        config: Record<string, any> | null = null
    ): Form {

        const form = new (this._formBuilders[id])(entity, this);

        form.getForm().addClass(this._class);

        form.build(config);

        return form.getForm();
    }


    /**
     * Indique si un FormBuilder est enregistré ou nom
     * @param id Identifiant du FormBuilder
     */
    has (id: string): boolean {

        return Object.keys(this._formBuilders).indexOf(id) > -1;
    }


    /**
     * Supprime un FormBuilder enegistré
     * @param id Identifiant du FormBuilder à retirer
     */
    delete (id: string): void {

        delete this._formBuilders[id];
    }


    /**
     * Enregistre un fiedl (écrasera un précédemment définit)
     * @param id Identifiant du field
     * @param field Field à enregistrer
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    registerField (id: string, field: typeof Field): void {

        this._fields[id] = field;
    }


    /**
     * Retourne un field
     * @param id Identifiant du field à retourner
     * @throws `Error` si le field n'est pas enregistré
     */
    getField (id: string): typeof Field {

        if (!this.hasField(id))
            throw new Error('Le type de Field `' + id + '` n\'est pas enregistré');
        return this._fields[id];
    }


    /**
     * Indique si un field est enregistré ou nom
     * @param id Identifiant du field
     */
    hasField (id: string): boolean {

        return Object.keys(this._fields).indexOf(id) > -1;
    }


    /**
     * Supprime un field enegistré
     * @param id Identifiant du field à retirer
     */
    deleteField (id: string): void {

        delete this._fields[id];
    }


    /**
     * Ajoute automatiquement des classes à tous les champs ajoutés au formulaire.
     *
     * Cf. `Field.addClass()`
     *
     * @param cssObj Les classes à ajouter
     */
    addClass (cssObj: Record<string, string[] | string>): this {

        if (Utils.isObject(cssObj)) {

            Utils.forEach(cssObj, (css, type) => {

                if (Array.isArray(this._class[type])) {

                    if (Utils.isString(css))
                        this._class[type].push(css);
                    else if (Array.isArray(css))
                        this._class[type] = this._class[type].concat(css);
                }
            });
        }

        return this;
    }
}
