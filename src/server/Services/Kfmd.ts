/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Renderer, RendererInterface } from './Renderer';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Token = Record<string, any>;
type LexerOuput = {
    tokens: Token[];
    toc: { type: number; content: string }[];
    footNotes: string[];
};

/**
 * Ce service permet d'implémenter une version inspirée de la syntaxe Markdown :
 * Kepha Flavored Markdown.
 *
 * Pour l'utiliser, il suffit d'initialiser le service et de faire appel à la méthode
 * `render(texte à parser)` qui transformera la syntaxe en code HTML valide.
 *
 * Le code est très très largement inspiré de *marked* :
 *
 *     https://github.com/chjj/marked Christopher Jeffrey (MIT Licensed)
 *
 * Nous l'avons transformé en service compatible pour notre framework, supprimé des options et
 * complété un peu pour que tout corresponde à nos besoins.
 *
 * Le rendu permet de récupérer plusieurs choses :
 *
 *  - le code HTML correspondant
 *  - la table des matière (ou toc) : un `Array` contenant une serie d'objet
 *      `{type: 'hx', content: '...'}` avec `hx` entre `h1` et `h6`
 *  - la liste des notes : un `Array` contenant le texte de chaque note (le numéro correspond à son
 *      ordre d'arrivée.
 */
export class Kfmd {

    protected _block: Record<string, RegExp | string> = {};
    protected _inline: Record<string, RegExp | string> = {};
    protected _renderer: RendererInterface;
    protected _safeHtml: {
        block: string;
        inline: string;
        regExpSource: string;
        regExp?: RegExp;
    } = {
        block: 'h1|h2|h3|h4|h5|h6|ol|ul|li|table|thead|tbody|tr|th|td'
            + '|figure|caption|figcaption|div|blockquote|footer|iframe'
            + '|dl|dt|dd|p',
        inline: 'a|em|strong|small|s(?= |>)|cite|q|dfn|abbr|data|time|code'
            + '|var|samp|kbd|sub|sup|i|b|u|mark|ruby|rt|rp|bdi|bdo'
            + '|span|br|wbr|ins|del|img',
        regExpSource: '<(?!/?(?:block|inline))(.*?)>'
    };

    /**
     * Initialise le service
     */
    constructor () {

        this._renderer = new Renderer();
        this._initSafeHtml();
    }


    /**
     * Définit le moteur de rendu
     * @param renderer Le moteur
     */
    setRenderer (renderer: RendererInterface): void {

        this._renderer = renderer;
    }


    /**
     * Ajout d'un élément inline au safeHtml
     * @param element Élément à ajouter
     */
    addSafeInline (element: string): void {

        if (this._safeHtml.inline.indexOf('|' + element + '|') < 0) {

            this._safeHtml.inline += '|' + element;
            this._initSafeHtml();
        }
    }


    /**
     * Retrait d'un élément inline au safeHtml
     * @param element Élément à retirer
     */
    removeSafeInline (element: string): void {

        this._safeHtml.inline = this._safeHtml.inline
            .replace('|' + element + '|', '');
        this._initSafeHtml();
    }


    /**
     * Ajout d'un élément block au safeHtml
     * @param element Élément à ajouter
     */
    addSafeBlock (element: string): void {

        if (this._safeHtml.block.indexOf('|' + element + '|') < 0) {

            this._safeHtml.block += '|' + element;
            this._initSafeHtml();
        }
    }


    /**
     * Retrait d'un élément block au safeHtml
     * @param element Élément à retirer
     */
    removeSafeBlock (element: string): void {

        this._safeHtml.block = this._safeHtml.block
            .replace('|' + element + '|', '');
        this._initSafeHtml();
    }


    /**
     * Parse un texte et renvoit un objet contenant les éléments issus de la
     * transformation :
     *    results = {
     *      source: 'texte injecté au départ',
     *      contentHtml: texte parsé,
     *      toc: ['éléments de la table des matières'],
     *      footNotes: ['notes de bas de page extraites du document']
     *    } ;
     *
     * @param {String} text Texte à parser
     * @returns {Object}
     */
    render (
        text: string
    ): {
        source: string;
        contentHtml: string;
        toc: { type: number; content: string }[];
        footNotes: string[];
    } {

        let parsed = text;

        // Préprocesseur
        text = text
            .replace(/\r\n|\r/g, '\n')
            .replace(/\t/g, '    ')
            .replace(/\u00a0/g, ' ')
            .replace(/\u2424/g, '\n');

        // Parsage
        const lexer = this._lexer(text, true);
        parsed = this._parse(lexer);

        return {
            source: text,
            contentHtml: parsed,
            toc: lexer.toc,
            footNotes: lexer.footNotes
        };
    }


    /**
     * Initialise le safeHtml, ainsi que les syntaxe en fonction des éléments
     * autorisés ou interdits
     */
    protected _initSafeHtml (): void {

        const regExp = this._safeHtml.regExpSource
            .replace('block', this._safeHtml.block)
            .replace('inline', this._safeHtml.inline);
        this._safeHtml.regExp = new RegExp(regExp, 'gm');

        // Initalisation des regExp
        this._initBlock();
        this._initInline();
    }


    /**
     * Initialise les syntaxes de blocks
     */
    protected _initBlock (): void {

        // Grammaire
        const block = {
            newline: /^\n+/,
            code: /^( {4}[^\n]+\n*)+/,
            fences: /^ *(`{3,}|~{3,})[ .]*(\S+)? *\n([\s\S]*?)\n? *\1 *(?:\n+|$)/,
            heading: /^ *(#{1,6}) +([^\n]+?) *#* *(?:\n+|$)/,
            blockquote: /^( {0,3}> ?(paragraph|[^\n]*)(?:\n|$))+/,
            list: /^( *)(bull) [\s\S]+?(?:\n{2,}(?! )(?!\1bull )\n*|\s*$)/,
            taskList: /^( *)(bull) \[x?\] [\s\S]+?(?:\n{2,}(?! )(?!\1bull \[x?\] )\n*|\s*$)/,
            html: /^ *(?:comment *(?:\n|\s*$)|closed *(?:\n{2,}|\s*$)|closing *(?:\n{2,}|\s*$))/,
            lheading: /^([^\n]+)\n *(=|-){2,} *(?:\n+|$)/,
            paragraph: /^([^\n]+(?:\n?(?!heading|lheading| {0,3}>|tag)[^\n]+)+)/,
            text: /^[^\n]+/,
            bullet: /(?:[*+-]|\d+\.)/,
            item: /^( *)(bull) [^\n]*(?:\n(?!\1bull )[^\n]*)*/,
            taskItem: /^( *)(bull) \[x?\] [^\n]*(?:\n(?!\1bull \[x?\] )[^\n]*)*/,
            nptable: /^ *(\S.*\|.*)\n *([-:]+ *\|[-| :]*)\n((?:.*\|.*(?:\n|$))*)\n*/,
            table: /^ *\|(.+)\n *\|( *[-:]+[-| :]*)\n((?: *\|.*(?:\n|$))*)\n*/,
            _label: /(?:\\[[\]]|[^[\]])+/,
            _title: /(?:"(?:\\"|[^"]|"[^"\n]*")*"|'\n?(?:[^'\n]+\n?)*'|\([^()]*\))/,
            // safeHtml (éléments blocs autorisés : les autres seront placés dans un paragraphe)
            _tag: '(?=(?:' + this._safeHtml.block + ')\\b)\\w+(?!:|[^\\w\\s@]*@)\\b'
        };

        // On remplace des morceaux
        block.item = this._replace(block.item, 'gm')(/bull/g, block.bullet)();
        block.list = this._replace(block.list)(/bull/g, block.bullet)();
        block.taskItem = this._replace(block.taskItem, 'gm')(/bull/g, block.bullet)();
        block.taskList = this._replace(block.taskList)(/bull/g, block.bullet)();
        block.html = this._replace(block.html)('comment', /<!--[\s\S]*?-->/)(
            'closed', /<(tag)[\s\S]+?<\/\1>/)('closing',
                // eslint-disable-next-line no-useless-escape
                /<tag(?:"[^"]*"|'[^']*'|\s[^'"\/>]*)*?\/?>/)(/tag/g, block._tag)();
        block.paragraph = this._replace(block.paragraph)('heading', block.heading)(
            'lheading', block.lheading)('tag', '<' + block._tag
        )();
        block.blockquote = this._replace(block.blockquote)('paragraph', block.paragraph)();

        this._block = block;
    }


    /**
     * Initialise les syntaxes inline
     */
    protected _initInline (): void {

        const inline: Record<string, RegExp | string> = {
            escape: /^\\([\\`*{}[\]()#+\-.!_>])/,
            // eslint-disable-next-line no-useless-escape
            tag: /^<!--[\s\S]*?-->|^<\/?_tag(?:"[^"]*"|'[^']*'|\s[^<\'">\/]*)*?\/?>/,
            link: /^!?\[(inside)\]\(href\)/,
            nolink: /^!?\[((?:\[[^\]]*\]|\\[[\]]|[^[\]])*)\]/,
            strong: /^__([\s\S]+?)__(?!_)|^\*\*([\s\S]+?)\*\*(?!\*)/,
            em: /^_([^\s_](?:[^_]|__)+?[^\s_])_\b|^\*((?:\*\*|[^*])+?)\*(?!\*)/,
            code: /^(`+)(\s*)([\s\S]*?[^`]?)\2\1(?!`)/,
            br: /^ {2,}\n(?!\s*$)/,
            text: /^[\s\S]+?(?=[\\<![`*]|\b_| {2,}\n|$)/,
            footNote: /\(\(((?:.|\n)+?)\)\)/gm
        };

        // safeHtml (éléments inline autorisés, les autres seront échapés)
        inline._tag = '(?:' + this._safeHtml.inline + ')';
        inline.tag = this._replace(inline.tag as RegExp)('_tag', inline._tag)();

        inline._scheme = /[a-zA-Z][a-zA-Z0-9+.-]{1,31}/;
        inline._email = new RegExp('[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+(@)'
            + '[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?'
            + '(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+(?![-_])');

        inline._inside = /(?:\[[^\]]*\]|\\[[\]]|[^[\]]|\](?=[^[]*\]))*/;
        inline._href = /\s*<?([\s\S]*?)>?(?:\s+['"]([\s\S]*?)['"])?\s*/;

        inline.link = this._replace(inline.link as RegExp)(
            'inside', inline._inside)('href', inline._href)();

        inline.escape = this._replace(inline.escape as RegExp)('])', '~|])')();
        inline.url = this._replace(
            /^((?:ftp|https?):\/\/|www\.)(?:[a-zA-Z0-9-]+\.?)+[^\s<]*|^email/
        )('email', inline._email)();
        inline._backpedal =
            /(?:[^?!.,:;*_~()&]+|\([^)]*\)|&(?![a-zA-Z0-9]+;$)|[?!.,:;*_~)]+(?!$))+/;
        inline.del = /^~~(?=\S)([\s\S]*?\S)~~/;
        inline.text = this._replace(inline.text as RegExp)(']|', '~]|')('|',
            '|https?://|ftp://|www\\.|[a-zA-Z0-9.!#$%&\'*+/=?^_`{\\|}~-]+@|')();

        this._inline = inline;
    }


    /**
     * Tekennise le texe et retourne les tokens
     * @param text Texte à tokenniser
     * @param {top Indique si on est au premier niveau
     */
    protected _lexer (text: string, top: boolean): LexerOuput {

        let tokens: Token[] = [];
        let cap: RegExpExecArray | null;

        const toc: { type: number; content: string }[] = [];

        // Préprocesseur
        text = text.replace(/^ +$/gm, '');

        // Lexing
        while (text) {

            // newline
            cap = (this._block.newline as RegExp).exec(text);
            if (cap) {

                text = text.substring(cap[0].length);
                if (cap[0].length > 1)
                    tokens.push({ type: 'space' });
            }

            // code
            cap = (this._block.code as RegExp).exec(text);
            if (cap) {

                text = text.substring(cap[0].length);
                const capture = cap[0].replace(/^ {4}/gm, '');
                tokens.push({
                    type: 'code',
                    text: capture.replace(/n+$/, '')
                });
                continue;
            }

            // fences (code ave ```)
            cap = (this._block.fences as RegExp).exec(text);
            if (cap) {

                text = text.substring(cap[0].length);
                tokens.push({
                    type: 'code',
                    lang: cap[2],
                    text: cap[3] || ''
                });
                continue;
            }

            // heading
            cap = (this._block.heading as RegExp).exec(text);
            if (cap) {

                text = text.substring(cap[0].length);
                tokens.push({
                    type: 'heading',
                    depth: cap[1].length,
                    text: cap[2]
                });
                toc.push({ type: cap[1].length, content: cap[2] });
                continue;
            }

            // blockquote
            cap = (this._block.blockquote as RegExp).exec(text);
            if (cap) {

                text = text.substring(cap[0].length);

                tokens.push({
                    type: 'blockquote_start'
                });

                const capture = cap[0].replace(/^ *> ?/gm, '');

                // Pass `top` to keep the current "toplevel" state. This is exactly how markdown.pl works.
                tokens = tokens.concat(this._lexer(capture, top).tokens);

                tokens.push({
                    type: 'blockquote_end'
                });

                continue;
            }

            // task list
            cap = (this._block.taskList as RegExp).exec(text);
            if (cap) {

                text = text.substring(cap[0].length);
                const bull = cap[2];

                tokens.push({
                    type: 'taskList_start',
                    ordered: bull.length > 1
                });

                // Get each top-level item.
                const capture = cap[0].match(this._block.taskItem) as RegExpMatchArray;

                let next = false;
                const l = capture.length;
                let i = 0;

                for (; i < l; i++) {

                    let item = capture[i];

                    // Remove the list item's bullet so it is seen as the next token.
                    let space = item.length;
                    const checked = !!/^ *([*+-]|\d+\.) \[x\] +/.exec(item);
                    item = item.replace(/^ *([*+-]|\d+\.) \[x?\] +/, '');

                    // Outdent whatever the list item contains. Hacky.
                    if (~item.indexOf('\n ')) {

                        space -= item.length;
                        item = item.replace(new RegExp('^ {1,' + space + '}', 'gm'), '');
                    }

                    // Determine whether the next list item belongs here.
                    // Backpedal if it does not belong in this list.
                    if (i !== l - 1) {

                        // eslint-disable-next-line max-len
                        const b = ((this._block.bullet as RegExp).exec(capture[i + 1]) as RegExpExecArray)[0];
                        if (bull !== b && !(bull.length > 1 && b.length > 1)) {
                            text = capture.slice(i + 1).join('\n') + text;
                            i = l - 1;
                        }
                    }

                    // Determine whether item is loose or not.
                    // Use: /(^|\n)(?! )[^\n]+\n\n(?!\s*$)/
                    // for discount behavior.
                    let loose = next || /\n\n(?!\s*$)/.test(item);
                    if (i !== l - 1) {

                        next = item.charAt(item.length - 1) === '\n';

                        if (!loose) loose = next;
                    }

                    tokens.push({
                        type: loose ? 'loose_taskItem_start' : 'taskList_item_start',
                        checked: checked
                    });

                    // Recurse.
                    tokens = tokens.concat(this._lexer(item, false).tokens);

                    tokens.push({ type: 'taskList_item_end' });
                }

                tokens.push({ type: 'taskList_end' });

                continue;
            }

            // list
            cap = (this._block.list as RegExp).exec(text);
            if (cap) {

                text = text.substring(cap[0].length);
                const bull = cap[2];

                tokens.push({
                    type: 'list_start',
                    ordered: bull.length > 1
                });

                // Get each top-level item.
                const capture = cap[0].match(this._block.item) as RegExpMatchArray;

                let next = false;
                const l = capture.length;
                let i = 0;

                for (; i < l; i++) {

                    let item = capture[i];

                    // Remove the list item's bullet
                    // so it is seen as the next token.
                    let space = item.length;
                    item = item.replace(/^ *([*+-]|\d+\.) +/, '');

                    // Outdent whatever the
                    // list item contains. Hacky.
                    if (~item.indexOf('\n ')) {

                        space -= item.length;
                        item = item.replace(new RegExp('^ {1,' + space + '}', 'gm'), '');
                    }

                    // Determine whether the next list item belongs here.
                    // Backpedal if it does not belong in this list.
                    if (i !== l - 1) {

                        // eslint-disable-next-line max-len
                        const b = ((this._block.bullet as RegExp).exec(capture[i + 1]) as RegExpExecArray)[0];
                        if (bull !== b && !(bull.length > 1 && b.length > 1)) {
                            text = capture.slice(i + 1).join('\n') + text;
                            i = l - 1;
                        }
                    }

                    // Determine whether item is loose or not.
                    // Use: /(^|\n)(?! )[^\n]+\n\n(?!\s*$)/
                    // for discount behavior.
                    let loose = next || /\n\n(?!\s*$)/.test(item);
                    if (i !== l - 1) {

                        next = item.charAt(item.length - 1) === '\n';

                        if (!loose) loose = next;
                    }

                    tokens.push({ type: loose ? 'loose_item_start' : 'list_item_start' });

                    // Recurse.
                    tokens = tokens.concat(this._lexer(item, false).tokens);

                    tokens.push({ type: 'list_item_end' });
                }

                tokens.push({ type: 'list_end' });

                continue;
            }

            // html
            cap = (this._block.html as RegExp).exec(text);
            if (cap) {

                text = text.substring(cap[0].length);
                tokens.push({
                    type: 'html',
                    text: cap[0]
                });
                continue;
            }

            // table
            if (top && (cap = (this._block.table as RegExp).exec(text))) {

                text = text.substring(cap[0].length);

                const item: Token = {
                    type: 'table',
                    header: cap[1].replace(/^ *| *\| *$/g, '').split(/ *\| */),
                    align: cap[2].replace(/^ *|\| *$/g, '').split(/ *\| */),
                    cells: cap[3].replace(/(?: *\| *)?\n$/, '').split('\n')
                };

                item.align.forEach((align: string, i: number) => {

                    if (/^ *-+: *$/.test(align))
                        item.align[i] = 'right';
                    else if (/^ *:-+: *$/.test(align))
                        item.align[i] = 'center';
                    else if (/^ *:-+ *$/.test(align))
                        item.align[i] = 'left';
                    else
                        item.align[i] = null;
                });

                item.cells.forEach((cell: string | string[], i: number) => {
                    item.cells[i] = (cell as string).replace(/^ *\| *| *\| *$/g, '')
                                                    .split(/ *\| */);
                });

                tokens.push(item);
                continue;
            }

            // lheading
            cap = (this._block.lheading as RegExp).exec(text);
            if (cap) {

                text = text.substring(cap[0].length);
                tokens.push({
                    type: 'heading',
                    depth: cap[2] === '=' ? 1 : 2,
                    text: cap[1]
                });
                toc.push({ type: cap[2] === '=' ? 1 : 2, content: cap[1] });
                continue;
            }

            // text
            cap = (this._block.text as RegExp).exec(text);
            if (cap) {

                // Top-level should never reach here.
                text = text.substring(cap[0].length);
                tokens.push({
                    type: 'text',
                    text: cap[0]
                });
                continue;
            }

            if (text)
                throw new Error('Infinite loop on byte: ' + text.charCodeAt(0));
        }

        return { tokens, toc, footNotes: [] };
    }


    /**
     * Parse le texte à partir de sa tokenisation
     * @param lexerTokens Tokens
     */
    protected _parse (lexerTokens: LexerOuput): string {

        let token;

        lexerTokens.tokens = lexerTokens.tokens.reverse();

        let out = '';
        while ((token = lexerTokens.tokens.pop()) !== undefined)
            out += this._tok(token, lexerTokens);

        return out;
    }


    /**
     * Parse le `token` et génère son rendu.
     * @param token Token courant
     * @param lexerTokens Liste des tokens pas encore traités
     */
    protected _tok (token: Token, lexerTokens: LexerOuput): string {

        switch (token.type) {

            case 'space': {
                return '';
            }
            case 'heading': {
                return this._renderer.heading(
                    this._output(token.text as string, lexerTokens),
                    token.depth as number,
                    this._unescape(token.text as string));
            }
            case 'code': {
                return this._renderer.code(
                    this._escape(token.text as string, true),
                    token.lang as string
                );
            }
            case 'table': {
                let header = '',
                    body = '',
                    cell = '';

                // header
                token.header.forEach((th: string, i: number) => {

                    cell += this._renderer.tablecell(
                        this._output(th, lexerTokens),
                        { header: true, align: token.align[i] }
                    );
                });
                header += this._renderer.tablerow(cell);

                token.cells.forEach((row: string[]) => {

                    cell = '';

                    row.forEach((td, i) => {

                        cell += this._renderer.tablecell(
                            this._output(td, lexerTokens),
                            { header: false, align: token.align[i] }
                        );
                    });

                    body += this._renderer.tablerow(cell);
                });
                return this._renderer.table(header, body);
            }

            case 'blockquote_start': {
                let body = '';

                while ((token = (lexerTokens.tokens.pop()) as Token).type !== 'blockquote_end')
                    body += this._tok(token, lexerTokens);

                return this._renderer.blockquote(body);
            }
            case 'taskList_start': {
                let body = '';
                const ordered = token.ordered;

                while ((token = (lexerTokens.tokens.pop()) as Token).type !== 'taskList_end')
                    body += this._tok(token, lexerTokens);

                return this._renderer.taskList(body, ordered as boolean);
            }
            case 'taskList_item_start': {
                let body = '';
                const checked = token.checked;

                while ((token = (lexerTokens.tokens.pop()) as Token).type !== 'taskList_item_end') {

                    body += token.type === 'text' ?
                        this._parseText(token, lexerTokens)
                        : this._tok(token, lexerTokens);
                }

                return this._renderer.taskListItem(body, checked as boolean);
            }
            case 'loose_taskItem_start': {
                let body = '';
                const checked = token.checked;

                while ((token = (lexerTokens.tokens.pop()) as Token).type !== 'list_taskItem_end')
                    body += this._tok(token, lexerTokens);

                return this._renderer.taskListItem(body, checked as boolean);
            }
            case 'list_start': {
                let body = '';
                const ordered = token.ordered;

                while ((token = (lexerTokens.tokens.pop()) as Token).type !== 'list_end')
                    body += this._tok(token, lexerTokens);

                return this._renderer.list(body, ordered as boolean);
            }
            case 'list_item_start': {
                let body = '';

                while ((token = (lexerTokens.tokens.pop()) as Token).type !== 'list_item_end') {

                    body += token.type === 'text' ?
                        this._parseText(token, lexerTokens)
                        : this._tok(token, lexerTokens);
                }

                return this._renderer.listItem(body);
            }
            case 'loose_item_start': {
                let body = '';

                while ((token = (lexerTokens.tokens.pop()) as Token).type !== 'list_item_end')
                    body += this._tok(token, lexerTokens);

                return this._renderer.listItem(body);
            }
            case 'html': {
                token.text = (token.text as string)
                    .replace(this._safeHtml.regExp as RegExp, '&lt;$1&gt;')
                    .replace(/&(?!#?\w+;)/g, '&amp;');
                return this._renderer.html(token.text as string);
            }
            case 'text': {
                return this._renderer.paragraph(this._parseText(token, lexerTokens));
            }
        }
        return '';
    }


    /**
     * Parse un texte
     * @param token Token du texte
     * @param tokens Tokens non encore traités (pour avoir accès au suivant)
     */
    protected _parseText (token: Token, lexerTokens: LexerOuput): string {

        let body = token.text as string;

        while ((lexerTokens.tokens[lexerTokens.tokens.length - 1] || 0).type === 'text')
            body += '\n' + (lexerTokens.tokens.pop() as Token).text;

        return this._output(body, lexerTokens);
    }


    /**
     * Rendu d'un élément
     * @param src Élément à traiter
     * @param tokens Liste des tokens
     */
    protected _output (src: string, lexerTokens?: LexerOuput): string {

        let out = '',
            text,
            href,
            cap;

        if (!lexerTokens) lexerTokens = { tokens: [], toc: [], footNotes: [] };

        src = src.replace(this._inline.footNote, (m, $1) => {

            (lexerTokens as LexerOuput).footNotes.push(this._output($1));
            return this._renderer.footNote((lexerTokens as LexerOuput).footNotes.length);
        });

        while (src) {

            // escape
            cap = (this._inline.escape as RegExp).exec(src);
            if (cap) {
                src = src.substring(cap[0].length);
                out += cap[1];
                continue;
            }

            // url
            cap = (this._inline.url as RegExp).exec(src);
            if (cap) {
                cap[0] = ((this._inline._backpedal as RegExp).exec(cap[0]) as RegExpMatchArray)[0];
                src = src.substring(cap[0].length);
                if (cap[2] === '@') {
                    text = this._escape(cap[0]);
                    href = 'mailto:' + text;
                } else {
                    text = this._escape(cap[0]);
                    if (cap[1] === 'www.')
                        href = 'http://' + text;
                    else
                        href = text;
                }
                out += this._renderer.link(href, null, text);
                continue;
            }

            // tag
            cap = (this._inline.tag as RegExp).exec(src);
            if (cap) {
                src = src.substring(cap[0].length);
                out += cap[0];
                continue;
            }

            // link
            cap = (this._inline.link as RegExp).exec(src);
            if (cap) {
                src = src.substring(cap[0].length);
                out += this._outputLink(cap, {
                    href: cap[2],
                    title: cap[3]
                });
                continue;
            }

            // strong
            cap = (this._inline.strong as RegExp).exec(src);
            if (cap) {
                src = src.substring(cap[0].length);
                out += this._renderer.strong(this._output(cap[2] || cap[1], lexerTokens));
                continue;
            }

            // em
            cap = (this._inline.em as RegExp).exec(src);
            if (cap) {
                src = src.substring(cap[0].length);
                out += this._renderer.em(this._output(cap[2] || cap[1], lexerTokens));
                continue;
            }

            // code
            cap = (this._inline.code as RegExp).exec(src);
            if (cap) {
                src = src.substring(cap[0].length);
                out += this._renderer.codespan(this._escape(cap[3].trim(), true));
                continue;
            }

            // br
            cap = (this._inline.br as RegExp).exec(src);
            if (cap) {

                src = src.substring(cap[0].length);
                out += this._renderer.br();
                continue;
            }

            // del
            cap = (this._inline.del as RegExp).exec(src);
            if (cap) {
                src = src.substring(cap[0].length);
                out += this._renderer.del(this._output(cap[1]));
                continue;
            }

            // text
            cap = (this._inline.text as RegExp).exec(src);
            if (cap) {

                src = src.substring(cap[0].length);
                out += this._renderer.text(this._escape(this._smartypants(cap[0])));
                continue;
            }

            if (src)
                throw new Error('Infinite loop on byte: ' + src.charCodeAt(0));
        }
        return out;
    }


    /**
     * Rendu d'un lien
     * @param cap Expression capturée par la RegExp
     * @param link Élément du lien (href et title)
     */
    protected _outputLink (cap: RegExpExecArray, link: { href: string; title: string }): string {

        const href = this._escape(link.href),
              title = link.title ? this._escape(link.title) : null;

        return this._renderer.link(href, title, this._output(cap[1]));
    }


    /**
     * Remplace une partie d'une RegExp par une autre
     * @param regex RegExp à traiter
     * @param opt Chaîne que l'on remplace
     */
    protected _replace (regex: RegExp, opt?: string): Function {

        let regexSource = regex.source;
        opt = opt || '';

        return function self (name: string, val: string | RegExp): Function | RegExp {

            if (!name) return new RegExp(regexSource, opt);

            val = (val as RegExp).source || val;
            val = (val as string).replace(/(^|[^[])\^/g, '$1');
            regexSource = regexSource.replace(name, val);
            return self;
        };
    }


    /**
     * Quelques transformations cosmétiques
     * @param text Texte à traiter
     */
    protected _smartypants (text: string): string {

        return text
            // em-dashes
            .replace(/---/g, '\u2014')
            // en-dashes
            .replace(/--/g, '\u2013')
            // opening singles
            .replace(/(^|[-\u2014/([{"\s])'/g, '$1\u2018')
            // closing singles & apostrophes
            .replace(/'/g, '\u2019')
            // opening doubles
            .replace(/(^|[-\u2014/([{\u2018\s])"/g, '$1\u201c')
            // closing doubles
            .replace(/"/g, '\u201d')
            // ellipses
            .replace(/\.{3}/g, '\u2026');
    }


    /**
     * Protection de caractères
     * @param html HTML à échaper
     * @param encode Encoder ?
     */
    protected _escape (html: string, encode?: boolean): string {

        return html
            .replace(!encode ? /&(?!#?\w+;)/g : /&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;');
    }


    /**
     * unescape
     * @param html HTML
     */
    protected _unescape (html: string): string {

        // explicitly match decimal, hex, and named HTML entities
        return html.replace(/&(#(?:\d+)|(?:#x[0-9A-Fa-f]+)|(?:\w+));?/ig,
            (_, n) => {
                n = n.toLowerCase();
                if (n === 'colon') return ':';
                if (n.charAt(0) === '#') {
                    return n.charAt(1) === 'x' ?
                        String.fromCharCode(parseInt(n.substring(2), 16))
                        : String.fromCharCode(+n.substring(1));
                }
                return '';
            }
        );
    }
}
