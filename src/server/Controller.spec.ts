/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as http from 'http';
import { Socket } from 'net';

import { Controller } from './Controller';
import { Kernel } from '../core/Kernel';
import { Resp } from './Resp';
import { User } from './User/User';


describe('server/Controller', () => {

    let controller: Controller,
        kernel: Kernel,
        request: http.IncomingMessage,
        response: Resp;

    beforeEach(() => {

        request = new http.IncomingMessage(new Socket()),
        response = new Resp(new http.ServerResponse(request), request);
        kernel = new Kernel(true);
        controller = new Controller(kernel, request, response);
    });

    it('has Kernel access', () => {

        expect(controller.kernel()).to.be.equal(kernel);
    });

    it('can return a service', () => {

        const myService = {};
        kernel.container().register('myService', myService);
        expect(controller.get('myService')).to.be.equal(myService);
    });

    it('has access to the request and the response', () => {

        expect(controller.$request).to.be.equal(request);
        expect(controller.$response).to.be.equal(response);
    });

    it('if user is not transmitted, create a new', () => {

        expect(controller.$user).to.be.instanceOf(User);
        expect(controller.getUserInit()).to.be.instanceOf(Promise);
    });

    it('récupère la configuration de user dans la config générale', () => {

        kernel.get('$config').set('user', { sessionCookieName: 'hello' });
        controller = new Controller(kernel, request, response);
        kernel.get('$config').delete('user');

        expect(controller.$user.getConfig().sessionCookieName).to.be.equal('hello');
    });

    it('user peut être récupéré à partir d\'un objet particulier', () => {

        class MyObject extends User { }
        kernel.get('$config').set('$userObj', MyObject);
        controller = new Controller(kernel, request, response);
        kernel.get('$config').delete('$userObj');

        expect(controller.$user).to.be.instanceOf(MyObject);
    });

    it('l\'init d\'User a été appelé', () => {

        class MyObject extends User {
            public foo = '';
            init (): void {
                this.foo = 'bar';
            }
        }
        kernel.get('$config').set('$userObj', MyObject);
        controller = new Controller(kernel, request, response);
        kernel.get('$config').delete('$userObj');

        expect((controller.$user as MyObject).foo).to.be.equal('bar');
    });
});
