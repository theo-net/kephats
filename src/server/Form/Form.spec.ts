/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Block } from './Block';
import { Entity } from '../../core/Entity';
import { Field } from './Field';

import { Form } from './Form';
import { Forms } from '../Services/Forms';
import { Validate as ValidateS} from '../../core/Services/Validate';


describe('server/Form/Form', () => {

    it('étend Block', () => {

        const form = new Form();
        expect(form).to.be.instanceOf(Block);
    });

    it('setParent(), getParent() Retourneront tjs le formulaire', () => {

        const form = new Form(),
              formB = new Form();
        expect(form.setParent(formB)).to.be.equal(form);
        expect(form.getParent()).to.be.equal(form);
    });


    describe('build', () => {

        let form: Form;

        beforeEach(() => {

            form = new Form(null, new Forms(new ValidateS()));
        });

        it('retourne le code HTML du formulaire', () => {

            expect(form.build())
                .to.be.equal('<form action="#" method="get">\n</form>');
            form.setId('salut');
            expect(form.build())
                .to.be.equal('<form action="#" method="get" id="salut">\n</form>');
        });

        it('on peut préciser l\'action et la méthode', () => {

            expect(form.setAction('action.html', 'post')).to.be.equal(form);
            expect(form.build())
                .to.be.equal('<form action="action.html" method="post">\n</form>');
            form.setAction('act.html');
            expect(form.build())
                .to.be.equal('<form action="act.html" method="get">\n</form>');
        });

        it('construit tous les sous-éléments', () => {

            form
                .add('string', { name: 'foo', label: 'Un champ' })
                .add('string', { name: 'bar', label: 'Un autre champ' })
                .setAction('action.html', 'post');
            expect(form.build()).to.be.equal(
                '<form action="action.html" method="post">\n'
                + '  <div>\n'
                + '    <label for="foo">Un champ</label>\n'
                + '    <input id="foo" name="foo" type="text"><i></i>\n'
                + '      </div>\n'
                + '  <div>\n'
                + '    <label for="bar">Un autre champ</label>\n'
                + '    <input id="bar" name="bar" type="text"><i></i>\n'
                + '      </div>\n'
                + '</form>'
            );
        });
    });


    describe('process', () => {

        let form: Form, formB: Form, entity: Entity;

        beforeEach(() => {

            class MyEntity extends Entity {
                _setProperties (): void {

                    this._setProperty('nom', null);
                    this._setProperty('mail', null);
                    this._setProperty('description', null);
                }
            }
            entity = new MyEntity();
            entity.nom = 'a';
            entity.mail = 'b';
            entity.description = '';

            form = new Form(null, new Forms(new ValidateS()));
            form.addFieldset('Auteur')
                .add('string', { name: 'nom', required: true }, { string: { min: 3 } })
                .add('string', { name: 'mail', required: true }, 'mail')
                .getParent()
                .add('string', { name: 'description', required: true });

            formB = new Form(entity, new Forms(new ValidateS()));
            formB.addFieldset('Auteur')
                .add('string', { name: 'nom', required: true }, { string: { min: 3 } })
                .add('string', { name: 'mail', required: true }, 'mail')
                .getParent()
                .add('string', { name: 'description', required: true });
        });

        it('rempli tous les champs', () => {

            const req = {
                nom: 'Jésus',
                mail: 'jesus@cieux.god',
                description: 'Fils de Dieu'
            };
            form.process(req);

            expect(((form.getAllFields()[0] as Block).getAllFields()[0] as Field).getValue())
                .to.be.equal('Jésus');
            expect(((form.getAllFields()[0] as Block).getAllFields()[1] as Field).getValue())
                .to.be.equal('jesus@cieux.god');
            expect((form.getAllFields()[1] as Field).getValue()).to.be.equal('Fils de Dieu');
        });

        it('retourne la requête si tout est valide (pas d\'entity)', () => {

            const req = {
                nom: 'Jésus',
                mail: 'jesus@cieux.god',
                description: 'Fils de Dieu'
            };
            expect(form.process(req)).to.be.equal(req);
        });

        it('Si une entité est présente, la retourne mise-à-jour', () => {

            const req = {
                nom: 'Jésus',
                mail: 'jesus@cieux.god',
                description: 'Fils de Dieu'
            };
            const ret = formB.process(req);

            expect(ret).to.be.equal(entity);
            expect(ret?.getValues()).to.be.deep.equal(req);
        });

        it('Si une entité est présente, mais req invalide, non maj', () => {

            const req = {
                nom: 'Jésus',
                mail: 'jesuscieux.god',
                description: 'Fils de Dieu'
            };
            const ret = formB.process(req);

            expect(ret).to.be.equal(null);
            expect(formB.getEntity()?.getValues()).to.be.deep.equal({
                nom: 'a', mail: 'b', description: ''
            });
        });

        it('retourne null sinon', () => {

            const req1 = {
                nom: 'Jésus',
                mail: 'jesus@cieux.god',
                description: 'foobar'
            };
            const req2 = {
                nom: 'Jésus',
                mail: 'jesuscieux.god',
                description: 'foobar'
            };
            const req3 = {
                nom: 'Jésus',
                mail: 'jesus@cieux.god'
            };
            expect(form.process(req1)).to.be.equal(req1);
            expect(form.process(req2)).to.be.equal(null);
            expect(formB.process(req3)).to.be.equal(null);
        });

        it('tous les champs sont bien parcourus', () => {

            form.process({
                nom: 'Jé',
                mail: 'jesus@cieux.god',
            });

            expect(((form.getAllFields()[0] as Block).getAllFields()[0] as Field).isSubmit())
                .to.be.true;
            expect(((form.getAllFields()[0] as Block).getAllFields()[1] as Field).isSubmit())
                .to.be.true;
            expect((form.getAllFields()[1] as Field).isSubmit()).to.be.true;
        });
    });
});
