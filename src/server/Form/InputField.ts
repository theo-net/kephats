/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Field } from './Field';
import { ValidateValidator } from '../../core/Validate/Validate';

export class InputField extends Field {

    protected _list: string[] | null = null;
    protected _patternAttr: string | null = null;
    protected _placeholder: string | null = null;
    protected _size: string | null = null;
    protected _spellcheck: string | null = null;
    protected _type = 'text';

    /**
     * Nouveaux attributs disponibles :
     *
     *  - `[placeholder]` : Placeholder du champ
     *  - `[size]` : taille en caractère du champ (juste pour l'affichage)
     *  - `[spellcheck]` : correction orthographique (`true`, `false`, `default`)
     *  - `[list]` : liste des valeurs que pourra prendre le champ (ajoute le validateur correspondant)
     *  - `[pattern]` : pattern que doit respecter le champ (ajoute le validateur correspondant)
     *
     * Valeurs par défaut :
     *  - type : text
     */
    init (): void {

        this._placeholder = this._attr.placeholder ? this._attr.placeholder : null;
        this._size = this._attr.size ? this._attr.size : null;
        this._spellcheck = this._attr.spellcheck != undefined ? this._attr.spellcheck : null;

        if (Array.isArray(this._attr.list)) {

            this._list = this._attr.list;
            (this._validator as ValidateValidator).add(this._list);
        }
        else
            this._list = null;
    }


    /**
     * Étend la méthode de Field en exécutant un `trim` sur la valeur pour  supprimer les espaces
     * en début et fin de chaîne de caractères.
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    setValue (value: any): void {

        if (value)
            value = value.trim();
        super.setValue(value);
    }


    build (): string {

        let widget = '  <div'
            + (this.getGroupClass() != '' ?
                ' class="' + this.getGroupClass() + '"' : '')
            + '>\n';

        widget += this._buildLabel();
        widget += '<input' + this._buildAttributes()
            + '><i></i>\n    ' + this._buildMsg() + this._buildList() + '</div>';

        return widget;
    }


    /**
     * Construit les attributs du champ input
     */
    protected _buildAttributes (): string {

        return (this.getClass() != '' ? ' class="' + this.getClass() + '"' : '')
            + ' id="' + this._id + '" name="' + this._name
            + '" type="' + this._type + '"'
            + (this._valueAttr ? ' value="' + this._valueAttr + '"' : '')
            + (this._placeholder ? ' placeholder="' + this._placeholder + '"' : '')
            + (this._required ? ' required' : '')
            + (this._disabled ? ' disabled' : '')
            + (this._size ? ' size="' + this._size + '"' : '')
            + (this._spellcheck !== null ? (' spellcheck="'
                + (this._spellcheck == 'default' ? 'default' : (
                    (this._spellcheck ? 'true' : 'false')
                )) + '"'
            ) : '')
            + (this._list ? ' list="' + this._id + 'List"' : '')
            + (this._patternAttr ? ' pattern="' + this._patternAttr + '"' : '');
    }


    /**
     * Construit une datalist à partir de l'attribut list
     */
    protected _buildList (): string {

        if (this._list !== null) {

            let list = '    <datalist id="' + this._id + 'List">\n    ';
            this._list.forEach(elmt => {
                list += '  <option value="' + elmt + '">\n    ';
            });
            return list + '</datalist>';
        }
        else
            return '';
    }
}
