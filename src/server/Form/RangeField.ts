/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NumberField } from './NumberField';

export class RangeField extends NumberField {

    init (): void {

        super.init();
        this._type = 'range';
    }
}
