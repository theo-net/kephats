/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { StringValidator } from '../../core/Validate/String';
import { TextField } from './TextField';
import { Validate as ValidateS} from '../../core/Services/Validate';

describe('server/Form/TextField', () => {

    it('on peut préciser cols et rows', () => {

        const text = new TextField({ name: 'foo', cols: 10, rows: 5 }, undefined, new ValidateS());
        text.init();
        expect(/cols="10" rows="5"/.test(text.build())).to.be.true;
    });

    it('peut avoir un placeholder', () => {

        const text = new TextField({ name: 'foo', placeholder: 'bar' }, undefined, new ValidateS());
        text.init();
        expect(/placeholder="bar"/.test(text.build())).to.be.true;
    });

    it('peut avoir spellcheck', () => {

        const text1 = new TextField({ name: 'foo', spellcheck: true }, undefined, new ValidateS()),
              text2 = new TextField({ name: 'foo', spellcheck: false }, undefined, new ValidateS()),
              text3 = new TextField(
                  { name: 'foo', spellcheck: 'default' }, undefined, new ValidateS()
              );
        text1.init();
        text2.init();
        text3.init();

        expect(/spellcheck="true"/.test(text1.build())).to.be.true;
        expect(/spellcheck="false"/.test(text2.build())).to.be.true;
        expect(/spellcheck="default"/.test(text3.build())).to.be.true;
    });

    it('peut avoir wrap', () => {

        const text1 = new TextField({ name: 'foo', wrap: 'hard' }, undefined, new ValidateS()),
              text2 = new TextField({ name: 'foo', wrap: 'soft' }, undefined, new ValidateS()),
              text3 = new TextField({ name: 'foo', wrap: 'off' }, undefined, new ValidateS());
        text1.init();
        text2.init();
        text3.init();

        expect(/wrap="hard"/.test(text1.build())).to.be.true;
        expect(/wrap="soft"/.test(text2.build())).to.be.true;
        expect(/wrap="off"/.test(text3.build())).to.be.true;
    });

    it('validator String', () => {

        const text = new TextField({ name: 'foo' }, undefined, new ValidateS());
        text.init();
        expect(text.getValidator()?.getValidators()[1])
            .to.be.instanceOf(StringValidator);
    });

    it('attribut min -> minlength', () => {

        const text = new TextField({ name: 'foo', min: 1 }, undefined, new ValidateS());
        text.init();

        expect(/minlength="1"/.test(text.build())).to.be.true;
        expect(text.getValidator()?.getValidators()[1].getArguments().min)
            .to.be.equal(1);
    });

    it('attribut max -> maxlength', () => {

        const text = new TextField({ name: 'foo', max: 2 }, undefined, new ValidateS());
        text.init();

        expect(/maxlength="2"/.test(text.build())).to.be.true;
        expect(text.getValidator()?.getValidators()[1].getArguments().max)
            .to.be.equal(2);
    });

    it('build', () => {

        const text = new TextField({ name: 'foo', help: 'a' }, undefined, new ValidateS());
        text.init();

        expect(text.build()).to.be.equal(
            '  <div>\n    '
            + '<textarea id="foo" name="foo"></textarea><i></i>\n    '
            + '<p>a</p>\n  '
            + '</div>'
        );
    });
});
