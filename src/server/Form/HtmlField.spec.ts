/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { HtmlField } from './HtmlField';

describe('server/Form/HtmlField', () => {

    it('Lors de la construction retourne le code HTML', () => {

        const input = new HtmlField({ name: 'foo', html: 'bar' });
        input.init();
        expect(input.build()).to.be.equal('bar');
    });

    it('Par défaut, renvoit rien', () => {

        const input = new HtmlField({ name: 'foo' });
        input.init();
        expect(input.build()).to.be.equal('');
    });
});

