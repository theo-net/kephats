/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { StringField } from './StringField';
import { ValidateValidator } from '../../core/Validate/Validate';

export class EmailField extends StringField {

    init (): void {

        super.init();

        this._type = 'email';
        if (this._placeholder === null)
            this._placeholder = 'exemple@domain.com';
        (this._validator as ValidateValidator).add('mail');
    }
}
