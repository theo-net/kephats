/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Field } from './Field';
import { Validate } from '../../core/Services/Validate';
import { ValidateValidator } from '../../core/Validate/Validate';

describe('server/Form/Field', () => {

    it('sauve les attributs passés en paramètres', () => {

        const attr = { name: 'a', truc: 'b' };
        const field = new Field(attr);

        expect(field.getAttr()).to.be.equal(attr);
    });

    it('enregistre l\'identifiant du field', () => {

        const field = new Field({ name: 'bar' });
        expect(field.getName()).to.be.equal('bar');
    });

    it('l\'identifiant sera le `name` et l\'`id`', () => {

        const field = new Field({ name: 'bar' });
        expect(field.getId()).to.be.equal('bar');
    });

    it('mais on peut forcer la valeur de l\'id', () => {

        const field = new Field({ name: 'foo', id: 'bar' });
        expect(field.getId()).to.be.equal('bar');
    });

    it('on peut indiquer un bind', () => {

        const field1 = new Field({ name: 'foo1', bind: 'bar' }),
              field2 = new Field({ name: 'foo2' });
        expect(field1.getBind()).to.be.equal('bar');
        expect(field2.getBind()).to.be.equal(false);
    });

    it('on peut lui donner un label', () => {

        const field = new Field({ name: 'bar', label: 'BAR' });
        expect(field.getLabel()).to.be.equal('BAR');
    });

    it('on peut lui associer une valeur', () => {

        const field = new Field({ name: 'bar', value: 'foo' });
        expect(field.getValue()).to.be.equal('foo');

        field.setValue('foobar');
        expect(field.getValue()).to.be.equal('foobar');
    });

    it('par défaut, la valeur affecte l\'attribut value', () => {

        const field = new Field({ name: 'bar' });
        expect(field.getValue()).to.be.equal(undefined);
        field.setValue('foobar');
        expect(field.getValueAttr()).to.be.equal('foobar');
    });

    it('mais on peut changer ce comportement', () => {

        class MyField extends Field {
            public checked = false;
            setValue (value: boolean): void {
                if (value) this.checked = true;
                else this.checked = false;
                this._value = value;
            }
        }
        const field = new MyField({ name: 'bar', value: 'foo' });

        field.setValue(true);
        expect(field.getValueAttr()).to.be.equal('foo');
        expect(field.checked).to.be.equal(true);
        expect(field.getValue()).to.be.equal(true);
    });

    it('on peut lui associer un ou plusieurs validateur', () => {

        let field = new Field({ name: 'bar' }, 'string', new Validate());
        expect(field.getValidator()).to.be.instanceOf(ValidateValidator);

        field = new Field(
            { name: 'bar' },
            [{ string: { max: 10 } }, { mail: {} }],
            new Validate()
        );
        expect(field.getValidator()).to.be.instanceOf(ValidateValidator);
    });

    it('on peut appliquer des class CSS', () => {

        const field = new Field({
            name: 'foobar',
            class: {
                field: 'a',
                label: 'b',
                group: 'c',
                groupSuccess: 'd',
                groupError: 'e',
                msg: 'f',
                msgHelp: 'g',
                msgError: 'h'
            },
            required: true
        });

        expect(field.getClass()).to.be.equal('a');
        expect(field.getLabelClass()).to.be.equal('b');
        expect(field.getGroupClass()).to.be.equal('c');
        expect(field.getMsgClass()).to.be.equal('f g');
        expect(field.getMsgClass('help')).to.be.equal('f g');
        expect(field.getMsgClass('error')).to.be.equal('f h');

        field.isValid();
        expect(field.getGroupClass()).to.be.equal('c e');
        field.setValue('ok');
        field.isValid();
        expect(field.getGroupClass()).to.be.equal('c d');
    });

    it('on peut ajouter des class CSS', () => {

        const field = new Field({
            name: 'foobar',
            class: {
                field: 'a',
                label: 'b',
                group: 'c',
                groupSuccess: 'd',
                groupError: 'e',
                msg: 'f',
                msgHelp: 'g',
                msgError: 'h'
            },
            required: true
        });
        field.addClass({
            field: 'i',
            label: 'j',
            group: 'k',
            groupSuccess: 'l',
            groupError: 'm',
            msg: 'n',
            msgHelp: 'o',
            msgError: 'p'
        });


        expect(field.getClass()).to.be.equal('a i');
        expect(field.getLabelClass()).to.be.equal('b j');
        expect(field.getGroupClass()).to.be.equal('c k');
        expect(field.getMsgClass()).to.be.equal('f n g o');
        expect(field.getMsgClass('help')).to.be.equal('f n g o');
        expect(field.getMsgClass('error')).to.be.equal('f n h p');

        field.isValid();
        expect(field.getGroupClass()).to.be.equal('c k e m');
        field.setValue('ok');
        field.isValid();
        expect(field.getGroupClass()).to.be.equal('c k d l');
    });

    it('Fonctionne aussi avec des tableaux', () => {

        const field = new Field({
            name: 'foobar',
            required: true
        });
        field.addClass({
            field: ['a', 'i'],
            label: ['b', 'j'],
            group: ['c', 'k'],
            groupSuccess: ['d', 'l'],
            groupError: ['e', 'm'],
            msg: ['f', 'n'],
            msgHelp: ['g', 'o'],
            msgError: ['h', 'p']
        });


        expect(field.getClass()).to.be.equal('a i');
        expect(field.getLabelClass()).to.be.equal('b j');
        expect(field.getGroupClass()).to.be.equal('c k');
        expect(field.getMsgClass()).to.be.equal('f n g o');
        expect(field.getMsgClass('help')).to.be.equal('f n g o');
        expect(field.getMsgClass('error')).to.be.equal('f n h p');

        field.isValid();
        expect(field.getGroupClass()).to.be.equal('c k e m');
        field.setValue('ok');
        field.isValid();
        expect(field.getGroupClass()).to.be.equal('c k d l');
    });

    it('par défaut est non requis et actif', () => {

        const field1 = new Field({ name: 'bar' }),
              field2 = new Field({ name: 'bar', required: true, disabled: true });

        expect(field1.isRequired()).to.be.false;
        expect(field1.isDisabled()).to.be.false;
        expect(field2.isRequired()).to.be.true;
        expect(field2.isDisabled()).to.be.true;
    });


    describe('help', () => {

        it('par défaut aucun msg', () => {

            const field = new Field({ name: 'bar' });
            expect(field.getHelp()).to.be.equal(null);
        });

        it('on peut ajouter un message d\'aide', () => {

            const field = new Field({ name: 'bar', help: 'message' });
            expect(field.getHelp()).to.be.equal('message');
        });

        it('msg par défaut si required sauf si help===null', () => {

            const field1 = new Field({ name: 'bar', help: 'message', required: true }),
                  field2 = new Field({ name: 'bar', required: true }),
                  field3 = new Field({ name: 'bar', help: null, required: true });
            expect(field1.getHelp()).to.be.equal('message');
            expect(field2.getHelp()).to.be.equal('Ce champ est obligatoire.');
            expect(field3.getHelp()).to.be.equal(null);
        });
    });


    describe('validation', () => {

        it('test si un champ requis à bien une valeur', () => {

            const field1 = new Field({ name: 'foo' }),
                  field2 = new Field({ name: 'bar', required: true });
            expect(field1.isValid()).to.be.true;
            expect(field2.isValid()).to.be.false;
            field2.setValue('foobar');
            expect(field2.isValid()).to.be.true;
        });

        it('utilise le validator pour le test', () => {

            const field = new Field(
                { name: 'foo', required: true },
                'mail', new Validate()
            );
            expect(field.isValid()).to.be.false;
            field.setValue('foobar');
            expect(field.isValid()).to.be.false;
            field.setValue('foo@bar.com');
            expect(field.isValid()).to.be.true;
        });

        it('ssi quand validé, isSubmit() = true', () => {

            const field1 = new Field({ name: 'foo' }),
                  field2 = new Field({ name: 'bar', required: true });

            expect(field1.isSubmit()).to.be.false;
            expect(field2.isSubmit()).to.be.false;

            field1.isValid();
            field2.isValid();

            expect(field1.isSubmit()).to.be.true;
            expect(field2.isSubmit()).to.be.true;
        });

        it('erreurs issues du validateur', () => {

            const field = new Field(
                { name: 'foo' },
                [{ mail: {} }, { string: { max: 10 } }], new Validate()
            );

            field.setValue('foobar');
            expect(field.getErrors()).to.be.deep.equal([]);
            expect(field.hasErrors()).to.be.equal(null);
            field.isValid();
            expect(field.getErrors()).to.be.deep.equal([
                'Doit être une adresse mail.'
            ]);
            expect(field.hasErrors()).to.be.equal(true);

            field.setValue('foobar@foobar.com');
            field.isValid();
            expect(field.getErrors()).to.be.deep.equal([
                'La valeur est trop longue, le maximum est 10 et vous avez entré'
                + ' 17 caractère(s).'
            ]);

            field.setValue('foobarFoobar');
            field.isValid();
            expect(field.getErrors()).to.be.deep.equal([
                'Doit être une adresse mail.',
                'La valeur est trop longue, le maximum est 10 et vous avez entré'
                + ' 12 caractère(s).'
            ]);

            field.setValue('foo@ba.ar');
            field.isValid();
            expect(field.getErrors()).to.be.deep.equal([]);
            expect(field.hasErrors()).to.be.equal(false);
        });

        it('required et validator errors', () => {

            const field = new Field(
                { name: 'foo', required: 'true' },
                [{ mail: {} }, { string: { max: 10 } }], new Validate()
            );

            field.isValid();
            expect(field.getErrors()).to.be.deep.equal([
                'Ce champ est obligatoire.'
            ]);

            field.setValue('foobar');
            field.isValid();
            expect(field.getErrors()).to.be.deep.equal([
                'Doit être une adresse mail.'
            ]);
        });

        it('pas de validator', () => {

            const field = new Field({ name: 'foo', required: 'true' });

            field.isValid();
            expect(field.getErrors()).to.be.deep.equal([
                'Ce champ est obligatoire.'
            ]);

            field.setValue('foobar');
            field.isValid();
            expect(field.getErrors()).to.be.deep.equal([]);
        });

        it('validation avec validateur que si champ rempli', () => {

            const field = new Field({ name: 'foo' }, 'mail', new Validate());

            expect(field.isValid()).to.be.true;
            field.setValue('test');
            expect(field.isValid()).to.be.false;
        });

        it('message d\'erreur personnalisé', () => {

            const field = new Field({
                name: 'foo', required: true,
                error: 'Champ obligatoire. Adresse mail non valide ou trop longue.'
            }, [{ mail: {} }, { string: { max: 10 } }], new Validate());

            field.setValue('foobar');
            field.isValid();
            expect(field.getErrors()).to.be.deep.equal([
                'Champ obligatoire. Adresse mail non valide ou trop longue.'
            ]);

            field.setValue('foobar@foobar.com');
            field.isValid();
            expect(field.getErrors()).to.be.deep.equal([
                'Champ obligatoire. Adresse mail non valide ou trop longue.'
            ]);

            field.setValue('foobarFoobar');
            field.isValid();
            expect(field.getErrors()).to.be.deep.equal([
                'Champ obligatoire. Adresse mail non valide ou trop longue.'
            ]);

            field.setValue('foo@ba.ar');
            field.isValid();
            expect(field.getErrors()).to.be.deep.equal([]);
        });

        it('on peut forcer, après coup, le statut à invalide', () => {

            const field = new Field({ name: 'foo' });

            field.setValue('foobar');
            field.isValid();
            expect(field.hasErrors()).to.be.equal(false);
            expect(field.getErrors()).to.be.deep.equal([]);

            field.setInvalid('Un message');
            expect(field.hasErrors()).to.be.true;
            expect(field.getErrors()).to.be.deep.equal(['Un message']);
        });
    });


    describe('_buildMsg', () => {

        const fieldProto = Object.getPrototypeOf(new Field({ name: 'a' }));

        it('Retourne juste des espaces si aucun msg', () => {

            const field = new Field({ name: 'bar' });
            expect(fieldProto._buildMsg.call(field)).to.be.equal('  ');
        });

        it('Retourne le msg sinon', () => {

            const field = new Field({ name: 'bar', help: 'msg' });
            expect(fieldProto._buildMsg.call(field)).to.be.equal('<p>msg</p>\n  ');
        });
    });
});
