/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Entity } from '../../core/Entity';
import { FormBuilder } from './FormBuilder';
import { Forms } from '../Services/Forms';
import { MessageFormBuilder } from '../../../docs/demo-server/src/Form/MessageFormBuilder';
import { Validate as ValidateS } from '../../core/Services/Validate';

describe('server/Form/FormBuilder', () => {

    it('Créé un nouveau formulaire', () => {

        const formBuilder1 = new FormBuilder();
        const formBuilder2 = new FormBuilder();
        formBuilder1.build();
        formBuilder2.build();

        expect(formBuilder1.getForm()).to.be.not.equal(formBuilder2.getForm());
    });

    it('Transmet une entité', () => {

        class MyEntity extends Entity {
            _setProperties (): void { return; }
        }
        const entity = new MyEntity();
        const formBuilder = new FormBuilder(entity);
        formBuilder.build();

        expect(formBuilder.getForm().getEntity()).to.be.equal(entity);
    });

    it('On peut lui transmettre le service $forms', () => {

        const forms = new Forms(new ValidateS());
        const formBuilder = new FormBuilder(null, forms);
        formBuilder.build();

        expect(formBuilder.$forms).to.be.equal(forms);
    });

    it('Exécute build lors de l\'initialisation', () => {

        const test = new MessageFormBuilder(null, new Forms(new ValidateS()));
        test.build();

        expect(test.a).to.be.true;
    });
});
