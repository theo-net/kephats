/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InputField } from './InputField';
import { ValidateValidator } from '../../core/Validate/Validate';

/**
 * Définie un group `radio` : on ne peut choisir qu'un des éléments.
 */
export class RadioField extends InputField {

    protected _group: {
        label: string;
        value: any;         // eslint-disable-line @typescript-eslint/no-explicit-any
        checked: boolean;
    }[] = [];

    /**
     * Vous devez définir un attribut `group` qui doit être un `Array` d'objets décrivant les
     * différentes valeurs possibles : `{label, value}`.
     */
    init (): void {

        this._type = 'radio';

        const validator: any[] = [];       // eslint-disable-line @typescript-eslint/no-explicit-any

        if (Array.isArray(this._attr.group)) {

            this._attr.group.forEach(elmt => {

                validator.push(elmt.value);
                this._group.push({
                    label: elmt.label,
                    value: elmt.value,
                    checked: this._value == elmt.value ? true : false
                });
            });
        }

        (this._validator as ValidateValidator).add(validator);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    setValue (value: any): any {

        this._value = value;

        // Le constructor ne peut définir la valeur, car field non initialisé
        if (Array.isArray(this._group)) {
            this._group.forEach(elmt => {
                elmt.checked = value == elmt.value ? true : false;
            });
        }
    }

    build (): string {

        let widget = '  <div'
            + (this.getGroupClass() != '' ?
                ' class="' + this.getGroupClass() + '"' : '')
            + '>\n' + this._buildLabel();

        this._group.forEach((elmt, index) => {
            widget += '<div>\n      <input'
                + (this.getClass() != '' ? ' class="' + this.getClass() + '"' : '')
                + ' id="' + this._id + index + '" name="' + this._name
                + '" type="' + this._type + '" value="' + elmt.value + '"'
                + (this._disabled ? ' disabled' : '')
                + (elmt.checked ? ' checked' : '')
                + '>\n      <label'
                + (this.getLabelClass() != '' ?
                    ' class="' + this.getLabelClass() + '"' : '')
                + ' for="' + this._id + index + '">'
                + elmt.label + '</label>\n    </div>\n    ';
        });

        widget += this._buildMsg() + '</div>';

        return widget;
    }


    protected _getGroups (): {
        label: string;
        value: any;         // eslint-disable-line @typescript-eslint/no-explicit-any
        checked: boolean;
    }[] {

        return this._group;
    }
}
