/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Entity } from '../../core/Entity';
import { Field } from './Field';
import { Forms } from '../Services/Forms';
import { tValidator } from '../../core/Validate/Validate';
import * as Utils from '../../cli/Utils';


/**
 * Représente un block
 */
export class Block {

    public $forms: Forms | null;

    protected _block: Block | null = null;
    protected _class: Record<string, string[]> = {};
    protected _classBlock: string[] = [];
    protected _entity: Entity | null = null;
    protected _fields: Array<Field | Block> = [];
    protected _id: string | undefined = undefined;
    protected _idPrefix = '';

    /**
     * Initialise un nouveau block.
     * En lui donnant une entité, on permet l'autoremplissage des champs et
     * l'autovalidation
     * @param {Entity} [entity=null] Entité à transmettre
     * @param {Forms} [forms] Service $forms
     * @param {String} [idPrefix] Préfixe de l'id quand celui-ci est défini
     */
    constructor (
        entity: Entity | null = null,
        forms: Forms | null = null,
        idPrefix = ''
    ) {

        this.$forms = forms;
        this.setEntity(entity);

        this.resetClass();
        this._idPrefix += idPrefix;
    }


    /**
     * Associe une entité au block. On peut spécifier de ne pas en avoir avec la valeur `null`
     * (par défaut)
     * @param entity Entité à associer
     */
    setEntity (entity: Entity | null = null): this {

        this._entity = entity;
        return this;
    }


    /**
     * Retourne l'entité associée au block
     */
    getEntity (): Entity | null {

        return this._entity;
    }


    /**
     * Définie un block parent associé au block
     * @param {Block} block Block
     * @returns {this}
     */
    setParent (block: Block): this {

        this._block = block;
        return this;
    }


    /**
     * Retourne le block parent
     */
    getParent (): Block {

        return this._block as Block;
    }


    /**
     * Définit l'id du block. Celui-ci sera utilisé pour préfixer tous les noms et id des champs.
     * Il sera préfixé, en camelCase, par le préfixe défini lors de la construction de l'objet.
     * @param id Id du block
     */
    setId (id: string): this {

        if (this._idPrefix)
            this._id = this._idPrefix + Utils.ucfirst(id);
        else
            this._id = id;

        return this;
    }


    /**
     * Retourne l'id du block.
     */
    getId (): string | undefined {

        return this._id;
    }


    /**
     * Ajoute un champ au block
     * @param type Type du champ
     * @param attributes Attributs du champs, doit avoir au moins `name`
     * @param validator Validator (cf service)
     * @throws `Error` Si attributs ne possède pas de champ `name`
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    add (type: string, attributes: Record<string, any>, validator?: tValidator): this {

        if (!attributes || !attributes.name)
            throw new Error('Pas d\'attribut `name` transmis au champ !');

        if (this._id || this._idPrefix) {

            const prefix = this._id ? this._id : this._idPrefix;

            if (!attributes.bind)
                attributes.bind = attributes.name;

            attributes.name = prefix + Utils.ucfirst(attributes.name);

            if (attributes.id)
                attributes.id = prefix + Utils.ucfirst(attributes.id);
        }

        const field = new ((this.$forms as Forms).getField(type))(
            attributes, validator, (this.$forms as Forms).$validate
        );
        field.init();
        field.addClass(this._class);

        if (this._entity != null) {
            if (Object.keys(this._entity).indexOf(field.getName()) > -1)
                field.setValue(this._entity[attributes.name]);
            else if (Object.keys(this._entity).indexOf(attributes.bind) > -1)
                field.setValue(this._entity[attributes.bind]);
        }

        this._fields.push(field);

        return this;
    }


    /**
     * Ajoute un bloc et le  retourne
     * @param cssObj Classes css à appliquer au bloc
     */
    addBlock (cssObj?: string | string[] | Record<string, string | string[]>): Block {

        const block = new Block(this.getEntity(), this.$forms);

        if (cssObj) block.addClass(cssObj);
        block.setParent(this);

        block.addClass(this._class);

        this._fields.push(block);

        return block;
    }


    /**
     * Ajoute un fieldset (groupe de champs).
     * @param legend Légende du Fieldset
     * @param classCss Classes CSS à transmettre aux fields enfants du fieldset
     */
    addFieldset (
        legend: string | null = null,
        cssObj?: Record<string, string | string[]>
    ): Fieldset {

        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        const fieldset = new Fieldset(this.getEntity(), this.$forms, this.getId());

        if (cssObj)
            fieldset.addClass(cssObj);

        fieldset.setParent(this).setLegend(legend).addClass(this._class);

        this._fields.push(fieldset);

        return fieldset;
    }


    /**
     * Retourne un field qui a été ajouté au bloc.
     * @param name Identifiant du field
     */
    getField (name: string): Field | undefined {

        let found: Field | undefined;

        Utils.forEach(this._fields, field => {

            if (field instanceof Field && field.getName() == name) {
                found = field;
                return false;
            }
            else if (field instanceof Block) {
                found = field.getField(name);
                if (found) return false;
            }
        });

        return found;
    }

    /**
     * Retourne tous les fields
     */
    getAllFields (): Array<Block | Field> {

        return this._fields;
    }


    /**
     * Ajoute automatiquement des classes à tous les champs ajoutés au formulaire.
     *
     * Cf. `Field.addClass()`. S'il s'agit d'un `Array` ou d'un `String`, les classes seront ajoutées
     * pour le block courant.
     * @param cssObj Les classes à ajouter
     */
    addClass (cssObj: string | string[] | Record<string, string | string[]>): this {

        if (Array.isArray(cssObj))
            this._classBlock = this._classBlock.concat(cssObj);
        else if (Utils.isString(cssObj))
            this._classBlock.push(cssObj as string);
        if (Utils.isObject(cssObj)) {

            Utils.forEach(cssObj as Record<string, string[]>, (css, type) => {

                if (Array.isArray(this._class[type])) {

                    if (Utils.isString(css))
                        this._class[type].push(css);
                    else if (Array.isArray(css))
                        this._class[type] = this._class[type].concat(css);
                }
            });
        }

        return this;
    }


    /**
     * Remet à zéro toutes les classes CSS
     */
    resetClass (): this {

        this._class = {
            field: [],
            label: [],
            group: [],
            groupSuccess: [],
            groupError: [],
            msg: [],
            msgHelp: [],
            msgError: [],
            fieldset: []
        };

        return this;
    }


    /**
     * Retourne les classes CSS appliquées au block.
     */
    getClass (): string {

        return this._classBlock.join(' ');
    }


    /**
     * Retourne toutes les classes CSS.
     */
    getClasses (): Record<string, string[]> {

        return this._class;
    }


    /**
     * Rendu du bloc
     */
    build (): string {

        const classCss = this.getClass();
        let block = '<div' + (this._id ? ' id="' + this._id + '"' : '')
            + (classCss != '' ? ' class="' + classCss + '"' : '')
            + '>\n';

        this._fields.forEach(field => {

            block += field.build() + '\n';
        });

        return block + '</div>';
    }


    /**
     * Exécute le formulaire sur tous les fields : test valeurs + maj
     * @param vars Variables soumises `{id: value, ...}`
     * @returns `null` si erreur, l'entité si elle est associée ou les valeurs transmisent sinon
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected _process (vars: Record<string, any>): Entity | Record<string,any> | null {

        if (!this._fieldsValid(vars))
            return null;
        else if (this.getEntity() === null)
            return vars;
        else {

            this._majFields();
            return this.getEntity();
        }
    }


    /**
     * Test la valeur de tous les fields
     * @param vars Variables soumises
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected _fieldsValid (vars: Record<string, any>): boolean {

        let test = true;

        this._fields.forEach(field => {

            if (field instanceof Field) {

                if (vars[field.getId()])
                    field.setValue(vars[field.getId()]);

                test = field.isValid() && test;
            }
            else if (field instanceof Block)
                test = field._fieldsValid(vars) && test;
        });

        return test;
    }


    /**
     * Met à jour les valeurs des fields avec l'entité
     */
    protected _majFields (): void {

        this._fields.forEach(field => {
            if (field instanceof Block)
                field._majFields();
            else if (Object.keys(this._entity as Entity).indexOf(field.getName()) > -1)
                (this.getEntity() as Entity)[field.getName()] = field.getValue();
            else if (Object.keys(this._entity as Entity).indexOf(field.getBind() as string) > -1)
                (this.getEntity() as Entity)[field.getBind() as string] = field.getValue();
        });
    }
}


// On doit importer Fieldset une fois que Block est défini
import { Fieldset } from './Fieldset';
