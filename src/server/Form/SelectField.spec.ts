/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { SelectField } from './SelectField';

describe('server/Form/SelectField', () => {

    it('peut voir un attribut size', () => {

        const select = new SelectField({ name: 'foo', size: 2 });
        select.init();
        const selectProto = Object.getPrototypeOf(select);

        expect(selectProto._buildAttributes.call(select))
            .to.be.equal(' id="foo" name="foo" size="2"');
    });

    it('peut voir un attribut multiple', () => {

        const select1 = new SelectField({ name: 'foo' }),
              select2 = new SelectField({ name: 'foo', multiple: false }),
              select3 = new SelectField({ name: 'foo', multiple: true });
        select1.init();
        select2.init();
        select3.init();
        const selectProto = Object.getPrototypeOf(select1);

        expect(selectProto._buildAttributes.call(select1)).to.be.equal(' id="foo" name="foo"');
        expect(selectProto._buildAttributes.call(select2)).to.be.equal(' id="foo" name="foo"');
        expect(selectProto._buildAttributes.call(select3))
            .to.be.equal(' id="foo" name="foo" multiple');
    });

    it('liste de valeur', () => {

        const select = new SelectField({
            name: 'foo',
            list: [
                { value: 'foo', label: 'Foo' },
                { value: 'bar', label: 'Bar' }
            ]
        });
        select.init();
        const selectProto = Object.getPrototypeOf(select);

        expect(selectProto._buildList.call(select, selectProto._getList.call(select))).to.be.equal(
            '<option value="foo">Foo</option>\n    '
            + '<option value="bar">Bar</option>\n    '
        );
    });

    it('sous-groupe', () => {

        const select = new SelectField({
            name: 'foo',
            list: [
                { value: 'foo', label: 'Foo' },
                {
                    group: [
                        { value: 'bar', label: 'Bar' }
                    ],
                    label: 'Group'
                }
            ]
        });
        select.init();
        const selectProto = Object.getPrototypeOf(select);

        expect(selectProto._buildList.call(select, selectProto._getList.call(select))).to.be.equal(
            '<option value="foo">Foo</option>\n    '
            + '<optgroup label="Group">\n    '
            + '<option value="bar">Bar</option>\n    '
            + '</optgroup>\n    '
        );
    });

    it('les éléments peuvent être disabled ou selected', () => {

        const select = new SelectField({
            name: 'foo',
            list: [
                { value: 'foo', label: 'Foo', selected: true },
                { value: 'bar', label: 'Bar', disabled: true }
            ]
        });
        select.init();
        const selectProto = Object.getPrototypeOf(select);

        expect(selectProto._buildList.call(select, selectProto._getList.call(select))).to.be.equal(
            '<option value="foo" selected>Foo</option>\n    '
            + '<option value="bar" disabled>Bar</option>\n    '
        );
    });

    it('les sous-groupes peuvent être disabled', () => {

        const select = new SelectField({
            name: 'foo',
            list: [
                { value: 'foo', label: 'Foo' },
                {
                    group: [
                        { value: 'bar', label: 'Bar' }
                    ],
                    label: 'Group',
                    disabled: true
                }
            ]
        });
        select.init();
        const selectProto = Object.getPrototypeOf(select);

        expect(selectProto._buildList.call(select, selectProto._getList.call(select))).to.be.equal(
            '<option value="foo">Foo</option>\n    '
            + '<optgroup label="Group" disabled>\n    '
            + '<option value="bar">Bar</option>\n    '
            + '</optgroup>\n    '
        );
    });

    it('construction par défaut', () => {

        const select = new SelectField({
            name: 'foo', id: 'bar',
            label: 'Un truc',
            help: 'Une aide',
            required: true,
            disabled: true,
            size: 20,
            multiple: true,
            list: [{ value: 'foo', label: 'bar' }]
        });
        select.init();
        select.addClass({ field: 'a', label: 'b', group: 'c', msg: 'd' });

        expect(select.build()).to.be.equal(
            '  <div class="c">\n    '
            + '<label class="b" for="bar">Un truc</label>\n    '
            + '<select class="a" id="bar" name="foo" required disabled'
            + ' multiple size="20">\n    '
            + '<option value="foo">bar</option>\n    '
            + '</select><i></i>\n    '
            + '<p class="d">Une aide</p>\n'
            + '  </div>'
        );
    });


    describe('Gestion de la valeur', () => {

        it('valeur non multiple', () => {

            const select = new SelectField({
                name: 'foo',
                list: [
                    { value: 'a', label: 'A' },
                    { value: 'b', label: 'B', selected: true },
                    {
                        group: [
                            { value: 'c', label: 'C' },
                            { value: 'd', label: 'D' }
                        ],
                        label: 'Group'
                    },
                    { value: 'e', label: 'E' }
                ]
            });
            select.init();
            const selectProto = Object.getPrototypeOf(select);

            expect(select.getValue()).to.be.equal('b');
            select.setValue('c');
            expect(select.getValue()).to.be.equal('c');
            expect(selectProto._getList.call(select)[1].selected).to.be.false;
            expect(selectProto._getList.call(select)[2].group[0].selected).to.be.true;

            select.setValue('e');
            expect(select.getValue()).to.be.equal('e');
            expect(selectProto._getList.call(select)[2].group[0].selected).to.be.false;
            expect(selectProto._getList.call(select)[3].selected).to.be.true;
        });

        it('valeur multiple', () => {

            const select = new SelectField({
                name: 'foo',
                multiple: true,
                list: [
                    { value: 'a', label: 'A' },
                    { value: 'b', label: 'B', selected: true },
                    {
                        group: [
                            { value: 'c', label: 'C' },
                            { value: 'd', label: 'D' }
                        ],
                        label: 'Group'
                    },
                    { value: 'e', label: 'E' }
                ]
            });
            select.init();
            const selectProto = Object.getPrototypeOf(select);

            expect(select.getValue()).to.be.deep.equal(['b']);
            select.setValue(['c']);
            expect(select.getValue()).to.be.deep.equal(['c']);
            expect(selectProto._getList.call(select)[1].selected).to.be.false;
            expect(selectProto._getList.call(select)[2].group[0].selected).to.be.true;

            select.setValue(['c', 'd']);
            expect(select.getValue()).to.be.deep.equal(['c', 'd']);
            expect(selectProto._getList.call(select)[2].group[0].selected).to.be.true;
            expect(selectProto._getList.call(select)[2].group[1].selected).to.be.true;

            select.setValue(['d', 'e']);
            expect(select.getValue()).to.be.deep.equal(['d', 'e']);
            expect(selectProto._getList.call(select)[2].group[0].selected).to.be.false;
            expect(selectProto._getList.call(select)[2].group[1].selected).to.be.true;
            expect(selectProto._getList.call(select)[3].selected).to.be.true;
        });
    });
});
