/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InputField } from './InputField';
import { ValidateValidator } from '../../core/Validate/Validate';

export class TimeField extends InputField {

    init (): void {

        super.init();
        this._type = 'time';

        (this._validator as ValidateValidator).add(/^[0-1]\d:[0-5]\d$/);
    }
}
