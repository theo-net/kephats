/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Entity } from '../../core/Entity';
import { Form } from './Form';
import { Forms } from '../Services/Forms';

/**
 * Permet de gérer la construction d'un formulaire. Avec cette classe, un formulaire peut facilement
 * être réutilisé.
 */
export class FormBuilder {

    public $forms: Forms | undefined;

    protected _form: Form;

    /**
     * Initialise un nouveau formulaire. Si une entité est transmise, elle sera donnée au formulaire
     * qui pourra ainsi automatiser certaines choses.
     * @param entity Entité à transmettre
     * @param forms Service `$forms`, dispo dans l'objet avec `this.$forms`
     */
    constructor (entity: Entity | null = null, forms?: Forms) {

        this.$forms = forms;
        this._form = new Form(entity, forms);
    }


    /**
     * Fonction que doit implémenter chaque builder, c'est celle-ci qui construit le formulaire. On
     * peut spécifier un paramètre, `config` qui permettra de paramètrer le formulaire. Celui-ci sera
     * transmis par le service `$forms` qui appelle cette méthode après l'instanciation de ce
     * FormBuilder.
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    build (config: Record<string, any> | null = null): void { return; } // eslint-disable-line @typescript-eslint/no-explicit-any


    /**
     * Retourne le formulaire associé
     */
    getForm (): Form {

        return this._form;
    }
}
