/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InputField } from './InputField';
import { ValidateValidator } from '../../core/Validate/Validate';

export class NumberField extends InputField {

    protected _max: number | null = null;
    protected _min: number | null = null;
    protected _step: number | null = null;

    init (): void {

        super.init();
        this._type = 'number';

        const validator: Record<string, number> = {};

        if (this._attr.min) {
            validator.min = this._attr.min;
            this._min = this._attr.min;
        }
        if (this._attr.max) {
            validator.max = this._attr.max;
            this._max = this._attr.max;
        }

        (this._validator as ValidateValidator).add({ float: validator });

        if (this._attr.step) {
            this._step = this._attr.step;
            (this._validator as ValidateValidator).add((value: number) => {
                return value % (this._step as number) === 0;
            });
        }
    }


    /**
     * Essayera de transformer en nombre la valeur
     * @param value La valeur
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    setValue (value: any): void {

        this._value = Number(value);
        this._valueAttr = Number(value);
    }


    protected _buildAttributes (): string {

        return super._buildAttributes()
            + (this._min ? ' min="' + this._min + '"' : '')
            + (this._max ? ' max="' + this._max + '"' : '')
            + (this._step ? ' step="' + this._step + '"' : '');
    }
}
