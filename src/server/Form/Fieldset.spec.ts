/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Block } from './Block';
import { Fieldset } from './Fieldset';
import { Forms } from '../Services/Forms';
import { Validate as ValidateS} from '../../core/Services/Validate';

describe('server/Form/Fieldset', () => {

    it('étend Block', () => {

        const fieldset = new Fieldset();
        expect(fieldset).to.be.instanceOf(Block);
    });

    it('setLegend() définie la légende associée au fieldset', () => {

        const fieldset = new Fieldset();
        expect(fieldset.getLegend()).to.be.equal(null);
        expect(fieldset.setLegend('Une légende')).to.be.equal(fieldset);
        expect(fieldset.getLegend()).to.be.equal('Une légende');
    });

    it('class CSS spécifiques au fieldset', () => {

        const fieldset = new Fieldset();
        expect(fieldset.getClass()).to.be.equal('');
        fieldset.addClass('foobar');
        expect(fieldset.getClass()).to.be.equal('foobar');
        fieldset.addClass(['foo', 'bar']);
        expect(fieldset.getClass()).to.be.equal('foobar foo bar');
        fieldset.addClass({ fieldset: 'test' });
        expect(fieldset.getClass()).to.be.equal('test foobar foo bar');
    });


    describe('build', () => {

        let fieldset: Fieldset;

        beforeEach(() => {

            fieldset = new Fieldset(null, new Forms(new ValidateS()));
        });

        it('retourne le code HTML du fieldset', () => {

            expect(fieldset.build())
                .to.be.equal('<fieldset>\n</fieldset>');
            fieldset.setId('salut');
            expect(fieldset.build())
                .to.be.equal('<fieldset id="salut">\n</fieldset>');
            fieldset.setLegend('Foobar');
            expect(fieldset.build()).to.be.equal(
                '<fieldset id="salut">\n  <legend>Foobar</legend>\n</fieldset>'
            );
            fieldset.addClass('foobar');
            expect(fieldset.build()).to.be.equal(
                '<fieldset id="salut" class="foobar">\n  '
                + '<legend>Foobar</legend>\n</fieldset>'
            );
        });
    });
});
