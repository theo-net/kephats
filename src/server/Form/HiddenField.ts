/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InputField } from './InputField';

export class HiddenField extends InputField {

    init (): void {

        super.init();
        this._type = 'hidden';
        this._required = true;
    }

    build (): string {

        return '<input id="' + this._id + '" name="' + this._name
            + '" type="hidden" value="' + this._valueAttr + '">\n';
    }
}
