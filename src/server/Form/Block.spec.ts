/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Block } from './Block';
import { Entity } from '../../core/Entity';
import { Field } from './Field';
import { Fieldset } from './Fieldset';
import { Forms } from '../Services/Forms';
import { StringField } from './StringField';
import { ValidateValidator as Validate } from '../../core/Validate/Validate';
import { Validate as ValidateS} from '../../core/Services/Validate';

describe('server/Form/Block', () => {

    class MyEntity extends Entity {
        _setProperties (): void { return; }
    }

    it('Peut assigner une entité lors de la construction', () => {

        const entity = new MyEntity();
        const block = new Block(entity);
        expect(block.getEntity()).to.be.equal(entity);
    });

    it('On peut associer le service $forms', () => {

        const forms = new Forms(new ValidateS());
        const block = new Block(null, forms);
        expect(block.$forms).to.be.equal(forms);
    });

    it('setEntity() assigne une entité au block', () => {

        const entity = new MyEntity();
        const block = new Block();
        expect(block.setEntity(entity)).to.be.equal(block);
        expect(block.getEntity()).to.be.equal(entity);
    });

    it('setId() associe un id au block', () => {

        const block = new Block();
        expect(block.setId('foobar')).to.be.equal(block);
        expect(block.getId()).to.be.equal('foobar');
    });

    it('un prefixe peut avoir été déf lors de l\'init', () => {

        const block = new Block(new MyEntity(), new Forms(new ValidateS()), 'foo');
        expect(block.setId('bar').getId()).to.be.equal('fooBar');
    });

    it('on peut associer un parent au Block', () => {

        const block = new Block(),
              blockP = new Block();
        expect(block.setParent(blockP)).to.be.equal(block);
        expect(block.getParent()).to.be.equal(blockP);
    });

    it('on peut remettre à zéro les classes Css', () => {

        const block = new Block();
        block.addClass({field: 'a'});

        expect(block.resetClass()).to.be.equal(block);
        expect(block.getClasses().field.length).to.be.equal(0);
    });


    describe('add', () => {

        let block: Block, blockB: Block;
        const entity = new MyEntity();
        entity.foo = 'a';
        entity.bar = 'b';

        beforeEach(() => {

            block = new Block(entity, new Forms(new ValidateS()));
            blockB = new Block(entity, new Forms(new ValidateS()), 'foo');
        });

        it('returns the block object', () => {

            expect(block.add('string', { name: 'test' })).to.be.equal(block);
        });

        it('throw Error si pas d\'attribut name', () => {

            expect(block.add.bind(block, 'string')).to.throws(Error);
            expect(block.add.bind(block, 'string', { foo: 'bar' }))
                .to.throws(Error);
        });

        it('enregistre le field', () => {

            block.add('string', { name: 'foo' });
            expect(block.getAllFields()[0]).to.be.instanceOf(StringField);
            expect((block.getAllFields()[0] as Field).getName()).to.be.equal('foo');
        });

        it('appelle la méthode `init()` du Field', () => {

            block.add('string', { name: 'foo', max: 20 });
            const validators = (block.getAllFields()[0] as Field).getValidator();
            expect(validators?.getValidators()[1].getArguments().max).to.be.equal(20);

        });

        it('transmet le service $validate pour créer le validator', () => {

            block.add('string', { name: 'foo' }, 'mail');
            expect((block.getAllFields()[0] as Field).getValidator()).to.be.instanceOf(Validate);
        });

        it('les champs sont remplis avec la valeur de l\'entité', () => {

            block.add('string', { name: 'foo' })
                .add('string', { name: 'bar' });

            expect((block.getAllFields()[0] as Field).getValue()).to.be.equal(entity.foo);
            expect((block.getAllFields()[1] as Field).getValue()).to.be.equal(entity.bar);
        });

        it('champs bindés remplis avec l\'entité', () => {

            block.add('string', { name: 'fooB', bind: 'foo' })
                .add('string', { name: 'barB', bind: 'bar' });

            expect((block.getAllFields()[0] as Field).getValue()).to.be.equal(entity.foo);
            expect((block.getAllFields()[1] as Field).getValue()).to.be.equal(entity.bar);
        });

        it('on peut forcer l\'ajout de classes CSS', () => {

            expect(block.addClass({
                field: 'i',
                label: 'j',
                group: 'k',
                groupSuccess: 'l',
                groupError: 'm',
                msg: 'n',
                msgHelp: 'o',
                msgError: 'p',
                fieldset: 'q'
            })).to.be.equal(block);

            block.add('string', {
                name: 'foo', required: true, class: {
                    field: 'a',
                    label: 'b',
                    group: 'c',
                    groupSuccess: 'd',
                    groupError: 'e',
                    msg: 'f',
                    msgHelp: 'g',
                    msgError: 'h'
                }
            });

            expect((block.getAllFields()[0] as Field).getClass()).to.be.equal('a i');
            expect((block.getAllFields()[0] as Field).getLabelClass()).to.be.equal('b j');
            expect((block.getAllFields()[0] as Field).getGroupClass()).to.be.equal('c k');
            expect((block.getAllFields()[0] as Field).getMsgClass()).to.be.equal('f n g o');
            expect((block.getAllFields()[0] as Field).getMsgClass('help')).to.be.equal('f n g o');
            expect((block.getAllFields()[0] as Field).getMsgClass('error')).to.be.equal('f n h p');

            (block.getAllFields()[0] as Field).isValid();
            expect((block.getAllFields()[0] as Field).getGroupClass()).to.be.equal('c k d l');
            (block.getAllFields()[0] as Field).setValue('');
            (block.getAllFields()[0] as Field).isValid();
            expect((block.getAllFields()[0] as Field).getGroupClass()).to.be.equal('c k e m');
        });

        it('si le block a un id, prefixe le nom des champs', () => {

            expect(block.setId('salut')).to.be.equal(block);
            block.add('string', { name: 'foo' })
                .add('string', { name: 'bar' })
                .add('string', { name: 'bind', bind: 'bar' });
            const field1 = block.getAllFields()[0] as Field,
                  field2 = block.getAllFields()[1] as Field,
                  field3 = block.getAllFields()[2] as Field;

            expect(field1.getName()).to.be.equal('salutFoo');
            expect(field1.getId()).to.be.equal('salutFoo');
            expect(field2.getName()).to.be.equal('salutBar');
            expect(field2.getId()).to.be.equal('salutBar');
            expect(field1.getValue()).to.be.equal(entity.foo);
            expect(field2.getValue()).to.be.equal(entity.bar);
            expect(field3.getValue()).to.be.equal(entity.bar);
        });

        it('si l\'id d\'un parent transmis, utilisé pour préfixer', () => {

            blockB.add('string', { name: 'foo' })
                .add('string', { name: 'bar' })
                .add('string', { name: 'bind', bind: 'bar' });
            const field1 = blockB.getAllFields()[0] as Field,
                  field2 = blockB.getAllFields()[1] as Field,
                  field3 = blockB.getAllFields()[2] as Field;

            expect(field1.getName()).to.be.equal('fooFoo');
            expect(field1.getId()).to.be.equal('fooFoo');
            expect(field2.getName()).to.be.equal('fooBar');
            expect(field2.getId()).to.be.equal('fooBar');
            expect(field1.getValue()).to.be.equal(entity.foo);
            expect(field2.getValue()).to.be.equal(entity.bar);
            expect(field3.getValue()).to.be.equal(entity.bar);
        });

        it('combinaison des deux derniers tests', () => {

            blockB.setId('bar');
            blockB.add('string', { name: 'foo' })
                .add('string', { name: 'bar' })
                .add('string', { name: 'bind', bind: 'bar' });
            const field1 = blockB.getAllFields()[0] as Field,
                  field2 = blockB.getAllFields()[1] as Field,
                  field3 = blockB.getAllFields()[2] as Field;

            expect(field1.getName()).to.be.equal('fooBarFoo');
            expect(field1.getId()).to.be.equal('fooBarFoo');
            expect(field2.getName()).to.be.equal('fooBarBar');
            expect(field2.getId()).to.be.equal('fooBarBar');
            expect(field1.getValue()).to.be.equal(entity.foo);
            expect(field2.getValue()).to.be.equal(entity.bar);
            expect(field3.getValue()).to.be.equal(entity.bar);
        });
    });


    describe('addBlock', () => {

        let block: Block, blockB: Block, entity: Entity, forms: Forms;

        beforeEach(() => {

            entity = new MyEntity();
            forms = new Forms(new ValidateS());
            block = new Block(entity, forms);
            blockB = new Block();
        });

        it('return new Block', () => {

            const block1 = block.addBlock(),
                  block2 = block.addBlock();
            expect(block1).to.be.instanceOf(Block);
            expect(block1).to.be.not.equal(block2);
        });

        it('transmet les classes CSS', () => {

            const classCss = {
                field: 'a',
            };
            block.addClass(classCss);

            expect(block.addBlock().getClasses().field[0]).to.be.equal('a');
        });

        it('on peut définir directement les classes CSS', () => {

            expect(block.addBlock().getClass()).to.be.equal('');
            expect(block.addBlock('foobar').getClass()).to.be.equal('foobar');
        });

        it('on peut récupérer le parent', () => {

            expect(block.addBlock().getParent()).to.be.equal(block);
        });

        it('transmet l\'entité', () => {

            expect(block.addBlock().getEntity()).to.be.equal(entity);
            expect(blockB.addBlock().getEntity()).to.be.equal(null);
        });

        it('transmet le service $form', () => {

            expect(block.addBlock().$forms).to.be.equal(forms);
            expect(blockB.addBlock().$forms).to.be.equal(null);
        });
    });


    describe('addFieldset', () => {

        let block: Block, blockB: Block, entity: Entity, forms: Forms;

        beforeEach(() => {

            entity = new MyEntity();
            forms = new Forms(new ValidateS());
            block = new Block(entity, forms);
            blockB = new Block();
        });

        it('return new Fieldset', () => {

            const fieldset1 = block.addFieldset(),
                  fieldset2 = block.addFieldset();
            expect(fieldset1).to.be.instanceOf(Fieldset);
            expect(fieldset1).to.be.not.equal(fieldset2);
        });

        it('on peut définir directement la légende', () => {

            expect(block.addFieldset().getLegend()).to.be.equal(null);
            expect(block.addFieldset('foobar').getLegend()).to.be.equal('foobar');
        });

        it('on peut récupérer le parent', () => {

            expect(block.addFieldset().getParent()).to.be.equal(block);
        });

        it('transmet l\'entité', () => {

            expect(block.addFieldset().getEntity()).to.be.equal(entity);
            expect(blockB.addFieldset().getEntity()).to.be.equal(null);
        });

        it('transmet le service $form', () => {

            expect(block.addFieldset().$forms).to.be.equal(forms);
            expect(blockB.addFieldset().$forms).to.be.equal(null);
        });

        it('prefixe l\'id du fieldset avec celui du block', () => {

            const fieldset1 = block.addFieldset();
            expect(fieldset1.getId()).to.be.undefined;
            fieldset1.setId('bar');
            expect(fieldset1.getId()).to.be.equal('bar');

            block.setId('foo');
            const fieldset2 = block.addFieldset();
            expect(fieldset2.getId()).to.be.undefined;
            fieldset2.setId('bar');
            expect(fieldset2.getId()).to.be.equal('fooBar');
        });

        it('on peut forcer l\'ajout de classes CSS', () => {

            expect(block.addClass({
                field: 'i',
                label: 'j',
                group: 'k',
                groupSuccess: 'l',
                groupError: 'm',
                msg: 'n',
                msgHelp: 'o',
                msgError: 'p'
            })).to.be.equal(block);

            block.addFieldset('legend', {
                field: 'a',
                label: 'b',
                group: 'c',
                groupSuccess: 'd',
                groupError: 'e',
                msg: 'f',
                msgHelp: 'g',
                msgError: 'h'
            }).add('string', { name: 'foobar', required: true });

            expect((block.getAllFields()[0] as Fieldset).getAllFields()[0]
                .getClass()).to.be.equal('a i');
            expect(((block.getAllFields()[0] as Fieldset).getAllFields()[0] as Field)
                .getLabelClass()).to.be.equal('b j');
            expect(((block.getAllFields()[0] as Fieldset).getAllFields()[0] as Field)
                .getGroupClass()).to.be.equal('c k');
            expect(((block.getAllFields()[0] as Fieldset).getAllFields()[0] as Field)
                .getMsgClass()).to.be.equal('f n g o');
            expect(((block.getAllFields()[0] as Fieldset).getAllFields()[0] as Field)
                .getMsgClass('help')).to.be.equal('f n g o');
            expect(((block.getAllFields()[0] as Fieldset).getAllFields()[0] as Field)
                .getMsgClass('error')).to.be.equal('f n h p');

            ((block.getAllFields()[0] as Fieldset).getAllFields()[0] as Field).isValid();
            expect(((block.getAllFields()[0] as Fieldset).getAllFields()[0] as Field)
                .getGroupClass()).to.be.equal('c k e m');
            ((block.getAllFields()[0] as Fieldset).getAllFields()[0] as Field).setValue('ok');
            ((block.getAllFields()[0] as Fieldset).getAllFields()[0] as Field).isValid();
            expect(((block.getAllFields()[0] as Fieldset).getAllFields()[0] as Field)
                .getGroupClass()).to.be.equal('c k d l');
        });
    });


    describe('getField', () => {

        let block: Block, forms: Forms;

        beforeEach(() => {

            forms = new Forms(new ValidateS());
            block = new Block(new MyEntity(), forms);
        });

        it('Peut retourner un field enfant', () => {

            block.add('string', { name: 'foobar' })
                .add('string', { name: 'foo' })
                .add('string', { name: 'bar' });
            expect(block.getField('foo')).to.be.equal(block.getAllFields()[1]);
        });

        it('S\'arrête au premier trouvé', () => {

            block.add('string', { name: 'foo' })
                .add('string', { name: 'foo' })
                .add('string', { name: 'bar' });
            expect(block.getField('foo')).to.be.equal(block.getAllFields()[0]);
        });

        it('cherche dans les sous-blocks', () => {

            block.add('string', { name: 'foobar' })
                .addBlock()
                .add('string', { name: 'foo' })
                .add('string', { name: 'bar' })
                .getParent()
                .addBlock().addFieldset()
                .add('string', { name: 'salut' });

            expect(block.getField('salut')).to.be.equal(
                ((block.getAllFields()[2] as Block).getAllFields()[0] as Block).getAllFields()[0]
            );
        });
    });


    describe('build', () => {

        let block: Block;

        beforeEach(() => {

            block = new Block(null, new Forms(new ValidateS()));
        });

        it('retourne le code HTML du block', () => {

            expect(block.build()).to.be.equal('<div>\n</div>');

            block.setId('salut');
            expect(block.build()).to.be.equal('<div id="salut">\n</div>');

            block.addClass('a');
            expect(block.build()).to.be.equal('<div id="salut" class="a">\n</div>');
            block.addClass(['b', 'c']);
            expect(block.build()).to.be.equal('<div id="salut" class="a b c">\n</div>');
        });

        it('construit tous les sous-éléments', () => {

            block
                .add('string', { name: 'foo', label: 'Un champ' })
                .add('string', { name: 'bar', label: 'Un autre champ' });
            expect(block.build()).to.be.equal(
                '<div>\n'
                + '  <div>\n'
                + '    <label for="foo">Un champ</label>\n'
                + '    <input id="foo" name="foo" type="text"><i></i>\n'
                + '      </div>\n'
                + '  <div>\n'
                + '    <label for="bar">Un autre champ</label>\n'
                + '    <input id="bar" name="bar" type="text"><i></i>\n'
                + '      </div>\n'
                + '</div>'
            );
        });
    });


    describe('_process', () => {

        let block: Block, blockB: Block, blockC: Block, entity: Entity;
        const blockProto = Object.getPrototypeOf(new Block());

        beforeEach(() => {

            entity = new MyEntity();
            entity.nom = 'a';
            entity.mail = 'b';
            entity.description = '';

            block = new Block(null, new Forms(new ValidateS()));
            block.setParent(block); // hack pour les tests
            block.addBlock()
                .add('string', { name: 'nom', required: true }, { string: { min: 3 } })
                .add('string', { name: 'mail', required: true }, 'mail')
                .getParent()
                .add('string', { name: 'description' });

            blockB = new Block(entity, new Forms(new ValidateS()));
            blockB.setParent(blockB); // hack pour les tests
            blockB.addBlock()
                .add('string', { name: 'nom', required: true }, { string: { min: 3 } })
                .add('string', { name: 'mail', required: true }, 'mail')
                .getParent()
                .add('string', { name: 'description' });

            blockC = new Block(entity, new Forms(new ValidateS()));
            blockC.setParent(blockC); // hack pour les tests
            blockC.addBlock()
                .add('string', { name: 'nom', required: true }, { string: { min: 3 } })
                .add('string', { name: 'mail', required: true }, 'mail')
                .getParent()
                .add('string', { name: 'desc', bind: 'description' });
        });

        it('rempli tous les champs', () => {

            const req = {
                nom: 'Jésus',
                mail: 'jesus@cieux.god',
                description: 'Fils de Dieu'
            };
            blockProto._process.call(block, req);

            expect(((block.getAllFields()[0] as Block).getAllFields()[0] as Field).getValue())
                .to.be.equal('Jésus');
            expect(((block.getAllFields()[0] as Block).getAllFields()[1] as Field).getValue())
                .to.be.equal('jesus@cieux.god');
            expect((block.getAllFields()[1] as Field).getValue()).to.be.equal('Fils de Dieu');
        });

        it('retourne la requête si tout est valide (pas d\'entity)', () => {

            const req = {
                nom: 'Jésus',
                mail: 'jesus@cieux.god',
                description: 'Fils de Dieu'
            };
            expect(blockProto._process.call(block, req)).to.be.equal(req);
        });

        it('Si une entité est présente, la retourne mise-à-jour', () => {

            const req = {
                nom: 'Jésus',
                mail: 'jesus@cieux.god',
                description: 'Fils de Dieu'
            };
            const ret = blockProto._process.call(blockB, req);

            expect(ret).to.be.equal(entity);
            expect(ret).to.include(req);
        });

        it('Ne rajoute pas de propriétés', () => {

            blockB.add('string', { name: 'test' });
            const req = {
                nom: 'Jésus',
                mail: 'jesus@cieux.god',
                description: 'Fils de Dieu',
                test: 'ok'
            };
            const ret = blockProto._process.call(blockB, req);

            expect(ret).to.be.equal(entity);
            expect(ret.test).to.be.undefined;
        });

        it('Si une entité est présente, mais req invalide, non maj', () => {

            const req = {
                nom: 'Jésus',
                mail: 'jesuscieux.god',
                description: 'Fils de Dieu'
            };
            const ret = blockProto._process.call(blockB, req);

            expect(ret).to.be.equal(null);
            expect(blockB.getEntity()).to.include({
                nom: 'a', mail: 'b', description: ''
            });
        });

        it('entité remplis avec champs bindés', () => {

            const req = {
                nom: 'Jésus',
                mail: 'jesus@cieux.god',
                desc: 'Fils de Dieu'
            };
            const ret = blockProto._process.call(blockC, req);

            expect(ret).to.be.equal(entity);
            expect(ret).to.include({
                nom: 'Jésus',
                mail: 'jesus@cieux.god',
                description: 'Fils de Dieu'
            });
        });

        it('retourne null sinon', () => {

            const req1 = {
                nom: 'Jésus',
                mail: 'jesus@cieux.god',
            };
            const req2 = {
                nom: 'Jésus',
                mail: 'jesuscieux.god',
            };
            const req3 = {
                nom: 'Jésus',
            };
            expect(blockProto._process.call(block, req1)).to.be.equal(req1);
            expect(blockProto._process.call(block, req2)).to.be.equal(null);
            expect(blockProto._process.call(block, req3)).to.be.equal(null);
        });

        it('tous les champs sont bien parcourus', () => {

            blockProto._process.call(block, {
                nom: 'Jé',
                mail: 'jesus@cieux.god',
            });

            expect(((block.getAllFields()[0] as Block).getAllFields()[0] as Field)
                .isSubmit()).to.be.true;
            expect(((block.getAllFields()[0] as Block).getAllFields()[1] as Field)
                .isSubmit()).to.be.true;
            expect((block.getAllFields()[1] as Field)
                .isSubmit()).to.be.true;
        });
    });
});
