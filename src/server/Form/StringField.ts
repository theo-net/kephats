/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InputField } from './InputField';
import { ValidateValidator } from '../../core/Validate/Validate';

/**
 * Représente un champ `input` de type `text`
 */
export class StringField extends InputField {

    protected _maxlength: number | null = null;
    protected _minlength: number | null = null;

    /**
     * Nouveaux attributs :
     *
     *   - `[min]` : taille minimum de la valeur
     *   - `[max]` : taille maximum de la valeur
     */
    init (): void {

        super.init();

        const validator: Record<string, number> = {};

        if (this._attr.min) {
            validator.min = this._attr.min;
            this._minlength = this._attr.min;
        }
        if (this._attr.max) {
            validator.max = this._attr.max;
            this._maxlength = this._attr.max;
        }

        (this._validator as ValidateValidator).add({ string: validator });

        if (this._attr.pattern) {

            if (!(this._attr.pattern instanceof RegExp))
                this._attr.pattern = new RegExp(this._attr.pattern);

            this._patternAttr = this._attr.pattern.toString().slice(1, -1);

            (this._validator as ValidateValidator).add(this._attr.pattern);
        }
        else
            this._patternAttr = null;
    }


    protected _buildAttributes (): string {

        return super._buildAttributes()
            + (this._minlength ? ' minlength="' + this._minlength + '"' : '')
            + (this._maxlength ? ' maxlength="' + this._maxlength + '"' : '');
    }
}
