/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { ButtonField } from './ButtonField';
import { CheckboxField } from './CheckboxField';
import { ColorField } from './ColorField';
import { DateField } from './DateField';
import { EmailField } from './EmailField';
import { HiddenField } from './HiddenField';
import {InputField} from './InputField';
import { NumberField } from './NumberField';
import { PasswordField } from './PasswordField';
import { RadioField } from './RadioField';
import { RangeField } from './RangeField';
import { ResetField } from './ResetField';
import { SearchField } from './SearchField';
import { StringField } from './StringField';
import { SubmitField } from './SubmitField';
import { TelField } from './TelField';
import { TimeField } from './TimeField';
import { UrlField } from './UrlField';

import {StringValidator} from '../../core/Validate/String';
import {RegExpValidator} from '../../core/Validate/RegExp';
import {MailValidator} from '../../core/Validate/Mail';
import {FloatValidator} from '../../core/Validate/Float';
import {ListValidator} from '../../core/Validate/List';
import { Validate as ValidateS} from '../../core/Services/Validate';


describe('server/Form/InputField', () => {

    let inputProto = Object.getPrototypeOf(new InputField({ name: 'foo' }));

    it('par défaut type text', () => {

        const input = new InputField({ name: 'foo' });
        input.init();
        expect(inputProto._buildAttributes.call(input))
            .to.be.equal(' id="foo" name="foo" type="text"');
    });

    it('peut avoir un placeholder', () => {

        const input = new InputField({ name: 'foo', placeholder: 'bar' });
        input.init();
        expect(inputProto._buildAttributes.call(input))
            .to.be.equal(' id="foo" name="foo" type="text" placeholder="bar"');
    });

    it('peut avoir spellcheck', () => {

        const input1 = new InputField({ name: 'foo', spellcheck: true }),
              input2 = new InputField({ name: 'foo', spellcheck: false }),
              input3 = new InputField({ name: 'foo', spellcheck: 'default' });
        input1.init();
        input2.init();
        input3.init();

        expect(inputProto._buildAttributes.call(input1))
            .to.be.equal(' id="foo" name="foo" type="text" spellcheck="true"');
        expect(inputProto._buildAttributes.call(input2))
            .to.be.equal(' id="foo" name="foo" type="text" spellcheck="false"');
        expect(inputProto._buildAttributes.call(input3))
            .to.be.equal(' id="foo" name="foo" type="text" spellcheck="default"');
    });

    it('peut avoir list', () => {

        const input = new InputField(
            { name: 'foo', list: ['a', 'b'] },
            undefined, new ValidateS()
        );
        const inputB = new InputField(
            { name: 'foo', list: ['a', 'b'] },
            'string', new ValidateS()
        );
        input.init();
        inputB.init();

        expect(inputProto._buildAttributes.call(input))
            .to.be.equal(' id="foo" name="foo" type="text" list="fooList"');
        expect(inputProto._buildList.call(input)).to.be.equal(
            '    <datalist id="fooList">\n    '
            + '  <option value="a">\n    '
            + '  <option value="b">\n    '
            + '</datalist>'
        );
        input.setValue('c');
        expect(input.isValid()).to.be.false;
        inputB.setValue('c');
        expect(inputB.isValid()).to.be.false;
    });

    it('construction par défaut', () => {

        const input = new InputField({
            name: 'foo', id: 'bar',
            label: 'Un truc',
            placeholder: 'test',
            value: 'cool',
            help: 'Une aide',
            required: true,
            disabled: true,
            size: 20
        });
        input.init();
        input.addClass({ field: 'a', label: 'b', group: 'c', msg: 'd' });

        expect(input.build()).to.be.equal(
            '  <div class="c">\n    '
            + '<label class="b" for="bar">Un truc</label>\n    '
            + '<input class="a" id="bar" name="foo" type="text" value="cool"'
            + ' placeholder="test" required disabled size="20"><i></i>\n    '
            + '<p class="d">Une aide</p>\n'
            + '  </div>'
        );
    });


    describe('String', () => {

        it('input de type text', () => {

            const input = new StringField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="text"');
        });

        it('validator String', () => {

            const input = new StringField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            expect(input.getValidator()?.getValidators()[1])
                .to.be.instanceOf(StringValidator);
        });

        it('attribut min -> minlength', () => {

            const input = new StringField(
                { name: 'foo', min: 1 }, undefined, new ValidateS()
            );
            input.init();
            const inputProto = Object.getPrototypeOf(input);

            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="text" minlength="1"');
            expect(input.getValidator()?.getValidators()[1].getArguments().min)
                .to.be.equal(1);
        });

        it('attribut max -> maxlength', () => {

            const input = new StringField(
                { name: 'foo', max: 2 }, undefined, new ValidateS()
            );
            input.init();
            const inputProto = Object.getPrototypeOf(input);

            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="text" maxlength="2"');
            expect(input.getValidator()?.getValidators()[1].getArguments().max)
                .to.be.equal(2);
        });

        it('attribut pattern', () => {

            const input1 = new StringField(
                { name: 'foo', pattern: /a.*/ }, undefined, new ValidateS()
            );
            input1.init();
            const input2 = new StringField(
                { name: 'foo', pattern: 'a.*' }, undefined, new ValidateS()
            );
            input2.init();

            let inputProto = Object.getPrototypeOf(input1);
            expect(inputProto._buildAttributes.call(input1))
                .to.be.equal(' id="foo" name="foo" type="text" pattern="a.*"');
            inputProto = Object.getPrototypeOf(input2);
            expect(inputProto._buildAttributes.call(input2))
                .to.be.equal(' id="foo" name="foo" type="text" pattern="a.*"');

            expect(input1.getValidator()?.getValidators()[2])
                .to.be.instanceOf(RegExpValidator);
            expect(input2.getValidator()?.getValidators()[2])
                .to.be.instanceOf(RegExpValidator);

            expect(input1.getValidator()?.getValidators()[2].getArguments().regExp)
                .to.be.deep.equal(/a.*/);
            expect(input2.getValidator()?.getValidators()[2].getArguments().regExp)
                .to.be.deep.equal(/a.*/);
        });

        it('trim sur la valeur', () => {

            const input = new StringField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            input.setValue(' foo');
            expect(input.getValue()).to.be.equal('foo');
            input.setValue('bar ');
            expect(input.getValue()).to.be.equal('bar');
            input.setValue(' foobar ');
            expect(input.getValue()).to.be.equal('foobar');
        });

        it('le trim fonctionne si undefined', () => {

            const input = new StringField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            input.setValue(undefined);
            expect(input.getValue()).to.be.equal(undefined);
        });
    });


    describe('Email', () => {

        it('est un dérivé de String', () => {

            const input = new EmailField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            expect(input).to.be.instanceOf(StringField);
        });

        it('input de type email', () => {

            const input = new EmailField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="email" placeholder="exemple@domain.com"');
        });

        it('placeholder définit par défaut', () => {

            const input1 = new EmailField({ name: 'foo' }, undefined, new ValidateS()),
                  input2 = new EmailField(
                      { name: 'foo', placeholder: 'bar' }, undefined, new ValidateS()
                  );
            input1.init();
            input2.init();
            const inputProto = Object.getPrototypeOf(input1);

            expect(inputProto._buildAttributes.call(input1))
                .to.be.equal(' id="foo" name="foo" type="email" placeholder="exemple@domain.com"');
            expect(inputProto._buildAttributes.call(input2))
                .to.be.equal(' id="foo" name="foo" type="email" placeholder="bar"');
        });

        it('validator Email', () => {

            const input = new EmailField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            expect(input.getValidator()?.getValidators()[2]).to.be.instanceOf(MailValidator);
        });
    });


    describe('Password', () => {

        it('est un dérivé de String', () => {

            const input = new PasswordField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            expect(input).to.be.instanceOf(StringField);
        });

        it('input de type password', () => {

            const input = new PasswordField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="password"');
        });
    });


    describe('Search', () => {

        it('est un dérivé de String', () => {

            const input = new SearchField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            expect(input).to.be.instanceOf(StringField);
        });

        it('input de type search', () => {

            const input = new SearchField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="search"');
        });
    });


    describe('Url', () => {

        it('est un dérivé de String', () => {

            const input = new UrlField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            expect(input).to.be.instanceOf(StringField);
        });

        it('input de type url', () => {

            const input = new UrlField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="url"');
        });
    });


    describe('Tel', () => {

        it('est un dérivé de String', () => {

            const input = new TelField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            expect(input).to.be.instanceOf(StringField);
        });

        it('input de type tel', () => {

            const input = new TelField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="tel"');
        });
    });


    describe('Number', () => {

        it('est un dérivé d\'Input', () => {

            const input = new NumberField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            expect(input).to.be.instanceOf(InputField);
        });

        it('input de type number', () => {

            const input = new NumberField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="number"');
        });

        it('transforme la valeur en un nombre', () => {

            const input = new NumberField({ name: 'foo' }, undefined, new ValidateS());
            input.init();

            input.setValue(2);
            expect(input.getValue()).to.be.equal(2);
            input.setValue('2');
            expect(input.getValue()).to.be.equal(2);
            input.setValue(1.2);
            expect(input.getValue()).to.be.equal(1.2);
            input.setValue('1.2');
            expect(input.getValue()).to.be.equal(1.2);
        });

        it('Vérifie qu\'il s\'agit bien d`un nombre', () => {

            const input = new NumberField({ name: 'foo' }, undefined, new ValidateS());
            input.init();

            input.setValue('2');
            expect(input.isValid()).to.be.true;
            input.setValue('a');
            expect(input.isValid()).to.be.false;
            expect(input.getValidator()?.getValidators()[1])
                .to.be.instanceOf(FloatValidator);
        });

        it('attribut min et validator', () => {

            const input = new NumberField(
                { name: 'foo', min: 1 }, undefined, new ValidateS()
            );
            input.init();
            const inputProto = Object.getPrototypeOf(input);

            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="number" min="1"');
            expect(input.getValidator()?.getValidators()[1].getArguments().min)
                .to.be.equal(1);
        });

        it('attribut max et validator', () => {

            const input = new NumberField(
                { name: 'foo', max: 2 }, undefined, new ValidateS()
            );
            input.init();
            const inputProto = Object.getPrototypeOf(input);

            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="number" max="2"');
            expect(input.getValidator()?.getValidators()[1].getArguments().max)
                .to.be.equal(2);
        });

        it('attribut step et validator', () => {

            const input = new NumberField(
                { name: 'foo', step: 2 }, undefined, new ValidateS()
            );
            input.init();
            const inputProto = Object.getPrototypeOf(input);

            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="number" step="2"');

            input.setValue(-2);
            expect(input.isValid()).to.be.true;
            input.setValue(0);
            expect(input.isValid()).to.be.true;
            input.setValue(4);
            expect(input.isValid()).to.be.true;
            input.setValue(5);
            expect(input.isValid()).to.be.false;
            input.setValue(2.5);
            expect(input.isValid()).to.be.false;
        });
    });


    describe('Range', () => {

        it('est un dérivé de Number', () => {

            const input = new RangeField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            expect(input).to.be.instanceOf(NumberField);
        });

        it('input de type range', () => {

            const input = new RangeField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="range"');
        });
    });


    describe('Color', () => {

        inputProto = Object.getPrototypeOf(new ColorField({ name: 'foo' }));

        it('input de type color', () => {

            const input = new ColorField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="color"');
        });

        it('possède un validateur : color format #xxxxxx', () => {

            const input = new ColorField({ name: 'foo' }, undefined, new ValidateS());
            input.init();

            input.setValue('foobar');
            expect(input.isValid()).to.be.false;

            input.setValue('#abc123');
            expect(input.isValid()).to.be.true;

            expect(input.getValidator()?.getValidators()[1]).to.be.instanceOf(RegExpValidator);
        });
    });


    describe('Date', () => {

        it('input de type date', () => {

            const input = new DateField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="date"');
        });

        it('possède un validateur : format aaaa-mm-jj', () => {

            const input = new DateField({ name: 'foo' }, undefined, new ValidateS());
            input.init();

            input.setValue('foobar');
            expect(input.isValid()).to.be.false;

            input.setValue('2018-04-08');
            expect(input.isValid()).to.be.true;

            expect(input.getValidator()?.getValidators()[1])
                .to.be.instanceOf(RegExpValidator);
        });
    });


    describe('Time', () => {

        it('input de type time', () => {

            const input = new TimeField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="time"');
        });

        it('possède un validateur : format hh:mm', () => {

            const input = new TimeField({ name: 'foo' }, undefined, new ValidateS());
            input.init();

            input.setValue('foobar');
            expect(input.isValid()).to.be.false;

            input.setValue('08:30');
            expect(input.isValid()).to.be.true;

            expect(input.getValidator()?.getValidators()[1])
                .to.be.instanceOf(RegExpValidator);
        });
    });


    describe('Hidden', () => {

        it('input de type hidden', () => {

            const input = new HiddenField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="hidden" required');
        });

        it('le champ est toujours requis', () => {

            const input1 = new HiddenField({ name: 'foo' }, undefined, new ValidateS()),
                  input2 = new HiddenField(
                      { name: 'foo', required: true }, undefined, new ValidateS()
                  ),
                  input3 = new HiddenField(
                      { name: 'foo', required: false }, undefined, new ValidateS()
                  );
            input1.init();
            input2.init();
            input3.init();

            expect(input1.isRequired()).to.be.true;
            expect(input2.isRequired()).to.be.true;
            expect(input3.isRequired()).to.be.true;
        });

        it('le rendu est simple, même si CSS défini', () => {

            const input = new HiddenField(
                { name: 'foo', class: { field: 'css' }, value: 'a' },
                undefined, new ValidateS()
            );
            input.init();

            expect(input.build())
                .to.be.equal('<input id="foo" name="foo" type="hidden" value="a">\n');
        });
    });


    describe('Submit', () => {

        it('input de type submit', () => {

            const input = new SubmitField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="submit"');
        });

        it('attribut formmethod', () => {

            const input = new SubmitField(
                { name: 'foo', formmethod: 'get' }, undefined, new ValidateS()
            );
            input.init();
            const inputProto = Object.getPrototypeOf(input);

            expect(inputProto._buildAttributes.call(input)).to.be.equal(
                ' id="foo" name="foo" type="submit" formmethod="get"'
            );
        });

        it('attribut formaction', () => {

            const input = new SubmitField(
                { name: 'foo', formaction: 'truc.html' }, undefined, new ValidateS()
            );
            input.init();
            const inputProto = Object.getPrototypeOf(input);

            expect(inputProto._buildAttributes.call(input)).to.be.equal(
                ' id="foo" name="foo" type="submit" formaction="truc.html"'
            );
        });

        it('pas de classes de feedback', () => {

            const input = new SubmitField(
                { name: 'foo', class: { groupSuccess: 'a', groupError: 'b' } },
                undefined, new ValidateS()
            );
            input.init();

            expect(input.getClasses().groupSuccess).to.be.deep.equal([]);
            expect(input.getClasses().groupError).to.be.deep.equal([]);
        });
    });


    describe('Button', () => {

        it('input de type button', () => {

            const input = new ButtonField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="button"');
        });

        it('pas de classes de feedback', () => {

            const input = new ButtonField(
                { name: 'foo', class: { groupSuccess: 'a', groupError: 'b' } },
                undefined, new ValidateS()
            );
            input.init();

            expect(input.getClasses().groupSuccess).to.be.deep.equal([]);
            expect(input.getClasses().groupError).to.be.deep.equal([]);
        });
    });


    describe('Reset', () => {

        it('input de type reset', () => {

            const input = new ResetField({ name: 'foo' }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="reset"');
        });

        it('pas de classes de feedback', () => {

            const input = new ResetField(
                { name: 'foo', class: { groupSuccess: 'a', groupError: 'b' } },
                undefined, new ValidateS()
            );
            input.init();

            expect(input.getClasses().groupSuccess).to.be.deep.equal([]);
            expect(input.getClasses().groupError).to.be.deep.equal([]);
        });
    });


    describe('Radio', () => {

        it('Listes des valeurs et validator', () => {

            const input = new RadioField({
                name: 'foo',
                group: [
                    { label: 'truc', value: 'a' },
                    { label: 'muche', value: 'b' }
                ]
            }, undefined, new ValidateS());
            input.init();

            expect(input.getValidator()?.getValidators()[1])
                .to.be.instanceOf(ListValidator);
            expect(input.getValidator()?.getValidators()[1].getArguments().list)
                .to.be.deep.equal(['a', 'b']);

            input.setValue('c');
            expect(input.isValid()).to.be.false;
            input.setValue('a');
            expect(input.isValid()).to.be.true;
        });

        it('valeur, indique celui qui est `checked`', () => {

            const input = new RadioField({
                name: 'foo',
                group: [
                    { label: 'truc', value: 'a' },
                    { label: 'muche', value: 'b' }
                ],
                value: 'a'
            }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);

            expect(inputProto._getGroups.call(input)[0].checked).to.be.true;
            expect(inputProto._getGroups.call(input)[1].checked).to.be.false;

            input.setValue('b');
            expect(inputProto._getGroups.call(input)[0].checked).to.be.false;
            expect(inputProto._getGroups.call(input)[1].checked).to.be.true;
        });

        it('build spécifique', () => {

            const input = new RadioField({
                name: 'foo',
                group: [
                    { label: 'truc', value: 'a' },
                    { label: 'muche', value: 'b' }
                ],
                value: 'a',
                label: 'Choisir quelque chose',
                help: 'c'
            }, undefined, new ValidateS());
            input.init();

            expect(input.build()).to.be.equal('  <div>\n  '
                + '  <label for="foo">Choisir quelque chose</label>\n  '
                + '  <div>\n  '
                + '    <input id="foo0" name="foo" type="radio" value="a" checked>\n  '
                + '    <label for="foo0">truc</label>\n  '
                + '  </div>\n  '
                + '  <div>\n  '
                + '    <input id="foo1" name="foo" type="radio" value="b">\n  '
                + '    <label for="foo1">muche</label>\n  '
                + '  </div>\n  '
                + '  <p>c</p>\n  '
                + '</div>'
            );
        });
    });


    describe('Checkbox', () => {

        it('Étend Radio, mais de type `checkbox`', () => {

            const input = new CheckboxField({
                name: 'foo',
                group: [
                    { label: 'truc', value: 'a' },
                    { label: 'muche', value: 'b' }
                ],
                label: 'Choisir quelque chose'
            }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);

            expect(input).to.be.instanceOf(RadioField);
            expect(inputProto._buildAttributes.call(input))
                .to.be.equal(' id="foo" name="foo" type="checkbox"');
        });

        it('setValue, toujours array, même avec une seule valeur', () => {

            const input = new CheckboxField({
                name: 'foo',
                group: [
                    { label: 'truc', value: 'a' },
                    { label: 'muche', value: 'b' }
                ],
                value: 'a',
                label: 'Choisir quelque chose'
            }, undefined, new ValidateS());
            input.init();
            const inputProto = Object.getPrototypeOf(input);

            expect(inputProto._getGroups.call(input)[0].checked).to.be.true;
            expect(inputProto._getGroups.call(input)[1].checked).to.be.false;
            expect(input.getValue()).to.be.deep.equal(['a']);

            input.setValue('b');
            expect(inputProto._getGroups.call(input)[0].checked).to.be.false;
            expect(inputProto._getGroups.call(input)[1].checked).to.be.true;
            expect(input.getValue()).to.be.deep.equal(['b']);

            input.setValue(['a', 'b']);
            expect(inputProto._getGroups.call(input)[0].checked).to.be.true;
            expect(inputProto._getGroups.call(input)[1].checked).to.be.true;
            expect(input.getValue()).to.be.deep.equal(['a', 'b']);
        });

        it('tests plusieurs valeurs', () => {

            const input = new CheckboxField({
                name: 'foo',
                group: [
                    { label: 'truc', value: 'a' },
                    { label: 'muche', value: 'b' }
                ],
                required: true,
                label: 'Choisir quelque chose'
            }, undefined, new ValidateS());
            input.init();

            expect(input.isValid()).to.be.false;

            input.setValue('b');
            expect(input.isValid()).to.be.true;

            input.setValue(['a', 'b']);
            expect(input.isValid()).to.be.true;

            input.setValue('c');
            expect(input.isValid()).to.be.false;

            input.setValue(['a', 'c']);
            expect(input.isValid()).to.be.false;
        });
    });
});
