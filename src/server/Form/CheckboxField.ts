/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { RadioField } from './RadioField';
import { ValidateValidator } from '../../core/Validate/Validate';

export class CheckboxField extends RadioField {

    init (): void {

        super.init();
        this._type = 'checkbox';
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    setValue (value: any): void {

        if (!Array.isArray(value))
            value = Array(value);
        this._value = value;

        // Le constructor ne peut définir la valeur, car field non initialisé
        if (Array.isArray(this._group)) {
            this._group.forEach(elmt => {
                elmt.checked = value.indexOf(elmt.value) > -1 ? true : false;
            });
        }
    }

    isValid (): boolean {

        this._validated = true;
        this._valid = true;
        this._requiredValid = true;

        if (this._required) {

            this._requiredValid = this._value !== undefined && this._value !== null &&
                this._value !== '';
        }

        if (this._validator && Array.isArray(this._value)) {
            this._value.forEach(value => {
                this._valid = this._valid && (this._validator as ValidateValidator).isValid(value);
            });
        }

        return this._requiredValid && this._valid;
    }
}
