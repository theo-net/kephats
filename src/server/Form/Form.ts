/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Block } from './Block';
import { Entity } from '../../core/Entity';

/**
 * Représente un formulaire
 * Techniquement, il s'agit d'un Block avec quelques propriétés et
 * méthodes spéciales.
 */
export class Form extends Block {

    protected _action = '#';
    protected _method = 'get';


    /**
     * Définit l'action et la méthode
     * @param action Action du formulaire
     * @param method Méthode du formulaire (`get` par défaut)
     */
    setAction (action: string, method = 'get'): this {

        this._action = action;
        this._method = method;

        return this;
    }


    /**
     * Retourne toujours lui-même
     * @returns {this}
     */
    getParent (): this {

        return this;
    }


    /**
     * Construit le code HTML du formulaire et le retourne.
     */
    build (): string {

        let form = '<form action="' + this._action + '" method="'
            + this._method + '"' + (this._id ? ' id="' + this._id + '"' : '')
            + '>\n';

        this._fields.forEach(field => {

            form += field.build() + '\n';
        });

        return form + '</form>';
    }


    /**
     * Exécute le formulaire
     * @param {Object} vars Variables soumises `{id: value, ...}`
     * @returns {Null|Object} Null si erreur, object sinon
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    process (vars: Record<string, any>): Entity | Record<string, any> | null {

        return this._process(vars);
    }
}
