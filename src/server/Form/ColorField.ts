/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InputField } from './InputField';
import { ValidateValidator } from '../../core/Validate/Validate';

export class ColorField extends InputField {

    init (): void {

        super.init();
        this._type = 'color';

        (this._validator as ValidateValidator).add(/^#[a-zA-z0-9]{6}$/);
    }
}
