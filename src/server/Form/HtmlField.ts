/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Field } from './Field';

export class HtmlField extends Field {

    protected _html = '';

    /**
     * Ce type de field sert uniquement à insérer du code HTML entre d'autres fields. Un attribut
     * `html` fournit le code HTML à insérer
     */
    init (): void {

        this._html = this._attr.html ? this._attr.html : '';
    }


    build (): string {

        return this._html;
    }
}
