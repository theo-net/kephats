/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Block } from './Block';

/**
 * Représente un block DIV
 * Techniquement, il s'agit d'un
 */
export class Fieldset extends Block {

    protected _legend: string | null = null;

    /**
     * Définie la légende du fieldset
     * @param {String} legend La légende
     * @returns {this}
     */
    setLegend (legend: string | null): this {

        this._legend = legend;
        return this;
    }


    /**
     * Retourne la légende associée au fieldset. `null` si aucune
     */
    getLegend (): string | null {

        return this._legend;
    }

    /**
     * Construit le code HTML du fieldset et le retourne.
     */
    build (): string {

        const classCss = this.getClass();
        let fieldset = '<fieldset' + (this._id ? ' id="' + this._id + '"' : '')
            + (classCss != '' ? ' class="' + classCss + '"' : '')
            + '>\n';

        if (this.getLegend() !== null)
            fieldset += '  <legend>' + this.getLegend() + '</legend>\n';

        this._fields.forEach(field => {

            fieldset += field.build() + '\n';
        });

        return fieldset + '</fieldset>';
    }


    /**
     * Retourne les classes CSS appliquées au fieldset. Assemblera celles définies spécifiquement
     * pour lui et celles ajoutées par `addClass{fieldset: ...}`.
     */
    getClass (): string {

        return this._class.fieldset.concat(this._classBlock).join(' ');
    }
}
