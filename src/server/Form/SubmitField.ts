/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InputField } from './InputField';

/**
 * Représente un champ `input` de type `submit`
 */
export class SubmitField extends InputField {

    protected _formaction: string | null = null;
    protected _formmethod: string | null = null;

    /**
     * Nouveaux attributs :
     *
     *   - `[formmethod]` : change la méthode du formulaire
     *   - `[formaction]` : change l'action du formulaire
     */
    init (): void {

        super.init();
        this._type = 'submit';

        this._formmethod = this._attr.formmethod ? this._attr.formmethod : null;
        this._formaction = this._attr.formaction ? this._attr.formaction : null;
    }


    /**
     * Par rapport aux éléments parents, ne prend pas en compte les classes `groupSuccess` et `groupError`
     * @param cssObj Les classes CSS
     */
    addClass (cssObj: Record<string, string | string[]>): void {

        super.addClass(cssObj);

        this._class.groupSuccess = [];
        this._class.groupError = [];
    }

    protected _buildAttributes (): string {

        return super._buildAttributes()
            + (this._formmethod ? ' formmethod="' + this._formmethod + '"' : '')
            + (this._formaction ? ' formaction="' + this._formaction + '"' : '');
    }
}
