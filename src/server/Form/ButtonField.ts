/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InputField } from './InputField';

export class ButtonField extends InputField {

    init (): void {

        super.init();
        this._type = 'button';
    }


    /**
     * Par rapport aux éléments parents, ne prend pas en compte les classes `groupSuccess` et
     * `groupError`
     * @param cssObj Les classes CSS
     */
    addClass (cssObj: Record<string, string | string[]>): void {

        super.addClass(cssObj);

        this._class.groupSuccess = [];
        this._class.groupError = [];
    }
}
