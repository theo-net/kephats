/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { StringField } from './StringField';

export class UrlField extends StringField {

    init (): void {

        super.init();
        this._type = 'url';
    }
}
