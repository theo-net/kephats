/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Validate } from '../../core/Services/Validate';
import { ValidateValidator, tValidator } from '../../core/Validate/Validate';
import * as Utils from '../../cli/Utils';

/**
 * Classe mère représentant un champ d'un formulaire
 */
export class Field {

    protected _attr: Record<string, any>; // eslint-disable-line @typescript-eslint/no-explicit-any
    protected _bind: string | false = false;
    protected _class: Record<string, string[]> = {
        field: [],
        label: [],
        group: [],
        groupSuccess: [],
        groupError: [],
        msg: [],
        msgHelp: [],
        msgError: []
    };
    protected _disabled: boolean;
    protected _error: string[] | false;
    protected _id = '';
    protected _help: string | null;
    protected _label: string;
    protected _msgRequired = 'Ce champ est obligatoire.';
    protected _name = '';
    protected _required: boolean;
    protected _requiredValid = false;
    protected _valid = false;
    protected _validated = false;
    protected _validator: ValidateValidator | null = null;
    protected _value: any; // eslint-disable-line @typescript-eslint/no-explicit-any
    protected _valueAttr: any; // eslint-disable-line @typescript-eslint/no-explicit-any


    /**
     * Construit un champ. L'argument `attributes` permet de spécifier différentes choses au champ :
     *
     *  - `name` : attribut `name`
     *  - `[id]` : attribut `id`, vaut `attributes.name` par défaut
     *  - `[bind]` : lie le `name` avec le nom d'une propriété de l'entité associée au formulaire.
     *  - `[value]` : attribut `value` du champ et appellera `this.setValue()`
     *  - `[label]` : label du champ
     *  - `[required]` : `boolean` le champ est-il requis ? Vaut `false` par défaut
     *  - `[disabled]` : `boolean` le champ est-il desactivé ? Vaut `false` par défaut
     *  - `[class]` : classe CSS à appliquer au champ (cf méthode `addClass()`)
     *  - `[help]` : message indicatif (format du champ, obligatoire, ...)
     *  - `[error]` : message si valeur non valide (écrase tous les autres)
     *
     * Pour information, voici l'arborescence des fields :
     *
     *     - Field
     *        |- Input
     *        |   |- Button
     *        |   |- Reset
     *        |   |- Radio
     *        |   |   |- Checkbox
     *        |   |- Date
     *        |   |- String
     *        |   |   |- Email
     *        |   |   |- Password
     *        |   |   |- Search
     *        |   |   |- Tel
     *        |   |   |- Url
     *        |   |- Number
     *        |   |   |- Range
     *        |   |- Time
     *        |   |- Hidden
     *        |- Text
     *
     * @param attributes Attributs du champ
     * @param validator Validateur pour tester les valeurs
     * @param validate Service permettant de construire le validator
     */
    constructor (
        attributes: Record<string, any>, // eslint-disable-line @typescript-eslint/no-explicit-any
        validator?: tValidator | tValidator[],
        validate?: Validate
    ) {

        this._attr = attributes;
        this._name = attributes.name;
        this._id = attributes.id ? attributes.id : attributes.name;
        this._bind = attributes.bind ? attributes.bind : false;
        this._label = attributes.label;
        this._valueAttr = attributes.value;
        this._required = attributes.required ? true : false;
        this._disabled = attributes.disabled ? true : false;
        this._error = attributes.error ? [attributes.error] : false;

        if (attributes.value) this.setValue(attributes.value);

        if (validate)
            this._validator = validate.make();
        if (validator && validate)
            (this._validator as ValidateValidator).add(validator);

        if (attributes.help)
            this._help = attributes.help;
        else if (attributes.help !== null && this._required)
            this._help = this._msgRequired;
        else
            this._help = null;

        if (attributes.class) this.addClass(attributes.class);
    }


    /**
     * Retourne tous les attributs
     */
    getAttr (): Record<string, any> { // eslint-disable-line @typescript-eslint/no-explicit-any

        return this._attr;
    }


    /**
     * Retourne le nom du champ
     */
    getName (): string {

        return this._name;
    }


    /**
     * Retourne l'id du champ
     */
    getId (): string {

        return this._id;
    }


    /**
     * Retourne la propriété de l'entité avec laquelle est lié ce champ (`false` si il n'est pas
     * lié à une propriété différente de `name`)
     */
    getBind (): string | false {

        return this._bind;
    }


    /**
     * Retourne le validateur associé au champ
     */
    getValidator (): ValidateValidator | null {

        return this._validator;
    }


    /**
     * Retourne la valeur du label
     */
    getLabel (): string {

        return this._label;
    }


    /**
     * Retourne le message d'aide
     */
    getHelp (): string | null {

        return this._help;
    }


    /**
     * Le champ est-il requis ?
     */
    isRequired (): boolean {

        return this._required;
    }


    /**
     * Le champ est-il désactivé ?
     */
    isDisabled (): boolean {

        return this._disabled;
    }


    /**
     * Construit le champ (rendu HTML). Vous devrez redéfinir cette méthode
     * @returns Code HTML
     */
    build (): string {

        return '';
    }


    /**
     * Définit la valeur du field. Sera appelé lors de l'initialisation du champ avec la valeur de
     * `attributes.value` et remplira la propriété `this._valueAttr` utilisée pour spécifier
     * l'attribut `value` de l'élément HTML. Peut être personnalisé
     *
     * ex :
     *     setValue (value) {
     *         if (value) this._checked = true ;
     *         else this._checked = false ;
     *         this._value = value ;
     *     }
     *     // Dans ce cas, il faut étendre le `constructor` pour appliquer la valeur de
     *     // `attributes.checked`
     * @param value Valeur
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    setValue (value: any): void {

        this._value = value;
        this._valueAttr = value;
    }


    /**
     * Retourne la valeur du field
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    getValue (): any {

        return this._value;
    }


    /**
     * Retourne la valeur de l'attribut `value`
     */
    getValueAttr (): any { // eslint-disable-line @typescript-eslint/no-explicit-any

        return this._valueAttr;
    }


    /**
     * Indique si la valeur associée au champ est valide. Enverra également une erreur si le champ
     * est requis, mais qu'aucune valeur n'est associée. La vérification avec les valdiateurs n'a
     * pas lieue pour un champ non requis vide.
     *
     * Vous pouvez étendre son comportement.
     */
    isValid (): boolean {

        this._validated = true;
        this._valid = true;
        this._requiredValid = true;

        if (this._required) {

            this._requiredValid = this._value !== undefined && this._value !== null &&
                this._value !== '';
        }

        if (this._validator && this._value != '' && this._value != undefined)
            this._valid = this._valid && this._validator.isValid(this._value);

        return this._requiredValid && this._valid;
    }


    /**
     * Initialisation du field personnalisée, sera appelé lors de son initialisation lorsque l'on
     * ajoute ce champ. à un block.
     */
    init (): void { return; }

    /**
     * Retourne les erreurs issues du validator. Attention, `isValid()` doit avoir été exécuté.
     */
    getErrors (): string[] {

        if (this.isSubmit()) {

            if (this._required && !this._requiredValid)
                return this._error ? this._error : [this._msgRequired];
            else {
                if (this._error && !this._valid)
                    return this._error;
                else
                    return this._validator ? this._validator.getOccurredErrors() : [];
            }
        }
        else
            return [];
    }


    /**
     * Indique si le champ a été testé (formulaire soumis)
     */
    isSubmit (): boolean {

        return this._validated;
    }


    /**
     * Indique s'il y a des erreurs de validations
     * @returns `null` si non validé/soumis
     */
    hasErrors (): null | boolean {

        if (this.isSubmit())
            return !(this._requiredValid && this._valid);
        else
            return null;
    }


    /**
     * Force le statut du champ à invalide et transmet un message personnalisé.
     * @param msg Message d'erreur personnalisé
     */
    setInvalid (msg: string): void {

        this._valid = false;
        this._error = [msg];
    }


    /**
     * Ajoute des classes CSS au champ. Prend comme argument un objet décrivant la liste des classes
     * à appliquer. Les propriétés de l'objet peuvent être :
     *
     *  - `field` : classes appliquées au champ
     *  - `label` : classes appliquées au label
     *  - `group` : classes appliquées au groupe (élément englobant le field)
     *  - `groupSuccess` : classes, en plus de celles de `group`, appliquées
     *     lorsque la validation a eu lieue sans erreur.
     *  - `groupError` : classes, en plus de celles de `group`, appliquées
     *     lorsque la validation a eu lieue avec des erreurs.
     *  - `msg` : classes appliquées au message associé au champ
     *  - `msgHelp` : classes, en plus de celles de `msg`,  appliquées
     *     lorsque le message est l'indication d'aide
     *  - `msgError` : classes, en plus de celles de `msg`,  appliquées
     *     lorsque le message est la description de l'erreur.
     *
     * Leur valeur est soit une chaîne de caractères soit un tableau de chaînes
     * @param cssObj Classes CSS
     */
    addClass (cssObj: Record<string, string | string[]>): void {

        if (Utils.isObject(cssObj)) {

            Utils.forEach(cssObj, (css, type) => {

                if (Array.isArray(this._class[type])) {

                    if (Utils.isString(css))
                        this._class[type].push(css);
                    else if (Array.isArray(css))
                        this._class[type] = this._class[type].concat(css);
                }
            });
        }
    }


    /**
     * Retourne toutes les classes
     */
    getClasses (): Record<string, string[]> {

        return this._class;
    }


    /**
     * Retourne la liste des classes appliquées au champ
     */
    getClass (): string {

        return this._class.field.join(' ');
    }


    /**
     * Retourne la liste des classes appliquées au label
     */
    getLabelClass (): string {

        return this._class.label.join(' ');
    }


    /**
     * Retourne la liste des classes appliquées au groupe
     */
    getGroupClass (): string {

        if (!this.isSubmit())
            return this._class.group.join(' ');
        else if (this.hasErrors())
            return (this._class.group.concat(this._class.groupError)).join(' ');
        else
            return (this._class.group.concat(this._class.groupSuccess)).join(' ');
    }


    /**
     * Retourne la liste des classes appliquées au message
     * @param type Type du message (`help` par défaut, peut-être définit à `error`)
     */
    getMsgClass (type = 'help'): string {

        if (type == 'error')
            return (this._class.msg.concat(this._class.msgError)).join(' ');
        else
            return (this._class.msg.concat(this._class.msgHelp)).join(' ');
    }


    /**
     * Construit un label
     */
    protected _buildLabel (): string {

        if (this._label) {
            return '    <label'
                + (this.getLabelClass() != '' ?
                    ' class="' + this.getLabelClass() + '"' : '')
                + ' for="' + this._id + '">' + this._label + '</label>\n    ';
        }
        else
            return '';
    }


    /**
     * Construit le message
     */
    protected _buildMsg (): string {

        const msg = (this.isSubmit() && this.hasErrors() ?
            this.getErrors().join(' ')
            : (this._help ? this._help : ''));

        if (msg) {
            return '<p'
                + (this.getMsgClass() != '' ?
                    ' class="' + this.getMsgClass() + '"' : '')
                + '>' + msg + '</p>\n  ';
        }
        else
            return '  ';
    }
}
