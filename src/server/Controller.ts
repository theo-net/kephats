/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as http from 'http';

import { Kernel } from "../core/Kernel";
import { Resp } from './Resp';
import { User } from './User/User';
import { View } from './View';


/**
 * Chacun des controlleurs de l'application devra étendre cette classe.
 *
 * Les controllers peuvent posséder une méthode `init()` qui sera appellée avant l'exécution de
 * chaque action. Si celle-ci possède un traitement asynchrone, elle doit retourner une `Promise`,
 * ainsi le serveur attendra que ce traitement soit terminé avant d'exécuter l'action. Elle peut
 * recevoir en paramètres les variables issues du routeur (dont données GET) et POST.
 */
export class Controller {

    public $request: http.IncomingMessage;
    public $response: Resp;
    public $user: User;
    public $view: View = new View(null);

    protected _kernel: Kernel;
    protected _userInit: Promise<unknown>;

    constructor (
        kernel: Kernel,
        request: http.IncomingMessage,
        response: Resp,
        user?: User
    ) {

        this.$request = request;
        this.$response = response;
        this._kernel = kernel;

        // On créé l'objet User si aucun transmis
        let userInit;
        if (!user) {

            const UserObj = this.kernel().get('$config').get('$userObj', () => User) as typeof User;
            user = new UserObj(
                request, response,
                this.kernel().get('$sessions'),
                this.kernel().get('$config').get('user')
            );
            userInit = user.init();
        }
        if (!(userInit instanceof Promise))
            this._userInit = new Promise(resolve => { resolve(); });
        else
            this._userInit = userInit;
        this.$user = user;
    }


    /**
     * Retourne le kernel
     */
    kernel (): Kernel {

        return this._kernel;
    }


    /**
     * Alias vers `kernel.get()`
     */
    get (name: string): any {  // eslint-disable-line @typescript-eslint/no-explicit-any

        return this._kernel.get(name);
    }


    /**
     * Cette méthode peut être écrasée par votre controlleur. Elle doit retourner un objet associant
     * à chaque action une vue.
     * Si aucune correspondance n'est définie, le framework essayera de la trouver toute seule. Si
     * au contraire la valeur est `null`, aucune vue ne sera associée.
     */
    setViews (): Record<string, string | null> {

        return {};
    }


    /**
     * Fonction d'initialisation qui sera appelée juste avant l'exécution d'une action : très
     * pratique pour définir un comportement commun à toutes les actions d'un controlleur.
     * Elle peut retourner une `Promise`.
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    init (vars?: Record<string, any>): void | Promise<never> { // eslint-disable-line @typescript-eslint/no-explicit-any

        return;
    }


    /**
     * Retourne la Promise liée à l'initialisation de `User`
     */
    getUserInit (): Promise<unknown> {

        return this._userInit;
    }
}
