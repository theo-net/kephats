/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as path from 'path';
import * as http from 'http';

import { Config } from '../core/Services/Config';
import { FormBuilder } from './Form/FormBuilder';
import { Forms } from './Services/Forms';
import { Kernel } from '../core/Kernel';
import { Kfmd } from './Services/Kfmd';
import { Models } from './Services/Models';
import { Router } from './Services/Router';
import { Server } from './Server';
import { Sessions } from './User/Sessions';
import * as Utils from '../cli/Utils';


const basepath = path.normalize(__dirname + '../../../docs/demo-server');
const config = {
    'basepath': basepath + '/',
    'staticDir': 'web/',
    'routes': 'config/routes.json',
    'foo': 'bar',
};

describe('server/Server', () => {

    let server: Server,
        close: http.Server;

    beforeEach(() => {

        server = new Server(new Kernel(true), config);
        server.run(['start']);
        close = server.kernel().get('$config').get('$server');
    });

    afterEach(() => {

        if (close) close.close();
        server.kernel().get('$config').delete('$server');
    });


    it('charge la configuration', () => {

        Utils.forEach(config, (value: string, id: string) => {
            expect(server.kernel().get('$config').get(id)).to.be.equal(value);
        });
        server.kernel().get('$config').get('$server').close();
    });

    it('initialise les canaux d\'events de bases', () => {

        expect(server.kernel().events().has('Router')).to.be.true;
        expect(server.kernel().events().has('Slot')).to.be.true;
    });

    it('initialise les services', () => {

        expect(server.get('$router')).to.be.instanceOf(Router);
        expect(server.get('$models')).to.be.instanceOf(Models);
        expect(server.get('$sessions')).to.be.instanceOf(Sessions);
        expect(server.get('$kfmd')).to.be.instanceOf(Kfmd);
        expect(server.get('$forms')).to.be.instanceOf(Forms);
        expect(server.get('$forms').$validate).to.be.equal(server.kernel().get('$validate'));
        expect(server.get('$staticCache')).to.be.instanceOf(Config);
    });

    it('charge les modèles avec setModels()', () => {

        const model1 = {},
              model2 = {};

        server.setModels({ foo: model1, bar: model2 });

        expect(server.get('$models').has('foo')).to.be.true;
        expect(server.get('$models').get('foo')).to.be.equal(model1);
        expect(server.get('$models').has('bar')).to.be.true;
        expect(server.get('$models').get('bar')).to.be.equal(model2);
    });

    it('charge les formBuilders avec setFormBuilders()', () => {

        class Builder1 extends FormBuilder {
            build (): void { return; }
        }
        class Builder2 extends FormBuilder {
            build (): void { return; }
        }

        server.setFormBuilders({ foo: Builder1, bar: Builder2 });

        expect(server.get('$forms').has('foo')).to.be.true;
        expect(server.get('$forms')._formBuilders.foo).to.be.equal(Builder1);
        expect(server.get('$forms').has('bar')).to.be.true;
        expect(server.get('$forms')._formBuilders.bar).to.be.equal(Builder2);
    });

    it('exécute l\'init après le chargement de la configuration', (done) => {

        close.close();
        server.kernel().get('$config').delete('$server');
        server = new Server(new Kernel(true), config);

        server.init = ($config, kernel): void => {

            expect($config.get('foo')).to.be.equal('bar');
            expect(kernel).to.be.equal(server.kernel());
            done();
        };

        server.run(['start']);
        close = server.kernel().get('$config').get('$server');
    });
});
