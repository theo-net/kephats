/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as fs from 'fs';
import * as http from 'http';
import { Socket } from 'net';

import { Color } from '../cli/Color';
import { Kernel } from '../core/Kernel';
import { MemoryStorage } from './User/MemoryStorage';
import { Resp } from './Resp';
import { Router } from './Services/Router';
import { Sessions } from './User/Sessions';
import { User } from './User/User';
import { View  } from './View';
import { ViewError  } from './ViewError';

const basepath = __dirname + '/../../docs/demo-server/';

describe('server/View', () => {

    let kernel: Kernel;

    before(() => {

        // On force une nouvelle instance
        kernel = new Kernel(true);
        kernel.get('$config').set('basepath', basepath);
        kernel.get('$config').set('controllerDir', 'src/Controller/');
        kernel.container().register('$color', new Color());
    });

    it('charge une vue', () => {

        const view = new View(basepath + 'templates/index/index.html');
        expect(view.$getContentSource()).to.be.equal(
            '<h1>Hello World!</h1>\n\n<p>{{message}}</p>\n\n<div>\n  <p>\n  '
            + 'Ici, nous avons un bloc inclu dans une autre vue !\n</p>\n<p>\n  '
            + 'Il peut également utiliser les données transmises à la vue : '
            + '{{foobar}}\n</p>\n\n\n</div>\n\n'
        );
    });

    it('peut charger une vue vide', (done) => {

        const view = new View(null);
        expect(view.$getContentSource()).to.be.equal('');
        view.$render().then((str: string) => {
            expect(str).to.be.equal('');
            done();
        });
    });

    it('lève une exception si la vue ne peut être chargée', () => {

        const exec = (): void => {
            new View(basepath + 'templates/index/idex.html');
        };

        expect(exec.bind({})).to.throw(ViewError, 'Impossible de charger la vue '
            + __dirname
            + '/../../docs/demo-server/templates/index/idex.html: '
            + 'accès au fichier impossible'
        );
    });

    it('par défaut gère du HTML', () => {

        const view = new View(basepath + 'templates/index/index.html');
        expect(view.$getMimeType()).to.be.equal('text/html');
    });

    it('on peut préciser le type Mime', () => {

        const view = new View(
            basepath + 'templates/index/index.html', 'json');
        expect(view.$getMimeType()).to.be.equal('application/json');
    });

    it('on peut changer le type Mime', () => {

        const view = new View(basepath + 'templates/index/index.html');
        view.$setMimeType('json');
        expect(view.$getMimeType()).to.be.equal('application/json');
    });

    it('lève une exception si le type mime n\'est pas supporté', () => {

        const exec = (): void => {
            new View(basepath + 'templates/index/index.html', 'foobar');
        };

        expect(exec.bind({})).to.throw(ViewError, 'Impossible de charger la vue '
            + __dirname
            + '/../../docs/demo-server/templates/index/index.html: '
            + 'MIME type `foobar` non supporté'
        );
    });

    describe('$setContent', () => {

        it('modifie le contenu', () => {

            const view = new View(basepath + 'templates/index/index.html');
            view.$setContent('test');

            expect(view.$getContentSource()).to.be.equal('test');
        });

        it('on peut inclure un fichier', () => {

            const view = new View(basepath + 'templates/index/bloc.html');
            const source = view.$getContentSource();
            view.$setContent('foo{{$include(\'/bloc.html\')}}bar');

            expect(view.$getContentSource()).to.be.equal('foo' + source + 'bar');
        });
    });

    describe('$render', () => {

        it('pour une vue sans var, renverra juste le contenu', (done) => {

            const view = new View(basepath + 'templates/index/index.html');
            view.$render().then(render => {
                expect(render).to.be.equal(
                    '<h1>Hello World!</h1>\n\n<p></p>\n\n<div>\n  <p>\n  '
                    + 'Ici, nous avons un bloc inclu dans une autre vue !\n</p>\n<p>\n  '
                    + 'Il peut également utiliser les données transmises à la vue : '
                    + '\n</p>\n\n\n</div>\n\n'
                );
                done();
            });
        });

        it('sait utiliser les variables', (done) => {

            const view = new View(basepath + 'templates/index/index.html');
            view.message = 'Un message';
            view.$render().then(render => {
                expect(render).to.be.equal(
                    '<h1>Hello World!</h1>\n\n<p>Un message</p>\n\n<div>\n  <p>\n  '
                    + 'Ici, nous avons un bloc inclu dans une autre vue !\n</p>\n<p>\n  '
                    + 'Il peut également utiliser les données transmises à la vue : '
                    + '\n</p>\n\n\n</div>\n\n'
                );
                done();
            });
        });

        it('utilise aussi les var si le contenu a été modifié', (done) => {

            const view = new View(basepath + 'templates/index/index.html');
            view.message = 'Un message';
            view.$setContent('Il dit « {{message}} »');

            view.$render().then(render => {
                expect(render).to.be.equal('Il dit « Un message »');
                done();
            });
        });

        it('si un layout est défini, renvoi son contenu', (done) => {

            const view = new View(basepath + 'templates/index/index.html');
            view.$setLayout(basepath + 'templates/layout.html');
            view.title = 'Test';
            view.message = 'Message...';

            view.$render().then(render => {
                expect(render).to.be.equal(
                    '<!DOCTYPE html>\n\n'
                    + '<html>\n  <head>\n    <meta charset="utf-8" />\n'
                    + '    <title>Test - Démo</title>\n'
                    + '    <link rel="stylesheet" href="./styles.css" />\n'
                    + '  </head>\n\n  <body>\n    '
                    + '<h1>Hello World!</h1>\n\n<p>Message...</p>\n\n<div>\n  <p>\n  '
                    + 'Ici, nous avons un bloc inclu dans une autre vue !\n</p>\n<p>\n  '
                    + 'Il peut également utiliser les données transmises à la vue : '
                    + '\n</p>\n\n\n</div>\n\n\n  </body>\n</html>\n');
                done();
            });
        });

        it('accès aux helpers déf dans $config.$view.helpers', (done) => {

            const view = new View(basepath + 'templates/index/index.html');
            view.kernel().get('$config').set('$view', {
                helpers: { message: 'Salut', foobar: 'test' }
            });
            view.$render().then(render => {
                expect(render).to.be.equal(
                    '<h1>Hello World!</h1>\n\n<p>Salut</p>\n\n<div>\n  <p>\n  '
                    + 'Ici, nous avons un bloc inclu dans une autre vue !\n</p>\n<p>\n  '
                    + 'Il peut également utiliser les données transmises à la vue : test'
                    + '\n</p>\n\n\n</div>\n\n'
                );
                done();
            });
        });
    });

    describe('helpers', () => {

        it('$getSlot existe', (done) => {

            const req =  new http.IncomingMessage(new Socket()),
                  resp = new Resp(new http.ServerResponse(req), req);
            const view = new View(basepath + 'templates/index/test.html');
            view.$request = req;
            view.$response = resp;
            view.$user = new User(req, resp, new Sessions(new MemoryStorage()));
            view.$render().then(render => {
                expect(render).to.be.equal(
                    'On inclue Slot\nSlot test\n\n'
                );
                done();
            }).catch(e => {
                console.log(e);
                expect(1).to.be.equal(0);
                done();
            });
        });

        it('$getSlot accès à request, response, user', (done) => {

            const req =  new http.IncomingMessage(new Socket()),
                  resp = new Resp(new http.ServerResponse(req), req);
            const view = new View(basepath + 'templates/index/test.html');
            view.$request = req;
            view.$response = resp;
            view.$user = new User(req, resp, new Sessions(new MemoryStorage()));
            view.$render().then(() => {

                expect(view.kernel().get('$config').get('testRequest'))
                    .to.be.equal(view.$request);
                expect(view.kernel().get('$config').get('testResponse'))
                    .to.be.equal(view.$response);
                expect(view.kernel().get('$config').get('testUser'))
                    .to.be.equal(view.$user);
                done();
            });
        });


        describe('$getRoute', () => {

            let view: View,
                req: http.IncomingMessage,
                resp: Resp;

            beforeEach(() => {

                const router = new Router();
                router.load({
                    test: {
                        method: 'get',
                        pattern: '/test.html',
                        controller: 'ctr',
                        action: 'act'
                    },
                    domain: {
                        method: 'get',
                        pattern: '/foobar.html',
                        domain: 'admin.theo-net.*',
                        controller: 'admin',
                        action: 'foo'
                    },
                    complex: {
                        method: 'get',
                        pattern: '/add-{var1}-{var2}.html',
                        requirements: {
                            var1: '\\w+',
                            var2: '\\w+',
                        }
                    }
                });
                view = new View(basepath + 'templates/index/test.html');
                req =  new http.IncomingMessage(new Socket()),
                resp = new Resp(new http.ServerResponse(req), req);
                view.$request = req;
                view.$response = resp;
                view.kernel().container().register('$router', router);
            });

            it('sans paramètres', (done) => {

                view.$setContent('{{$getRoute(\'test\')}}');

                view.$render().then(render => {
                    expect(render).to.be.equal('/test.html');
                    done();
                });
            });

            it('avec paramètres', (done) => {

                view.$setContent(
                    '{{$getRoute(\'complex\', {var1: \'foo\', var2: \'bar\'})}}'
                );

                view.$render().then(render => {
                    expect(render).to.be.equal('/add-foo-bar.html');
                    done();
                });
            });

            it('même domaine', (done) => {

                view.$request.headers.host = 'admin.theo-net.org';
                view.$setContent('{{$getRoute(\'domain\')}}');
                view.$render().then(render => {
                    expect(render).to.be.equal('/foobar.html');
                    done();
                });
            });

            it('url absolue', (done) => {

                view.$request.headers.host = 'www.theo-net.org';
                view.$setContent('{{$getRoute(\'domain\')}}');

                view.$render().then(render => {
                    expect(render)
                        .to.be.equal('http://admin.theo-net.org/foobar.html');
                    done();
                });
            });

            it('HTTPS automatique', (done) => {

                view.$request.headers.host = 'www.theo-net.org';
                view.kernel().get('$config').set('forceHttps', true, true);
                view.$setContent('{{$getRoute(\'domain\')}}');

                view.$render().then(render => {
                    expect(render)
                        .to.be.equal('https://admin.theo-net.org/foobar.html');
                    done();
                });
            });
        });
    });

    describe('$setLayout', () => {

        it('charge le fichier passé en paramère', () => {

            const view = new View(basepath + 'templates/index/index.html');
            view.$setLayout(basepath + 'templates/layout.html');

            expect(view.$getLayoutSource()).to.be.equal(
                fs.readFileSync(basepath + 'templates/layout.html').toString());
        });

        it('lève une exception si le fichier ne peut être chargé', () => {

            const view = new View(basepath + 'templates/index/index.html');

            expect(view.$setLayout.bind({}, 'controller/laout.html'))
                .to.throw('Impossible de charger le layout controller/laout.html');
        });

        it('false désactive le layout', () => {

            const view = new View(basepath + 'templates/index/index.html');
            view.$setLayout(basepath + 'templates/layout.html');
            view.$setLayout(false);

            expect(view.$getLayout()).to.be.false;
        });

        it('on peut inclure un fichier', () => {

            const view = new View(basepath + 'templates/index/index.html');
            view.$setLayout(basepath + 'templates/layout.html');
            const source = view.$getLayoutSource();

            view.$setLayout(basepath + 'templates/layoutBis.html');

            expect(view.$getLayoutSource()).to.be.equal('foo' + source + 'bar\n');
        });
    });
});

