/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as http from 'http';
import * as sinon from 'sinon';
import { Socket } from 'net';

import { Color } from '../cli/Color';
import { Controller } from './Controller';
import { IndexController } from '../../docs/demo-server/src/Controller/IndexController';
import { Kernel } from '../core/Kernel';
import { MemoryStorage } from './User/MemoryStorage';
import { Resp } from './Resp';
import { Sessions } from './User/Sessions';
import { Slot } from './Slot';
import { SlotError } from './SlotError';
import { StaticController } from './StaticController';
import { User } from './User/User';
import { View  } from './View';


describe('server/Slot', () => {

    let kernel: Kernel,
        request: http.IncomingMessage,
        response: Resp;

    before(() => {

        // On force une nouvelle instance
        kernel = new Kernel(true);
        const basepath = process.cwd() + '/docs/demo-server/';
        kernel.get('$config').set('basepath', basepath);
        kernel.get('$config').set('controllerDir', 'src/Controller/');
        kernel.container().register('$color', new Color());
    });

    beforeEach(() => {

        request = new http.IncomingMessage(new Socket()),
        response = new Resp(new http.ServerResponse(request), request);
    });

    it('charge un controller et l\'action correspondante', () => {

        const vars = { a: 1, b: 2 };
        const slot = new Slot('index', 'index', vars, request, response);

        expect(slot.getControllerName()).to.be.equal('Index');
        expect(slot.getController()).to.be.instanceof(IndexController);
        expect(slot.getAction()).to.be.equal('index');
        expect(slot.getVars()).to.be.equal(vars);
        expect(slot.getRequest()).to.be.equal(request);
        expect(slot.getResponse()).to.be.equal(response);
    });

    it('exécute un controller interne si prefix @', () => {

        const slot = new Slot('@static', '', {}, request, response);

        expect(slot.getControllerName()).to.be.equal('@static');
        expect(slot.getController()).to.be.instanceof(StaticController);
    });

    it('on peut transmettre un objet comme controller', () => {

        class Ctrl extends Controller {
            indexAction (): void { return; }
            setViews (): Record<string, string | null> { return { index: null }; }
        }

        const slot = new Slot(Ctrl, 'index', {}, request, response);
        expect(slot.getControllerName()).to.be.equal('[object]');
        expect(slot.getController()).to.be.instanceof(Ctrl);
    });

    it('lance une exception si le controller n\'est pas trouvé', () => {

        const exec = (): void => {
            new Slot('salut', '', {}, request, response);
        };

        expect(exec.bind({})).to.throw(SlotError, 'Controller `Salut` not found');
    });

    it('lance une exception si l\'action n\'est pas trouvée', () => {

        const exec = (): void => {
            new Slot('index', 'foobar', {}, request, response);
        };

        expect(exec.bind({}))
            .to.throw(SlotError, 'Action `foobar` of controller `Index` not found');
    });

    it('si slot(... user), le transmet', () => {

        const vars = { a: 1, b: 2 },
              user = new User(request, response, new Sessions(new MemoryStorage()));
        const slot = new Slot('index', 'index', vars, request, response, user);

        expect(slot.getController().$user).to.be.equal(user);
    });

    it('l\'objet réponse reçoit cet objet User', () => {

        const vars = { a: 1, b: 2 },
              user = new User(request, response, new Sessions(new MemoryStorage()));
        const slot = new Slot('index', 'index', vars, request, response, user);

        expect(slot.getResponse().$user).to.be.equal(user);
    });

    it('associe une vue au Slot', () => {

        const slot = new Slot('index', 'index', {}, request, response);

        expect(slot.getView()).to.be.instanceof(View);
        expect((slot.getView() as View).$getContentSource())
            .to.be.equal(
                '<h1>Hello World!</h1>\n\n<p>{{message}}</p>\n\n<div>\n  <p>\n  '
                + 'Ici, nous avons un bloc inclu dans une autre vue !\n</p>\n<p>\n  '
                + 'Il peut également utiliser les données transmises à la vue : '
                + '{{foobar}}\n</p>\n\n\n</div>\n\n'
            );
        expect(slot.getController().$view).to.be.equal(slot.getView());
    });

    it('transmet à la vue : requête, réponse, user', () => {

        const slot = new Slot('index', 'index', {}, request, response);

        expect((slot.getController().$view as View).$request)
            .to.be.equal(slot.getController().$request);
        expect((slot.getController().$view as View).$response)
            .to.be.equal(slot.getController().$response);
        expect((slot.getController().$view as View).$user)
            .to.be.equal(slot.getController().$user);
    });

    it('peut associer une vue vide si demandée', () => {

        const slot = new Slot('index', 'blank', {}, request, response);

        expect(slot.getView()).to.be.instanceof(View);
        expect((slot.getView() as View).$getContentSource()).to.be.equal('');
    });

    it('associe une vue spécifique au Slot', () => {

        const slot = new Slot('index', 'alias', {}, request, response);

        expect(slot.getView()).to.be.instanceof(View);
        expect((slot.getView() as View).$getContentSource())
            .to.be.equal(
                '<h1>Hello World!</h1>\n\n<p>{{message}}</p>\n\n<div>\n  <p>\n  '
                + 'Ici, nous avons un bloc inclu dans une autre vue !\n</p>\n<p>\n  '
                + 'Il peut également utiliser les données transmises à la vue : '
                + '{{foobar}}\n</p>\n\n\n</div>\n\n'
            );
        expect(slot.getController().$view).to.be.equal(slot.getView());
    });

    it('charge un layout si défini dans la config', () => {

        kernel.get('$config').set('layout.html', 'templates/layout.html');
        const slot = new Slot('index', 'alias', {}, request, response);

        expect((slot.getController().$view as View).$getLayoutSource())
            .to.be.equal('<!DOCTYPE html>\n\n'
                + '<html>\n  <head>\n    <meta charset="utf-8" />\n'
                + '    <title>{{title}} - Démo</title>\n'
                + '    <link rel="stylesheet" href="./styles.css" />\n'
                + '  </head>\n\n  <body>\n    '
                + '{{content}}'
                + '\n  </body>\n</html>\n');
    });

    it('exécute le slot', async () => {

        const vars = { a: 1, b: 2 };
        const slot = new Slot('index', 'index', vars, request, response);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const spy = sinon.spy(slot.getController() as any, 'indexAction');

        const execute = slot.execute();
        expect(execute).to.be.instanceOf(Promise);

        return execute.then(() => {
            expect(spy.calledOnce).to.be.true;
        });
    });

    it('exécute la ressource si le controller en est une', async () => {

        const slot = new Slot(
            '@static', '',
            { file: 'test.css' },
            request, response
        );
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const spy = sinon.spy(slot.getController() as any, 'slotAction');

        return slot.execute().catch(() => {
            expect(spy.calledOnce).to.be.true;
        });
    });

    it('si User.init() Promise, exec que lors du resolve', async () => {

        const vars = { a: 1, b: 2 };
        class MyObject extends User {
            init (): Promise<never> {
                return new Promise(resolve => {
                    setTimeout(() => {
                        this.set('foo', 'bar');
                        resolve();
                    }, 100);
                });
            }
        }
        kernel.get('$config').set('$userObj', MyObject);
        const slot = new Slot('index', 'index', vars, request, response);
        kernel.get('$config').delete('$userObj');

        await slot.execute();
        expect((slot.getController() as IndexController).foo).to.be.equal('bar');
    });

    it('si un init() existe, il est exécuté', async () => {

        const vars = { a: 1, b: 2 };
        const slot = new Slot('index', 'index', vars, request, response);
        slot.getController().init = (): void => { return; };
        const spy = sinon.spy(slot.getController(), 'init');

        return slot.execute().then(() => {
            expect(spy.calledOnce).to.be.true;
        });
    });

    it('si un init() peut être asynchrone', async () => {

        const vars = { a: 1, b: 2 };
        let foo = '';
        const slot = new Slot('index', 'index', vars, request, response);
        slot.getController().init = (): Promise<never> => {
            return new Promise(resolve => {
                foo = 'bar';
                resolve();
            });
        };
        const spy = sinon.spy(slot.getController(), 'init');

        return slot.execute().then(() => {
            expect(spy.calledOnce).to.be.true;
            expect(foo).to.be.equal('bar');
        });
    });

    it('l\'init reçoit les paramètres', async () => {

        const vars = { a: 1, b: 2 };
        const slot = new Slot('index', 'index', vars, request, response);
        slot.getController().init = (): void => { return; };
        const spy = sinon.spy(slot.getController(), 'init');

        return slot.execute().then(() => {
            expect(spy.calledWith(vars)).to.be.true;
        });
    });

    it('l\'action peut être asynchrone', async () => {

        const slot = new Slot('index', 'async', {}, request, response);

        return slot.execute().then(() => {
            expect((slot.getController() as IndexController).okAsync).to.be.true;
        });
    });

    it('l\'action reçoit les paramètres', async () => {

        const vars = { a: 1, b: 2 };
        const slot = new Slot('index', 'index', vars, request, response);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const spy = sinon.spy(slot.getController() as any, 'indexAction');

        return slot.execute().then(() => {
            expect(spy.calledWith(vars)).to.be.true;
        });
    });

    it('l\'action reçoit les data POST', async () => {

        const vars = { a: 1, b: 2 };
        const slot = new Slot('index', 'index', vars, request, response);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const spy = sinon.spy(slot.getController() as any, 'indexAction');

        return slot.execute({ c: 'test' }).then(() => {
            expect(spy.calledWith({ a: 1, b: 2, c: 'test' })).to.be.true;
        });
    });

    it('if async action returns reject(code), send error', async () => {

        class Ctrl extends Controller {
            indexAction (): void { return; }
            error404Action (): Promise<never> { return Promise.reject(404); }
            errorAction (): Promise<never> { return Promise.reject('Un message'); }
            setViews (): Record<string, string | null> {
                return {
                    index: null,
                    error404: null,
                    error: null
                };
            }
        }

        let anError = 0;
        const resp = new Resp(new http.ServerResponse(request), request);
        resp.sendError404 = (): Promise<number> => {
            anError = 404; return Promise.resolve(anError);
        };
        resp.sendError500 = (): Promise<number> => {
            anError = 500; return Promise.resolve(anError);
        };

        const slot1 = new Slot(Ctrl, 'error404', {}, request, resp);
        const slot2 = new Slot(Ctrl, 'error', {}, request, resp);

        return slot1.execute().catch((code: number) => {

            expect(code).to.be.equal(404);
            expect(anError).to.be.equal(404);

            return slot2.execute().catch((code: number) => {

                expect(code).to.be.equal(500);
                expect(anError).to.be.equal(500);
            });
        });
    });

    it('notifie le canal `Slot` lorsqu\'un slot est initialisé', () => {

        kernel.events().register('Slot');
        const spy = sinon.spy();
        kernel.events().attach('Slot', 'spy', spy);

        const slot = new Slot('index', 'index', {}, request, response);

        expect(spy.getCall(0).args[0]).to.be.equal(
            'Slot ' + slot.getControllerName() + ':' + slot.getAction() + ' initialisé'
        );
    });
});

