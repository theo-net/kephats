/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as http from 'http';
import * as sinon from 'sinon';
import { Socket } from 'net';

import { Config } from '../core/Services/Config';
import { Controller } from './Controller';
import { Kernel } from '../core/Kernel';
import { MemoryStorage } from './User/MemoryStorage';
import { Resp } from './Resp';
import { Sessions } from './User/Sessions';
import { StaticController } from './StaticController';
import { User } from './User/User';
import { View  } from './View';


describe('server/StaticController', () => {

    let staticCtrl: StaticController,
        kernel: Kernel,
        basepath: string,
        spy: sinon.SinonSpy,
        staticCache: Config,
        request: http.IncomingMessage;

    beforeEach(() => {

        kernel = new Kernel(true);
        basepath = process.cwd() + '/docs/demo-server/';
        kernel.get('$config').set('basepath', basepath);
        kernel.get('$config').set('staticDir', 'public/');
        kernel.container().register('$staticCache', new Config());
        staticCache = kernel.get('$staticCache');
        request = new http.IncomingMessage(new Socket());

        staticCtrl = new StaticController(
            kernel, request, new Resp(new http.ServerResponse(request), request)
        );
        staticCtrl.$view = new View(null);
        staticCtrl.init();

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        spy = sinon.spy(staticCtrl as any, '_staticFile');
    });

    it('is Controller', () => {

        expect(staticCtrl).to.be.instanceOf(Controller);
    });

    it('récupère le staticDir depuis la config', () => {

        expect(staticCtrl.getStaticDir()).to.be.equal('public/');
    });

    it('on peut le préciser dans les variables', async () => {

        return staticCtrl.slotAction({ staticDir: 'foobar', file: '' }).catch(() => {
            expect(staticCtrl.getStaticDir()).to.be.equal('foobar');
        });
    });

    it('pas de layout !', async () => {

        return staticCtrl.slotAction({ file: '' }).catch(() => {
            expect(staticCtrl.$view.$getLayout()).to.be.equal(false);
        });
    });

    it('envoi fichier s\'il existe (semi-test)', async () => {

        return staticCtrl.slotAction({ file: 'styles.css' }).then(() => {
            expect(spy.calledWith(basepath + 'public/styles.css')).to.be.true;
        });
    });

    it('envoi dir/index.html si répertoire demandé', async () => {

        return staticCtrl.slotAction({ file: 'dir' }).then(() => {
            expect(spy.calledWith(basepath + 'public/dir/index.html')).to.be.true;
        });
    });

    it('reject 404 si fichier inexistant', async () => {

        return staticCtrl.slotAction({ file: 'foobar' }).catch((code) => {
            expect(code).to.be.equal(404);
        });
    });

    it('reject 404 si dir/index.html inexistant', async () => {

        return staticCtrl.slotAction({ file: '' }).catch((code) => {
            expect(code).to.be.equal(404);
        });
    });

    it('enregistre dans `_staticCache` le path du fichier demandé', async () => {

        return staticCtrl.slotAction({ file: 'styles.css' }).then(() => {

            expect(staticCache.has(basepath + 'public/styles.css')).to.be.true;
        });
    });

    it('ETag et date du premier accès sauvés', async () => {

        return staticCtrl.slotAction({ file: 'styles.css' }).then(() => {

            const cache = staticCache.get(basepath + 'public/styles.css'),
                  now = new Date();
            expect(cache.lastModified.getUTCSeconds())
                .to.be.equal(now.getUTCSeconds());
            expect(cache.etag).to.be.not.equal(undefined);
        });
    });

    it('transmet un header Last-Modified et un header ETag', async () => {

        return staticCtrl.slotAction({ file: 'styles.css' }).then(() => {

            const cache = staticCache.get(basepath + 'public/styles.css');
            const result = staticCtrl.$response.getHeaders();
            result['set-cookie'] = [];
            expect(result).to.be.deep.equal({
                'content-type': 'text/css',
                'last-modified': cache.lastModified.toUTCString(),
                etag: cache.etag,
                'set-cookie': []
            });
        });
    });

    it('si changement date et/ou etag renvoit fichier', async () => {

        request.headers['if-modified-since'] = 'Wed, 08 Nov 2017 08:45:33 +0000';
        request.headers['if-none-match'] = 'foobar';

        return staticCtrl.slotAction({ file: 'styles.css' }).then(() => {

            expect(staticCtrl.$response.getInternalResponse().statusCode).to.be.equal(200);
            expect(spy.calledWith(basepath + 'public/styles.css')).to.be.true;
        });
    });

    it('si pas changement date et etag envoit 304', async () => {

        return staticCtrl.slotAction({ file: 'styles.css' }).then(() => {

            return Promise.resolve(staticCache.get(basepath + 'public/styles.css'));
        }).then(cache => {

            request.headers['if-modified-since'] = cache.lastModified.toUTCString();
            request.headers['if-none-match'] = cache.etag;

            staticCtrl.$response = new Resp(new http.ServerResponse(request), request);

            return staticCtrl.slotAction({ file: 'styles.css' }).then(() => {

                expect(staticCtrl.$response.getHeaders()).to.be.deep.equal({
                    'last-modified': cache.lastModified.toUTCString(),
                    etag: cache.etag,
                    'set-cookie': []
                });
                expect(staticCtrl.$response.getInternalResponse().statusCode).to.be.equal(304);
                expect(spy.calledOnce).to.be.true;
            });
        });
    });

    it('les cookies sont bien envoyés', async () => {

        staticCtrl.$response._cookies = [];
        const user = new User(request, staticCtrl.$response, new Sessions(new MemoryStorage())),
              cookie = ['kts_session_id=' + user.getSessionId() + ';HttpOnly'];
        staticCtrl.$user = user;

        return staticCtrl.slotAction({ file: 'styles.css' }).then(() => {

            const cache = staticCache.get(basepath + 'public/styles.css');
            expect(staticCtrl.$response.getHeaders()).to.be.deep.equal({
                'content-type': 'text/css',
                'last-modified': cache.lastModified.toUTCString(),
                etag: cache.etag,
                'set-cookie': cookie
            });
        });
    });

    it('cookie également envoyé pour un fichier en cache', async () => {

        return staticCtrl.slotAction({ file: 'styles.css' }).then(() => {

            return Promise.resolve(staticCache.get(basepath + 'public/styles.css'));
        }).then(cache => {

            request.headers['if-modified-since'] = cache.lastModified.toUTCString();
            request.headers['if-none-match'] = cache.etag;

            staticCtrl.$response = new Resp(new http.ServerResponse(request), request);

            const user = new User(request, staticCtrl.$response, new Sessions(new MemoryStorage())),
                  cookie = ['kts_session_id=' + user.getSessionId() + ';HttpOnly'];
            staticCtrl.$user = user;

            return staticCtrl.slotAction({ file: 'styles.css' }).then(() => {

                expect(staticCtrl.$response.getHeaders()).to.be.deep.equal({
                    'last-modified': cache.lastModified.toUTCString(),
                    etag: cache.etag,
                    'set-cookie': cookie
                });
            });
        });
    });
});

