/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as http from 'http';
import { Socket } from 'net';

import { Resp } from './Resp';

describe('server/Resp', () => {

    it('setContent définit l\'header correspondant', () => {

        const req =  new http.IncomingMessage(new Socket()),
              resp = new Resp(new http.ServerResponse(req), req);
        resp.setContentType('application/json');

        expect(resp.getHeader('Content-Type')).to.be.equal('application/json');
    });

    it('setStatusCode définit le code de retour', () => {

        const req =  new http.IncomingMessage(new Socket()),
              resp = new Resp(new http.ServerResponse(req), req);
        resp.setStatusCode(2);

        expect(resp.getStatusCode()).to.be.equal(2);
    });
});
