/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { assert, expect } from 'chai';
import * as fs from 'fs';
import * as path from 'path';

import { Application } from '../core/Application';
import * as CoreFramework from '../core/Framework';
import * as Framework from './Framework';
import * as Utils from './Utils';

describe('cli/Framework', () => {

    it('extends `cli/Utils`', () => {

        assert.containsAllKeys(Framework, Utils);
    });

    it('extends `core/Framework`', () => {

        assert.containsAllKeys(Framework, CoreFramework);
    });

    it('app() retourne une nouvelle instance d\'Application', () => {

        expect(Framework.app()).to.be.instanceOf(Application);
        expect(Framework.app()).to.be.not.equal(Framework.app());
    });

    it('app() transmet le kernel à l\'application', () => {

        expect(Framework.app().kernel()).to.be.equal(Framework.kernel());
    });

    it('app() enregistre l\'applocation comme service', () => {

        const app = Framework.app();
        expect(app.kernel().get('$application')).to.be.equal(app);
    });

    it('app() charge les variables d\'environnement', () => {

        process.env.FOO = 'bar';
        const app = Framework.app();
        expect(app.kernel().get('$config').get('FOO')).to.be.equal('bar');
    });

    it('app() charge les variables d\'environnement depuis un .env', (done) => {

        fs.writeFile(path.resolve(process.cwd(), '.env'), 'BAR=foo', (err) => {
            if (err) throw err;
            const app = Framework.app();
            expect(app.kernel().get('$config').get('BAR')).to.be.equal('foo');
            done();
        });
    });

    it('app() les variables d\'un .env n\'écrasent pas les autres variables d\'env', (done) => {

        process.env.FOO = 'bar';
        fs.writeFile(path.resolve(process.cwd(), '.env'), 'FOO=foo', (err) => {
            if (err) throw err;
            const app = Framework.app();
            expect(app.kernel().get('$config').get('FOO')).to.be.equal('bar');
            done();
        });
    });
});
