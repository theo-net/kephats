/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { parseDotEnv } from './DotEnv';

describe('cli/DotEnv/DotEnv', () => {

    it('trim value name', () => {

        expect(parseDotEnv(' FOO =bar')).to.be.deep.equal({FOO: 'bar'});
    });

    it('parse unquoted string value', () => {

        expect(parseDotEnv('FOO=bar')).to.be.deep.equal({FOO: 'bar'});
    });

    it('trim unquoted string', () => {

        expect(parseDotEnv('FOO=  bar ')).to.be.deep.equal({FOO: 'bar'});
    });

    it('parse quoted string value', () => {

        expect(parseDotEnv('FOO=\'bar\'')).to.be.deep.equal({FOO: 'bar'});
        expect(parseDotEnv('FOO="bar"')).to.be.deep.equal({FOO: 'bar'});
    });

    it('parse correct escape cahr in quoted string value', () => {

        expect(parseDotEnv("FOO='b\\'ar'")).to.be.deep.equal({FOO: "b'ar"});
        expect(parseDotEnv('FOO=\'b"ar\'')).to.be.deep.equal({FOO: "b\"ar"});
        expect(parseDotEnv('FOO="b\'ar"')).to.be.deep.equal({FOO: "b'ar"});
        expect(parseDotEnv('FOO="b\\"ar"')).to.be.deep.equal({FOO: "b\"ar"});
        expect(parseDotEnv("FOO='b\\'a\\'r'")).to.be.deep.equal({FOO: "b'a'r"});
    });

    it('parse number', () => {

        expect(parseDotEnv('FOO=256')).to.be.deep.equal({FOO: 256});
        expect(parseDotEnv('FOO=2.5')).to.be.deep.equal({FOO: 2.5});
        expect(parseDotEnv('FOO=2e5')).to.be.deep.equal({FOO: 2e5});
    });

    it('parse boolean', () => {

        expect(parseDotEnv('FOO=true')).to.be.deep.equal({FOO: true});
        expect(parseDotEnv('FOO=false')).to.be.deep.equal({FOO: false});
    });

    it('parse null', () => {

        expect(parseDotEnv('FOO=null')).to.be.deep.equal({FOO: null});
    });

    it('empty value is empty string', () => {

        expect(parseDotEnv('FOO= ')).to.be.deep.equal({FOO: ''});
    });

    it('parse multi-line', () => {

        expect(parseDotEnv('FOO=bar\nBAR=foo')).to.be.deep.equal({FOO: 'bar', BAR: 'foo'});
    });

    it('throw error if no var assignation', () => {

        expect(() => { parseDotEnv('FOO=bar\nFOObar'); })
            .to.throw('Syntax error "=" not found in ligne 2');
    });

    it('ignore empty line', () => {

        expect(parseDotEnv('FOO=bar\n\n  \n\t\nBAR=foo\n '))
            .to.be.deep.equal({FOO: 'bar', BAR: 'foo'});
    });

    it('ignore comment', () => {

        expect(parseDotEnv('FOO=bar\n# a comment\nBAR=foo\n #an other  '))
            .to.be.deep.equal({FOO: 'bar', BAR: 'foo'});
    });
});
