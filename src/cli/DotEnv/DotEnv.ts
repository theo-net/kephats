/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as Utils from '../Utils';

/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * Parse une valeur et la retourne selon le bon type (`string`, `number`, `boolean`, ...)
 * @param value Valeur à parser
 */
function _parsevalue (value: string): any {

    // null
    if (value === 'null')
        return null;
    // boolean
    else if (value === 'true')
        return true;
    else if (value === 'false')
        return false;
    // number
    else if (Utils.isNumber(value, true))
        return Number(value);

    // Cas par défaut : une chaîne de caractère
    let quote: string | boolean = false;
    if (value.charAt(0) === '"' && value.charAt(value.length - 1) === '"')
        quote = '"';
    else if (value.charAt(0) === "'" && value.charAt(value.length - 1) === "'")
        quote = "'";

    if (quote) {

        value = value.substring(1, value.length - 1);
        value = value.replace(RegExp('\\\\' + quote, 'g'), quote);
    }

    return value;
}


/**
 * Parse le contenu d'un `.env` et le retourne sous forme de JSON
 *
 * Les règles suivantes sont appliquées :
 *
 *   - `BASIC=basic` devient `{ BASIC: 'basic' }`
 *   - les lignes vides sont ignorées
 *   - les lignes débutant par `#` sont traitées comme des commentaires
 *   - les valeurs vide sont traitées comme une chaîne vide (`EMPTY=` devient `{ EMPTY: "" }`)
 *   - les espaces sont suprimé en début et fin de chaîne sans guillemets (`FOO= some value` devient `{ FOO: "some value" }`)
 *   - les guillemets simples deviennent des doubles (on échappe le contenu)
 *   - le contenu est traité comme du JSON
 *
 * Les types suivants seront reconnus : `number`, `string`, `boolean`.
 * @param content Contenu à parser
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function parseDotEnv (content: string): Record<string, any> {

    const lines = content.split('\n');
    const result: Record<string, any> = {};

    lines.forEach((line, index) => {

        line = line.trim();

        // Empty line
        if (/^\s*$/.test(line))
            return;
        // Comment
        if (/^#/.test(line))
            return;

        const equalIndex = line.indexOf('=');
        const varName = line.substring(0, equalIndex).trim();

        if (equalIndex === -1)
            throw new Error('Syntax error "=" not found in ligne ' + (index + 1));

        result[varName] = _parsevalue(line.substring(equalIndex + 1).trim());
    });

    return result;
}
