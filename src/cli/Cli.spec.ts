/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as sinon from 'sinon';

import { Application } from '../core/Application';
import { Cli } from './Cli';
import { Color } from './Color';
import { Command } from './Cli/Command';
import { Kernel } from '../core/Kernel';
import { ParseArgs } from './Cli/ParseArgs';
import { Table } from './Table/Table';

describe('cli/Cli', () => {

    let cli: Cli;
    let exit: sinon.SinonStub;

    before(() => {

        exit = sinon.stub(process, 'exit').withArgs(2);
    });

    after(() => {

        (process.exit as unknown as sinon.SinonStub).restore();
    });

    beforeEach(() => {

        cli = new Cli(new Kernel());
    });


    it('étend Application', () => {

        expect(cli).to.be.instanceof(Application);
    });

    it('enregistre des services', () => {

        expect(cli.kernel().container().has('$color')).to.be.true;
        expect(cli.kernel().container().get('$color')).to.be.instanceOf(Color);
    });

    it('renvoit le service color', () => {

        expect(cli.color()).to.be.an.instanceof(Color);
    });

    describe('logs', () => {

        let consoleLog: sinon.SinonStub, consoleError: sinon.SinonStub;

        before(() => {

            consoleLog = sinon.stub(console, 'log');
            consoleError = sinon.stub(console, 'error');
        });

        after(() => {

            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            (console.log as any).restore();
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            (console.error as any).restore();
        });

        it('personnalise le rendu des messages de log', () => {

            const logs = cli.kernel().get('$logs'),
                  color = cli.kernel().get('$color');
            cli.kernel().get('$config').set('logLevel', 'debug', true);

            logs.log('msg info', 'info');
            expect(consoleLog.calledWith(color.blue('Info: msg info'))).to.be.true;

            logs.log('msg success', 'success');
            expect(consoleLog.calledWith(color.green('msg success'))).to.be.true;

            logs.log('msg warning', 'warning');
            expect(consoleLog.calledWith(color.yellow('Warning: msg warning'))).to.be.true;

            logs.log('msg error', 'error');
            expect(consoleError.calledWith('msg error')).to.be.true;

            logs.log('msg debug', 'debug');
            expect(consoleLog.calledWith(color.cyan('msg debug'))).to.be.true;

            logs.log('msg data', 'data');
            expect(consoleLog.calledWith(color.magenta('msg data'))).to.be.true;
        });
    });

    it('parser enregistré', () => {

        expect(cli.parseArgs).to.be.instanceof(ParseArgs);
    });

    describe('newTable', () => {

        it('retourne une nouvelle instance de `Table`', () => {

            expect(cli.newTable()).to.be.an.instanceof(Table);
        });

        it('transmet les options au tableau', () => {

            const table = cli.newTable({ style: { border: ['foo'], head: ['bar'] } });

            expect(table.getOptions().style.border[0]).to.equal('foo');
            expect(table.getOptions().style.head[0]).to.equal('bar');
        });

        it('Demande le style `noBorder`', () => {

            const table = cli.newTable(null, 'noBorder');

            expect(table.getOptions().chars).to.be.deep.equal({
                bottom: '', 'bottom-left': '', 'bottom-mid': '', 'bottom-right': '',
                left: '', 'left-mid': '', mid: '', 'mid-mid': '', middle: '',
                right: '', 'right-mid': '',
                top: '', 'top-left': '', 'top-mid': '', 'top-right': ''
            });
        });
    });


    describe('description', () => {

        it('définit la description de l\'application', () => {

            cli.description('Ma superbe app');
            expect(cli.description()).to.be.equal('Ma superbe app');
        });

        it('si une description est définit, retourne l\'app', () => {

            expect(cli.description('Une app')).to.be.equal(cli);
        });

        it('retourne la description de l\'application', () => {

            cli.description('Une app');
            expect(cli.description()).to.be.equal('Une app');
        });
    });

    describe('command', () => {

        it('créé une nouvelle commande et la retourne', () => {

            const command = cli.command('test', 'un test');

            expect(cli.getCommands().slice()[0]).to.be.equal(command);
        });
    });


    describe('action', () => {

        beforeEach(() => {

            cli = new Cli(new Kernel());
        });

        it('ajoute l\'action à la commande par défaut', () => {

            cli.setDefaultCommand(cli.command('test', 'lol'));
            const fct = (): void => { return; };
            cli.action(fct);

            expect((cli.getDefaultCommand() as Command)._action).to.be.equal(fct);
        });

        it('créé une commande par défaut si besoin', () => {

            const fct = (): void => { return; };
            cli.action(fct);

            expect(cli.getDefaultCommand()).to.be.instanceof(Command);
            expect((cli.getDefaultCommand() as Command)._action).to.be.equal(fct);
        });

        it('retourne l\'application', () => {

            expect(cli.action((): void => { return; })).to.be.equal(cli);
        });
    });


    describe('run', () => {

        let log: sinon.SinonStub;

        before(() => {

            log = sinon.stub(console, 'log');
        });

        after(() => {

            (console.log as unknown as sinon.SinonStub).restore();
        });

        beforeEach(() => {

            cli = new Cli(new Kernel());
            cli.command('test');
        });

        it('appel help si demandé ou si la commande demandée n\'existe pas',
            () => {

                const spy = sinon.spy(cli, 'help');

                cli.run(['truc']);
                expect(spy.calledOnce).to.be.true;
                cli.command('order');
                cli.run(['order']);
                expect((cli.help as sinon.SinonSpy).calledOnce).to.be.true;
                cli.run(['--help']);
                expect((cli.help as sinon.SinonSpy).calledTwice).to.be.true;
                cli.run(['-h']);
                expect((cli.help as sinon.SinonSpy).calledThrice).to.be.true;
                cli.run(['help', 'arg1', 'arg2', '-o']);
                expect((cli.help as sinon.SinonSpy).calledWith('arg1 arg2')).to.be.true;
            });

        it('print version of script', () => {

            cli.run(['-V']);
            expect(log.calledWith(cli.version())).to.be.true;
            cli.run(['--version']);
            expect(log.calledWith(cli.version())).to.be.true;
        });

        it('set quiet mode', () => {

            cli.run(['test', '--quiet']);
            expect(cli.kernel().get('$config').get('logLevel')).to.be.equal('warn');
            cli.run(['test', '--silent']);
            expect(cli.kernel().get('$config').get('logLevel')).to.be.equal('warn');
        });

        it('set verbose mode', () => {

            cli.run(['test', '-v']);
            expect(cli.kernel().get('$config').get('logLevel')).to.be.equal('debug');
            cli.run(['test', '--verbose']);
            expect(cli.kernel().get('$config').get('logLevel')).to.be.equal('debug');
        });

        it('set no-color mode', () => {

            cli.run(['test']);
            expect(cli.color().blue('test')).to.be.equal('\u001b[34mtest\u001b[39m');
            cli.run(['test', '--no-color']);
            expect(cli.color().blue('test')).to.be.equal('test');
            // On rétabli les couleurs
            cli.color().enableColor(true);
            expect(cli.color().blue('test')).to.be.equal('\u001b[34mtest\u001b[39m');
        });
    });

    describe('help', () => {

        before(() => {

            sinon.stub(console, 'log');
        });

        after(() => {

            (console.log as unknown as sinon.SinonStub).restore();
        });

        beforeEach(() => {

            cli = new Cli(new Kernel());
            sinon.spy(cli.getHelp(), 'display');
        });

        it('appel Help.display() avec la bonne commande', () => {

            const cmd = cli.command('test', 'un test');

            cli.run(['help', 'test']);
            expect((cli.getHelp().display as sinon.SinonSpy).calledWithExactly(cmd)).to.be.true;
        });

        it('fonctionne avec un alias', () => {

            const cmd = cli.command('test', 'un test');
            cmd.alias('lol');

            cli.run(['help', 'lol']);
            expect((cli.getHelp().display as sinon.SinonSpy).lastCall.calledWithExactly(cmd))
                .to.be.true;
        });

        it('si la commande n\'existe pas : undefined', () => {

            cli.run(['help', 'mince']);
            expect((cli.getHelp().display as sinon.SinonSpy).calledWithExactly(undefined))
                .to.be.true;
        });

        it('si aucune commande n\'est demandée : undefined', () => {

            cli.run(['help']);
            expect((cli.getHelp().display as sinon.SinonSpy).calledWithExactly(undefined))
                .to.be.true;
        });
    });


    describe('fatalError', () => {

        before(() => {

            sinon.stub(console, 'log');
        });

        after(() => {

            (console.log as unknown as sinon.SinonStub).restore();
        });

        it('called `process.exit(2)`', () => {

            exit.reset();
            cli.fatalError(new Error('test'));
            expect(exit.called).to.be.true;
        });
    });
});
