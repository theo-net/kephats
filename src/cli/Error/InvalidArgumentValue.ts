/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Argument } from '../Cli/Argument';
import { BaseError } from './BaseError';
import { Cli } from '../Cli';
import { Command } from '../Cli/Command';

export class InvalidArgumentValueError extends BaseError {

    constructor (arg: Argument, value: string, command: Command, cli: Cli) {

        let msg = 'Invalid value \'' + value + '\' for argument '
            + cli.color().italic(arg.name()) + '.';
        arg.getValidator().getOccurredErrors().forEach((err: string) => {
            msg += '\n  - ' + err;
        });

        super(msg, { arg, command, value }, cli);
    }
}
