/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


import { Cli } from '../Cli';
import { Command } from '../Cli/Command';
import { BaseError } from './BaseError';
import { getSuggestions, getBoldDiffString } from '../Suggest';

export class UnknownOptionError extends BaseError {

    constructor (option: string, command: Command, cli: Cli) {

        const suggestions = getSuggestions(option, command.getLongOptions());

        let msg = 'Unknown option '
            + cli.color().italic(BaseError.getDashedOption(option)) + '.';

        if (suggestions.length) {
            msg += ' Did you mean ' + suggestions.map(
                s => '--' + getBoldDiffString(option, s)
            ).join(' or maybe ') + ' ?';
        }

        super(msg, { option, command }, cli);
    }
}
