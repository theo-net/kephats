/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { BaseError } from './BaseError';
import { Cli } from '../Cli';
import { Command } from '../Cli/Command';

export class MissingOptionError extends BaseError {

    constructor (option: string, command: Command, cli: Cli) {

        const msg = 'Missing option '
            + cli.color().italic(BaseError.getDashedOption(option))
            + '.';

        super(msg, { option, command }, cli);
    }
}
