/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { BaseError } from './BaseError';
import { Cli } from '../Cli';

export class OptionSyntaxError extends BaseError {

    constructor (synopsis: string, cli: Cli) {

        const msg = 'Syntax error in option synopsis: ' + synopsis;

        super(msg, { synopsis }, cli);
    }
}
