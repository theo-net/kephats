/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Cli } from '../Cli';
import { BaseError as CoreBaseError } from '../../core/Error/BaseError';

export class BaseError extends CoreBaseError {

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    constructor (message: string, meta: any, cli: Cli) {

        const color = cli.color();
        const spaces = '   ';
        const msg = spaces + color.red('Error: ' + message) + '\n' + spaces
            + 'Type ' + color.bold(cli.bin + ' --help') + ' for help.\n';

        super(msg, meta, cli as unknown as string, message);
    }


    /**
     * Retourne le nom de l'option avec `-` ou `--` selon qu'il s'agit d'une option courte ou longue.
     * @param option Nom de l'option
     */
    static getDashedOption (option: string): string {

        if (option.length === 1)
            return '-' + option;
        return '--' + option;
    }
}
