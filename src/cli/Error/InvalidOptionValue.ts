/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { BaseError } from './BaseError';
import { Cli } from '../Cli';
import { Command } from '../Cli/Command';
import { Option } from '../Cli/Option';

export class InvalidOptionValueError extends BaseError {

    constructor (option: Option, value: string, command: Command, cli: Cli) {

        let msg = 'Invalid value \'' + value + '\' for option '
            + cli.color().italic(BaseError.getDashedOption(option.name()))
            + '.';
        option.getValidator().getOccurredErrors().forEach((err: string) => {
            msg += '\n  - ' + err;
        });

        super(msg, { option, command, value }, cli);
    }
}
