/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/*
 Inspiré par la librairie `chalk 1.1.3`
   Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)
*/

import * as Utils from './Utils';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type CodeCache = { [keys: string]: Record<string, any>};

/**
 * Objet permettant personnaliser le rendu de notre sortie dans la console.
 *
 * L'utilisation est très simple :
 *
 *     const color = new Color() ;
 *
 *     console.log(color.blue.bold('Hello World!') ;
 *
 * Lorsque des styles sont combinés, on commence toujours par appliquer le
 * dernier de la liste.
 *
 * Vous pouvez ajouter des styles :
 *
 *     color.addStyle('test', text => { return text + '!' ;}) ;
 *     color.addStyle('theme', text => { return color.yellow.bold(text) ;});
 *
 *     console.log(color.theme.text('Brice de Nice')) ;
 */
export class Color {

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [k: string]: any;

    protected static _codeCache: CodeCache | undefined = undefined;
    protected _enabled: boolean;
    protected _proto = function (): void { return; };
    protected _styles: { [keys: string]: Function } = {};

    /**
     * Si `enable` n'est pas donné, les couleurs seront appliquées si la console les supporte.
     *
     * @param enable Activation des couleurs
     */
    constructor (enable?: boolean) {

        this._enabled = this.supportsColor(enable);

        const styles = {
            reset: [0, 0],
            bold: [1, 22],
            dim: [2, 22],
            italic: [3, 23],
            underline: [4, 24],
            inverse: [7, 27],
            hidden: [8, 28],
            strikethrough: [9, 29],
            black: [30, 39],
            red: [31, 39],
            green: [32, 39],
            yellow: [33, 39],
            blue: [34, 39],
            magenta: [35, 39],
            cyan: [36, 39],
            white: [37, 39],
            grey: [90, 39],
            bgBlack: [40, 49],
            bgRed: [41, 49],
            bgGreen: [42, 49],
            bgYellow: [43, 49],
            bgBlue: [44, 49],
            bgMagenta: [45, 49],
            bgCyan: [46, 49],
            bgWhite: [47, 49]
        };
        // On créé tous les styles
        Utils.forEach(styles, (style, name) => {
            this._styles[name] = this._createStyle(style[0], style[1]);
        }, this);


        // On ajoute les styles comme des méthodes
        // Il y a deux manière de construire notre méthode :
        //   - comme ici quand nous travaillons avec Color
        //   - avec _setProto() pour les objets retournés par les styles
        const ret: { [keys: string]: PropertyDescriptor } = {};
        Utils.forEach(this._styles, (style, name) => {
            ret[name] = {
                get: (): Function => {
                    return this._build([name]);
                }
            };
        });

        // On rend accessible les styles
        Object.defineProperties(this, ret);
        this._setProto(this._styles);
    }


    /**
     * Indique si le terminal supporte les couleurs
     * Le paramètre `enable` permet de forcer le résultat. On peut également forcer le support
     * en définissant la variable d'environnement `FORCE_COLOR` ou avec `COLORTERM`
     *
     * @param enable Force le résultat
     */
    supportsColor (enable?: boolean): boolean {

        if (enable === false)
            return false;

        if ('FORCE_COLOR' in process.env)
            return true;

        if (enable === true)
            return true;

        if (process.stdout && !process.stdout.isTTY)
            return false;

        if (process.platform === 'win32')
            return true;

        if ('COLORTERM' in process.env)
            return true;

        if (process.env.TERM === 'dumb')
            return false;

        if (/^screen|^xterm|^vt100|color|ansi|cygwin|linux/i.test((process.env.TERM as string)))
            return true;

        return false;
    }


    /**
     * Active ou désactive le support des couleurs
     */
    enableColor (enable: boolean): void {

        this._enabled = enable;
    }


    /**
     * Indique si le support des couleurs est activé ou non
     */
    isEnabled (): boolean {

        return this._enabled;
    }


    /**
     * Ajoute un style
     *
     * @param name Nom du style
     * @param style Le style à ajouter
     * @throws Lève une exception si `name` correspond à un style existant ou une méthode de notre
     *          objet.
     */
    addStyle (name: string, style: Function): void {

        if (this[name] !== undefined) {
            throw 'Can\'t has `' + name + '` style because a style already exist '
            + 'or is a forbidden name';
        }
        else {

            this._styles[name] = style;

            // On rend accessible le style
            Object.defineProperty(this, name, {
                get: (): Function => {
                    return this._build([name]);
                }
            });
            this._setProto({ [name]: style });
        }
    }


    /**
     * Helper pour la coloration sur plusieurs lignes
     * En effet, lors de l'affichage, une cellule sur plusieurs lignes doit avoir une unique
     * coloration, sans cet helper, celle-ci serait perturbée par les cellules voisines.
     *
     * @param input Lignes à coloriser
     */
    static colorizeLines (input: string[]): string[] {

        const output: string[] = [];
        let state = {};

        input.forEach(row => {
            const line = this._rewindState(state, row);
            state = this._readState(line);
            const temp = Utils.extend({}, state) as { [keys: string]: string};
            output.push(this._unwindState(temp, line));
        });

        return output;
    }


    /**
     * Retourne une RegExp pour détecter les codes couleurs
     *
     * @param capture La RegExp doit-elle capturer le code ?
     */
    static codeRegex (capture?: boolean): RegExp {

        return capture ?
            /\u001b\[((?:\d*;){0,5}\d*)m/g : /\u001b\[(?:\d*;){0,5}\d*m/g; // eslint-disable-line no-control-regex
    }


    /**
     * Créé un style que l'on pourra ajouter (utilisant les balises bash)
     *
     * @param open Code du début
     * @param close Code de fin
     */
    protected _createStyle (open: number, close: number): Function {

        return (text: string): string => {

            return '\u001b[' + open + 'm' + text + '\u001b[' + close + 'm';
        };
    }

    /**
     * Construit une fonction de stylisation, c'est celle-ci qui permet d'accéder aux styles à la fois
     * comme des fonctions (pour les appliquer à un texte) et comme des méthode (pour chaîner des
     * styles).
     *
     * @param styles Styles appliqués
     */
    protected _build (styles: string[]): Function {

        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const self = this;
        const builder = function (text: string): string {
            return self._applyStyle.call(builder, text, self._styles, self._enabled);
        };
        builder._styles = styles;
        builder.__proto__ = self._proto;

        return builder;
    }

    /**
     * Applique le(s) style(s)
     *
     * @param text Texte à formater
     * @param styles Styles à appliquer
     * @param enabled Couleurs activées
     */
    protected _applyStyle (
        text: string,
        styles: { [keys: string]: Function },
        enabled: boolean
    ): string {

        if (!enabled || !text)
            return text;

        Utils.forEachRight(this._styles, name => {
            text = styles[name](text);
        });

        return text;
    }


    /**
     * Défini le prototype qui sera utilisé par les styles
     *
     * @param styles Liste des styles à ajouter
     */
    protected _setProto (styles: { [keys: string]: Function }): void {

        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const self = this;

        const stylesProto = ((): { [keys: string]: PropertyDescriptor } => {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const ret: Record<string, any> = {};
            Object.keys(styles).forEach(name => {
                ret[name] = {
                    get: function (): Function {
                        return self._build(this._styles.concat(name));
                    }
                };
            });
            return ret;
        })();

        Object.defineProperties(this._proto, stylesProto);
    }


    /**
     * Retourne un object permettant de faire le lien entre les codes d'ouvertures, de fermetures
     * et le nom de certains styles.
     */
    protected static _getCodeCache (): CodeCache {

        if (this._codeCache !== undefined)
            return this._codeCache;

        // Code cache pour certains utilitaires
        const codeCache: CodeCache = {};
        function addToCodeCache (name: string, on: number, off: number): void {
            const onStr = '\u001b[' + on + 'm',
                  offStr = '\u001b[' + off + 'm';
            codeCache[onStr] = {set: name, to: true};
            codeCache[offStr] = {set: name, to: false};
            codeCache[name] = {on: onStr, off: offStr};
        }
        addToCodeCache('bold', 1, 22);
        addToCodeCache('italics', 3, 23);
        addToCodeCache('underline', 4, 24);
        addToCodeCache('inverse', 7, 27);
        addToCodeCache('strikethrough', 9, 29);

        return this._codeCache = codeCache;
    }


    protected static _rewindState (state: { [keys: string]: string}, ret: string): string {

        const lastBackgroundAdded = state.lastBackgroundAdded,
              lastForegroundAdded = state.lastForegroundAdded;

        delete state.lastBackgroundAdded;
        delete state.lastForegroundAdded;

        Utils.forEach(state, (value, key) => {
            if (value)
                ret = this._getCodeCache()[key].on + ret;
        });

        if (lastBackgroundAdded && (lastBackgroundAdded != '\u001b[49m'))
            ret = lastBackgroundAdded + ret;
        if (lastForegroundAdded && (lastForegroundAdded != '\u001b[39m'))
            ret = lastForegroundAdded + ret;

        return ret;
    }


    /**
     * Retourne l'état de la ligne (en terme de coloration active)
     */
    protected static _readState (line: string): { [keys: string]: string} {

        const code = this.codeRegex(true);
        let controlChars = code.exec(line);
        const state: { [keys: string]: string} = {};

        while (controlChars !== null) {
            this._updateState(state, controlChars);
            controlChars = code.exec(line);
        }

        return state;
    }


    /**
     * Met à jour un state avec des caractères de contrôle capturés
     * @param state State à mettre à jour
     * @param controlChars Caractères de contrôle capturés
     */
    static _updateState (
        state: { [keys: string]: string},
        controlChars: RegExpExecArray
    ): void {

        const controlCode = controlChars[1] ?
            parseInt(controlChars[1].split(';')[0]) : 0;

        if ((controlCode >= 30 && controlCode <= 39) ||
            (controlCode >= 90 && controlCode <= 97)) {

            state.lastForegroundAdded = controlChars[0];
            return;
        }
        if ((controlCode >= 40 && controlCode <= 49) ||
            (controlCode >= 100 && controlCode <= 107)) {

            state.lastBackgroundAdded = controlChars[0];
            return;
        }

        if (controlCode === 0) {
            for (const i in state) {
                if (Object.prototype.hasOwnProperty.call(state, i))
                    delete state[i];
            }
            return;
        }

        const info = this._getCodeCache()[controlChars[0]];
        if (info)
            state[info.set] = info.to;
    }


    static _unwindState (state: { [keys: string]: string}, ret: string): string {

        const lastBackgroundAdded = state.lastBackgroundAdded;
        const lastForegroundAdded = state.lastForegroundAdded;

        delete state.lastBackgroundAdded;
        delete state.lastForegroundAdded;

        Utils.forEach(state, (value, key) => {
            if (value)
                ret += this._getCodeCache()[key].off;
        });

        if (lastBackgroundAdded && (lastBackgroundAdded != '\u001b[49m'))
            ret += '\u001b[49m';
        if (lastForegroundAdded && (lastForegroundAdded != '\u001b[39m'))
            ret += '\u001b[39m';

        return ret;
    }
}
