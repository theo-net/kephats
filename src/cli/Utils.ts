/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Outils pour la partie CLI du framework.
 * Contient tous les outils du `core`
 */

import * as crypto from 'crypto';
import * as fs from 'fs';
import * as npath from 'path';

export * from '../core/Utils';

// Bytes to hex
const BYTE2HEX: Array<string> = [];
for (let i = 0; i < 256; ++i)
    BYTE2HEX[i] = (i + 0x100).toString(16).substr(1);

/**
 * Test l'existence d'un fichier ou d'un répertoire
 *
 * @param path Fichier/dossier à tester
 */
export function fileExists (path: string): boolean {

    try {
        fs.statSync(path);
        return true;
    }
    catch (e) {
        return false;
    }
}


/**
 * Copie un fichier. S'il s'agit d'un répertoire, la copie sera récursive
 * @param source Fichier à copier
 * @param dest   Emplacement de destination
 * @param callback Fonction appelée après la copie
 * @throws `Error` Si `source` n'existe pas ou n'est ni un fichier ni un répertoire
 */
export function cpr (
    source: string, dest: string,
    callback: (err?: Error) => void = (): void => { return; }
): void {

    fs.stat(source, (err, stat) => {

        if (err) throw new Error(err.toString());

        if (stat.isFile()) {

            const write = fs.createWriteStream(dest)
                .on('error', (err) => { if (err) throw err; })
                .on('close', callback);
            fs.createReadStream(source)
                .on('error', (err) => { if (err) throw err; })
                .pipe(write);
        }
        else if (stat.isDirectory()) {

            fs.readdir(source, (err, files) => {

                if (err) throw new Error(err.toString());

                let n = files.length;

                fs.mkdir(dest, (err) => {

                    if (err) throw new Error(err.toString());

                    if (n === 0) callback();

                    else {
                        files.forEach(file => {
                            cpr(npath.join(source, file),
                                npath.join(dest, file), () => {
                                    if (--n === 0) callback();
                                });
                        });
                    }
                });
            });
        }
        else
            throw new Error(source + ' isn\'t file or dir!');
    });
}

/**
 * Supprime un fichier ou un dossier et tout son contenu
 * @param {String} path Fichier ou dossier à supprimer
 * @param {Function} [callback] Function à exécuter après la suppression
 *                              `(err){}`
 */
export function rmrf (
    path: string,
    callback: fs.NoParamCallback = (): void => { return; }
): void {

    fs.stat(path, (err, stat) => {

        if (err && callback)
            return callback(err);
        else if (err)
            throw new Error(err.toString());

        if (stat.isFile())
            fs.unlink(path, callback);
        else if (stat.isDirectory()) {

            fs.readdir(path, (err, files) => {

                if (err && callback)
                    return callback(err);
                else if (err)
                    throw new Error(err.toString());

                let n = files.length;

                if (n === 0)
                    fs.rmdir(path, callback);

                else {
                    files.forEach(file => {

                        rmrf(npath.join(path, file), (err) => {
                            if (err && callback)
                                return callback(err);
                            else if (--n === 0)
                                fs.rmdir(path, callback);
                        });
                    });
                }

            });
        }
        else
            throw new Error(path + ' isn\'t file or dir!');
    });
}


/**
 * Génère un UUID v4
 * De la forme `xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx`, où chaque `x` est un caractère
 * hexadécimal aléatoire et `y` vaut `8`, `9`, `a` ou `b`.
 *
 * 4B - 2B - 2B - 2B - 6B = 16 Bytes
 */
export function uuid (): string {

    const buf = crypto.randomBytes(16);

    buf[6] = (buf[6] & 0x0f) | 0x40;
    buf[8] = (buf[8] & 0x3f) | 0x80;

    return BYTE2HEX[buf[0]] + BYTE2HEX[buf[1]]
        + BYTE2HEX[buf[2]] + BYTE2HEX[buf[3]] + '-'
        + BYTE2HEX[buf[4]] + BYTE2HEX[buf[5]] + '-'
        + BYTE2HEX[buf[6]] + BYTE2HEX[buf[7]] + '-'
        + BYTE2HEX[buf[8]] + BYTE2HEX[buf[9]] + '-'
        + BYTE2HEX[buf[10]] + BYTE2HEX[buf[11]]
        + BYTE2HEX[buf[12]] + BYTE2HEX[buf[13]]
        + BYTE2HEX[buf[14]] + BYTE2HEX[buf[15]];
}
