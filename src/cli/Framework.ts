/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as fs from 'fs';
import * as path from 'path';

import { Application } from '../core/Application';
import { Kernel } from '../core/Kernel';
import { parseDotEnv } from './DotEnv/DotEnv';

/**
 * Point d'entrée du Framework chargé par NodeJs (ligne de commande)
 */
export * from '../core/Framework';
export * from './Utils';

/**
 * Charge des variables d'environnement et les retourne
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function _loadEnv (): Record<string, any> {

    try {
        const parsed = parseDotEnv(fs.readFileSync(path.resolve(process.cwd(), '.env'), 'utf-8'));

        Object.keys(parsed).forEach((varName) => {
            if (!Object.prototype.hasOwnProperty.call(process.env, varName))
                process.env[varName] = parsed[varName];
        });
    } catch (e) {
        return process.env;
    }

    return process.env;
}

/**
 * Créé une nouvelle application
 */
export function app (): Application {

    const kernel = new Kernel();

    const app = new Application(kernel, _loadEnv());
    kernel.container().register('$application', app);
    return app;
}
