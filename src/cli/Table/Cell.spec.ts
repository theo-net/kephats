/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


/*
 Inspiré de cli-table2
 */

import { expect } from 'chai';
import * as sinon from 'sinon';

import { Color } from '../Color';
import { Kernel } from '../../core/Kernel';
const kernel = new Kernel(true);
kernel.container().register('$color', new Color());

import { Cell } from './Cell';
import { ColSpanCell } from './ColSpanCell';
import { RowSpanCell } from './RowSpanCell';
import { Table } from './Table';


describe('cli/Table/Cell', () => {

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function defaultOptions (): Record<string, any> {

        //overwrite coloring of head and border by default for easier testing.
        return Table._mergeOptions({ style: { head: [], border: [] } });
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function defaultChars (): Record<string, any> {

        return {
            'top': '─'
            , 'topMid': '┬'
            , 'topLeft': '┌'
            , 'topRight': '┐'
            , 'bottom': '─'
            , 'bottomMid': '┴'
            , 'bottomLeft': '└'
            , 'bottomRight': '┘'
            , 'left': '│'
            , 'leftMid': '├'
            , 'mid': '─'
            , 'midMid': '┼'
            , 'right': '│'
            , 'rightMid': '┤'
            , 'middle': '│'
        };
    }


    describe('constructor', () => {

        it('colSpan and rowSpan default to 1', () => {

            const cell = new Cell();
            expect(cell.colSpan).to.equal(1);
            expect(cell.rowSpan).to.equal(1);
        });

        it('colSpan and rowSpan can be set via constructor', () => {

            const cell = new Cell({ rowSpan: 2, colSpan: 3 });
            expect(cell.rowSpan).to.equal(2);
            expect(cell.colSpan).to.equal(3);
        });

        it('content can be set as a string', () => {

            const cell = new Cell('hello\nworld');
            expect(cell.content).to.equal('hello\nworld');
        });

        it('content can be set as a options property', () => {

            const cell = new Cell({ content: 'hello\nworld' });
            expect(cell.content).to.equal('hello\nworld');
        });

        it('default content is an empty string', () => {

            const cell = new Cell();
            expect(cell.content).to.equal('');
        });

        it('new Cell(null) will have empty string content', () => {

            const cell = new Cell(null);
            expect(cell.content).to.equal('');
        });

        it('new Cell({content: null}) will have empty string content', () => {

            const cell = new Cell({ content: null });
            expect(cell.content).to.equal('');
        });

        it('new Cell(0) will have "0" as content', () => {

            const cell = new Cell(0);
            expect(cell.content).to.equal('0');
        });

        it('new Cell({content: 0}) will have "0" as content', () => {

            const cell = new Cell({ content: 0 });
            expect(cell.content).to.equal('0');
        });

        it('new Cell(false) will have "false" as content', () => {

            const cell = new Cell(false);
            expect(cell.content).to.equal('false');
        });

        it('new Cell({content: false}) will have "false" as content', () => {

            const cell = new Cell({ content: false });
            expect(cell.content).to.equal('false');
        });
    });

    describe('mergeTableOptions', () => {

        describe('chars', () => {

            it('unset chars take on value of table', () => {

                const cell = new Cell();
                const tableOptions = defaultOptions();
                cell.mergeTableOptions(tableOptions);
                expect(cell.chars).to.eql(defaultChars());
            });

            it('set chars override the value of table', () => {

                const cell = new Cell({ chars: { bottomRight: '=' } });
                cell.mergeTableOptions(defaultOptions());
                const chars = defaultChars();
                chars.bottomRight = '=';
                expect(cell.chars).to.eql(chars);
            });

            it('hyphenated names will be converted to camel-case', () => {

                const cell = new Cell({ chars: { 'bottom-left': '=' } });
                cell.mergeTableOptions(defaultOptions());
                const chars = defaultChars();
                chars.bottomLeft = '=';
                expect(cell.chars).to.eql(chars);
            });
        });

        describe('truncate', () => {

            it('if unset takes on value of table', () => {

                const cell = new Cell();
                cell.mergeTableOptions(defaultOptions());
                expect(cell.truncate).to.equal('…');
            });

            it('if set overrides value of table', () => {

                const cell = new Cell({ truncate: '...' });
                cell.mergeTableOptions(defaultOptions());
                expect(cell.truncate).to.equal('...');
            });
        });

        describe('style.padding-left', () => {

            it('if unset will be copied from tableOptions.style', () => {

                let cell = new Cell();
                cell.mergeTableOptions(defaultOptions());
                expect(cell.paddingLeft).to.equal(1);

                cell = new Cell();
                let tableOptions = defaultOptions();
                tableOptions.style['padding-left'] = 2;
                cell.mergeTableOptions(tableOptions);
                expect(cell.paddingLeft).to.equal(2);

                cell = new Cell();
                tableOptions = defaultOptions();
                tableOptions.style.paddingLeft = 3;
                cell.mergeTableOptions(tableOptions);
                expect(cell.paddingLeft).to.equal(3);
            });

            it('if set will override tableOptions.style', () => {

                let cell = new Cell({ style: { 'padding-left': 2 } });

                cell.mergeTableOptions(defaultOptions());
                expect(cell.paddingLeft).to.equal(2);

                cell = new Cell({ style: { paddingLeft: 3 } });
                cell.mergeTableOptions(defaultOptions());
                expect(cell.paddingLeft).to.equal(3);
            });
        });

        describe('style.padding-right', () => {

            it('if unset will be copied from tableOptions.style', () => {

                let cell = new Cell();
                cell.mergeTableOptions(defaultOptions());
                expect(cell.paddingRight).to.equal(1);

                cell = new Cell();
                let tableOptions = defaultOptions();
                tableOptions.style['padding-right'] = 2;
                cell.mergeTableOptions(tableOptions);
                expect(cell.paddingRight).to.equal(2);

                cell = new Cell();
                tableOptions = defaultOptions();
                tableOptions.style.paddingRight = 3;
                cell.mergeTableOptions(tableOptions);
                expect(cell.paddingRight).to.equal(3);
            });

            it('if set will override tableOptions.style', () => {

                let cell = new Cell({ style: { 'padding-right': 2 } });
                cell.mergeTableOptions(defaultOptions());
                expect(cell.paddingRight).to.equal(2);

                cell = new Cell({ style: { paddingRight: 3 } });
                cell.mergeTableOptions(defaultOptions());
                expect(cell.paddingRight).to.equal(3);
            });
        });

        describe('desiredWidth', () => {

            it('content(hello) padding(1,1) == 7', () => {

                const cell = new Cell('hello');
                cell.mergeTableOptions(defaultOptions());
                expect(cell.desiredWidth).to.equal(7);
            });

            it('content(hi) padding(1,2) == 5', () => {

                const cell = new Cell({ content: 'hi', style: { paddingRight: 2 } });
                const tableOptions = defaultOptions();
                cell.mergeTableOptions(tableOptions);
                expect(cell.desiredWidth).to.equal(5);
            });

            it('content(hi) padding(3,2) == 7', () => {

                const cell = new Cell({
                    content: 'hi',
                    style: { paddingLeft: 3, paddingRight: 2 }
                });
                const tableOptions = defaultOptions();
                cell.mergeTableOptions(tableOptions);
                expect(cell.desiredWidth).to.equal(7);
            });
        });

        describe('desiredHeight', () => {

            it('1 lines of text', () => {

                const cell = new Cell('hi');
                cell.mergeTableOptions(defaultOptions());
                expect(cell.desiredHeight).to.equal(1);
            });

            it('2 lines of text', () => {

                const cell = new Cell('hi\nbye');
                cell.mergeTableOptions(defaultOptions());
                expect(cell.desiredHeight).to.equal(2);
            });

            it('2 lines of text', () => {

                const cell = new Cell('hi\nbye\nyo');
                cell.mergeTableOptions(defaultOptions());
                expect(cell.desiredHeight).to.equal(3);
            });
        });
    });


    describe('init', () => {

        describe('hAlign', () => {

            it('if unset takes colAlign value from tableOptions', () => {

                const tableOptions = defaultOptions();
                tableOptions.colAligns = ['left', 'right', 'both'];

                let cell = new Cell();
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.hAlign).to.equal('left');

                cell = new Cell();
                cell.x = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.hAlign).to.equal('right');

                cell = new Cell();
                cell.mergeTableOptions(tableOptions);
                cell.x = 2;
                cell.init(tableOptions);
                expect(cell.hAlign).to.equal('both');
            });

            it('if set overrides tableOptions', () => {

                const tableOptions = defaultOptions();
                tableOptions.colAligns = ['left', 'right', 'both'];

                let cell = new Cell({ hAlign: 'right' });
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.hAlign).to.equal('right');

                cell = new Cell({ hAlign: 'left' });
                cell.x = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.hAlign).to.equal('left');

                cell = new Cell({ hAlign: 'right' });
                cell.x = 2;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.hAlign).to.equal('right');
            });
        });

        describe('vAlign', () => {

            it('if unset takes rowAlign value from tableOptions', () => {

                const tableOptions = defaultOptions();
                tableOptions.rowAligns = ['top', 'bottom', 'center'];

                let cell = new Cell();
                cell.y = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.vAlign).to.equal('top');

                cell = new Cell();
                cell.y = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.vAlign).to.equal('bottom');

                cell = new Cell();
                cell.y = 2;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.vAlign).to.equal('center');
            });

            it('if set overrides tableOptions', () => {

                const tableOptions = defaultOptions();
                tableOptions.rowAligns = ['top', 'bottom', 'center'];

                let cell = new Cell({ vAlign: 'bottom' });
                cell.y = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.vAlign).to.equal('bottom');

                cell = new Cell({ vAlign: 'top' });
                cell.y = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.vAlign).to.equal('top');

                cell = new Cell({ vAlign: 'center' });
                cell.y = 2;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.vAlign).to.equal('center');
            });
        });

        describe('width', () => {

            it('will match colWidth of x', () => {

                const tableOptions = defaultOptions();
                tableOptions.colWidths = [5, 10, 15];

                let cell = new Cell();
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.width).to.equal(5);

                cell = new Cell();
                cell.x = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.width).to.equal(10);

                cell = new Cell();
                cell.x = 2;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.width).to.equal(15);
            });

            it('will add colWidths if colSpan > 1', () => {

                const tableOptions = defaultOptions();
                tableOptions.colWidths = [5, 10, 15];

                let cell = new Cell({ colSpan: 2 });
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.width).to.equal(16);

                cell = new Cell({ colSpan: 2 });
                cell.x = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.width).to.equal(26);

                cell = new Cell({ colSpan: 3 });
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.width).to.equal(32);
            });
        });

        describe('height', () => {

            it('will match rowHeight of x', () => {

                const tableOptions = defaultOptions();
                tableOptions.rowHeights = [5, 10, 15];

                let cell = new Cell();
                cell.y = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.height).to.equal(5);

                cell = new Cell();
                cell.y = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.height).to.equal(10);

                cell = new Cell();
                cell.y = 2;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.height).to.equal(15);
            });

            it('will add rowHeights if rowSpan > 1', () => {

                const tableOptions = defaultOptions();
                tableOptions.rowHeights = [5, 10, 15];

                let cell = new Cell({ rowSpan: 2 });
                cell.y = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.height).to.equal(16);

                cell = new Cell({ rowSpan: 2 });
                cell.y = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.height).to.equal(26);

                cell = new Cell({ rowSpan: 3 });
                cell.y = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.height).to.equal(32);
            });
        });

        describe('drawRight', () => {

            let tableOptions = defaultOptions();

            beforeEach(() => {
                tableOptions = defaultOptions();
                tableOptions.colWidths = [20, 20, 20];
            });

            it('col 1 of 3, with default colspan', () => {

                const cell = new Cell();
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.drawRight).to.equal(false);
            });

            it('col 2 of 3, with default colspan', () => {

                const cell = new Cell();
                cell.x = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.drawRight).to.equal(false);
            });

            it('col 3 of 3, with default colspan', () => {

                const cell = new Cell();
                cell.x = 2;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.drawRight).to.equal(true);
            });

            it('col 3 of 4, with default colspan', () => {

                const cell = new Cell();
                cell.x = 2;
                tableOptions.colWidths = [20, 20, 20, 20];
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.drawRight).to.equal(false);
            });

            it('col 2 of 3, with colspan of 2', () => {

                const cell = new Cell({ colSpan: 2 });
                cell.x = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.drawRight).to.equal(true);
            });

            it('col 1 of 3, with colspan of 3', () => {

                const cell = new Cell({ colSpan: 3 });
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.drawRight).to.equal(true);
            });

            it('col 1 of 3, with colspan of 2', () => {

                const cell = new Cell({ colSpan: 2 });
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.drawRight).to.equal(false);
            });
        });

    });

    describe('drawLine', () => {

        let cell: Cell;
        const color = new Color();

        beforeEach(() => {

            cell = new Cell();

            //manually init
            cell.chars = defaultChars();
            cell.paddingLeft = cell.paddingRight = 1;
            cell.width = 7;
            cell.height = 3;
            cell.hAlign = 'center';
            cell.vAlign = 'center';
            cell.chars.left = 'L';
            cell.chars.right = 'R';
            cell.chars.middle = 'M';
            cell.content = 'hello\nhowdy\ngoodnight';
            cell.lines = cell.content.split('\n');
            cell.x = cell.y = 0;
        });

        describe('top line', () => {

            it('will draw the top left corner when x=0,y=0', () => {

                cell.x = cell.y = 0;
                expect(cell.draw('top')).to.equal('┌───────');
                cell.drawRight = true;
                expect(cell.draw('top')).to.equal('┌───────┐');
            });

            it('will draw the top mid corner when x=1,y=0', () => {

                cell.x = 1;
                cell.y = 0;
                expect(cell.draw('top')).to.equal('┬───────');
                cell.drawRight = true;
                expect(cell.draw('top')).to.equal('┬───────┐');
            });

            it('will draw the left mid corner when x=0,y=1', () => {

                cell.x = 0;
                cell.y = 1;
                expect(cell.draw('top')).to.equal('├───────');
                cell.drawRight = true;
                expect(cell.draw('top')).to.equal('├───────┤');
            });

            it('will draw the mid mid corner when x=1,y=1', () => {

                cell.x = 1;
                cell.y = 1;
                expect(cell.draw('top')).to.equal('┼───────');
                cell.drawRight = true;
                expect(cell.draw('top')).to.equal('┼───────┤');
            });

            it('will draw in the color specified by border style', () => {

                cell.border = ['grey'];
                expect(cell.draw('top')).to.equal(color.grey('┌───────'));
            });
        });

        describe('bottom line', () => {

            it('will draw the bottom left corner if x=0', () => {

                cell.x = 0;
                cell.y = 1;
                expect(cell.draw('bottom')).to.equal('└───────');
                cell.drawRight = true;
                expect(cell.draw('bottom')).to.equal('└───────┘');
            });

            it('will draw the bottom left corner if x=1', () => {

                cell.x = 1;
                cell.y = 1;
                expect(cell.draw('bottom')).to.equal('┴───────');
                cell.drawRight = true;
                expect(cell.draw('bottom')).to.equal('┴───────┘');
            });

            it('will draw in the color specified by border style', () => {

                cell.border = ['grey'];
                expect(cell.draw('bottom')).to.equal(color.grey('└───────'));
            });
        });

        describe('drawEmpty', () => {

            it('draws an empty line', () => {

                expect(cell.drawEmpty()).to.equal('L       ');
                expect(cell.drawEmpty(true)).to.equal('L       R');
            });

            it('draws an empty line', () => {

                cell.border = ['grey'];
                cell.head = ['red'];
                expect(cell.drawEmpty()).to.equal(
                    color.grey('L') + color.red('       ')
                );
                expect(cell.drawEmpty(true)).to.equal(
                    color.grey('L') + color.red('       ') + color.grey('R')
                );
            });
        });

        describe('first line of text', () => {

            beforeEach(() => {

                cell.width = 9;
            });

            it('will draw left side if x=0', () => {

                cell.x = 0;
                expect(cell.draw(0)).to.equal('L  hello  ');
                cell.drawRight = true;
                expect(cell.draw(0)).to.equal('L  hello  R');
            });

            it('will draw mid side if x=1', () => {

                cell.x = 1;
                expect(cell.draw(0)).to.equal('M  hello  ');
                cell.drawRight = true;
                expect(cell.draw(0)).to.equal('M  hello  R');
            });

            it('will align left', () => {

                cell.x = 1;
                cell.hAlign = 'left';
                expect(cell.draw(0)).to.equal('M hello   ');
                cell.drawRight = true;
                expect(cell.draw(0)).to.equal('M hello   R');
            });

            it('will align right', () => {

                cell.x = 1;
                cell.hAlign = 'right';
                expect(cell.draw(0)).to.equal('M   hello ');
                cell.drawRight = true;
                expect(cell.draw(0)).to.equal('M   hello R');
            });

            it('left and right will be drawn in color of border style', () => {

                cell.border = ['grey'];
                cell.x = 0;
                expect(cell.draw(0)).to.equal(color.grey('L') + '  hello  ');
                cell.drawRight = true;
                expect(cell.draw(0))
                    .to.equal(color.grey('L') + '  hello  ' + color.grey('R'));
            });

            it('text will be drawn in color of head style if y == 0', () => {

                cell.head = ['red'];
                cell.x = cell.y = 0;
                expect(cell.draw(0)).to.equal('L' + color.red('  hello  '));
                cell.drawRight = true;
                expect(cell.draw(0)).to.equal('L' + color.red('  hello  ') + 'R');
            });

            it('text will NOT be drawn in color of head style if y == 1', () => {

                cell.head = ['red'];
                cell.x = cell.y = 1;
                expect(cell.draw(0)).to.equal('M  hello  ');
                cell.drawRight = true;
                expect(cell.draw(0)).to.equal('M  hello  R');
            });

            it('head and border colors together', () => {

                cell.border = ['grey'];
                cell.head = ['red'];
                cell.x = cell.y = 0;
                expect(cell.draw(0))
                    .to.equal(color.grey('L') + color.red('  hello  '));
                cell.drawRight = true;
                expect(cell.draw(0)).to.equal(
                    color.grey('L') + color.red('  hello  ') + color.grey('R')
                );
            });
        });

        describe('second line of text', () => {

            beforeEach(() => {
                cell.width = 9;
            });

            it('will draw left side if x=0', () => {

                cell.x = 0;
                expect(cell.draw(1)).to.equal('L  howdy  ');
                cell.drawRight = true;
                expect(cell.draw(1)).to.equal('L  howdy  R');
            });

            it('will draw mid side if x=1', () => {

                cell.x = 1;
                expect(cell.draw(1)).to.equal('M  howdy  ');
                cell.drawRight = true;
                expect(cell.draw(1)).to.equal('M  howdy  R');
            });

            it('will align left', () => {

                cell.x = 1;
                cell.hAlign = 'left';
                expect(cell.draw(1)).to.equal('M howdy   ');
                cell.drawRight = true;
                expect(cell.draw(1)).to.equal('M howdy   R');
            });

            it('will align right', () => {

                cell.x = 1;
                cell.hAlign = 'right';
                expect(cell.draw(1)).to.equal('M   howdy ');
                cell.drawRight = true;
                expect(cell.draw(1)).to.equal('M   howdy R');
            });
        });

        describe('truncated line of text', () => {

            beforeEach(() => {

                cell.width = 9;
            });

            it('will draw left side if x=0', () => {

                cell.x = 0;
                expect(cell.draw(2)).to.equal('L goodni… ');
                cell.drawRight = true;
                expect(cell.draw(2)).to.equal('L goodni… R');
            });

            it('will draw mid side if x=1', () => {

                cell.x = 1;
                expect(cell.draw(2)).to.equal('M goodni… ');
                cell.drawRight = true;
                expect(cell.draw(2)).to.equal('M goodni… R');
            });

            it('will not change when aligned left', () => {

                cell.x = 1;
                cell.hAlign = 'left';
                expect(cell.draw(2)).to.equal('M goodni… ');
                cell.drawRight = true;
                expect(cell.draw(2)).to.equal('M goodni… R');
            });

            it('will not change when aligned right', () => {

                cell.x = 1;
                cell.hAlign = 'right';
                expect(cell.draw(2)).to.equal('M goodni… ');
                cell.drawRight = true;
                expect(cell.draw(2)).to.equal('M goodni… R');
            });
        });

        describe('vAlign', () => {

            beforeEach(() => {

                cell.height = 5;
            });

            it('center', () => {

                cell.vAlign = 'center';
                expect(cell.draw(0)).to.equal('L       ');
                expect(cell.draw(1)).to.equal('L hello ');
                expect(cell.draw(2)).to.equal('L howdy ');
                expect(cell.draw(3)).to.equal('L good… ');
                expect(cell.draw(4)).to.equal('L       ');

                cell.drawRight = true;
                expect(cell.draw(0)).to.equal('L       R');
                expect(cell.draw(1)).to.equal('L hello R');
                expect(cell.draw(2)).to.equal('L howdy R');
                expect(cell.draw(3)).to.equal('L good… R');
                expect(cell.draw(4)).to.equal('L       R');

                cell.x = 1;
                cell.drawRight = false;
                expect(cell.draw(0)).to.equal('M       ');
                expect(cell.draw(1)).to.equal('M hello ');
                expect(cell.draw(2)).to.equal('M howdy ');
                expect(cell.draw(3)).to.equal('M good… ');
                expect(cell.draw(4)).to.equal('M       ');
            });

            it('top', () => {

                cell.vAlign = 'top';
                expect(cell.draw(0)).to.equal('L hello ');
                expect(cell.draw(1)).to.equal('L howdy ');
                expect(cell.draw(2)).to.equal('L good… ');
                expect(cell.draw(3)).to.equal('L       ');
                expect(cell.draw(4)).to.equal('L       ');

                cell.vAlign = null; //top is the default
                cell.drawRight = true;
                expect(cell.draw(0)).to.equal('L hello R');
                expect(cell.draw(1)).to.equal('L howdy R');
                expect(cell.draw(2)).to.equal('L good… R');
                expect(cell.draw(3)).to.equal('L       R');
                expect(cell.draw(4)).to.equal('L       R');

                cell.x = 1;
                cell.drawRight = false;
                expect(cell.draw(0)).to.equal('M hello ');
                expect(cell.draw(1)).to.equal('M howdy ');
                expect(cell.draw(2)).to.equal('M good… ');
                expect(cell.draw(3)).to.equal('M       ');
                expect(cell.draw(4)).to.equal('M       ');
            });

            it('bottom', () => {

                cell.vAlign = 'bottom';
                expect(cell.draw(0)).to.equal('L       ');
                expect(cell.draw(1)).to.equal('L       ');
                expect(cell.draw(2)).to.equal('L hello ');
                expect(cell.draw(3)).to.equal('L howdy ');
                expect(cell.draw(4)).to.equal('L good… ');

                cell.drawRight = true;
                expect(cell.draw(0)).to.equal('L       R');
                expect(cell.draw(1)).to.equal('L       R');
                expect(cell.draw(2)).to.equal('L hello R');
                expect(cell.draw(3)).to.equal('L howdy R');
                expect(cell.draw(4)).to.equal('L good… R');

                cell.x = 1;
                cell.drawRight = false;
                expect(cell.draw(0)).to.equal('M       ');
                expect(cell.draw(1)).to.equal('M       ');
                expect(cell.draw(2)).to.equal('M hello ');
                expect(cell.draw(3)).to.equal('M howdy ');
                expect(cell.draw(4)).to.equal('M good… ');
            });
        });

        it('vertically truncated will show truncation on last visible line', () => {

            cell.height = 2;
            expect(cell.draw(0)).to.equal('L hello ');
            expect(cell.draw(1)).to.equal('L howd… ');
        });

        it('won\'t vertically truncate if the lines just fit', () => {

            cell.height = 2;
            cell.content = 'hello\nhowdy';
            cell.lines = cell.content.split('\n');
            expect(cell.draw(0)).to.equal('L hello ');
            expect(cell.draw(1)).to.equal('L howdy ');
        });

        it('will vertically truncate even if last line is short', () => {

            cell.height = 2;
            cell.content = 'hello\nhi\nhowdy';
            cell.lines = cell.content.split('\n');
            expect(cell.draw(0)).to.equal('L hello ');
            expect(cell.draw(1)).to.equal('L  hi…  ');
        });

        it('allows custom truncation', () => {

            cell.height = 2;
            cell.truncate = '...';
            cell.content = 'hello\nhi\nhowdy';
            cell.lines = cell.content.split('\n');
            expect(cell.draw(0)).to.equal('L hello ');
            expect(cell.draw(1)).to.equal('L hi... ');

            cell.content = 'hello\nhowdy\nhi';
            cell.lines = cell.content.split('\n');
            expect(cell.draw(0)).to.equal('L hello ');
            expect(cell.draw(1)).to.equal('L ho... ');
        });
    });

    describe('ColSpanCell', () => {

        it('has an init function', () => {

            expect(new ColSpanCell()).to.respondTo('init');
            new ColSpanCell().init(); // nothing happens.
        });

        it('draw returns an empty string', () => {

            expect(new ColSpanCell().draw('top')).to.equal('');
            expect(new ColSpanCell().draw('bottom')).to.equal('');
            expect(new ColSpanCell().draw(1)).to.equal('');
        });
    });

    describe('RowSpanCell', () => {

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        let original: Record<string, any>, tableOptions: Record<string, any>;

        beforeEach(function () {
            original = {
                rowSpan: 3,
                y: 0,
                draw: sinon.spy()
            };
            tableOptions = {
                rowHeights: [2, 3, 4, 5]
            };
        });

        it('drawing top of the next row', () => {

            const spanner = new RowSpanCell(original as Cell);
            spanner.x = 0;
            spanner.y = 1;
            spanner.init(tableOptions);
            spanner.draw('top');
            expect(original.draw.calledOnce).to.be.true;
            expect(original.draw.calledWith(2)).to.be.true;
        });

        it('drawing line 0 of the next row', () => {

            const spanner = new RowSpanCell(original as Cell);
            spanner.x = 0;
            spanner.y = 1;
            spanner.init(tableOptions);
            spanner.draw(0);
            expect(original.draw.calledOnce).to.be.true;
            expect(original.draw.calledWith(3)).to.be.true;
        });

        it('drawing line 1 of the next row', () => {

            const spanner = new RowSpanCell(original as Cell);
            spanner.x = 0;
            spanner.y = 1;
            spanner.init(tableOptions);
            spanner.draw(1);
            expect(original.draw.calledOnce).to.be.true;
            expect(original.draw.calledWith(4)).to.be.true;
        });

        it('drawing top of two rows below', () => {

            const spanner = new RowSpanCell(original as Cell);
            spanner.x = 0;
            spanner.y = 2;
            spanner.init(tableOptions);
            spanner.draw('top');
            expect(original.draw.calledOnce).to.be.true;
            expect(original.draw.calledWith(6)).to.be.true;
        });

        it('drawing line 0 of two rows below', () => {

            const spanner = new RowSpanCell(original as Cell);
            spanner.x = 0;
            spanner.y = 2;
            spanner.init(tableOptions);
            spanner.draw(0);
            expect(original.draw.calledOnce).to.be.true;
            expect(original.draw.calledWith(7)).to.be.true;
        });

        it('drawing line 1 of two rows below', () => {

            const spanner = new RowSpanCell(original as Cell);
            spanner.x = 0;
            spanner.y = 2;
            spanner.init(tableOptions);
            spanner.draw(1);
            expect(original.draw.calledOnce).to.be.true;
            expect(original.draw.calledWith(8)).to.be.true;
        });

        it('drawing bottom', () => {

            const spanner = new RowSpanCell(original as Cell);
            spanner.x = 0;
            spanner.y = 1;
            spanner.init(tableOptions);
            spanner.draw('bottom');
            expect(original.draw.calledOnce).to.be.true;
            expect(original.draw.calledWith('bottom')).to.be.true;
        });
    });
});
