/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Color } from '../Color';
import { Table } from './Table';

const color = new Color();


describe('cli/Table/Table', () => {

    it('étend un Array', () => {

        expect(Object.getPrototypeOf(Table)).to.equal(Array);
    });

    it('wordWrap with colored text', () => {

        const table = new Table({
            style: { border: [], head: [] },
            wordWrap: true,
            colWidths: [7, 9]
        });

        table.push([
            color.red('Hello how are you?'), color.blue('I am fine thanks!')
        ]);

        const expected = [
            '┌───────┬─────────┐',
            '│ ' + color.red('Hello') + ' │ ' + color.blue('I am') + '    │',
            '│ ' + color.red('how') + '   │ ' + color.blue('fine') + '    │',
            '│ ' + color.red('are') + '   │ ' + color.blue('thanks!') + ' │',
            '│ ' + color.red('you?') + '  │         │',
            '└───────┴─────────┘'
        ];

        expect(table.toString()).to.equal(expected.join('\n'));
    });

    it('allows numbers as `content` property of cells defined using object notation', () => {

        const table = new Table({
            style: { border: [], head: [] }
        });

        table.push([{ content: 12 }]);

        const expected = [
            '┌────┐',
            '│ 12 │',
            '└────┘'
        ];

        expect(table.toString()).to.equal(expected.join('\n'));
    });

    it('throws if content is not a string or number', () => {

        const table = new Table({
            style: { border: [], head: [] }
        });

        expect(function () {
            table.push([{ content: { a: 'b' } }]);
            table.toString();
        }).to.throw();

    });

    it('works with CJK values', () => {

        const table = new Table({
            style: { border: [], head: [] },
            colWidths: [5, 10, 5]
        });

        table.push(
            ['foobar', 'English test', 'baz'],
            ['foobar', '中文测试', 'baz'],
            ['foobar', '日本語テスト', 'baz'],
            ['foobar', '한국어테스트', 'baz']
        );

        const expected = [
            '┌─────┬──────────┬─────┐',
            '│ fo… │ English… │ baz │',
            '├─────┼──────────┼─────┤',
            '│ fo… │ 中文测试 │ baz │',
            '├─────┼──────────┼─────┤',
            '│ fo… │ 日本語…  │ baz │',
            '├─────┼──────────┼─────┤',
            '│ fo… │ 한국어…  │ baz │',
            '└─────┴──────────┴─────┘'
        ];

        expect(table.toString()).to.equal(expected.join('\n'));
    });

    describe('@api original-cli-table index tests', () => {

        it('test complete table', () => {

            const table = new Table({
                head: ['Rel', 'Change', 'By', 'When'],
                style: {
                    'padding-left': 1,
                    'padding-right': 1,
                    head: [],
                    border: [],
                },
                colWidths: [6, 21, 25, 17]
            });

            table.push(
                ['v0.1', 'Testing something cool',
                    'rauchg@gmail.com', '7 minutes ago'],
                ['v0.1', 'Testing something cool',
                    'rauchg@gmail.com', '8 minutes ago']
            );

            const expected = [
                '┌──────┬─────────────────────┬─────────────────────────┬─────────────────┐',
                '│ Rel  │ Change              │ By                      │ When            │',
                '├──────┼─────────────────────┼─────────────────────────┼─────────────────┤',
                '│ v0.1 │ Testing something … │ rauchg@gmail.com        │ 7 minutes ago   │',
                '├──────┼─────────────────────┼─────────────────────────┼─────────────────┤',
                '│ v0.1 │ Testing something … │ rauchg@gmail.com        │ 8 minutes ago   │',
                '└──────┴─────────────────────┴─────────────────────────┴─────────────────┘'
            ];

            expect(table.toString()).to.equal(expected.join('\n'));
        });

        it('test width property', () => {

            const table = new Table({
                head: ['Cool'],
                style: {
                    head: [],
                    border: []
                }
            });

            expect(table.width).to.equal(8);
        });

        it('test vertical table output', () => {

            const table = new Table({
                style: { 'padding-left': 0, 'padding-right': 0, head: [], border: [] }
            }); // clear styles to prevent color output

            table.push(
                { 'v0.1': 'Testing something cool' },
                { 'v0.1': 'Testing something cool' }
            );

            const expected = [
                '┌────┬──────────────────────┐',
                '│v0.1│Testing something cool│',
                '├────┼──────────────────────┤',
                '│v0.1│Testing something cool│',
                '└────┴──────────────────────┘'
            ];

            expect(table.toString()).to.equal(expected.join('\n'));
        });

        it('test cross table output', () => {

            const table = new Table({
                head: ['', 'Header 1', 'Header 2'],
                style: { 'padding-left': 0, 'padding-right': 0, head: [], border: [] }
            });

            table.push(
                { 'Header 3': ['v0.1', 'Testing something cool'] },
                { 'Header 4': ['v0.1', 'Testing something cool'] }
            );

            const expected = [
                '┌────────┬────────┬──────────────────────┐',
                '│        │Header 1│Header 2              │',
                '├────────┼────────┼──────────────────────┤',
                '│Header 3│v0.1    │Testing something cool│',
                '├────────┼────────┼──────────────────────┤',
                '│Header 4│v0.1    │Testing something cool│',
                '└────────┴────────┴──────────────────────┘'
            ];

            expect(table.toString()).to.equal(expected.join('\n'));
        });

        it('test table colors', () => {

            const table = new Table({
                head: ['Rel', 'By'],
                style: { head: ['red'], border: ['grey'] }
            });

            const off = '\u001b[39m',
                  red = '\u001b[31m',
                  orange = '\u001b[38;5;221m',
                  grey = '\u001b[90m',
                  c256s = orange + 'v0.1' + off;

            table.push(
                [c256s, 'rauchg@gmail.com']
            );

            const expected = [
                grey + '┌──────' + off + grey + '┬──────────────────┐' + off,
                grey + '│' + off + red + ' Rel  ' + off + grey + '│' + off + red
                + ' By               ' + off + grey + '│' + off,
                grey + '├──────' + off + grey + '┼──────────────────┤' + off,
                grey + '│' + off + ' ' + c256s + ' ' + grey + '│' + off
                + ' rauchg@gmail.com ' + grey + '│' + off,
                grey + '└──────' + off + grey + '┴──────────────────┘' + off
            ];

            expect(table.toString()).to.equal(expected.join('\n'));
        });

        it('test custom chars', () => {

            const table = new Table({
                chars: {
                    'top': '═',
                    'top-mid': '╤',
                    'top-left': '╔',
                    'top-right': '╗',
                    'bottom': '═',
                    'bottom-mid': '╧',
                    'bottom-left': '╚',
                    'bottom-right': '╝',
                    'left': '║',
                    'left-mid': '╟',
                    'right': '║',
                    'right-mid': '╢',
                },
                style: {
                    head: [],
                    border: []
                }
            });

            table.push(
                ['foo', 'bar', 'baz'],
                ['frob', 'bar', 'quuz']
            );

            const expected = [
                '╔══════╤═════╤══════╗',
                '║ foo  │ bar │ baz  ║',
                '╟──────┼─────┼──────╢',
                '║ frob │ bar │ quuz ║',
                '╚══════╧═════╧══════╝'
            ];

            expect(table.toString()).to.equal(expected.join('\n'));
        });

        it('test compact shortand', () => {

            const table = new Table({
                style: {
                    head: [],
                    border: [],
                    compact: true
                }
            });

            table.push(
                ['foo', 'bar', 'baz'],
                ['frob', 'bar', 'quuz']
            );

            const expected = [
                '┌──────┬─────┬──────┐',
                '│ foo  │ bar │ baz  │',
                '│ frob │ bar │ quuz │',
                '└──────┴─────┴──────┘'
            ];

            expect(table.toString()).to.equal(expected.join('\n'));
        });

        it('test compact empty mid line', () => {

            const table = new Table({
                chars: {
                    'mid': '',
                    'left-mid': '',
                    'mid-mid': '',
                    'right-mid': ''
                },
                style: {
                    head: [],
                    border: []
                }
            });

            table.push(
                ['foo', 'bar', 'baz'],
                ['frob', 'bar', 'quuz']
            );

            const expected = [
                '┌──────┬─────┬──────┐',
                '│ foo  │ bar │ baz  │',
                '│ frob │ bar │ quuz │',
                '└──────┴─────┴──────┘'
            ];

            expect(table.toString()).to.equal(expected.join('\n'));
        });

        it('test decoration lines disabled', () => {

            const table = new Table({
                chars: {
                    'top': '',
                    'top-mid': '',
                    'top-left': '',
                    'top-right': '',
                    'bottom': '',
                    'bottom-mid': '',
                    'bottom-left': '',
                    'bottom-right': '',
                    'left': '',
                    'left-mid': '',
                    'mid': '',
                    'mid-mid': '',
                    'right': '',
                    'right-mid': '',
                    'middle': ' ' // a single space
                },
                style: {
                    head: [],
                    border: [],
                    'padding-left': 0,
                    'padding-right': 0
                }
            });

            table.push(
                ['foo', 'bar', 'baz'],
                ['frobnicate', 'bar', 'quuz']
            );

            const expected = [
                'foo        bar baz ',
                'frobnicate bar quuz'
            ];

            expect(table.toString()).to.equal(expected.join('\n'));
        });

        it('test with null/undefined as values or column names', () => {

            const table = new Table({
                style: {
                    head: [],
                    border: []
                }
            });

            table.push(
                [null, undefined, 0]
            );

            const expected = [
                '┌──┬──┬───┐',
                '│  │  │ 0 │',
                '└──┴──┴───┘'
            ];

            expect(table.toString()).to.equal(expected.join('\n'));
        });
    });

    describe('@api original-cli-table newline tests', () => {

        it('test table with newlines in headers', () => {

            const table = new Table({
                head: ['Test', '1\n2\n3'],
                style: {
                    'padding-left': 1,
                    'padding-right': 1,
                    head: [],
                    border: []
                }
            });

            const expected = [
                '┌──────┬───┐',
                '│ Test │ 1 │',
                '│      │ 2 │',
                '│      │ 3 │',
                '└──────┴───┘'
            ];

            expect(table.toString()).to.equal(expected.join('\n'));
        });

        it('test column width is accurately reflected when newlines are present', () => {

            const table = new Table({
                head: ['Test\nWidth'],
                style: { head: [], border: [] }
            });
            expect(table.width).to.equal(9);
        });

        it('test newlines in body cells', () => {

            const table = new Table({ style: { head: [], border: [] } });

            table.push(['something\nwith\nnewlines']);

            const expected = [
                '┌───────────┐',
                '│ something │',
                '│ with      │',
                '│ newlines  │',
                '└───────────┘'
            ];

            expect(table.toString()).to.equal(expected.join('\n'));
        });

        it('test newlines in vertical cell header and body', () => {

            const table = new Table({
                style: {
                    'padding-left': 0,
                    'padding-right': 0,
                    head: [],
                    border: []
                }
            });

            table.push(
                { 'v\n0.1': 'Testing\nsomething cool' }
            );

            const expected = [
                '┌───┬──────────────┐',
                '│v  │Testing       │',
                '│0.1│something cool│',
                '└───┴──────────────┘'
            ];

            expect(table.toString()).to.equal(expected.join('\n'));
        });

        it('test newlines in cross table header and body', () => {

            const table = new Table({
                head: ['', 'Header\n1'],
                style: { 'padding-left': 0, 'padding-right': 0, head: [], border: [] }
            });

            table.push({ 'Header\n2': ['Testing\nsomething\ncool'] });

            const expected = [
                '┌──────┬─────────┐',
                '│      │Header   │',
                '│      │1        │',
                '├──────┼─────────┤',
                '│Header│Testing  │',
                '│2     │something│',
                '│      │cool     │',
                '└──────┴─────────┘'
            ];

            expect(table.toString()).to.equal(expected.join('\n'));
        });
    });


    describe('strlen', () => {

        it('length of "hello" is 5', () => {

            expect(Table.strlen('hello')).to.equal(5);
        });

        it('length of "hi" is 2', () => {

            expect(Table.strlen('hi')).to.equal(2);
        });

        it('length of "hello" in red is 5', () => {

            expect(Table.strlen(color.red('hello'))).to.equal(5);
        });

        it('length of "hello" underlined is 5', () => {

            expect(Table.strlen(color.underline('hello'))).to.equal(5);
        });

        it('length of "hello\\nhi\\nheynow" is 6', () => {

            expect(Table.strlen('hello\nhi\nheynow')).to.equal(6);
        });

        it('length of "中文字符" is 8', () => {

            expect(Table.strlen('中文字符')).to.equal(8);
        });

        it('length of "日本語の文字" is 12', () => {

            expect(Table.strlen('日本語の文字')).to.equal(12);
        });

        it('length of "한글" is 4', () => {

            expect(Table.strlen('한글')).to.equal(4);
        });
    });

    describe('truncate', () => {

        it('truncate("hello", 5) === "hello"', () => {

            expect(Table.truncate('hello', 5)).to.equal('hello');
        });

        it('truncate("hello sir", 7, "…") == "hello …"', () => {

            expect(Table.truncate('hello sir', 7, '…')).to.equal('hello …');
        });

        it('truncate("hello sir", 6, "…") == "hello…"', () => {

            expect(Table.truncate('hello sir', 6, '…')).to.equal('hello…');
        });

        it('truncate("goodnight moon", 8, "…") == "goodnig…"', () => {

            expect(Table.truncate('goodnight moon', 8, '…')).to.equal('goodnig…');
        });

        it('truncate(color.red("goodnight moon"), 15, "…") == color.red("goodnight moon")', () => {

            const original = color.red('goodnight moon');
            expect(Table.truncate(original, 15, '…')).to.equal(original);
        });

        it('truncate(color.red("goodnight moon"), 8, "…") == color.red("goodnig") + "…"', () => {

            const original = color.red('goodnight moon');
            const expected = color.red('goodnig') + '…';
            expect(Table.truncate(original, 8, '…')).to.equal(expected);
        });

        it('truncate(color.red("goodnight moon"), 9, "…") == color.red("goodnig") + "…"', () => {

            const original = color.red('goodnight moon');
            const expected = color.red('goodnigh') + '…';
            expect(Table.truncate(original, 9, '…')).to.equal(expected);
        });

        it('red(hello) + green(world) truncated to 9 chars', () => {

            const original = color.red('hello') + color.green(' world');
            const expected = color.red('hello') + color.green(' wo') + '…';
            expect(Table.truncate(original, 9)).to.equal(expected);
        });

        it('red-on-green(hello) + green-on-red(world) truncated to 9 chars', () => {

            const original = color.red.bgGreen('hello') + color.green.bgRed(' world');
            const expected = color.red.bgGreen('hello') + color.green.bgRed(' wo') + '…';
            expect(Table.truncate(original, 9)).to.equal(expected);
        });

        it('red-on-green(hello) + green-on-red(world) truncated to 10 chars - using inverse',
        () => {

            const original = color.red.bgGreen('hello' + color.inverse(' world'));
            const expected = color.red.bgGreen('hello' + color.inverse(' wor')) + '…';
            expect(Table.truncate(original, 10)).to.equal(expected);
        });

        it('red-on-green( italic(hello world) ) truncated to 11 chars', () => {

            const original = color.red.bgGreen(color.italic('hello world'));
            const expected = color.red.bgGreen(color.italic('hello world'));
            expect(Table.truncate(original, 11)).to.equal(expected);
        });

        it('red-on-green( italic (hello world) ) truncated to 10 chars ', () => {

            const original = color.red.bgGreen(color.italic('hello world'));
            const expected = color.red.bgGreen(color.italic('hello wor')) + '…';
            expect(Table.truncate(original, 10)).to.equal(expected);
        });

        it('handles reset code', () => {

            const original = '\x1b[31mhello\x1b[0m world';
            const expected = '\x1b[31mhello\x1b[0m wor…';
            expect(Table.truncate(original, 10)).to.equal(expected);
        });

        it('handles reset code (EMPTY VERSION)', () => {

            const original = '\x1b[31mhello\x1b[0m world';
            const expected = '\x1b[31mhello\x1b[0m wor…';
            expect(Table.truncate(original, 10)).to.equal(expected);
        });

        it('truncateWidth("漢字テスト", 15) === "漢字テスト"', () => {

            expect(Table.truncate('漢字テスト', 15)).to.equal('漢字テスト');
        });

        it('truncateWidth("漢字テスト", 6) === "漢字…"', () => {

            expect(Table.truncate('漢字テスト', 6)).to.equal('漢字…');
        });

        it('truncateWidth("漢字テスト", 5) === "漢字…"', () => {

            expect(Table.truncate('漢字テスト', 5)).to.equal('漢字…');
        });

        it('truncateWidth("漢字testてすと", 12) === "漢字testて…"', () => {

            expect(Table.truncate('漢字testてすと', 12)).to.equal('漢字testて…');
        });

        it('handles color code with CJK chars', () => {

            const original = '漢字\x1b[31m漢字\x1b[0m漢字';
            const expected = '漢字\x1b[31m漢字\x1b[0m漢…';
            expect(Table.truncate(original, 11)).to.equal(expected);
        });
    });

    describe('_mergeOptions', () => {

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        function defaultOptions (): Record<string, any> {

            return {
                chars: {
                    'top': '─',
                    'top-mid': '┬',
                    'top-left': '┌',
                    'top-right': '┐',
                    'bottom': '─',
                    'bottom-mid': '┴',
                    'bottom-left': '└',
                    'bottom-right': '┘',
                    'left': '│',
                    'left-mid': '├',
                    'mid': '─',
                    'mid-mid': '┼',
                    'right': '│',
                    'right-mid': '┤',
                    'middle': '│'
                },
                truncate: '…',
                colWidths: [],
                rowHeights: [],
                colAligns: [],
                rowAligns: [],
                style: {
                    'padding-left': 1,
                    'padding-right': 1,
                    head: ['red'],
                    border: ['grey'],
                    compact: false
                },
                head: []
            };
        }

        it('allows you to override chars', () => {

            expect(Table._mergeOptions()).to.eql(defaultOptions());
        });

        it('chars will be merged deeply', () => {

            const expected = defaultOptions();
            expected.chars.left = 'L';
            expect(Table._mergeOptions({ chars: { left: 'L' } })).to.eql(expected);
        });

        it('style will be merged deeply', () => {

            const expected = defaultOptions();
            expected.style['padding-left'] = 2;
            expect(Table._mergeOptions({ style: { 'padding-left': 2 } })).to.eql(expected);
        });

        it('head will be overwritten', () => {

            const expected = defaultOptions();
            expected.style.head = [];
            expect(Table._mergeOptions({ style: { 'head': [] } })).to.eql(expected);
        });

        it('border will be overwritten', () => {

            const expected = defaultOptions();
            expected.style.border = [];
            expect(Table._mergeOptions({ style: { 'border': [] } })).to.eql(expected);
        });
    });

    describe('wordWrap', () => {

        it('length', () => {

            const input = 'Hello, how are you today? I am fine, thank you!';
            const expected = 'Hello, how\nare you\ntoday? I\nam fine,\nthank you!';

            expect(Table.wordWrap(10, input).join('\n')).to.equal(expected);
        });

        it('length with colors', () => {

            const input = color.red('Hello, how are')
                + color.blue(' you today? I')
                + color.green(' am fine, thank you!');

            const expected = color.red('Hello, how\nare')
                + color.blue(' you\ntoday? I')
                + color.green('\nam fine,\nthank you!');

            expect(Table.wordWrap(10, input).join('\n')).to.equal(expected);
        });

        it('will not create an empty last line', () => {

            const input = 'Hello Hello ';
            const expected = 'Hello\nHello';

            expect(Table.wordWrap(5, input).join('\n')).to.equal(expected);
        });

        it('will handle color reset code', () => {

            const input = '\x1b[31mHello\x1b[0m Hello ';
            const expected = '\x1b[31mHello\x1b[0m\nHello';

            expect(Table.wordWrap(5, input).join('\n')).to.equal(expected);
        });

        it('will handle color reset code (EMPTY version)', () => {

            const input = '\x1b[31mHello\x1b[m Hello ';
            const expected = '\x1b[31mHello\x1b[m\nHello';

            expect(Table.wordWrap(5, input).join('\n')).to.equal(expected);
        });

        it('words longer than limit will not create extra newlines', () => {

            const input = 'disestablishment is a multiplicity someotherlongword';
            const expected = 'disestablishment\nis a\nmultiplicity\nsomeotherlongword';

            expect(Table.wordWrap(7, input).join('\n')).to.equal(expected);
        });

        it('multiple line input', () => {

            const input = 'a\nb\nc d e d b duck\nm\nn\nr';
            const expected = ['a', 'b', 'c d', 'e d', 'b', 'duck', 'm', 'n', 'r'];

            expect(Table.wordWrap(4, input)).to.eql(expected);
        });

        it('will not start a line with whitespace', () => {

            const input = 'ab cd  ef gh  ij kl';
            const expected = ['ab cd', 'ef gh', 'ij kl'];
            expect(Table.wordWrap(7, input)).to.eql(expected);
        });

        it('wraps CJK chars', () => {

            const input = '漢字 漢\n字 漢字';
            const expected = ['漢字 漢', '字 漢字'];
            expect(Table.wordWrap(7, input)).to.eql(expected);
        });

        it('wraps CJK chars with colors', () => {

            const input = '\x1b[31m漢字\x1b[0m\n 漢字';
            const expected = ['\x1b[31m漢字\x1b[0m', ' 漢字'];
            expect(Table.wordWrap(5, input)).to.eql(expected);
        });
    });

    describe('getOptionsNoBorder', () => {

        it('Définit des styles, sans bordures', () => {

            const noBorder = Table.getOptionsNoBorder();
            const expected = {
                chars: {
                    bottom: '', 'bottom-left': '', 'bottom-mid': '', 'bottom-right': '',
                    left: '', 'left-mid': '', mid: '', 'mid-mid': '', middle: '',
                    right: '', 'right-mid': '',
                    top: '', 'top-left': '', 'top-mid': '', 'top-right': ''
                },
                colAligns: [], colWidths: [], head: [], rowAligns: [], rowHeights: [],
                style: {
                    border: ['grey'],
                    compact: false,
                    head: ['red'],
                    'padding-left': 1,
                    'padding-right': 1
                },
                truncate: '…'
            };

            expect(noBorder).to.be.deep.equal(expected);
        });

        it('Applique les options passées en paramètres', () => {

            const noBorder = Table.getOptionsNoBorder({
                chars: { bottom: 'b' },
                style: { compact: true },
                truncate: '!'
            });
            const expected = {
                chars: {
                    bottom: 'b', 'bottom-left': '', 'bottom-mid': '', 'bottom-right': '',
                    left: '', 'left-mid': '', mid: '', 'mid-mid': '', middle: '',
                    right: '', 'right-mid': '',
                    top: '', 'top-left': '', 'top-mid': '', 'top-right': ''
                },
                colAligns: [], colWidths: [], head: [], rowAligns: [], rowHeights: [],
                style: {
                    border: ['grey'],
                    compact: true,
                    head: ['red'],
                    'padding-left': 1,
                    'padding-right': 1
                },
                truncate: '!'
            };

            expect(noBorder).to.be.deep.equal(expected);
        });
    });
});
