/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/*
 Inspiré par cli-table2
 */

import { Cell } from './Cell';
import { ColSpanCell } from './ColSpanCell';
import { RowSpanCell } from './RowSpanCell';
import { Table } from './Table';
import * as Utils from '../Utils';

/**
 * Gère le layout du tableau
 */
export class LayoutManager {


    /**
     * Créé le layout du tableau (array)
     *
     * @param rows lignes du tableau
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    static makeTableLayout (rows: any[]): Cell[][] {

        const cellRows = LayoutManager._generateCells(rows);

        LayoutManager.layoutTable(cellRows as Table);
        LayoutManager.fillInTable(cellRows as Table);
        LayoutManager.addRowSpanCells(cellRows as Table);
        LayoutManager._addColSpanCells(cellRows);

        return cellRows;
    }


    /**
     * Génère le layout du tableau
     *
     * Renseigne `cell.x` et `cell.y`
     *
     * @param table Tableau
     */
    static layoutTable (table: Table): void {

        table.forEach((row, rowIndex) => {
            row.forEach((cell: Cell, columnIndex: number) => {

                cell.y = rowIndex;
                cell.x = columnIndex;

                for (let y = rowIndex; y >= 0; y--) {

                    const row2 = table[y];
                    const xMax = (y === rowIndex) ? columnIndex : row2.length;

                    for (let x = 0; x < xMax; x++) {

                        const cell2 = row2[x];
                        while (LayoutManager._cellsConflict(cell, cell2))
                            cell.x++;
                    }
                }
            });
        });
    }


    /**
     * Ajoute des cellules pour le `rowSpan`
     * @param table Tableau
     */
    static addRowSpanCells (table: Table): void {

        table.forEach((row, rowIndex) => {
            row.forEach((cell: Cell) => {

                for (let i = 1; i < cell.rowSpan; i++) {

                    const rowSpanCell = new RowSpanCell(cell);
                    rowSpanCell.x = cell.x;
                    rowSpanCell.y = (cell.y as number) + i;
                    rowSpanCell.colSpan = cell.colSpan;
                    LayoutManager._insertCell(rowSpanCell, table[rowIndex + i]);
                }
            });
        });
    }


    /**
     * Retourne la largeur maximum du tableau
     *
     * @param table Le tableau
     */
    static maxWidth (table: Table): number {

        let mw = 0;

        table.forEach(row => {
            row.forEach((cell: Cell) => {
                mw = Math.max(mw, (cell.x as number) + (cell.colSpan || 1));
            });
        });

        return mw;
    }


    /**
     * Remplit/complète un tableau
     * @param table Tableau
     */
    static fillInTable (table: Table): void {

        const hMax = table.length;
        const wMax = LayoutManager.maxWidth(table);

        for (let y = 0; y < hMax; y++) {
            for (let x = 0; x < wMax; x++) {

                if (!LayoutManager._conflictExists(table, x, y)) {

                    const opts = { x: x, y: y, colSpan: 1, rowSpan: 1 };
                    x++;
                    while (x < wMax &&
                        !LayoutManager._conflictExists(table, x, y)) {
                        opts.colSpan++;
                        x++;
                    }
                    let y2 = y + 1;
                    while (y2 < hMax && LayoutManager._allBlank(
                        table, y2, opts.x, opts.x + opts.colSpan)) {
                        opts.rowSpan++;
                        y2++;
                    }

                    const cell = new Cell(opts);
                    cell.x = opts.x;
                    cell.y = opts.y;
                    LayoutManager._insertCell(cell, table[y]);
                }
            }
        }
    }


    static _makeComputeWidths (
        colSpan: string, desiredWidth: string, x: string, forcedMin: number
    ): Function {

        // Renvoit une fonction
        // vals : valeurs forcées ([null, 2] : la deuxième colonne à 2
        //                         [2, 3, 4] : 1ère col 2, 2e 3, 3e 4)
        // table : le tableau à traiter
        return (vals: number[], table: Table): void => {

            const result: number[] = [];
            const spanners: Cell[] = [];

            table.forEach(row => {
                row.forEach((cell: Cell) => {

                    // S'il y a un col/rowSpan, on récupère la valeur
                    if ((cell[colSpan] || 1) > 1)
                        spanners.push(cell);
                    // Sinon on prend la valeur correspondant à la cellule
                    else {
                        result[cell[x]] = Math.max(result[cell[x]] || 0,
                            cell[desiredWidth] || 0, forcedMin);
                    }
                });
            });

            // Si une valeur est précisée, on prend celle-ci
            vals.forEach((val, index) => {

                if (Utils.isNumber(val))
                    result[index] = val;
            });

            // On parcours tous les sapnners enregistrés
            for (let k = spanners.length - 1; k >= 0; k--) {

                const cell = spanners[k];
                const span = cell[colSpan];
                const col = cell[x];
                let existingWidth = result[col];
                let editableCols = Utils.isNumber(vals[col]) ? 0 : 1;

                for (let i = 1; i < span; i++) {

                    existingWidth += 1 + result[col + i];
                    if (!Utils.isNumber(vals[col + i]))
                        editableCols++;
                }

                if (cell[desiredWidth] > existingWidth) {

                    let i = 0;
                    while (editableCols > 0 && cell[desiredWidth] > existingWidth) {

                        if (!Utils.isNumber(vals[col + i])) {

                            const dif = Math.round(
                                (cell[desiredWidth] - existingWidth) / editableCols);

                            existingWidth += dif;
                            result[col + i] += dif;
                            editableCols--;
                        }
                        i++;
                    }
                }
            }

            Utils.extend(vals, result);
            for (let j = 0; j < vals.length; j++)
                vals[j] = Math.max(forcedMin, vals[j] || 0);
        };
    }


    /**
     * Détecte s'il y a un conflit entre deux cellules à cause d'une propriété `colSpan` ou `rowSpan`
     * qui vient décaler leur placement `x` et `y`.
     *
     * @param cell1 Première cellule
     * @param cell2 Deuxième cellule
     */
    private static _cellsConflict (cell1: Cell, cell2: Cell): boolean {

        const yMin1 = cell1.y as number;
        const yMax1 = (cell1.y as number) - 1 + (cell2.rowSpan || 1);
        const yMin2 = cell2.y as number;
        const yMax2 = (cell2.y as number) - 1 + (cell2.rowSpan || 1);
        const yConflict = !(yMin1 > yMax2 || yMin2 > yMax1);

        const xMin1 = cell1.x as number;
        const xMax1 = (cell1.x as number) - 1 + (cell2.colSpan || 1);
        const xMin2 = cell2.x as number;
        const xMax2 = (cell2.x as number) - 1 + (cell2.colSpan || 1);
        const xConflict = !(xMin1 > xMax2 || xMin2 > xMax1);

        return yConflict && xConflict;
    }


    /**
     * Insert une cellule
     *
     * @param cell Cellule à insérer
     * @param row Ligne dans laquelle l'insérer
     */
    private static _insertCell (cell: Cell | RowSpanCell, row: Cell[]): void {

        let x = 0;
        while (x < row.length && ((row[x].x as number) < (cell.x as number)))
            x++;

        row.splice(x, 0, cell as Cell);
    }


    /**
     * Génère les cellules de plusieurs lignes
     *
     * @param rows Lignes du tableau
     */
    static _generateCells (rows: string[][]): Cell[][] {

        return rows.map(row => {

            if (!Array.isArray(row)) {

                const key = Object.keys(row)[0];
                row = row[key];

                if (Array.isArray(row)) {
                    row = row.slice();
                    row.unshift(key);
                }
                else
                    row = [key, row];
            }

            return row.map(cell => {
                return new Cell(cell);
            });
        });
    }


    /**
     * @param rows Lignes
     * @param y Position y
     * @param xMin Valeur minimale de x
     * @param xMax Valeur maximale de x
     */
    private static _allBlank (rows: Cell[][], y: number, xMin: number, xMax: number): boolean {

        for (let x = xMin; x < xMax; x++) {
            if (LayoutManager._conflictExists(rows, x, y))
                return false;
        }
        return true;
    }


    /**
     * Détecte s'il existe des conflits dans un tableau (cellules se chevauchants)
     * @param rows Lignes
     * @param x Position x
     * @param y Position y
     */
    private static _conflictExists (rows: Cell[][], x: number, y: number): boolean {

        const iMax = Math.min(rows.length - 1, y);
        const cell = { x: x, y: y };

        for (let i = 0; i <= iMax; i++) {

            const row = rows[i];
            for (let j = 0; j < row.length; j++) {
                if (LayoutManager._cellsConflict(cell as Cell, row[j]))
                    return true;
            }
        }

        return false;
    }


    /**
     * Ajoute un colSpan dans une rangée de cellules
     * @param cellRows Cellule d'une ligne
     */
    private static _addColSpanCells (cellRows: Cell[][]): void {

        for (let rowIndex = cellRows.length - 1; rowIndex >= 0; rowIndex--) {

            const cellColumns = cellRows[rowIndex];

            for (let columnIndex = 0; columnIndex < cellColumns.length; columnIndex++) {

                const cell = cellColumns[columnIndex];

                for (let k = 1; k < cell.colSpan; k++) {

                    const colSpanCell = new ColSpanCell();
                    colSpanCell.x = (cell.x as number) + k;
                    colSpanCell.y = cell.y;
                    cellColumns.splice(columnIndex + 1, 0, colSpanCell as unknown as Cell);
                }
            }
        }
    }
}

export const computeWidths = LayoutManager._makeComputeWidths('colSpan', 'desiredWidth', 'x', 1);
export const computeHeights = LayoutManager._makeComputeWidths('rowSpan', 'desiredHeight', 'y', 1);
