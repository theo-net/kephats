/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Cellule ne faisaint rien à part tracer des lignes vides. Sert pour le spanning des colonnes
 */
export class ColSpanCell {

    public x: number | null = 0;
    public y: number | null = 0;

    /**
     * Trace l'espace : renvoit donc une chaîne vide.
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    draw (fakeArg?: number | string): string {

        return '';
    }

    /**
     * Initialise la cellule
     */
    init (): void { return; }

    /**
     * Prépare les options. Ne fait rien
     */
    mergeTableOptions (): void { return; }
}
