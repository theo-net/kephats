/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Cell } from "./Cell";

/**
 * Gère les espacement des cellules avec les bordures supérieures et inférieures en ajoutant des
 * lignes vides. Délègue le rendu à la cellule original, mais celle classe ajoute le bon offset.
 */
export class RowSpanCell {

    public x: number | null = 0;
    public y: number | null = 0;
    public originalCell: Cell;
    public cellOffset = 0;
    public offset = 0;
    public colSpan = 0;

    /**
     * Renseigne la cellule originale
     * @param {Cell} originalCell Cellule originale
     * @constructor
     */
    constructor (originalCell: Cell) {

        this.originalCell = originalCell;
    }

    /**
     * Initialise l'espacement à partir des informations du tableau
     * @param tableOptions Options du tableau
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    init (tableOptions: Record<string, any>): void {

        const originalY = this.originalCell.y;
        this.cellOffset = this.y as number - (originalY as number);

        this.offset = tableOptions.rowHeights[originalY as number];
        for (let i = 1; i < this.cellOffset; i++)
            this.offset += 1 + tableOptions.rowHeights[originalY as number + i];
    }

    /**
     * Dessine le spanning
     * @param lineNum Numéro de la ligne, ou `top` ou `bottom`
     */
    draw (lineNum: number | string): string {

        if (lineNum == 'top')
            return this.originalCell.draw(this.offset, this.cellOffset);

        if (lineNum == 'bottom')
            return this.originalCell.draw('bottom');

        return this.originalCell.draw(this.offset + 1 + (lineNum as number));
    }

    /**
     * Prépare les options. Ne fait rien
     */
    mergeTableOptions (): void { return; }
}
