/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Cell } from './Cell';
import { LayoutManager, computeHeights, computeWidths } from './LayoutManager';
import { RowSpanCell } from './RowSpanCell';
import { Table } from './Table';
import * as Utils from '../Utils';

const layoutTable = LayoutManager.layoutTable;
const addRowSpanCells = LayoutManager.addRowSpanCells;
const maxWidth = LayoutManager.maxWidth;


describe('cli/Table/LayoutManager', () => {

    function findCell (table: Cell[][], x: number, y: number): Cell | undefined {

        for (let i = 0; i < table.length; i++) {

            const row = table[i];

            for (let j = 0; j < row.length; j++) {

                const cell = row[j];
                if (cell.x === x && cell.y === y)
                    return cell;
            }
        }
    }

    function checkExpectation (
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        actualCell: Cell, expectedCell: any,
        x: number, y: number,
        actualTable: Cell[][]
    ): void {

        if (Utils.isString(expectedCell))
            expectedCell = { content: expectedCell };

        const address = '(' + y + ',' + x + ')';

        if (Object.prototype.hasOwnProperty.call(expectedCell, 'content')) {
            expect(actualCell, address).to.be.instanceOf(Cell);
            expect(actualCell.content, 'content of ' + address)
                .to.equal((expectedCell as Cell).content);
        }

        if (Object.prototype.hasOwnProperty.call(expectedCell, 'rowSpan')) {
            expect(actualCell, address).to.be.instanceOf(Cell);
            expect(actualCell.rowSpan, 'rowSpan of ' + address)
                .to.equal((expectedCell as Cell).rowSpan);
        }

        if (Object.prototype.hasOwnProperty.call(expectedCell, 'colSpan')) {
            expect(actualCell, address).to.be.instanceOf(Cell);
            expect(actualCell.colSpan, 'colSpan of ' + address)
                .to.equal((expectedCell as Cell).colSpan);
        }

        if (Object.prototype.hasOwnProperty.call(expectedCell, 'spannerFor')) {

            expect(actualCell, address).to.be.instanceOf(RowSpanCell);
            expect(actualCell.originalCell, address
                + 'originalCell should be a cell').to.be.instanceOf(Cell);
            expect(actualCell.originalCell, address + 'originalCell not right')
                .to.equal(findCell(
                    actualTable,
                    (expectedCell as Cell).spannerFor[1],
                    (expectedCell as Cell).spannerFor[0]
                ));
        }
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function checkLayout (actualTable: Cell[][], expectedTable: any[][]): void {

        expectedTable.forEach((expectedRow, y) => {
            expectedRow.forEach((expectedCell, x) => {

                if (expectedCell !== null) {
                    const actualCell = findCell(actualTable, x, y);
                    checkExpectation(actualCell as Cell, expectedCell, x, y, actualTable);
                }
            });
        });
    }

    describe('layoutTable', () => {

        it('sets x and y', () => {

            const table = [
                [{}, {}],
                [{}, {}]
            ];

            layoutTable(table as Table);

            expect(table).to.eql([
                [{ x: 0, y: 0 }, { x: 1, y: 0 }],
                [{ x: 0, y: 1 }, { x: 1, y: 1 }]
            ]);

            const w = maxWidth(table as Table);
            expect(w).to.equal(2);
        });

        it('colSpan will push x values to the right', () => {

            const table = [
                [{ colSpan: 2 }, {}],
                [{}, { colSpan: 2 }]
            ];

            layoutTable(table as Table);

            expect(table).to.eql([
                [{ x: 0, y: 0, colSpan: 2 }, { x: 2, y: 0 }],
                [{ x: 0, y: 1 }, { x: 1, y: 1, colSpan: 2 }]
            ]);

            expect(maxWidth(table as Table)).to.equal(3);
        });

        it('rowSpan will push x values on cells below', () => {

            const table = [
                [{ rowSpan: 2 }, {}],
                [{}]
            ];

            layoutTable(table as Table);

            expect(table).to.eql([
                [{ x: 0, y: 0, rowSpan: 2 }, { x: 1, y: 0 }],
                [{ x: 1, y: 1 }]
            ]);

            expect(maxWidth(table as Table)).to.equal(2);
        });

        it('colSpan and rowSpan together', () => {

            const table = [
                [{ rowSpan: 2, colSpan: 2 }, {}],
                [{}]
            ];

            layoutTable(table as Table);

            expect(table).to.eql([
                [{ x: 0, y: 0, rowSpan: 2, colSpan: 2 }, { x: 2, y: 0 }],
                [{ x: 2, y: 1 }]
            ]);

            expect(maxWidth(table as Table)).to.equal(3);
        });

        it('complex layout', () => {

            const table = [
                [{ c: 'a' }, { c: 'b' }, { c: 'c', rowSpan: 3, colSpan: 2 }, { c: 'd' }],
                [{ c: 'e', rowSpan: 2, colSpan: 2 }, { c: 'f' }],
                [{ c: 'g' }]
            ];

            layoutTable(table as Table);

            expect(table).to.eql([
                [
                    { c: 'a', y: 0, x: 0 }, { c: 'b', y: 0, x: 1 },
                    { c: 'c', y: 0, x: 2, rowSpan: 3, colSpan: 2 }, { c: 'd', y: 0, x: 4 }
                ],
                [{ c: 'e', rowSpan: 2, colSpan: 2, y: 1, x: 0 }, { c: 'f', y: 1, x: 4 }],
                [{ c: 'g', y: 2, x: 4 }]
            ]);
        });

        it('maxWidth of single element', () => {

            const table = [[{}]];
            layoutTable(table as Table);
            expect(maxWidth(table as Table)).to.equal(1);
        });
    });

    describe('addRowSpanCells', () => {

        it('will insert a rowSpan cell - beginning of line', () => {

            const table = [
                [{ x: 0, y: 0, rowSpan: 2 }, { x: 1, y: 0 }],
                [{ x: 1, y: 1 }]
            ];

            addRowSpanCells(table as Table);

            expect(table[0]).to.eql([{ x: 0, y: 0, rowSpan: 2 }, { x: 1, y: 0 }]);
            expect(table[1].length).to.equal(2);
            expect(table[1][0]).to.be.instanceOf(RowSpanCell);
            expect(table[1][1]).to.eql({ x: 1, y: 1 });
        });

        it('will insert a rowSpan cell - end of line', () => {

            const table = [
                [{ x: 0, y: 0 }, { x: 1, y: 0, rowSpan: 2 }],
                [{ x: 0, y: 1 }]
            ];

            addRowSpanCells(table as Table);

            expect(table[0]).to.eql([{ x: 0, y: 0 }, { rowSpan: 2, x: 1, y: 0 }]);
            expect(table[1].length).to.equal(2);
            expect(table[1][0]).to.eql({ x: 0, y: 1 });
            expect(table[1][1]).to.be.instanceOf(RowSpanCell);
        });

        it('will insert a rowSpan cell - middle of line', () => {

            const table = [
                [{ x: 0, y: 0 }, { x: 1, y: 0, rowSpan: 2 }, { x: 2, y: 0 }],
                [{ x: 0, y: 1 }, { x: 2, y: 1 }]
            ];

            addRowSpanCells(table as Table);

            expect(table[0]).to.eql(
                [{ x: 0, y: 0 }, { rowSpan: 2, x: 1, y: 0 }, { x: 2, y: 0 }]
            );
            expect(table[1].length).to.equal(3);
            expect(table[1][0]).to.eql({ x: 0, y: 1 });
            expect(table[1][1]).to.be.instanceOf(RowSpanCell);
            expect(table[1][2]).to.eql({ x: 2, y: 1 });
        });

        it('will insert a rowSpan cell - multiple on the same line', () => {

            const table = [
                [
                    { x: 0, y: 0 }, { x: 1, y: 0, rowSpan: 2 },
                    { x: 2, y: 0, rowSpan: 2 }, { x: 3, y: 0 }
                ],
                [{ x: 0, y: 1 }, { x: 3, y: 1 }]
            ];

            addRowSpanCells(table as Table);

            expect(table[0]).to.eql([
                { x: 0, y: 0 }, { rowSpan: 2, x: 1, y: 0 },
                { rowSpan: 2, x: 2, y: 0 }, { x: 3, y: 0 }
            ]);
            expect(table[1].length).to.equal(4);
            expect(table[1][0]).to.eql({ x: 0, y: 1 });
            expect(table[1][1]).to.be.instanceOf(RowSpanCell);
            expect(table[1][2]).to.be.instanceOf(RowSpanCell);
            expect(table[1][3]).to.eql({ x: 3, y: 1 });
        });
    });

    it('simple 2x2 layout', () => {

        const actual = LayoutManager.makeTableLayout([
            ['hello', 'goodbye'],
            ['hola', 'adios']
        ]);

        const expected = [
            ['hello', 'goodbye'],
            ['hola', 'adios']
        ];

        checkLayout(actual, expected);
    });

    it('cross table', () => {

        const actual = LayoutManager.makeTableLayout([
            { '1.0': ['yes', 'no'] },
            { '2.0': ['hello', 'goodbye'] }
        ]);

        const expected = [
            ['1.0', 'yes', 'no'],
            ['2.0', 'hello', 'goodbye']
        ];

        checkLayout(actual, expected);
    });

    it('vertical table', () => {

        const actual = LayoutManager.makeTableLayout([
            { '1.0': 'yes' },
            { '2.0': 'hello' }
        ]);

        const expected = [
            ['1.0', 'yes'],
            ['2.0', 'hello']
        ];

        checkLayout(actual, expected);
    });

    it('colSpan adds RowSpanCells to the right', () => {

        const actual = LayoutManager.makeTableLayout([
            [{ content: 'hello', colSpan: 2 }],
            ['hola', 'adios']
        ]);

        const expected = [
            [{ content: 'hello', colSpan: 2 }, null],
            ['hola', 'adios']
        ];

        checkLayout(actual, expected);
    });

    it('rowSpan adds RowSpanCell below', () => {

        const actual = LayoutManager.makeTableLayout([
            [{ content: 'hello', rowSpan: 2 }, 'goodbye'],
            ['adios']
        ]);

        const expected = [
            ['hello', 'goodbye'],
            [{ spannerFor: [0, 0] }, 'adios']
        ];

        checkLayout(actual, expected);
    });

    it('rowSpan and cellSpan together', () => {

        const actual = LayoutManager.makeTableLayout([
            [{ content: 'hello', rowSpan: 2, colSpan: 2 }, 'goodbye'],
            ['adios']
        ]);

        const expected = [
            ['hello', null, 'goodbye'],
            [{ spannerFor: [0, 0] }, null, 'adios']
        ];

        checkLayout(actual, expected);
    });

    it('complex layout', () => {

        const actual = LayoutManager.makeTableLayout([
            [
                { content: 'hello', rowSpan: 2, colSpan: 2 },
                { content: 'yo', rowSpan: 2, colSpan: 2 }, 'goodbye'
            ],
            ['adios']
        ]);

        const expected = [
            ['hello', null, 'yo', null, 'goodbye'],
            [{ spannerFor: [0, 0] }, null, { spannerFor: [0, 2] }, null, 'adios']
        ];

        checkLayout(actual, expected);
    });

    it('complex layout2', () => {

        const actual = LayoutManager.makeTableLayout([
            ['a', 'b', { content: 'c', rowSpan: 3, colSpan: 2 }, 'd'],
            [{ content: 'e', rowSpan: 2, colSpan: 2 }, 'f'],
            ['g']
        ]);

        const expected = [
            ['a', 'b', 'c', null, 'd'],
            ['e', null, { spannerFor: [0, 2] }, null, 'f'],
            [{ spannerFor: [1, 0] }, null, { spannerFor: [0, 2] }, null, 'g']
        ];

        checkLayout(actual, expected);
    });

    it('stairstep spans', () => {

        const actual = LayoutManager.makeTableLayout([
            [{ content: '', rowSpan: 2 }, ''],
            [{ content: '', rowSpan: 2 }],
            ['']
        ]);

        const expected = [
            [{ content: '', rowSpan: 2 }, ''],
            [{ spannerFor: [0, 0] }, { content: '', rowSpan: 2 }],
            ['', { spannerFor: [1, 1] }]
        ];

        checkLayout(actual, expected);
    });

    describe('fillInTable', () => {

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        function mc (opts: Record<string, any> | string, y: number, x: number): Cell {

            const cell = new Cell(opts);

            cell.x = x;
            cell.y = y;

            return cell;
        }

        it('will blank out individual cells', () => {

            const cells = [
                [mc('a', 0, 1)],
                [mc('b', 1, 0)]
            ];
            LayoutManager.fillInTable(cells as Table);

            checkLayout(cells, [
                ['', 'a'],
                ['b', '']
            ]);
        });

        it('will autospan to the right', () => {

            const cells = [
                [],
                [mc('a', 1, 1)]
            ];
            LayoutManager.fillInTable(cells as Table);

            checkLayout(cells, [
                [{ content: '', colSpan: 2 }, null],
                ['', 'a']
            ]);
        });

        it('will autospan down', () => {

            const cells = [
                [mc('a', 0, 1)],
                []
            ];
            LayoutManager.fillInTable(cells as Table);
            addRowSpanCells(cells as Table);

            checkLayout(cells, [
                [{ content: '', rowSpan: 2 }, 'a'],
                [{ spannerFor: [0, 0] }, '']
            ]);
        });

        it('will autospan right and down', () => {

            const cells = [
                [mc('a', 0, 2)],
                [],
                [mc('b', 2, 1)]
            ];
            LayoutManager.fillInTable(cells as Table);
            addRowSpanCells(cells as Table);

            checkLayout(cells, [
                [{ content: '', colSpan: 2, rowSpan: 2 }, null, 'a'],
                [{ spannerFor: [0, 0] }, null, { content: '', colSpan: 1, rowSpan: 2 }],
                ['', 'b', { spannerFor: [1, 2] }]
            ]);
        });
    });

    describe('computeWidths', () => {

        function mc (
            y: number, x: number,
            desiredWidth: number,
            colSpan?: number
        ): Record<string, number | undefined> {

            return { x: x, y: y, desiredWidth: desiredWidth, colSpan: colSpan };
        }

        it('finds the maximum desired width of each column', () => {

            const widths: number[] = [];
            const cells = [
                [mc(0, 0, 7), mc(0, 1, 3), mc(0, 2, 5)],
                [mc(1, 0, 8), mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 6), mc(2, 1, 9), mc(2, 2, 1)]
            ];

            computeWidths(widths, cells);

            expect(widths).to.eql([8, 9, 5]);
        });

        it('won\'t touch hard coded values', () => {

            const widths = [null, 3];
            const cells = [
                [mc(0, 0, 7), mc(0, 1, 3), mc(0, 2, 5)],
                [mc(1, 0, 8), mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 6), mc(2, 1, 9), mc(2, 2, 1)]
            ];

            computeWidths(widths, cells);

            expect(widths).to.eql([8, 3, 5]);
        });

        it('assumes undefined desiredWidth is 1', () => {

            const widths: number[] = [];
            const cells = [[{ x: 0, y: 0 }], [{ x: 0, y: 1 }], [{ x: 0, y: 2 }]];
            computeWidths(widths, cells);
            expect(widths).to.eql([1]);
        });

        it('takes into account colSpan and wont over expand', () => {

            const widths: number[] = [];
            const cells = [
                [mc(0, 0, 10, 2), mc(0, 2, 5)],
                [mc(1, 0, 5), mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)]
            ];
            computeWidths(widths, cells);
            expect(widths).to.eql([5, 5, 5]);
        });

        it('will expand rows involved in colSpan in a balanced way', () => {

            const widths: number[] = [];
            const cells = [
                [mc(0, 0, 13, 2), mc(0, 2, 5)],
                [mc(1, 0, 5), mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)]
            ];
            computeWidths(widths, cells);
            expect(widths).to.eql([6, 6, 5]);
        });

        it('expands across 3 cols', () => {

            const widths: number[] = [];
            const cells = [
                [mc(0, 0, 25, 3)],
                [mc(1, 0, 5), mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)]
            ];
            computeWidths(widths, cells);
            expect(widths).to.eql([9, 9, 5]);
        });

        it('multiple spans in same table', () => {

            const widths: number[] = [];
            const cells = [
                [mc(0, 0, 25, 3)],
                [mc(1, 0, 30, 3)],
                [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)]
            ];
            computeWidths(widths, cells);
            expect(widths).to.eql([11, 9, 8]);
        });

        it('spans will only edit uneditable tables', () => {

            const widths = [null, 3];
            const cells = [
                [mc(0, 0, 20, 3)],
                [mc(1, 0, 4), mc(1, 1, 20), mc(1, 2, 5)]
            ];
            computeWidths(widths, cells);
            expect(widths).to.eql([7, 3, 8]);
        });

        it('spans will only edit uneditable tables - first column uneditable', () => {

            const widths = [3];
            const cells = [
                [mc(0, 0, 20, 3)],
                [mc(1, 0, 4), mc(1, 1, 3), mc(1, 2, 5)]
            ];
            computeWidths(widths, cells);
            expect(widths).to.eql([3, 7, 8]);
        });
    });

    describe('computeHeights', () => {

        function mc (
            y: number, x: number,
            desiredHeight: number,
            colSpan?: number
        ): Record<string, number | undefined> {

            return { x: x, y: y, desiredHeight: desiredHeight, rowSpan: colSpan };
        }

        it('finds the maximum desired height of each row', () => {

            const heights: number[] = [];
            const cells = [
                [mc(0, 0, 7), mc(0, 1, 3), mc(0, 2, 5)],
                [mc(1, 0, 8), mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 6), mc(2, 1, 9), mc(2, 2, 1)]
            ];

            computeHeights(heights, cells);

            expect(heights).to.eql([7, 8, 9]);
        });

        it('won\'t touch hard coded values', () => {

            const heights = [null, 3];
            const cells = [
                [mc(0, 0, 7), mc(0, 1, 3), mc(0, 2, 5)],
                [mc(1, 0, 8), mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 6), mc(2, 1, 9), mc(2, 2, 1)]
            ];

            computeHeights(heights, cells);

            expect(heights).to.eql([7, 3, 9]);
        });

        it('assumes undefined desiredHeight is 1', () => {

            const heights: number[] = [];
            const cells = [[{ x: 0, y: 0 }, { x: 1, y: 0 }, { x: 2, y: 0 }]];
            computeHeights(heights, cells);
            expect(heights).to.eql([1]);
        });

        it('takes into account rowSpan and wont over expand', () => {

            const heights: number[] = [];
            const cells = [
                [mc(0, 0, 10, 2), mc(0, 1, 5), mc(0, 2, 2)],
                [mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)]
            ];
            computeHeights(heights, cells);
            expect(heights).to.eql([5, 5, 4]);
        });

        it('will expand rows involved in rowSpan in a balanced way', () => {

            const heights: number[] = [];
            const cells = [
                [mc(0, 0, 13, 2), mc(0, 1, 5), mc(0, 2, 5)],
                [mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)]
            ];
            computeHeights(heights, cells);
            expect(heights).to.eql([6, 6, 4]);
        });

        it('expands across 3 rows', () => {

            const heights: number[] = [];
            const cells = [
                [mc(0, 0, 25, 3), mc(0, 1, 5), mc(0, 2, 4)],
                [mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 1, 2), mc(2, 2, 1)]
            ];
            computeHeights(heights, cells);
            expect(heights).to.eql([9, 9, 5]);
        });

        it('multiple spans in same table', () => {

            const heights: number[] = [];
            const cells = [
                [mc(0, 0, 25, 3), mc(0, 1, 30, 3), mc(0, 2, 4)],
                [mc(1, 2, 2)],
                [mc(2, 2, 1)]
            ];
            computeHeights(heights, cells);
            expect(heights).to.eql([11, 9, 8]);
        });
    });
});
