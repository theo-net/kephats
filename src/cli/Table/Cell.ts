/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


/*
 Inspiré par cli-table2
 */

import { Color } from '../Color';
import { ColSpanCell } from './ColSpanCell';
import { RowSpanCell } from './RowSpanCell';
import { Kernel } from '../../core/Kernel';
import { Table } from './Table';
import * as Utils from '../Utils';

/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * Classe représentant la cellule d'un tableau à afficher
 */
export class Cell {

    [k: string]: any;

    protected _options: Record<string, any>;

    public cells: Cell[][] | undefined = undefined;
    public content: string;
    public colSpan: number;
    public rowSpan: number;
    public x: number | null;
    public y: number | null;
    public paddingLeft = 0;
    public paddingRight = 0;
    public chars: Record<string, string> = {};
    public truncate: string | undefined = undefined;
    public head: string[] = [];
    public border: string[] = [];
    public lines: string[] = [];
    public desiredWidth = 0;
    public desiredHeight = 0;
    public widths: number[] = [];
    public heights: number[] = [];
    public width = 0;
    public height = 0;
    public hAlign: string | null = null;
    public vAlign: string | null = null;
    public drawRight = false;

    /**
     * Construit une nouvelle cellule, le paramètre option permet de la configurer :
     *
     *   - `rowSpan` Espacement ligne
     *   - `colSpan` Espacement colonne
     *   - `content` Contenu (converti tout en `string`)
     *
     * Si autre chose qu'un objet est transmis, alors il s'agira du contenu
     *
     * @param options Options à transmettre
     * @constructor
     */
    constructor (options?: any) {

        // C'est le contenu qui a été transmis (converti en string)
        if (Utils.isString(options) || Utils.isNumber(options) ||
            Utils.isBoolean(options))
            options = { content: '' + options };

        // Aucune options de transmise
        options = options || {};

        this._options = options;

        // On récupère le contenu
        const content = options.content;
        if (Utils.isString(content) || Utils.isNumber(content) ||
            Utils.isBoolean(content))
            this.content = String(content);
        else if (!content)
            this.content = '';
        else {
            throw new Error('Content needs to be a primitive, got: '
                + (typeof content));
        }

        // On définit colSpan et rowSpan
        this.colSpan = options.colSpan || 1;
        this.rowSpan = options.rowSpan || 1;

        // Propriété x et y (fixées par le layout manager)
        this.x = this.y = null;

    }


    /**
     * Transmet les options à notre cellule à partir du tableau
     *
     * @param tableOptions Options du tableau
     * @param cells Cellules du tableau
     */
    mergeTableOptions (tableOptions: Record<string, any>, cells?: Cell[][]): void {

        this.cells = cells;

        // Caractères de séparation
        const optionsChars = this._options.chars || {};
        const tableChars = tableOptions.chars;
        this.chars = {};
        ['top', 'top-mid', 'top-left', 'top-right',
            'bottom', 'bottom-mid', 'bottom-left', 'bottom-right',
            'left', 'left-mid', 'mid', 'mid-mid', 'right', 'right-mid', 'middle'
        ].forEach(name => {
            const nameCamel = Utils.camelCase(name);
            this.chars[nameCamel] = optionsChars[nameCamel] || optionsChars[name] ||
                tableChars[nameCamel] || tableChars[name];
        });

        // Truncate
        this.truncate = this._options.truncate || tableOptions.truncate;

        // Stylisation
        this._options.style = this._options.style || {};
        const tableStyle = tableOptions.style;
        this.paddingLeft =
            this._options.style['paddingLeft'] ||
            this._options.style['padding-left'] ||
            tableStyle['paddingLeft'] || tableStyle['padding-left'];
        this.paddingRight =
            this._options.style['paddingRight'] ||
            this._options.style['padding-right'] ||
            tableStyle['paddingRight'] || tableStyle['padding-right'];
        this.head = this._options.style.head || tableStyle.head;
        this.border = this._options.style.border || tableStyle.border;

        // width et height
        let fixedWidth = tableOptions.colWidths[this.x as number];
        if (tableOptions.wordWrap && fixedWidth) {
            fixedWidth -= this.paddingLeft + this.paddingRight;
            this.lines = Color.colorizeLines(
                Table.wordWrap(fixedWidth, this.content));
        }
        else
            this.lines = Color.colorizeLines(this.content.split('\n'));

        this.desiredWidth = Table.strlen(this.content)
            + this.paddingLeft + this.paddingRight;
        this.desiredHeight = this.lines.length;
    }


    /**
     * Initialise la structure des données de notre cellule.
     *
     * @param tableOptions Un objet complètement renseigné
     *
     * En plus des valeurs standards par défaut, il doit avoir les éléments `colWidths` et `rowWidths`
     * de renseignés. Ces éléments donnent le nombre de colonnes et de lignes du tableau (doivent
     * contenir un `number`).
     */
    init (tableOptions: Record<string, any>): void {

        this.widths = tableOptions.colWidths
            .slice(this.x, this.x as number + this.colSpan);
        this.heights = tableOptions.rowHeights
            .slice(this.y, this.y as number + this.rowSpan);

        this.width = this.widths.reduce(
            (a, b) => { return a + b + 1; },
            this.widths.length ? -1 : 0);
        this.height = this.heights.reduce(
            (a, b) => { return a + b + 1; },
            this.heights.length ? -1 : 0);

        this.hAlign = this._options.hAlign || tableOptions.colAligns[this.x as number];
        this.vAlign = this._options.vAlign || tableOptions.rowAligns[this.y as number];

        this.drawRight = (this.x as number) + this.colSpan == tableOptions.colWidths.length;
    }


    /**
     * Dessine la ligne demandée de la cellule
     *
     * @param lineNum Peut être `top`, `bottom` ou un numéro de ligne
     * @param spanningCell Doit être un nombre si doit être appelé depuis `RowSpanCell` et doit
     *          représenter le nombre de ligne d'espace avant le contenu. Sinon est `undefined`.
     * @returns La représentation de la ligne
     */
    draw (lineNum: string | number, spanningCell?: number): string {

        // Top et Bottom
        if (lineNum == 'top')
            return this.drawTop(this.drawRight);
        if (lineNum == 'bottom')
            return this.drawBottom(this.drawRight);

        // Longueur du padding
        const padLen = Math.max(this.height - this.lines.length, 0);

        // Padding appliqué
        let padTop;
        switch (this.vAlign) {
            case 'center':
                padTop = Math.ceil(padLen / 2);
                break;
            case 'bottom':
                padTop = padLen;
                break;
            default:
                padTop = 0;
        }

        // Ligne vide
        if ((lineNum < padTop) || (lineNum >= (padTop + this.lines.length)))
            return this.drawEmpty(this.drawRight, spanningCell as number);

        // Trunc
        const forceTruncation = (this.lines.length > this.height) &&
            ((lineNum as number) + 1 >= this.height);

        return this.drawLine((lineNum as number) - padTop, this.drawRight,
            forceTruncation, spanningCell as number);
    }


    /**
     * Rendu de la bordure supérieure de la cellule
     *
     * @param drawRight `true` si la méthode doit rendre la bordure droite de la cellule
     */
    drawTop (drawRight: boolean): string {

        const content = [];

        if (this.cells) {

            this.widths.forEach((width, index) => {

                content.push(this._topLeftChar(index));
                content.push(
                    this.chars[this.y == 0 ? 'top' : 'mid'].repeat(width)
                );
            });
        }
        else {

            content.push(this._topLeftChar(0));
            content.push(
                this.chars[this.y == 0 ? 'top' : 'mid'].repeat(this.width)
            );
        }

        // On dessine la bordure de droite
        if (drawRight)
            content.push(this.chars[this.y == 0 ? 'topRight' : 'rightMid']);

        return this._wrapWithStyleColor('border', content.join(''));
    }


    /**
     * Dessine la bordure inférieur de la cellule
     * @param drawRight `true` si on doit dessiner la bordure droite de la cellule
     */
    drawBottom (drawRight: boolean): string {

        const left = this.chars[this.x == 0 ? 'bottomLeft' : 'bottomMid'];
        const content = this.chars.bottom.repeat(this.width);
        const right = drawRight ? this.chars['bottomRight'] : '';

        return this._wrapWithStyleColor('border', left + content + right);
    }


    /**
     * Dessine une ligne vide pour la cellule. Utilisé pour le padding-top/bottom.
     *
     * @param drawRight `true` si on doit dessiner la bordure droite de la cellule
     * @param spanningCell Espacemment
     */
    drawEmpty (drawRight?: boolean, spanningCell?: number): string {

        let left = this.chars[this.x == 0 ? 'left' : 'middle'];

        if (this.x && spanningCell && this.cells) {

            let cellLeft = this.cells[this.y as number + spanningCell][this.x as number - 1];
            while (cellLeft instanceof ColSpanCell)
                cellLeft = this.cells[cellLeft.y as number][cellLeft.x as number - 1];

            if (!(cellLeft instanceof RowSpanCell))
                left = this.chars['rightMid'];
        }

        const right = drawRight ? this.chars['right'] : '';
        const content = ' '.repeat(this.width);

        return this._stylizeLine(left, content, right);
    }


    /**
     * Rendu d'une ligne de texte
     *
     * @param lineNum Numéro de la ligne du texte à rendre. Ne correspond pas forcément au contenu,
     *                  peut être le padding
     * @param drawRight `true` pour le rendu de la bordure droite
     * @param forceTruncationSymbol `true` si on doit insérer le symbole de troncature si le texte a
     *                  été tronqué verticalement. Sinon, ne le sera que lors d'une toncature
     *                  horizontale
     * @param spanningCell Padding
     */
    drawLine (
        lineNum: number, drawRight: boolean,
        forceTruncationSymbol: boolean, spanningCell: number
    ): string {

        let left = this.chars[this.x == 0 ? 'left' : 'middle'];

        if (this.x && spanningCell && this.cells) {

            let cellLeft = this.cells[this.y as number + spanningCell][this.x - 1];
            while (cellLeft instanceof ColSpanCell)
                cellLeft = this.cells[cellLeft.y as number][cellLeft.x as number - 1];

            if (!(cellLeft instanceof RowSpanCell))
                left = this.chars['rightMid'];
        }

        const leftPadding = ' '.repeat(this.paddingLeft);
        const right = drawRight ? this.chars['right'] : '';
        const rightPadding = ' '.repeat(this.paddingRight);
        let line = this.lines[lineNum];
        const len = this.width - (this.paddingLeft + this.paddingRight);
        if (forceTruncationSymbol) line += this.truncate || '…';

        let content = Table.truncate(line, len, this.truncate);
        const contentLength = Table.strlen(content);

        // On ajoute le padding
        if (len + 1 >= contentLength) {

            const padlen = len - contentLength;
            switch (this.hAlign) {
                case 'right':
                    content = ' '.repeat(padlen) + content;
                    break;
                case 'center':
                    content = ' '.repeat(padlen - Math.ceil(padlen / 2)) + content
                        + ' '.repeat(Math.ceil(padlen / 2));
                    break;
                default:
                    content = content + ' '.repeat(padlen);
                    break;
            }
        }

        content = leftPadding + content + rightPadding;

        return this._stylizeLine(left, content, right);
    }


    /**
     * Renvoit le caractère correspondant à l'angle supérieur gauche de la cellule.
     *
     * @param offset
     */
    private _topLeftChar (offset: number): string {

        const x = this.x as number + offset;
        let leftChar;

        // Nous somme sur la première ligne
        if (this.y == 0)
            leftChar = x == 0 ? 'topLeft' : (offset == 0 ? 'topMid' : 'top');
        else {

            if (x == 0)
                leftChar = 'leftMid';
            else {

                leftChar = offset == 0 ? 'midMid' : 'bottomMid';

                if (this.cells) {

                    const spanAbove =
                        this.cells[this.y as number - 1][x] instanceof ColSpanCell;
                    if (spanAbove)
                        leftChar = offset == 0 ? 'topMid' : 'mid';

                    if (offset == 0) {

                        let i = 1;
                        while (this.cells[this.y as number][x - 1] instanceof ColSpanCell)
                            i++;

                        if (this.cells[this.y as number][x - i] instanceof RowSpanCell)
                            leftChar = 'leftMid';
                    }
                }
            }
        }

        return this.chars[leftChar];
    }


    /**
     * Renvoit `content` formaté avec le bon style.
     *
     * @param styleProperty Style
     * @param content contenu
     */
    private _wrapWithStyleColor (styleProperty: string, content: string): string {

        if (this[styleProperty] && this[styleProperty].length) {

            try {
                this[styleProperty].forEach((style: string) => {
                    content = (new Kernel()).get('$color')[style](content);
                });
                return content;
            } catch (e) {
                return content;
            }
        }
        else
            return content;
    }


    /**
     * Applique les bons styles à une ligne
     *
     * @param left Bordure gauche
     * @param content Contenu de la cellule
     * @param right Bordure droite
     */
    private _stylizeLine (left: string, content: string, right: string): string {

        left = this._wrapWithStyleColor('border', left);
        right = this._wrapWithStyleColor('border', right);

        if (this.y === 0)
            content = this._wrapWithStyleColor('head', content);

        return left + content + right;
    }
}
