/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Color } from './Color';
import { levenshtein } from './Utils';

const color = new Color();

/**
 * À partir  d'une liste de possibilités, suggère les éléments correspondant à l'`input` (avec une
 * distance de Levenshtein <= 2).
 * @param input Entrée pour laquelle on cherche des suggestions
 * @param possibilities  Possibilités
 */
export function getSuggestions (input: string, possibilities: string[]): string[] {

    return possibilities
        .map(p => {
            return { suggestion: p, distance: levenshtein(input, p) };
        })
        .filter(p => p.distance <= 2)
        .sort((a, b) => a.distance - b.distance)
        .map(p => p.suggestion);
}


/**
 * Returne la différence, entre deux chaînes de caractères, en gras.
 *
 * @param from Chaîne d'origine
 * @param to Chaîne de destination
 */
export function getBoldDiffString (from: string, to: string): string {

    return to.split('').map((chr, index) => {

        if (chr != from.charAt(index))
            return color.bold(chr);
        return chr;
    }).join('');
}
