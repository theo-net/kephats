/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Inspiré par Caporal.js Matthias ETIENNE <matthias@etienne.in>
 */

import * as path from 'path';

import { Application } from '../core/Application';
import { Color } from './Color';
import { Command } from './Cli/Command';
import { Help } from './Cli/Help';
import { Kernel } from '../core/Kernel';
import { ParseArgs } from './Cli/ParseArgs';
import { Table } from './Table/Table';


/**
 * Créé une application pour la ligne de commande avec la gestion des paramètres
 *
 *      let cli = new Cli() ;
 *      cli
 *          .version('1.0.0')
 *          .command('hello', 'Affiche un Hello World!')
 *          .alias('salut')
 *          .argument('<nom>', 'Nom', 'string')
 *          .option('-e, --excla', 'Avec `!`')
 *          .action(function (args, options) {
 *              console.log('Hello ' + args.nom + (options.excla ? '!' : '.')) ;
 *          }) ;
 *      cli.run() ;
 *
 *      // nodejs monApp.js hello World -e
 *      // Hello World!
 *
 * NB: `option('--no-color')` sera récupérée dans `options.color` (avec `false` comme valeur)
 */
export class Cli extends Application {

    public bin = '';
    public parseArgs: ParseArgs;

    protected _commands: Command[] = [];
    protected _defaultCommand: Command | null = null;
    protected _description = '';
    protected _help: Help;

    /**
     * Initialise la ligne de commande
     */
    constructor (kernel: Kernel) {

        super(kernel);

        // Défini des services
        this.kernel().container().register('$color', new Color());

        // Personnalisation des logs
        const $logs = this.kernel().get('$logs'),
              $color = this.kernel().get('$color');
        $logs.setTransformMessage('info', $color.blue);
        $logs.setTransformMessage('success', $color.green);
        $logs.setTransformMessage('warning', $color.yellow);
        $logs.setTransformMessage('debug', $color.cyan);
        $logs.setTransformMessage('data', $color.magenta);

        // On récupère le nom de l'exécutable
        this.bin = path.basename(process.argv[1]);

        // Gestion de l'aide
        this._help = new Help(this);
        // Parser
        this.parseArgs = new ParseArgs();

        // On capture les signaux
        process.on('SIGINT', this._handleExit.bind(this));
        process.on('SIGQUIT', this._handleExit.bind(this));
        process.on('SIGTERM', this._handleExit.bind(this));
    }


    /**
     * Renvoit le service `$color`
     */
    color (): Color {

        return this.kernel().get('$color');
    }


    /**
     * Renvoit un nouveau `Table`
     *
     * @param options Options du tableau
     * @param style Fait appel directement à un style (`noBorder`)
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    newTable (options?: Record<string, any> | null, style = ''): Table {

        if (options === null)
            options = undefined;
        if (style == 'noBorder')
            options = Table.getOptionsNoBorder(options);

        return new Table(options);
    }


    /**
     * Retourne la liste des commandes enregistrées
     */
    getCommands (): Command[] {

        return this._commands;
    }


    /**
     * Retourne la commande par défaut
     */
    getDefaultCommand (): Command | null {

        return this._defaultCommand;
    }


    /**
     * Définit la commande par défaut (normalement on y touche pas, il suffit d'utiliser la
     * méthode `action()`)
     * @param command Commande à définir par défaut
     */
    setDefaultCommand (command: Command): void {

        this._defaultCommand = command;
    }


    /**
     * Définit ou donne la description de notre application
     *
     * @param description Description de l'applciation
     * @returns La description, si aucun paramètre, `this` sinon
     */
    description (description?: string): string | this {

        if (description) {
            this._description = description;
            return this;
        }
        else
            return this._description;
    }


    /**
     * Créé une nouvelle commande et la retourne
     *
     * @param synopsis Synopsis de la commande
     * @param description Description de la commande
     */
    command (synopsis: string, description?: string): Command {

        const cmd = new Command(synopsis, description, this);
        this._commands.push(cmd);
        return cmd;
    }


    /**
     * Définie une action pour le programme. Il s'agira de l'action par défaut si aucune commande
     * spécifique est appelée.
     *
     * @param action Action à exécuter
     */
    action (action: Function): this {

        if (!this._defaultCommand)
            this._defaultCommand = this.command('_default', 'Commande par défaut');

        this._defaultCommand.action(action);

        return this;
    }


    /**
     * Lance l'application en parsant les arguments de la ligne de commande.
     *
     * Pour accéder à l'aide, plusieurs manières :
     *
     *   - aide spécifique à une commande
     *
     *          prog help <nomCommand>
     *
     *   - aide générale
     *
     *          prog help
     *          prog --help
     *          prog -h
     *
     *      ou si la commande demandée n'existe pas
     *
     * @param argv Arguments à traiter, prendra ceux de la ligne de commande si le paramètre n'est
     *             pas défini.
     * @returns {*}
     */
    run (argv?: string[]): void {

        // Parse les arguments de la ligne de commande
        if (argv === undefined)
            argv = process.argv.slice(2);

        const args = this.parseArgs.parse(argv);
        const options = Object.assign({}, args);
        delete options._;

        // On désactive les couleurs
        if (options.color === false)
            this.color().enableColor(false);


        // L'utilisateur demande de l'aide, on lui envoit l'aide de la commande
        if (args._[0] === 'help')
            return this.help(args._.slice(1).join(' '));

        // Sinon on part à la recherche de la commande
        let argsCopy = args._.slice();
        const commandArr = [];
        let cmd;

        while (!cmd && argsCopy.length) {

            commandArr.push(argsCopy.shift());
            const cmdStr = commandArr.join(' ');
            cmd = this._commands.filter(c => {
                return (c.name() === cmdStr || c.getAlias() === cmdStr);
            })[0];
        }

        // L'utilisateur demande la version
        if (options.V || options.version)
            return console.log(this.version());

        // S'il n'y a pas de commande trouvée, on prend celle par défaut
        if (!cmd && this._defaultCommand) {
            cmd = this._defaultCommand;
            argsCopy = args._.slice();
        }

        // Pas de commande trouvée ou aide demandée : on envoit l'aide par défaut
        if (!cmd || options.help || options.h)
            return this.help();


        // If quiet, only output warning & errors
        if (options.quiet)
            this.kernel().get('$config').set('logLevel', 'warn', true);
        // verbose mode
        else if (options.v || options.verbose)
            this.kernel().get('$config').set('logLevel', 'debug', true);

        // On vérifie les arguments et les options
        let validated;
        try {
            validated = cmd.validateCall(argsCopy, options);
        } catch (err) {
            return this.fatalError(err);
        }

        return cmd.run(validated.args, validated.options);
    }


    /**
     * Retourne l'aide associée à l'application
     */
    getHelp (): Help {

        return this._help;
    }


    /**
     * Affiche l'aide de la commande demandée
     *
     * @param cmdStr Nom de la commande dont on cherche l'aide (si aucun nom ne correspond, affichera
     *               l'aide par défaut)
     */
    help (cmdStr?: string): void {

        const cmd = this._commands.filter(
            c => (c.name() === cmdStr || c.getAlias() === cmdStr)
        )[0];

        // On affiche l'aide
        console.log(this._help.display(cmd));
    }


    /**
     * Renvoit une erreur fatale
     * @param errObj Erreur à transmettre
     */
    fatalError (errObj: Error): void {

        console.error('\n' + errObj.message);
        process.exit(2);
    }


    /**
     * Capture les signaux pour quitter correctement l'application
     * @param signal Signal capturé
     */
    protected _handleExit (signal: string): void {

        console.log('\nReceived ' + signal + '. Close the application');
        process.exit(0);
    }
}
