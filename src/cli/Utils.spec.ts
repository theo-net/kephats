/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { assert, expect } from 'chai';
import * as fs from 'fs';

import * as Utils from './Utils';
import * as CoreUtils from '../core/Utils';


describe('cli/Utils', () => {

    it('extends `core/Utils`', () => {

        assert.containsAllKeys(Utils, CoreUtils);
    });


    describe('Files methods', () => {

        before(() => {

            // On prépare le terrain
            if (!Utils.fileExists('./tests'))
                fs.mkdirSync('./tests');

            fs.mkdirSync('./tests/empty');
            fs.mkdirSync('./tests/simple');
            fs.mkdirSync('./tests/complex');
            fs.mkdirSync('./tests/complex/empty');
            fs.mkdirSync('./tests/complex/sub');
            fs.mkdirSync('./tests/complex/sub/sub');

            fs.writeFileSync('./tests/file', 'foobar');
            fs.writeFileSync('./tests/simple/file1', 'foobar');
            fs.writeFileSync('./tests/simple/file2', 'foobar');
            fs.writeFileSync('./tests/complex/file1', 'foobar');
            fs.writeFileSync('./tests/complex/file2', 'foobar');
            fs.writeFileSync('./tests/complex/sub/file', 'foobar');
            fs.writeFileSync('./tests/complex/sub/sub/file1', 'foobar');
            fs.writeFileSync('./tests/complex/sub/sub/file2', 'foobar');
        });

        after((done) => {

            Utils.rmrf('./tests', done);
        });


        describe('fileExists', () => {

            it('return `true` if the file exists', function () {

                expect(Utils.fileExists('./tests/file')).to.be.equal(true);
            });

            it('return `false` if the file doesn\'t exist', function () {

                expect(Utils.fileExists('./tests/fil')).to.be.equal(false);
            });

            it('return `true` if the dir exists', function () {

                expect(Utils.fileExists('./tests/empty/')).to.be.equal(true);
            });

            it('return `false` if the dir doesn\'t exist', function () {

                expect(Utils.fileExists('./tests/dir/')).to.be.equal(false);
            });
        });


        describe('cpr', () => {

            it('copy a simple file', function (done) {

                Utils.cpr('./tests/file', './tests/file_copy', () => {
                    expect(Utils.fileExists('./tests/file_copy')).to.be.true;
                    expect(fs.readFileSync('./tests/file'))
                        .to.be.deep.equal(fs.readFileSync('./tests/file_copy'));
                    done();
                });
            });

            it('copy an empty directory', function (done) {

                Utils.cpr('./tests/empty', './tests/empty_copy', () => {
                    expect(Utils.fileExists('./tests/empty_copy')).to.be.true;
                    done();
                });
            });

            it('copy a directory with files', function (done) {

                Utils.cpr('./tests/simple', './tests/simple_copy', () => {
                    expect(Utils.fileExists('./tests/simple_copy/file1')).to.be.true;
                    expect(Utils.fileExists('./tests/simple_copy/file2')).to.be.true;
                    expect(fs.readFileSync('./tests/simple/file1'))
                        .to.be.deep.equal(fs.readFileSync('./tests/simple_copy/file1'));
                    done();
                });
            });

            it('copy a complex directory', function (done) {

                Utils.cpr('./tests/complex', './tests/complex_copy', () => {
                    expect(Utils.fileExists('./tests/complex_copy/file1')).to.be.true;
                    expect(Utils.fileExists('./tests/complex_copy/sub/file')).to.be.true;
                    expect(Utils.fileExists('./tests/complex_copy/sub/sub/file1'))
                        .to.be.true;
                    done();
                });
            });
        });


        describe('rmrf', () => {

            it('remove a file', (done) => {

                Utils.rmrf('./tests/file', () => {
                    expect(Utils.fileExists('./tests/file')).to.be.false;
                    done();
                });
            });

            it('remove an empty dir', (done) => {

                Utils.rmrf('./tests/empty', () => {
                    expect(Utils.fileExists('./tests/empty')).to.be.false;
                    done();
                });
            });

            it('remove a dir with files', (done) => {

                Utils.rmrf('./tests/simple', () => {
                    expect(Utils.fileExists('./tests/simple')).to.be.false;
                    done();
                });
            });

            it('remove a dir with subdir and not empty subdir', (done) => {

                Utils.rmrf('./tests/complex', () => {
                    expect(Utils.fileExists('./tests/complex')).to.be.false;
                    done();
                });
            });
        });
    });


    describe('.uuid', () => {

        it('retourne un uuid v4', () => {

            const reg = RegExp(
                '^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$',
                'i'
            );
            expect(reg.test(Utils.uuid())).to.be.true;
        });

        it('toujours différent', () => {

            expect(Utils.uuid()).to.be.not.equal(Utils.uuid());
        });
    });
});

