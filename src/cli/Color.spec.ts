/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


import { expect } from 'chai';
import * as sinon from 'sinon';

import { Color } from './Color';
import * as Utils from './Utils';

describe('core/Cli/Color', () => {

    const color = new Color();

    describe('supportsColor', () => {

        it('peut forcer le résultat à false', () => {

            expect(color.supportsColor(false)).to.equal(false);
        });

        it('peut forcer le résultat à true', () => {

            expect(color.supportsColor(true)).to.equal(true);
        });
    });

    describe('enableColor', () => {

        it('peut désactiver le support des couleurs', () => {

            color.enableColor(false);
            expect(color.isEnabled()).to.equal(false);
        });

        it('peut activer le support des couleurs', () => {

            color.enableColor(true);
            expect(color.isEnabled()).to.equal(true);
        });
    });

    describe('addStyle', () => {

        it('déclenche une erreur si `name` existe déjà', () => {

            expect(color.addStyle.bind(color, 'black', (text: string) => text)).to.throw();
        });

        it('déclenche une erreur si `name` n\'est pas valide', () => {

            expect(color.addStyle.bind(color, 'addStyle', (text: string) => text)).to.throw();
        });

        it('définie une méthode permettant d\'appliquer le style', () => {

            const spy = sinon.spy();
            color.addStyle('aStyle', spy);
            color.aStyle('a text');
            expect(spy.calledOnce).to.equal(true);
        });

        it('la fonction définie sera appelée avec le paramètre passé à la méthode', () => {

            const spy = sinon.spy();
            color.addStyle('secondStyle', spy);
            color.secondStyle('a text');
            expect(spy.calledWith('a text')).to.equal(true);
        });
    });

    describe('styles', () => {

        const styles = {
            reset: [0, 0],
            bold: [1, 22],
            dim: [2, 22],
            italic: [3, 23],
            underline: [4, 24],
            inverse: [7, 27],
            hidden: [8, 28],
            strikethrough: [9, 29],
            black: [30, 39],
            red: [31, 39],
            green: [32, 39],
            yellow: [33, 39],
            blue: [34, 39],
            magenta: [35, 39],
            cyan: [36, 39],
            white: [37, 39],
            grey: [90, 39],
            bgBlack: [40, 49],
            bgRed: [41, 49],
            bgGreen: [42, 49],
            bgYellow: [43, 49],
            bgBlue: [44, 49],
            bgMagenta: [45, 49],
            bgCyan: [46, 49],
            bgWhite: [47, 49]
        };

        Utils.forEach(styles, (style, name) => {
            it(name, () => {
                expect(color[name]('a text'))
                    .to.equal('\u001b[' + style[0] + 'ma text\u001b[' + style[1] + 'm');
            });
        });

        it('disable', () => {

            color.enableColor(false);
            expect(color.blue('a text')).to.equal('a text');
            color.enableColor(true);
        });

        it('combined', () => {

            expect(color.bold.inverse('a text'))
                .to.equal('\u001b[1m\u001b[7ma text\u001b[27m\u001b[22m');
        });

        it('combined (multiple)', () => {

            expect(color.bold.inverse.blue('a text'))
                .to.equal(
                    '\u001b[1m\u001b[7m\u001b[34ma text\u001b[39m\u001b[27m\u001b[22m'
                );
        });

        it('combined (with perso style)', () => {

            color.addStyle('perso', (text: string) => (text + '!'));
            expect(color.bold.perso('a text'))
                .to.equal('\u001b[1ma text!\u001b[22m');
        });
    });

    describe('colorizeLines', () => {

        it('foreground colors continue on each line', () => {

            const input = color.red('Hello\nHi').split('\n');

            expect(Color.colorizeLines(input)).to.eql([
                color.red('Hello'),
                color.red('Hi')
            ]);
        });

        it('background colors continue on each line', () => {

            const input = color.bgRed('Hello\nHi').split('\n');

            expect(Color.colorizeLines(input)).to.eql([
                color.bgRed('Hello'),
                color.bgRed('Hi')
            ]);
        });

        it('styles will continue on each line', () => {

            const input = color.underline('Hello\nHi').split('\n');

            expect(Color.colorizeLines(input)).to.eql([
                color.underline('Hello'),
                color.underline('Hi')
            ]);
        });

        it('styles that end before the break will not be applied to the next line', () => {

            const input = (color.underline('Hello') + '\nHi').split('\n');

            expect(Color.colorizeLines(input)).to.eql([
                color.underline('Hello'),
                'Hi'
            ]);
        });

        it('the reset code can be used to drop styles', () => {

            const input = '\x1b[31mHello\x1b[0m\nHi'.split('\n');
            expect(Color.colorizeLines(input)).to.eql([
                '\x1b[31mHello\x1b[0m',
                'Hi'
            ]);
        });

        it('handles aixterm 16-color foreground', () => {

            const input = '\x1b[90mHello\nHi\x1b[0m'.split('\n');
            expect(Color.colorizeLines(input)).to.eql([
                '\x1b[90mHello\x1b[39m',
                '\x1b[90mHi\x1b[0m'
            ]);
        });

        it('handles aixterm 16-color background', () => {

            const input = '\x1b[100mHello\nHi\x1b[m\nHowdy'.split('\n');
            expect(Color.colorizeLines(input)).to.eql([
                '\x1b[100mHello\x1b[49m',
                '\x1b[100mHi\x1b[m',
                'Howdy'
            ]);
        });

        it('handles aixterm 256-color foreground', () => {

            const input = '\x1b[48;5;8mHello\nHi\x1b[0m\nHowdy'.split('\n');
            expect(Color.colorizeLines(input)).to.eql([
                '\x1b[48;5;8mHello\x1b[49m',
                '\x1b[48;5;8mHi\x1b[0m',
                'Howdy'
            ]);
        });

        it('handles CJK chars', () => {

            const input = color.red('漢字\nテスト').split('\n');

            expect(Color.colorizeLines(input)).to.eql([
                color.red('漢字'),
                color.red('テスト')
            ]);
        });
    });
});
