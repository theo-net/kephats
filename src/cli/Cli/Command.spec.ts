/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Command } from './Command';
import { Cli } from '../Cli';
import { Kernel } from '../../core/Kernel';
import { InvalidArgumentValueError } from '../Error/InvalidArgumentValue';
import { InvalidOptionValueError } from '../Error/InvalidOptionValue';
import { MissingOptionError } from '../Error/MissingOption';
import { UnknownOptionError } from '../Error/UnknownOption';
import { ValidateValidator } from '../../core/Validate/Validate';
import { WrongNumberOfArgumentError } from '../Error/WrongNumberOfArgument';

describe('cli/Cli/Command', () => {

    const application = new Cli(new Kernel());
    let command = new Command('', '', application);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let commandProto: any;



    beforeEach(() => {

        command = new Command('test', 'un test', application);
        commandProto = Object.getPrototypeOf(command);
    });

    it('save name, description and application\'s instance', () => {

        expect(command.name()).to.be.equal('test');
        expect(command.getDescription()).to.be.equal('un test');
        expect(command._application).to.be.equal(application);
    });

    describe('alias', () => {

        it('enregistre un alias', () => {

            command.alias('alias');
            expect(command._alias).to.be.equal('alias');
        });

        it('retourne la commande', () => {

            expect(command.alias('alias')).to.be.equal(command);
        });
    });


    describe('argument', () => {

        it('enregistre un argument', () => {

            command.argument('arg', 'un argument', /[0-9]/, 0);
            const arg = command._args.slice()[0];

            expect(arg.synopsis).to.be.equal('arg');
            expect(arg.description).to.be.equal('un argument');
            expect(arg.getValidator()).to.be.instanceOf(ValidateValidator);
            expect(arg.getValidator().getValidators()[0].getArguments().regExp)
                .to.be.deep.equal(/[0-9]/);
            expect(arg.default).to.be.equal(0);
            expect(arg._application).to.be.equal(command._application);
        });

        it('retourne la commande', () => {

            expect(command.argument('rien', 'ne fais rien')).to.be.equal(command);
        });
    });


    describe('option', () => {

        it('enregistre une option', () => {

            command.option('-c', 'active les couleurs', /[0-9]/, 0, true);
            const opt = command.getOptions().slice()[0];

            expect(opt.synopsis).to.be.equal('-c');
            expect(opt.description).to.be.equal('active les couleurs');
            expect(opt.getValidator()).to.be.instanceOf(ValidateValidator);
            expect(opt.getValidator().getValidators()[0].getArguments().regExp)
                .to.be.deep.equal(/[0-9]/);
            expect(opt.default).to.be.equal(0);
            expect(opt.isRequired()).to.be.true;
            expect(opt._application).to.be.equal(command._application);
        });

        it('retourne la commande', () => {

            expect(command.option('--rien', 'ne fais rien')).to.be.equal(command);
        });
    });


    describe('action', () => {

        it('enregistre l\'action associée à la commande', () => {

            const fct = function (): void { return; };
            command.action(fct);

            expect(command._action).to.be.equal(fct);
        });

        it('retourne la commande', () => {

            expect(command.action(function (): void { return; })).to.be.equal(command);
        });
    });


    describe('command', () => {

        it('créé une nouvelle commande', () => {

            const cli = new Cli(new Kernel());
            const cmd1 = cli.command('test', 'un test');

            // on espionne Application.command pour voir si la méthode est appelée
            const cmd2 = cmd1.command('lol', 'desc');
            expect(cli.getCommands()[1]).to.be.equal(cmd2);
        });
    });

    describe('name', () => {

        it('retourne le nom de la commande', () => {

            command = new Command('unNom', 'un test', application);
            expect(command.name()).to.be.equal('unNom');
        });

        it('retourne une chaîne vide s\'il s\'agit de la commande par défaut', () => {

            command = new Command('_default', 'un test', application);
            expect(command.name()).to.be.equal('');
        });
    });


    describe('getAlias', () => {

        it('retourne le nom de l\'alias', () => {

            command = new Command('unNom', 'un test', application);
            command.alias('unAlias');
            expect(command.getAlias()).to.be.equal('unAlias');
        });
    });


    describe('getSynopsis', () => {

        it('retourne le synopsis de la commande (sans arguments)', () => {

            command = new Command('unNom', 'description', application);
            expect(command.getSynopsis()).to.be.equal('unNom ');
        });

        it('retourne le synopsis de la commande (avec arguments)', () => {

            command = new Command('unNom', 'description', application);
            command.argument('[truc]', 'lol', /a/)
                .argument('[foo]', 'bar', /a/);
            expect(command.getSynopsis()).to.be.equal('unNom [truc] [foo]');
        });
    });


    describe('args', () => {

        beforeEach(() => {

            command = new Command('unNom', 'description', application);
            command.argument('[truc]', 'lol', /a/)
                .argument('[foo]', 'bar', /a/);
        });

        it('renvoit tous les arguments', () => {

            expect(command.args()).to.be.equal(command._args);
        });

        it('renvoit un argument selon l\'index', () => {

            expect(command.args(1)).to.be.equal(command._args[1]);
        });
    });

    describe('_validateCall', () => {

        beforeEach(() => {

            command = new Command('unNom', 'un test', application);
            command.argument('<aa>', '', /a/, 'a')
                .argument('<ab>', '', /b/, 'b')
                .argument('[ac]', '', /c/, 'c')
                .argument('[ad]', '', /d/, 'd')
                .option('-m <num>', '', 'integer', undefined, true)
                .option('-n <num>', '', 'integer')
                .option('-k, --koala-lol', '')
                .option('-l, --list <what>', '', 'string');
        });

        describe('_checkArgsRange', () => {

            it('pas d\'erreur si nombre d\'arg correct', () => {

                expect(commandProto._checkArgsRange.bind(command, ['a', 'b', 'c', 'd']))
                    .to.not.throw(WrongNumberOfArgumentError);
                expect(commandProto._checkArgsRange.bind(command, ['a', 'b', 'c']))
                    .to.not.throw(WrongNumberOfArgumentError);
                expect(commandProto._checkArgsRange.bind(command, ['a', 'b']))
                    .to.not.throw(WrongNumberOfArgumentError);
            });

            it('erreur si nombre d\'arg insuffisant', () => {

                expect(commandProto._checkArgsRange.bind(command, ['a']))
                    .to.throw(WrongNumberOfArgumentError);
                expect(commandProto._checkArgsRange.bind(command, ['a']))
                    .to.throw('Wrong number of argument(s) for command unNom. Got 1, '
                        + 'expected between 2 and 4.');
            });

            it('erreur si nombre d\'arg trop grand', () => {

                expect(commandProto._checkArgsRange.bind(command, ['a', 'b', 'c', 'd', 'e']))
                    .to.throw(WrongNumberOfArgumentError);
                expect(commandProto._checkArgsRange.bind(command, ['a', 'b', 'c', 'd', 'e']))
                    .to.throw('Wrong number of argument(s) for command unNom. Got 5, '
                        + 'expected between 2 and 4.');
            });
        });

        describe('_validateArgs', () => {

            it('erreur si un arg est invalide', () => {

                expect(commandProto._validateArgs.bind(command, { aa: 'b', ab: 'a' }))
                    .to.throw(InvalidArgumentValueError);
                expect(commandProto._validateArgs.bind(command, { aa: 'b', ab: 'a' }))
                    .to.throw('   \u001b[31mError: Invalid value \'b\' '
                        + 'for argument \u001b[3maa\u001b[23m.\n  '
                        + '- La valeur n\'est pas valide.\u001b[39m\n   '
                        + 'Type \u001b[1mmocha --help\u001b[22m for help.\n');
            });
        });

        describe('_checkRequiredOptions', () => {

            it('pas d\'erreur si options requises présentes', () => {

                expect(commandProto._checkRequiredOptions.bind(command, { m: 1 }))
                    .to.not.throw(MissingOptionError);
                expect(commandProto._checkRequiredOptions.bind(command, { m: 1, n: 1 }))
                    .to.not.throw(MissingOptionError);
            });

            it('erreur si option requise absente', () => {

                expect(commandProto._checkRequiredOptions.bind(command, { n: 1 }))
                    .to.throw(MissingOptionError);
                expect(commandProto._checkRequiredOptions.bind(command, { n: 1 }))
                    .to.throw('Missing option \u001b[3m-m\u001b[23m.\u001b[39m');
            });
        });

        describe('_validateOptions', () => {

            it('erreur si option inconnue transmise', () => {

                expect(commandProto._validateOptions.bind(command, { b: 'a' }))
                    .to.throw(UnknownOptionError);
                expect(commandProto._validateOptions.bind(command, { b: 'a' }))
                    .to.throw('Error: Unknown option \u001b[3m-b\u001b[23m.\u001b[39m');
            });

            it('erreur si une option a une valeur invalide', () => {

                expect(commandProto._validateOptions.bind(command, { m: 'a' }))
                    .to.throw(InvalidOptionValueError);
                expect(commandProto._validateOptions.bind(command, { m: 'a' }))
                    .to.throw('   \u001b[31mError: Invalid value \'a\' '
                        + 'for option \u001b[3m-m\u001b[23m.\n  '
                        + '- Doit être un entier.\u001b[39m\n   '
                        + 'Type \u001b[1mmocha --help\u001b[22m for help.\n');
            });
        });

        describe('_addLongNotationToOptions', () => {

            it('ajoute la version longue des arguments si besoin', () => {

                expect(commandProto._addLongNotationToOptions.call(command, { m: 1, l: 'test' }))
                    .to.be.deep.equal({ m: 1, l: 'test', list: 'test' });
                expect(commandProto._addLongNotationToOptions.call(command, { k: true }))
                    .to.be.deep.equal({ k: true, 'koala-lol': true });
            });
        });

        describe('_camelCaseOptions', () => {

            it('On nettoie la liste d\'options', () => {

                expect(commandProto._camelCaseOptions.call(command, {
                    m: 1, l: 'test', list: 'test', k: true, 'koala-lol': true
                })).to.be.deep.equal({
                    m: 1, list: 'test', koalaLol: true
                });
            });
        });
    });
});
