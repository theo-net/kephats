/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Argument } from './Argument';
import { Cli } from '../Cli';
import { Kernel } from '../../core/Kernel';


describe('cli/Cli/Argument', () => {

    const app = new Cli(new Kernel());

    describe('constructor', () => {

        let arg = new Argument('<test>', 'une description', /[0-9]/, 0, app);

        beforeEach(() => {

            arg = new Argument('<test>', 'une description', /[0-9]/, 0, app);
        });

        it('détermine si l\'argument est obligatoire ou optionnel', () => {

            expect(arg.REQUIRED).to.be.not.equal(arg.OPTIONAL);
            expect(arg.getType()).to.be.equal(arg.REQUIRED);
            arg = new Argument('[test]', 'une description', /[0-9]/, 0, app);
            expect(arg.getType()).to.be.equal(arg.OPTIONAL);
        });

        it('détermine si l\'argument peut contenir un nombre variable d\'éléments', () => {

            expect(arg.isVariadic()).to.be.false;
            arg = new Argument('[test...]', 'une description', /[0-9]/, 0, app);
            expect(arg.isVariadic()).to.be.true;
        });

        it('définit le nom de l\'argument', () => {

            expect(arg.name()).to.be.equal('test');
            arg = new Argument('[test...]', 'une description', /[0-9]/, 0, app);
            expect(arg.name()).to.be.equal('test');
            arg = new Argument('<salut-toi>', 'une description', /[0-9]/, 0, app);
            expect(arg.name()).to.be.equal('salutToi');
            arg = new Argument('[HelloWorld3]', 'une description', /[0-9]/, 0, app);
            expect(arg.name()).to.be.equal('helloWorld3');
            arg = new Argument('[my-HTTP]', 'une description', /[0-9]/, 0, app);
            expect(arg.name()).to.be.equal('myHttp');
        });
    });


    describe('isRequired', () => {

        it('return true if argument is required', () => {

            const arg = new Argument('<test>', 'une description', /[0-9]/, 2, app);
            expect(arg.isRequired()).to.be.true;
        });

        it('return false if argument is not required', () => {

            const arg = new Argument('[test]', 'une description', /[0-9]/, 2, app);
            expect(arg.isRequired()).to.be.false;
        });
    });


    describe('isVariadic', () => {

        it('return true if arg can have variadic number value', () => {

            const arg = new Argument('[test...]', '', /a/, 'a', app);
            expect(arg.isVariadic()).to.be.equal(true);
        });

        it('return false if arg can\'t have variadic number value', () => {

            const arg = new Argument('[test]', '', /a/, 'a', app);
            expect(arg.isVariadic()).to.be.equal(false);
        });
    });


    describe('isOptional', () => {

        it('return true if arg is optional', () => {

            const arg = new Argument('[test]', '', /a/, 'a', app);
            expect(arg.isOptional()).to.be.equal(true);
        });

        it('return false if arg isn\'t optional', () => {

            const arg = new Argument('<test>', '', /a/, 'a', app);
            expect(arg.isOptional()).to.be.equal(false);
        });
    });
});
