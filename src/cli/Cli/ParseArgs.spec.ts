/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { ParseArgs } from './ParseArgs';

let parser: ParseArgs;

describe('cli/Cli/ParseArgs', () => {

    beforeEach(() => {

        parser = new ParseArgs();
    });


    describe('setOpts', () => {

        it('définie des opts', () => {

            parser.setOpts({ stopEarly: true, '--': true });
            expect(parser.getOpts()).to.be.deep.equal({
                stopEarly: true, '--': true
            });
        });

        it('écrase les options préenregistrée', () => {

            parser.setOpts({ stopEarly: true, '--': true });
            parser.setOpts({ stopEarly: false, '--': true });
            expect(parser.getOpts()).to.be.deep.equal({
                stopEarly: false, '--': true
            });
        });
    });


    describe('parse', () => {

        it('parse args', () => {

            expect(parser.parse(['--no-moo']))
                .to.be.deep.equal({ moo: false, _: [] });
            expect(parser.parse(['-v', 'a', '-v', 'b', '-v', 'c']))
                .to.be.deep.equal({ v: ['a', 'b', 'c'], _: [] });
        });

        it('comprehensive', () => {

            expect(
                parser.parse([
                    '--name=meowmers', 'bare', '-cats', 'woo',
                    '-h', 'awesome', '--multi=quux',
                    '--key', 'value',
                    '-b', '--bool', '--no-meep', '--multi=baz',
                    '--', '--not-a-flag', 'eek'
                ])).to.deep.equal(
                    {
                        c: true,
                        a: true,
                        t: true,
                        s: 'woo',
                        h: 'awesome',
                        b: true,
                        bool: true,
                        key: 'value',
                        multi: ['quux', 'baz'],
                        meep: false,
                        name: 'meowmers',
                        _: ['bare', '--not-a-flag', 'eek']
                    }
            );
        });

        it('newlines in params', () => {

            let args = parser.parse(['-s', 'X\nX']);
            expect(args).to.be.deep.equal({ _: [], s: 'X\nX' });

            // reproduce in bash:
            // VALUE="new
            // line"
            // node program.js --s="$VALUE"
            args = parser.parse(['--s=X\nX']);
            expect(args).to.be.deep.equal({ _: [], s: 'X\nX' });
        });

        it('slashBreak', () => {

            expect(parser.parse(['-I/foo/bar/baz']))
                .to.be.deep.equal({ I: '/foo/bar/baz', _: [] });
            expect(parser.parse(['-xyz/foo/bar/baz']))
                .to.be.deep.equal({ x: true, y: true, z: '/foo/bar/baz', _: [] });
        });

        it('nested dotted objects', () => {

            const argv = parser.parse([
                '--foo.bar', '3', '--foo.baz', '4',
                '--foo.quux.quibble', '5', '--foo.quux.oO',
                '--beep.boop'
            ]);

            expect(argv.foo).to.be.deep.equal({
                bar: 3,
                baz: 4,
                quux: {
                    quibble: 5,
                    oO: true
                }
            });
            expect(argv.beep).to.be.deep.equal({ boop: true });
        });

        it('nums', () => {

            const argv = parser.parse(['-x', '1234', '-y', '5.67', '-z', '1e7',
                '-w', '10f', '--hex', '0xdeadbeef', '789']);

            expect(argv).to.be.deep.equal({
                x: 1234, y: 5.67, z: 1e7,
                w: '10f', hex: 0xdeadbeef, _: [789]
            });
            expect(typeof argv.x).to.be.equal('number');
            expect(typeof argv.y).to.be.equal('number');
            expect(typeof argv.z).to.be.equal('number');
            expect(typeof argv.w).to.be.equal('string');
            expect(typeof argv.hex).to.be.equal('number');
            expect(typeof argv._[0]).to.be.equal('number');
        });

        it('already a number', () => {

            const argv = parser.parse(['-x', 1234, 789]);
            expect(argv).to.be.deep.equal({ x: 1234, _: [789] });
            expect(typeof argv.x).to.be.equal('number');
            expect(typeof argv._[0]).to.be.equal('number');
        });

        it('long opts', () => {

            expect(parser.parse(['--bool'])).to.be.deep.equal({ bool: true, _: [] });
            expect(parser.parse(['--pow', 'xixxle']))
                .to.be.deep.equal({ pow: 'xixxle', _: [] });
            expect(parser.parse(['--pow=xixxle']))
                .to.be.deep.equal({ pow: 'xixxle', _: [] });
            expect(parser.parse(['--host', 'localhost', '--port', '555']))
                .to.be.deep.equal({ host: 'localhost', port: 555, _: [] });
            expect(parser.parse(['--host=localhost', '--port=555']))
                .to.be.deep.equal({ host: 'localhost', port: 555, _: [] });
        });

        it('short -k=v', () => {

            const argv = parser.parse(['-b=123']);
            expect(argv).to.be.deep.equal({ b: 123, _: [] });
        });

        it('multi short -k=v', () => {

            const argv = parser.parse(['-a=whatever', '-b=robots']);
            expect(argv).to.be.deep.equal({ a: 'whatever', b: 'robots', _: [] });
        });

        it('-', () => {

            expect(parser.parse(['-n', '-'])).to.be.deep.equal({ n: '-', _: [] });
            expect(parser.parse(['-'])).to.be.deep.equal({ _: ['-'] });
            expect(parser.parse(['-f-'])).to.be.deep.equal({ f: '-', _: [] });
        });

        it('-a -- b', () => {

            expect(parser.parse(['-a', '--', 'b']))
                .to.be.deep.equal({ a: true, _: ['b'] });
            expect(parser.parse(['--a', '--', 'b']))
                .to.be.deep.equal({ a: true, _: ['b'] });
            expect(parser.parse(['--a', '--', 'b']))
                .to.be.deep.equal({ a: true, _: ['b'] });
        });

        it('move arguments after the -- into their own `--` array', () => {

            parser.setOpts({ '--': true });
            expect(parser.parse(['--name', 'John', 'before', '--', 'after']))
                .to.be.deep.equal({ name: 'John', _: ['before'], '--': ['after'] });
        });

        it('stops parsing on the first non-option when stopEarly is set', () => {

            parser.setOpts({ stopEarly: true });
            const argv = parser.parse(['--aaa', 'bbb', 'ccc', '--ddd']);

            expect(argv).to.be.deep.equal({ aaa: 'bbb', _: ['ccc', '--ddd'] });
        });


        it('whitespace should be whitespace', () => {

            const x = parser.parse(['-x', '\t']).x;
            expect(x).to.be.equal('\t');
        });


        it('numeric short args', () => {

            expect(parser.parse(['-n123'])).to.be.deep.equal({ n: 123, _: [] });
            expect(parser.parse(['-123', '456']))
                .to.be.deep.equal({ 1: true, 2: true, 3: 456, _: [] });
        });

        it('short', () => {

            expect(parser.parse(['-b'])).to.be.deep.equal({ b: true, _: [] });
            expect(parser.parse(['foo', 'bar', 'baz']))
                .to.be.deep.equal({ _: ['foo', 'bar', 'baz'] });
            expect(parser.parse(['-cats']))
                .to.be.deep.equal({ c: true, a: true, t: true, s: true, _: [] });
            expect(parser.parse(['-cats', 'meow']))
                .to.be.deep.equal({ c: true, a: true, t: true, s: 'meow', _: [] });
            expect(parser.parse(['-h', 'localhost']))
                .to.be.deep.equal({ h: 'localhost', _: [] });
            expect(parser.parse(['-h', 'localhost', '-p', '555']))
                .to.be.deep.equal({ h: 'localhost', p: 555, _: [] });
        });

        it('mixed short bool and capture', () => {

            expect(parser.parse(['-h', 'localhost', '-fp', '555', 'script.js']))
                .to.be.deep.equal({
                    f: true, p: 555, h: 'localhost',
                    _: ['script.js']
                });
        });
    });

});
