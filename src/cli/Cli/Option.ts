/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ArgOpt } from './ArgOpt';
import { Cli } from '../Cli';
import { OptionSyntaxError } from '../Error/OptionSyntax';
import { tValidator } from '../../core/Validate/Validate';

import * as Utils from '../Utils';

export class Option extends ArgOpt {

    readonly OPTIONAL_VALUE = 1;
    readonly REQUIRED_VALUE = 2;

    protected _long: string;
    protected _longCleanName: string;
    protected _required: boolean;
    protected _short: string;
    protected _shortCleanName: string;
    protected _valueType: number | undefined;
    protected _variadic: boolean | undefined;

    /**
     * @param synopsis Synopsis de l'option
     * @param description Description de l'option
     * @param validator Validateur
     * @param defaultValue Valeur par défaut (`undefined` si aucune)
     * @param required L'option est-elle nécessaire ou facultative
     * @param application Instance de l'application
     */
    constructor (
        synopsis: string, description?: string,
        validator?: tValidator|Array<tValidator>,
        defaultValue?: any, // eslint-disable-line @typescript-eslint/no-explicit-any
        required = false,
        application?: Cli
    ) {

        super(synopsis, description, validator, defaultValue, application);

        this._required = required;
        const analysis = this.analyseSynopsis();
        this._valueType = analysis.valueType;
        this._variadic = analysis.variadic;
        this._longCleanName = analysis.longCleanName;
        this._shortCleanName = analysis.shortCleanName;
        this._short = analysis.short;
        this._long = analysis.long;

        this._name = this._longCleanName || this._shortCleanName;
        this._name = Utils.camelCase(
            this._name.replace(/([[\]<>]+)/g, '').replace('...', '')
        );
    }


    getVariadic (): boolean | undefined {

        return this._variadic;
    }


    getValueType (): number | undefined {

        return this._valueType;
    }


    /**
     * Analyse le synopsis pour déterminer certaines informations
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    analyseSynopsis (): Record<string, any> {

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const info = this.synopsis.split(/[\s\t,]+/).reduce((acc: Record<string, any>, value) => {

            if (value.substring(0, 2) === '--') {

                acc.long = value;
                acc.longCleanName = value.substring(2);
            }
            else if (value.substring(0, 1) === '-') {

                acc.short = value;
                acc.shortCleanName = value.substring(1);
            }
            else if (value.substring(0, 1) === '[') {

                acc.valueType = this.OPTIONAL_VALUE;
                acc.variadic = value.substr(-4, 3) === '...';
            }
            else if (value.substring(0, 1) === '<') {

                acc.valueType = this.REQUIRED_VALUE;
                acc.variadic = value.substr(-4, 3) === '...';
            }
            else
                throw new OptionSyntaxError(this.synopsis, this._application as Cli);

            return acc;
        }, {});

        return info;
    }


    /**
     * Indique si l'argument est requis
     */
    isRequired (): boolean {

        return this._required;
    }

    getLongCleanName (): string {

        return this._longCleanName;
    }

    getShortCleanName (): string {

        return this._shortCleanName;
    }

    getLongOrShortCleanName (): string {

        return this.getLongCleanName() || this.getShortCleanName();
    }
}
