/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Cli } from '../Cli';
import { ValidateValidator, tValidator } from '../../core/Validate/Validate';

export class ArgOpt {

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public default: any;
    public description: string | undefined;
    public synopsis: string;

    public _application: Cli | undefined;
    protected _name = '';
    protected _validator: ValidateValidator;

    /**
     * @param synopsis Synopsis de l'argument/option
     * @param description Description de l'option/argument
     * @param validator Validateur
     * @param defaultValue Valeur par défaut
     * @param application Instance du programme
     */
    constructor (
        synopsis: string, description?: string,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        validator?: tValidator|Array<tValidator>, defaultValue?: any,
        application?: Cli
    ) {

        this.synopsis = synopsis;
        this.description = description;
        this.default = defaultValue;
        this._application = application;

        // Ajout du validateur
        this._validator = validator ?
            (application as Cli).kernel().get('$validate').make(validator) : null;
    }


    /**
     * Indique si l'option/argument possède une valeur par défaut
     */
    hasDefault (): boolean {

        return this.default !== undefined;
    }


    /**
     * Retourne le nom de l'option/argument
     */
    name (): string {

        return this._name;
    }


    /**
     * Valide la valeur de l'option/argument
     *
     * @param value Valeur à tester
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    isValid (value: any): boolean {

        if (!this._validator)
            return value;

        return this._validator.isValid(value);
    }


    /**
     * Retourne le validateur de l'argument/de l'option
     */
    getValidator (): ValidateValidator {

        return this._validator;
    }
}
