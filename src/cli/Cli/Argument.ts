/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ArgOpt } from './ArgOpt';
import { Cli } from '../Cli';
import { tValidator } from '../../core/Validate/Validate';
import * as Utils from '../Utils';

export class Argument extends ArgOpt {

    readonly REQUIRED = 1;
    readonly OPTIONAL = 2;

    protected _type: number;
    protected _variadic: boolean;

    /**
     * @param synopsis Synopsis de l'argument/de l'option
     *        Si commence par [, l'argument sera optionel. Si vaut ... alors
     *        correspond à un nombre variable de valeurs
     * @param description Description de l'option/ de l'argument
     * @param validator Validateur
     * @param defaultValue Valeur par défaut
     * @param application Instance du programme
     */
    constructor (
        synopsis: string, description: string,
        validator?: tValidator|Array<tValidator>,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        defaultValue?: any,
        application?: Cli
    ) {

        super(synopsis, description, validator, defaultValue, application);

        this._type = synopsis.substring(0, 1) === '[' ? this.OPTIONAL : this.REQUIRED;
        this._variadic = synopsis.substr(-4, 3) === '...';

        this._name = Utils.camelCase(
            synopsis.replace(/([[\]<>]+)/g, '').replace('...', '')
        );
    }


    /**
     * Indique si l'argument est requis
     */
    isRequired (): boolean {

        return this._type === this.REQUIRED;
    }


    /**
     * Indique si l'argument peut posséder un nombre variable de valeur
     */
    isVariadic (): boolean {

        return this._variadic === true;
    }


    /**
     * Indique si l'argument est optionnel
     */
    isOptional (): boolean {

        return this._type === this.OPTIONAL;
    }


    /**
     * Retourne le type
     */
    getType (): number {

        return this._type;
    }
}
