/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Cli } from '../Cli';
import { Color } from '../Color';
import { Kernel } from '../../core/Kernel';
import { Option } from './Option';
import { OptionSyntaxError } from '../Error/OptionSyntax';

const kernel = new Kernel(true);
kernel.container().register('$color', new Color());

describe('cli/Cli/Option', () => {

    const app = new Cli(new Kernel());


    describe('constructor', () => {

        let opt: Option = new Option('-a');

        beforeEach(() => {

            opt = new Option('-c', 'une description', /[0-9]/, 0, true, app);
        });

        it('détermine si l\'option peut contenir un nombre variable d\'éléments', () => {

            expect(opt.getVariadic()).to.be.equal(undefined);
            opt = new Option('-test [lol...]', 'une description');
            expect(opt.getVariadic()).to.be.true;
            opt = new Option('-test [lol]', 'une description');
            expect(opt.getVariadic()).to.be.false;
        });

        it('détermine si l\'option doit avoir une valeur ou non', () => {

            expect(opt.getValueType()).to.be.equal(undefined);
            opt = new Option('-test [lol]', 'une description');
            expect(opt.getValueType()).to.be.equal(opt.OPTIONAL_VALUE);
            opt = new Option('-test <lol>', 'une description');
            expect(opt.getValueType()).to.be.equal(opt.REQUIRED_VALUE);
        });

        it('définit le nom de l\'option', () => {

            expect(opt.name()).to.be.equal('c');
            opt = new Option('--test...', 'une description');
            expect(opt.name()).to.be.equal('test');
            opt = new Option('--salut-toi>', 'une description');
            expect(opt.name()).to.be.equal('salutToi');
            opt = new Option('--HelloWorld3 <path>', 'une description');
            expect(opt.name()).to.be.equal('helloWorld3');
            opt = new Option('--my-HTTP [file]', 'une description');
            expect(opt.name()).to.be.equal('myHttp');
        });

        it('enregistre si l\'option est obligatoire ou facultative', () => {

            expect(opt.isRequired()).to.be.true;
            opt = new Option('-c', 'une description', undefined, 0, false, app);
            expect(opt.isRequired()).to.be.false;
        });
    });


    describe('_analyseSynopsis', () => {

        it('long option', () => {

            const opt = new Option('--force');
            const expected = {
                long: '--force',
                longCleanName: 'force'
            };

            expect(opt.analyseSynopsis()).to.be.deep.equal(expected);
        });

        it('short option', () => {

            const opt = new Option('-f');
            const expected = {
                short: '-f',
                shortCleanName: 'f'
            };

            expect(opt.analyseSynopsis()).to.be.deep.equal(expected);
        });

        it('constantes OPTIONAL_VALUE et REQUIRED_VALUE définies', () => {

            const opt = new Option('-a');
            expect(opt.OPTIONAL_VALUE).to.be.not.equal(opt.REQUIRED_VALUE);
        });

        it('optional value', () => {

            const opt = new Option('-f [valeur]');
            const expected = {
                short: '-f',
                shortCleanName: 'f',
                valueType: opt.OPTIONAL_VALUE,
                variadic: false
            };

            expect(opt.analyseSynopsis()).to.be.deep.equal(expected);
        });

        it('required value', () => {

            const opt = new Option('-f <valeur>');
            const expected = {
                short: '-f',
                shortCleanName: 'f',
                valueType: opt.REQUIRED_VALUE,
                variadic: false
            };

            expect(opt.analyseSynopsis()).to.be.deep.equal(expected);
        });

        it('l\'option peut avoir un nombre variable de valeurs', () => {

            const opt = new Option('-f <valeur...>');
            const expected = {
                short: '-f',
                shortCleanName: 'f',
                valueType: opt.REQUIRED_VALUE,
                variadic: true
            };

            expect(opt.analyseSynopsis()).to.be.deep.equal(expected);
        });

        it('envoit une erreur s\'il y a un problème de syntaxe', () => {

            const opt = new Option('-f <valeur...>', '', /a/, 0, true, app);
            opt.synopsis = 'f <valeur...>';

            expect(opt.analyseSynopsis.bind(opt)).to.throw(OptionSyntaxError);
            expect(opt.analyseSynopsis.bind(opt))
                .to.throw('Syntax error in option synopsis: f <valeur...>');
        });
    });


    describe('isRequired', () => {

        it('return true if option is required', () => {

            const opt = new Option('-f <value>', 'desc', /[0-9]/, 'truc', true, app);
            expect(opt.isRequired()).to.be.true;
        });

        it('return false if option is not required', () => {

            const opt = new Option('-f <value>', 'desc', /[0-9]/, 'truc', false, app);
            expect(opt.isRequired()).to.be.false;
        });
    });
});
