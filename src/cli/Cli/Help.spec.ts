/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Cli } from '../Cli';
import { Help } from './Help';
import { Kernel } from '../../core/Kernel';
import  { Table } from '../Table/Table';

const globalOpts = `\u001b[1mGLOBAL OPTIONS\u001b[22m

    \u001b[32m -h\u001b[39m,\u001b[32m --help\u001b[39m        Display help`
    + '                                      ' + `
    \u001b[32m -V\u001b[39m,\u001b[32m --version\u001b[39m     Display version`
    + '                                   ' + `
    \u001b[32m --no-color\u001b[39m        Disable colors`
    + '                                    ' + `
    \u001b[32m --quiet\u001b[39m           Quiet mode - `
    + `only displays warn and error messages
    \u001b[32m -v\u001b[39m,\u001b[32m --verbose\u001b[39m     `
    + 'Verbose mode - will also output debug messages    ';

describe('cli/Cli/Help', () => {

    let cli: Cli = new Cli(new Kernel());

    describe('display', () => {

        beforeEach(() => {

            cli = new Cli(new Kernel());
            (cli.name('myApp') as Cli).version('0.0.1a');
        });

        it('Aide pour un programme vide', () => {

            cli = new Cli(new Kernel());
            expect(cli.getHelp().display())
                .to.be.equal(
                    '\n   \u001b[36mmocha\u001b[39m \n\n'
                    + '   \u001b[1mUSAGE\u001b[22m\n\n     '
                    + '\u001b[3mmocha\u001b[23m '
                    + '\u001b[34m\u001b[35m<command>\u001b[39m\u001b[39m '
                    + '\u001b[33m[options]\u001b[39m\n\n   '
                    + globalOpts + '\n'
                );
        });

        it('Aide pour un programme avec un nom', () => {

            cli = new Cli(new Kernel());
            cli.name('myAppCool');
            expect(cli.getHelp().display())
                .to.be.equal('\n   \u001b[36mmyAppCool\u001b[39m \n\n'
                    + '   \u001b[1mUSAGE\u001b[22m\n\n     '
                    + '\u001b[3mmocha\u001b[23m '
                    + '\u001b[34m\u001b[35m<command>\u001b[39m\u001b[39m '
                    + '\u001b[33m[options]\u001b[39m\n\n   '
                    + globalOpts + '\n'
                );
        });

        it('Aide pour un programme avec une version', () => {

            cli = new Cli(new Kernel());
            (cli.name('myApp') as Cli).version('0.0.1a');
            expect(cli.getHelp().display())
                .to.be.equal(
                    '\n   \u001b[36mmyApp\u001b[39m \u001b[2m0.0.1a\u001b[22m\n\n'
                    + '   \u001b[1mUSAGE\u001b[22m\n\n     '
                    + '\u001b[3mmocha\u001b[23m '
                    + '\u001b[34m\u001b[35m<command>\u001b[39m\u001b[39m '
                    + '\u001b[33m[options]\u001b[39m\n\n   '
                    + globalOpts + '\n'
                );
        });

        it('Aide pour un programme avec une description', () => {

            cli = new Cli(new Kernel());
            ((cli.name('myApp') as Cli).version('0.0.1a') as Cli).description('Une app trop cool');
            expect(cli.getHelp().display())
                .to.be.equal(
                    '\n   \u001b[36mmyApp\u001b[39m \u001b[2m0.0.1a\u001b[22m'
                    + ' - Une app trop cool\n\n'
                    + '   \u001b[1mUSAGE\u001b[22m\n\n     '
                    + '\u001b[3mmocha\u001b[23m '
                    + '\u001b[34m\u001b[35m<command>\u001b[39m\u001b[39m '
                    + '\u001b[33m[options]\u001b[39m\n\n   '
                    + globalOpts + '\n'
                );
        });
    });

    describe('_colorize', () => {

        const help = new Help(new Cli(new Kernel()));
        const HelpProto = Object.getPrototypeOf(help);

        it('Ne fait rien pour un texte basique', () => {

            expect(HelpProto._colorize.call(help, 'Un texte')).to.be.equal('Un texte');
        });

        it('Met en bleu les arguments (entre `<>`)', () => {

            expect(HelpProto._colorize.call(help, 'Un texte <arg> ou <arg2>'))
                .to.be.equal(
                    'Un texte \u001b[34m<arg>\u001b[39m ou \u001b[34m<arg2>\u001b[39m'
                );
        });

        it('Met en magenta `<command>`', () => {

            expect(HelpProto._colorize.call(help, 'Une <command> dans un texte'))
                .to.be.equal(
                    'Une \u001b[34m\u001b[35m<command>\u001b[39m\u001b[39m dans un texte'
                );
        });

        it('Met en jaune les options (entre `[]`', () => {

            expect(HelpProto._colorize.call(help, 'Une [opt] et une autre [opt2]'))
                .to.be.equal(
                    'Une \u001b[33m[opt]\u001b[39m et une autre \u001b[33m[opt2]\u001b[39m'
                );
        });

        it('Met en vert les options que l\'on détaille ` - ...`', () => {

            expect(HelpProto._colorize.call(help, 'Un texte -h, --help'))
                .to.be.equal(
                    'Un texte\u001b[32m -h\u001b[39m,\u001b[32m --help\u001b[39m'
                );
        });
    });

    describe('_getGlobalOptions', () => {

        it('Renvoit la description des options globales', () => {

            const help = new Help(new Cli(new Kernel()));
            const HelpProto = Object.getPrototypeOf(help);
            expect(HelpProto._getGlobalOptions.call(help)).to.be.equal(globalOpts);
        });
    });

    describe('_getSimpleTable', () => {

        it('Retourne un nouveau tableau', () => {

            const help = new Help(new Cli(new Kernel()));
            const HelpProto = Object.getPrototypeOf(help);
            expect(HelpProto._getSimpleTable.call(help) instanceof Table).to.be.true;
        });

        it('Définit des styles, sans bordures', () => {

            const help = new Help(new Cli(new Kernel()));
            const HelpProto = Object.getPrototypeOf(help);
            const noBorder = HelpProto._getSimpleTable.call(help);
            const expected = {
                chars: {
                    bottom: '', 'bottom-left': '', 'bottom-mid': '', 'bottom-right': '',
                    left: '', 'left-mid': '', mid: '', 'mid-mid': '', middle: '',
                    right: '', 'right-mid': '',
                    top: '', 'top-left': '', 'top-mid': '', 'top-right': ''
                },
                colAligns: [], colWidths: [], head: [], rowAligns: [], rowHeights: [],
                style: {
                    border: ['grey'],
                    compact: false,
                    head: ['red'],
                    'padding-left': 5,
                    'padding-right': 0
                },
                truncate: '…'
            };

            expect(noBorder.getOptions()).to.be.deep.equal(expected);
        });
    });

    describe('_getPredefinedOptions', () => {

        it('retourne la liste des options par défaut', () => {

            const help = new Help(new Cli(new Kernel()));
            const expected = [
                ['-h, --help', 'Display help'],
                ['-V, --version', 'Display version'],
                ['--no-color', 'Disable colors'],
                ['--quiet', 'Quiet mode - only displays warn and error messages'],
                ['-v, --verbose', 'Verbose mode - will also output debug messages']
            ];

            expect(help.getPredefinedOptions()).to.be.deep.equal(expected);
        });
    });
});
