/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Argument } from './Argument';
import { Cli } from '../Cli';
import { Option } from './Option';
import { InvalidArgumentValueError } from '../Error/InvalidArgumentValue';
import { InvalidOptionValueError } from '../Error/InvalidOptionValue';
import { MissingOptionError } from '../Error/MissingOption';
import { NoActionError } from '../Error/NoAction';
import { tValidator } from '../../core/Validate/Validate';
import { UnknownOptionError } from '../Error/UnknownOption';
import { WrongNumberOfArgumentError } from '../Error/WrongNumberOfArgument';

export class Command {

    readonly NATIVE_OPTIONS =  [
        'h', 'help', 'V', 'version', 'no-color', 'quiet', 'v', 'verbose'
    ];

    public _action: Function = (): void => { return; };
    public _alias: string | null = null;
    public _application: Cli | undefined;
    public _args: Argument[] = [];
    protected _description: string | undefined;
    protected _name: string | null;
    protected _options: Option[] = [];

    /**
     * Créé une nouvelle commande
     *
     * @param name Nom de la commande
     * @param description Description de la commande
     * @param application Instance de l'application.
     */
    constructor (name: string | null, description?: string, application?: Cli) {

        this._name = name;
        this._description = description;
        this._application = application;
    }


    /**
     * Retourne la decription
     */
    getDescription (): string | undefined {

        return this._description;
    }


    /**
     * Défini un alias pour cette commande. Un seul alias est possible par commande.
     *
     * @param alias Alias
     */
    alias (alias: string): Command {

        this._alias = alias;

        return this;
    }


    /**
     * Ajoute un argument
     *
     * @param synopsis Synopsis de l'argument comme `<mon-argument>` ou `[mon-argument]`. `<>`
     *        indiquent un argument requis et `[]` un argument optionnel.
     * @param description Description de l'option ou de l'argument
     * @param validator Option validator, used for checking or casting.
     * @param defaultValue Valeur par défaut
     */
    argument (
        synopsis: string, description: string,
        validator?: tValidator|Array<tValidator>,
        defaultValue?: any // eslint-disable-line @typescript-eslint/no-explicit-any
    ): Command {

        const arg = new Argument(
            synopsis, description,
            validator, defaultValue,
            this._application);

        this._args.push(arg);

        return this;
    }


    /**
     * Ajoute une option
     *
     * @param synopsis Synopsis de l'option comme `-f, --force` ou `-f, --file <file>` ou
     *                  `--hello [path]`
     * @param description Description de l'option
     * @param validator Validateur
     * @param defaultValue Valeur par défaut
     * @param required L'option est-elle nécessaire
     */
    option (
        synopsis: string, description: string,
        validator?: tValidator|Array<tValidator>,
        defaultValue?: any, // eslint-disable-line @typescript-eslint/no-explicit-any
        required = false
    ): Command {

        const opt = new Option(synopsis, description, validator, defaultValue,
            required, this._application);
        this._options.push(opt);

        return this;
    }


    /**
     * Retourne les options
     */
    getOptions (): Option[] {

        return this._options;
    }


    /**
     * Définie l'action correspondante à la commande
     *
     * @param action Action à exécuter
     */
    action (action: Function): Command {

        this._action = action;
        return this;
    }


    /**
     * Permet d'enchaîner une méthode `command()` avec une autre `.command()`
     */
    command (synopsis: string, description: string): Command {  // eslint-disable-line @typescript-eslint/no-explicit-any

        return (this._application as Cli).command(synopsis, description);
    }

    /**
     * Retourne le nom de la commande ou une chaîne vide s'il s'agit de la commande par défaut.
     */
    name (): string | null {

        return this._name === '_default' ? '' : this._name;
    }

    /**
     * Retourne l'alias de la commande
     */
    getAlias (): string | null {

        return this._alias;
    }


    /**
     * Retourne le synopsis
     */
    getSynopsis (): string {

        return this.name() + ' ' + ((this.args() as Argument[]).map(a => a.synopsis).join(' '));
    }


    /**
     * @param index Index de l'argument
     */
    args (index?: number): Argument | Argument[] {

        return typeof index !== 'undefined' ? this._args[index] : this._args;
    }


    /**
     * Valide les arguments et les options
     * @param args Arguments à valider
     * @param options Options à valider
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    validateCall (args: any[], options: Record<string, any>): Record<string, any> {

        // vérifie que le bon nombre d'arguments est transmis
        this._checkArgsRange(args);

        // Récupère les arguments dans un tableau : chaque élément est un arguments ou un tableau
        // contenant la liste des valeurs (variadic)
        args = (this.args() as Argument[]).reduce(
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            (acc: (Argument | any[])[], arg: Argument) => {
                if (arg.isVariadic())
                    acc.push(args.slice());
                else
                    acc.push(args.shift() as Argument);
                return acc;
            }, []);

        // Transfrome le tableau en objet, et récupère si besoin les valeurs par défauts
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        let argsFinal = (this.args() as Argument[]).reduce(
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            (acc: Record<string, any>, arg, index) => {
                if (typeof args[index] !== 'undefined')
                    acc[arg.name()] = args[index];
                else if (arg.hasDefault())
                    acc[arg.name()] = arg.default();
                return acc;
            }, {});

        // Vérifie la valeur des arguments
        argsFinal = this._validateArgs(argsFinal);

        // On valide les options
        options = this._checkRequiredOptions(options);
        options = this._validateOptions(options);

        // On nettoie les options
        options = this._addLongNotationToOptions(options);
        options = this._camelCaseOptions(options);

        return { args: argsFinal, options };
    }


    /**
     * Vérifie si le bon nombre d'arguments est présent. Lance une exception sinon
     *
     * @param args Arguments à tester
     * @throws {WrongNumberOfArgumentError}
     */
    private _checkArgsRange (args: Argument[]): void {

        // On récupère le nombre minimum et maximum d'arguments requis
        const min = (this.args() as Argument[]).filter(a => a.isRequired()).length;
        const max = ((this.args() as Argument[]).find(a => a.isVariadic()) !== undefined) ?
            Infinity : (min + (this.args() as Argument[]).filter(a => a.isOptional()).length);

        // Nombre actuel d'argument
        const count = args.length;

        if (count < min || count > max) {

            throw new WrongNumberOfArgumentError(
                'Wrong number of argument(s)'
                + (this.name() ? ' for command ' + this.name() : '')
                + '. Got ' + count + ', expected '
                + (min === max ? 'exactly ' + min : 'between ' + min + ' and ' + max)
                + '.',
                {}, this._application as Cli
            );
        }
    }


    /**
     * Valide les arguments
     *
     * @param {Object} args Arguments à tester
     * @returns {Object} Arguments qui seront transmis à la commande
     * @throws {InvalidArgumentValueError}
     */
    private _validateArgs (args: Record<string, string>): Record<string, string> {

        return Object.keys(args).reduce((acc: Record<string, string>, key) => {

            const arg = this._args.find(a => a.name() === key);
            const value = args[key];

            if (!(arg as Argument).isValid(value)) {
                throw new InvalidArgumentValueError(arg as Argument, value, this,
                    this._application as Cli);
            }
            else
                acc[key] = value;

            return acc;
        }, {});
    }


    /**
     * Vérifie si les options requises ont été transmises. Si une option est
     * requise, mais n'a pas été transmise : si une valeur par défaut existe, on
     * prendra cette valeur, sinon une exception sera levée.
     *
     * @param options Options transmises
     * @throws {MissingOptionError}
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private _checkRequiredOptions (options: Record<string, Option>): Record<string, any> {

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        return this._options.reduce((acc: Record<string, any>, opt) => {

            if (typeof acc[opt.getLongCleanName()] === 'undefined' &&
                typeof acc[opt.getShortCleanName()] === 'undefined') {

                if (opt.hasDefault())
                    acc[opt.getLongCleanName()] = opt.default;
                else if (opt.isRequired()) {
                    throw new MissingOptionError(opt.getLongOrShortCleanName(),
                        this, this._application as Cli);
                }
            }

            return acc;
        }, options);
    }


    /**
     * Vérifie la valeur des options
     * @param options Options à vérifier
     * @throws UnknownOptionError
     * @throws InvalidOptionError
     * @returns Options traitées
     */
    private _validateOptions (options: Record<string, string>): Record<string, string> {

        return Object.keys(options).reduce((acc, key) => {

            if (this.NATIVE_OPTIONS.indexOf(key) !== -1)
                return acc;

            const value = acc[key];
            const opt = this._findOption(key);

            if (!opt)
                throw new UnknownOptionError(key, this, this._application as Cli);

            if (!opt.isValid(value))
                throw new InvalidOptionValueError(opt, value, this, this._application as Cli);
            else
                acc[key] = value;

            return acc;
        }, options);
    }


    /**
     * Ajoute la notation longue si elle existe.
     * @param options Options transmises
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private _addLongNotationToOptions (options: Record<string, any>): Record<string, any> {

        return Object.keys(options).reduce((acc, key) => {

            if (key.length === 1) {

                const value = acc[key];
                const opt = this._findOption(key);
                if (opt && opt.getLongCleanName())
                    acc[opt.getLongCleanName()] = value;
            }
            return acc;
        }, options);
    }


    /**
     * On prend le nom en camelCase pour les options longues et on supprime leur version courte.
     * Celles qui n'ont que la version courte sont conservées.
     * @param options Options transmises
     */
    private _camelCaseOptions (options: Record<string, Option>): Record<string, Option> {

        return this._options.reduce((acc: Record<string, Option>, opt) => {

            if (options[opt.getLongCleanName()] !== undefined)
                acc[opt.name()] = options[opt.getLongCleanName()];
            else if (options[opt.getShortCleanName()] !== undefined)
                acc[opt.name()] = options[opt.getShortCleanName()];

            return acc;
        }, {});
    }


    /**
     * Retourne l'option `name`
     * @param name Nom de l'option (court ou long)
     */
    private _findOption (name: string): Option | undefined {

        return this._options.find(
            o => (o.getShortCleanName() === name || o.getLongCleanName() === name)
        );
    }


    /**
     * Retourne la liste des options avec un nom long
     */
    getLongOptions (): string[] {

        return this._options.map(option => option.getLongCleanName())
            .filter(option => typeof option !== 'undefined');
    }



    /**
     * Exécute la commande
     *
     * @param args Arguments
     * @param options Options
     * @throws {NoActionError} Si aucune action n'est associée à la commande
     */
    run (args: Argument[], options: Option[]): void {

        if (!this._action) {

            return (this._application as Cli).fatalError(new NoActionError(
                'KephaJs Setup Error: You don\'t have defined an action for your '
                + 'program/command. Use .action()', {}, this._application as Cli));
        }

        const actionResults = this._action.apply(this, [args, options]);
        const response = Promise.resolve(actionResults);

        response
            .catch(error => {
                if (!(error instanceof Error))
                    error = new Error(error);
            });
    }
}
