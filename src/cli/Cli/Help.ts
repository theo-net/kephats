/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Argument } from './Argument';
import { Cli } from '../Cli';
import { Command } from './Command';
import { Table } from '../Table/Table';

/**
 * Gère l'affiche de l'aide
 */
export class Help {

    protected _cli: Cli;

    /**
     * Associe l'aide avec l'application
     * @param cli Instance de l'application
     */
    constructor (cli: Cli) {

        this._cli = cli;
    }

    /**
     * Affiche l'aide de `command`
     *
     * @param command Commande dont on veut l'aide
     */
    display (command?: Command | null): string {

        if (!command && this._cli.getCommands().length === 1)
            command = this._cli.getCommands()[0];

        let help = '\n   '
            + this._cli.color().cyan(this._cli.name() || this._cli.bin) + ' '
            + this._cli.color().dim(
                this._cli.version() ? this._cli.version() : '')
            + (this._cli.description() ? ' - ' + this._cli.description() : '')
            + '\n' + '\n'
            + '   ' + this._getUsage(command as Command)
            + '';

        if (!command && this._cli.getCommands().length > 1)
            help += '\n\n   ' + this._getCommands();

        help += '\n\n   ' + this._getGlobalOptions() + '\n';

        return help;
    }


    /**
     * Retourne la liste des options prédéfinies
     */
    getPredefinedOptions (): string[][] {

        return [
            ['-h, --help', 'Display help'],
            ['-V, --version', 'Display version'],
            ['--no-color', 'Disable colors'],
            ['--quiet', 'Quiet mode - only displays warn and error messages'],
            ['-v, --verbose', 'Verbose mode - will also output debug messages']
        ];
    }


    /**
     * Colorise un texte pour le rendu de l'aide
     * @param text Texte à formater
     */
    private _colorize (text: string): string {

        return text.replace(/<([a-z0-9-_.]+)>/gi, (match) => {
            return this._cli.color().blue(match);
        }).replace(/<command>/gi, (match) => {
            return this._cli.color().magenta(match);
        }).replace(/\[([a-z0-9-_.]+)\]/gi, (match) => {
            return this._cli.color().yellow(match);
        }).replace(/ --?([a-z0-9-_.]+)/gi, (match) => {
            return this._cli.color().green(match);
        });
    }


    /**
     * Retourne la liste des commandes
     */
    private _getCommands (): string {

        const commandTable = this._getSimpleTable();

        this._cli.getCommands().forEach(cmd => {
            commandTable.push(
                [this._cli.color().magenta(cmd.getSynopsis()), cmd.getDescription()]
            );
        });

        commandTable.push(
            [this._cli.color().magenta('help <command>'),
                'Display help for a specific command']
        );

        return this._cli.color().bold('COMMANDS') + '\n\n'
            + this._colorize(commandTable.toString());
    }


    /**
     * Retourne les options globales
     */
    private _getGlobalOptions (): string {

        const optionsTable = this._getSimpleTable();

        this.getPredefinedOptions().forEach(opt => optionsTable.push(opt));

        return this._cli.color().bold('GLOBAL OPTIONS') + '\n\n'
            + this._colorize(optionsTable.toString());
    }


    /**
     * Retourne l'aide d'une commande
     * @param cmd Commande
     */
    private _getCommandHelp (cmd: Command): string {

        const args = cmd.args();
        const options = cmd.getOptions();

        // Usage
        let help = (cmd.name() ? cmd.name() + ' ' : '')
            + (args as Argument[]).map(a => a.synopsis).join(' ');

        // Arguments
        if ((args as Argument[]).length) {

            help += '\n\n   ' + this._cli.color().bold('ARGUMENTS') + '\n\n';

            const argsTable = this._getSimpleTable();
            (args as Argument[]).forEach(a => {

                const def = a.hasDefault() ?
                    'default: ' + JSON.stringify(a.default) : '';
                const req = a.isRequired() ? this._cli.color().bold('required')
                    : this._cli.color().grey('optional');
                argsTable.push([
                    a.synopsis, a.description, req, this._cli.color().grey(def)
                ]);
            });

            help += argsTable.toString();
        }

        // Options
        if (options.length) {

            help += '\n\n   ' + this._cli.color().bold('OPTIONS') + '\n\n';

            const optionsTable = this._getSimpleTable();
            options.forEach(o => {

                const def = o.hasDefault() ?
                    'default: ' + JSON.stringify(o.default) : '';
                const req = o.isRequired() ? this._cli.color().bold('required')
                    : this._cli.color().grey('optional');
                optionsTable.push([
                    o.synopsis, o.description, req, this._cli.color().grey(def)
                ]);
            });

            help += optionsTable.toString();
        }

        return help;
    }


    /**
     * Retourne l'usage d'une commande
     * @param cmd Commande
     */
    private _getUsage (cmd: Command): string {

        let help = this._cli.color().bold('USAGE')
            + '\n\n     '
            + this._cli.color().italic(this._cli.bin) + ' ';

        if (cmd)
            help += this._colorize(this._getCommandHelp(cmd));
        else
            help += this._colorize('<command> [options]');

        return help;
    }


    /**
     * Retourne un tableau sans bordure, formaté pour le rendu de l'aide
     * @private
     */
    private _getSimpleTable (): Table {

        return this._cli.newTable({
            style: {
                'padding-left': 5,
                'padding-right': 0
            }
        }, 'noBorder');
    }
}
