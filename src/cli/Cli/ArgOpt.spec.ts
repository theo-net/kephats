/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { ArgOpt } from './ArgOpt';
import { Cli } from '../Cli';
import { Kernel } from '../../core/Kernel';
import { Validator } from '../../core/Validate/Validator';
import { IntegerValidator } from '../../core/Validate/Integer';


describe('cli/Cli/ArgOpt', () => {

    const app = new Cli(new Kernel());

    describe('constructor', () => {

        let arg = new ArgOpt('', '');

        beforeEach(() => {

            arg = new ArgOpt('<test>', 'une description', /[0-9]/, 0, app);
        });

        it('sauve le synopsis', () => {

            expect(arg.synopsis).to.be.equal('<test>');
        });

        it('enregistre le validateur', () => {

            expect(arg.getValidator()).to.be.instanceOf(Validator);
            expect(arg.getValidator().getValidators()[0].getArguments().regExp)
                .to.be.deep.equal(/[0-9]/);
        });

        it('si le validateur est un objet, on récupère les args', () => {

            arg = new ArgOpt('<test>', 'une description',
                { integer: { min: 0 } }, 0, app);
            expect(arg.getValidator().getValidators()[0]).to.be.instanceOf(IntegerValidator);
            expect(arg.getValidator().getValidators()[0].getArguments().min)
                .to.be.equal(0);
        });

        it('null si aucun validateur', () => {

            arg = new ArgOpt('[my-HTTP]', 'une description', undefined, 0, app);
            expect(arg.getValidator()).to.be.equal(null);
        });

        it('sauve la valeur par défaut', () => {

            expect(arg.default).to.be.equal(0);
        });

        it('enregistre l\'instance de l\'application', () => {

            expect(arg._application).to.be.equal(app);
        });
    });


    describe('hasDefault', () => {

        it('return true if argument have default', () => {

            const arg = new ArgOpt('<test>', 'une description', /[0-9]/, 2, app);
            expect(arg.hasDefault()).to.be.true;
        });

        it('return false if argument have not default', () => {

            const arg = new ArgOpt('<test>', 'une description', /[0-9]/, undefined, app);
            expect(arg.hasDefault()).to.be.false;
        });
    });


    describe('isValid', () => {

        it('returns `true` if the value is good', () => {

            const arg = new ArgOpt('<test>', '', /a/, 'a', app);
            expect(arg.isValid('a')).to.be.equal(true);
        });

        it('returns `false` if the value isn\'t good', () => {

            const arg = new ArgOpt('<test>', '', /a/, 'a', app);
            expect(arg.isValid('b')).to.be.equal(false);
        });
    });
});
