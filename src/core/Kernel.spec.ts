/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Cache } from './Services/Cache';
import { Config } from './Services/Config';
import { Events } from './Services/Events';
import { Filter } from './Services/Filter';
import { Kernel } from './Kernel';
import { Logs } from './Services/Logs';
import { Plugin } from './Plugin';
import { Scope } from './Services/Scope';
import { ServiceContainer } from './Services/ServiceContainer';
import { Validate } from './Services/Validate';
import { version } from '../version';
import { Tmp } from './Services/Tmp';

describe('core/Kernel', () => {

    let kernel: Kernel = new Kernel();

    describe('init', () => {

        beforeEach(() => {

            kernel = new Kernel(true);
        });

        it('Si déjà initialisé, renvoit toujours la même instance', () => {

            const kernel2 = new Kernel();
            expect(kernel2).to.be.equal(kernel);
        });

        it('On peut forcer un réinitialisation', () => {

            const kernel2 = new Kernel(true);
            expect(kernel2).to.be.not.equal(kernel);
        });

        it('initialise les services', () => {

            const myFct = (kernel: Kernel): Kernel => kernel;
            kernel.container().register('_myDebugFct', myFct);
            expect(kernel.get('_myDebugFct')).to.be.equal(kernel);

            expect(kernel.container().get('$plugins')).to.be.instanceOf(ServiceContainer);
            expect(kernel.get('$config')).to.be.instanceOf(Config);
            expect(kernel.get('$logs')).to.be.instanceOf(Logs);
            expect(kernel.get('$validate')).to.be.instanceOf(Validate);
            expect(kernel.get('$events')).to.be.instanceOf(Events);
            expect(kernel.get('$cache')).to.be.instanceOf(Cache);
            expect(kernel.get('$tmp')).to.be.instanceOf(Tmp);
            expect(kernel.container().has('$parser')).to.be.true;
            expect(kernel.get('$filter')).to.be.instanceOf(Filter);
            expect(kernel.container().has('$interpolate')).to.be.true;
            expect(kernel.get('$rootScope')).to.be.instanceOf(Scope);
        });

        it('initialise l\'environnement', () => {

            expect(kernel.get('$config').get('logLevel')).to.be.equal('normal');
        });
    });


    describe('container', () => {

        it('retourne le service container', () => {

            expect(kernel.container() instanceof ServiceContainer).to.be.equal(true);
        });
    });


    describe('get', () => {

        it('retourne un service', () => {

            const service = {};
            kernel.container().register('test', service);

            expect(kernel.get('test')).to.be.equal(service);
        });
    });


    describe('load plugin', () => {

        class MyPlugin extends Plugin {
            protected _name = 'my-plugin';
            public a = false;
            init (): void {
                this.a = true;
            }
        }

        it('return the kernel', () => {

            expect(kernel.loadPlugin(MyPlugin)).to.be.equal(kernel);
        });

        it('register the plugin', () => {

            kernel.loadPlugin(MyPlugin);
            expect(kernel.get('$plugins').get('my-plugin')).to.be.instanceOf(MyPlugin);
        });

        it('give the kernel to the plugin', () => {

            kernel.loadPlugin(MyPlugin);
            expect(kernel.get('$plugins').get('my-plugin').kernel()).to.be.equal(kernel);
        });

        it('executes the init method of the plugin', () => {

            kernel.loadPlugin(MyPlugin);
            expect(kernel.get('$plugins').get('my-plugin').a).to.be.true;
        });
    });


    describe('events', () => {

        it('retourne le service $events', () => {

            expect(kernel.events()).to.be.instanceof(Events);
        });
    });


    it('version() return the framework version', () => {

        expect(kernel.version()).to.be.equal(version);
    });
});
