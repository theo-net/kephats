/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { KernelAccess } from './KernelAccess';
import * as Utils from './Utils';
import { tValidator } from './Validate/Validate';
import { Validator } from './Validate/Validator';


/**
 * Représente une entité d'un modèle
 */
export abstract class Entity extends KernelAccess {

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [k: string]: any;

    protected _alias: Record<string, string> = {};
    protected _isNew = true;
    protected _hasModifications = false;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected _properties: Record<string, any> = {};
    protected _validators: Record<string, Validator> = {};

    /**
     * Construit un objet
     * @param values Valeurs au départ de l'initialisation
     * @param isNew C'est une nouvelle entité
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    constructor (values?: Record<string, any>, isNew = false) {

        super();

        if (values && Utils.isObject(values)) {

            this._isNew = isNew;
            Utils.forEach(values, (value, property) => {
                this._properties[property] = value;
            });
        }

        this._setProperties();
    }


    /**
     * Retourne la valeur de toutes les propriétés
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    getValues (): Record<string, any> {

        return this._properties;
    }


    /**
     * Indique s'il s'agit d'une nouvelle entité
     */
    isNew (): boolean {

        return this._isNew;
    }


    /**
     * Indique si les propriétés de l'entité ont été modifiées
     */
    hasModifications (): boolean {

        return this._hasModifications;
    }


    /**
     * Indique que l'entité a été sauvée (`isNew` et `hasModifications` deviennent `false`)
     */
    saved (): void {

        this._isNew = false;
        this._hasModifications = false;
    }


    /**
     * Cette méthode, que chaque entité doit implémenter, initialise toutes les propriétés de l'entité.
     */
    protected abstract _setProperties (): void;


    /**
     * Définie une propriété
     * @param name Nom de la propriété
     * @param validator Validateur
     * @param aliasOf Alias vers lequel pointe la propriété
     * @param defaultValue Valeur par défaut. Sera utilisée si propriété est définie à `undefined`
     */
    protected _setProperty (
        name: string, validator: tValidator | Array<tValidator> | null,
        aliasOf?: string, defaultValue: string | null = null
    ): void {

        this._validators[name] = this.kernel().get('$validate').make(validator);

        if (aliasOf)
            this._alias[name] = aliasOf;
        else
            aliasOf = name;

        if (!Object.prototype.hasOwnProperty.call(this._properties, aliasOf))
            this._properties[aliasOf] = defaultValue !== null ? defaultValue : undefined;

        Object.defineProperty(this, name, {
            configurable: false,
            enumerable: true,
            get: () => {
                return this._properties[aliasOf ? aliasOf : ''];
            },
            set: value => {

                if (value === undefined && defaultValue !== null)
                    value = defaultValue;

                if (!this._validators[name].isValid(value))
                    throw new TypeError('Value of ' + name + ' invalid');

                this._hasModifications = true;
                this._properties[aliasOf ? aliasOf : ''] = value;
            }
        });
    }
}
