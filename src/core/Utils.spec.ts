/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/* eslint-disable @typescript-eslint/no-explicit-any */
import { expect } from 'chai';

import * as Utils from './Utils';


describe('core/Utils', function () {

    const root: any = (typeof global == 'object' && global) || this;

    // Used to provide falsey values to methods.
    const falsey: Array<any> = [null, undefined, false, 0, NaN, ''];
    const empties = [[], {}].concat(falsey.slice(1));

    // Pour les tests dans le navigateur
    const document = root.document ? root.document : false;
    const body = root.document && root.document.body;

    // Array typés
    const typedArrays = [
        'Float32Array',
        'Float64Array',
        'Int8Array',
        'Int16Array',
        'Int32Array',
        'Uint8Array',
        'Uint8ClampedArray',
        'Uint16Array',
        'Uint32Array'
    ];
    const arrayViews = typedArrays.concat('DataView');


    describe('.isString', () => {

        it('should return `true` for strings', () => {

            expect(Utils.isString('a')).to.equal(true);
            expect(Utils.isString(Object('a'))).to.equal(true);
        });

        it('should return `false` for non-strings', () => {

            const expected = falsey.map(value => value === '');
            const actual = falsey.map(value => {
                return Utils.isString(value);
            });

            expect(actual).to.deep.equal(expected);

            expect(Utils.isString([1, 2, 3])).to.equal(false);
            expect(Utils.isString(true)).to.equal(false);
            expect(Utils.isString(new Date)).to.equal(false);
            expect(Utils.isString(new Error)).to.equal(false);
            expect(Utils.isString(Utils)).to.equal(false);
            expect(Utils.isString(() => { return ''; })).to.equal(false);
            expect(Utils.isString({ '0': 1, 'length': 1 })).to.equal(false);
            expect(Utils.isString(1)).to.equal(false);
            expect(Utils.isString(/x/)).to.equal(false);
            expect(Utils.isString(Symbol('a'))).to.equal(false);
        });
    });


    describe('.isObjectLike', () => {

        it('should return `true` for objects', () => {

            expect(Utils.isObjectLike([1, 2, 3])).to.equal(true);
            expect(Utils.isObjectLike(Object(false))).to.equal(true);
            expect(Utils.isObjectLike(new Date)).to.equal(true);
            expect(Utils.isObjectLike(new Error)).to.equal(true);
            expect(Utils.isObjectLike({ 'a': 1 })).to.equal(true);
            expect(Utils.isObjectLike(Object(0))).to.equal(true);
            expect(Utils.isObjectLike(/x/)).to.equal(true);
            expect(Utils.isObjectLike(Object('a'))).to.equal(true);
            expect(Utils.isObjectLike(Utils)).to.equal(true);

            if (document)
                expect(Utils.isObjectLike(body)).to.equal(true);
            if (Symbol)
                expect(Utils.isObjectLike(Object(Symbol('a')))).to.equal(true);
        });

        it('should return `false` for non-objects', () => {

            const values = falsey.concat(true, 1, 'a', Symbol('a')),
                  expected = values.map(() => { return false; });

            const actual = values.map(value => {
                return Utils.isObjectLike(value);
            });

            expect(actual).to.deep.equal(expected);
            expect(Utils.isObjectLike(Array.prototype.slice)).to.equal(false);
        });
    });


    describe('.isObject', () => {

        it('should return `true` for objects', () => {

            expect(Utils.isObject([1, 2, 3])).to.equal(true);
            expect(Utils.isObject(Object(false))).to.equal(true);
            expect(Utils.isObject(new Date)).to.equal(true);
            expect(Utils.isObject(new Error)).to.equal(true);
            expect(Utils.isObject(Utils)).to.equal(true);
            expect(Utils.isObject(Array.prototype.slice)).to.equal(true);
            expect(Utils.isObject({ 'a': 1 })).to.equal(true);
            expect(Utils.isObject(Object(0))).to.equal(true);
            expect(Utils.isObject(/x/)).to.equal(true);
            expect(Utils.isObject(Object('a'))).to.equal(true);

            if (document)
                expect(Utils.isObject(body)).to.equal(true);
            if (Symbol)
                expect(Utils.isObject(Object(Symbol('a')))).to.equal(true);
        });

        it('should return `false` for non-objects', () => {

            const values = falsey.concat(true, 1, 'a', Symbol('a')),
                  expected = values.map(() => { return false; });

            const actual = values.map(value => {
                return Utils.isObject(value);
            });

            expect(actual).to.deep.equal(expected);
        });
    });


    describe('.isNumber', () => {

        it('should return `true` for numbers', () => {

            expect(Utils.isNumber(0)).to.equal(true);
            expect(Utils.isNumber(Object(0))).to.equal(true);
            expect(Utils.isNumber(NaN)).to.equal(true);
        });

        it('should return `false` for non-numbers', () => {

            const expected = falsey.map(value => {
                return typeof value == 'number';
            });

            const actual = falsey.map(value => {
                return Utils.isNumber(value);
            });

            expect(actual).to.deep.equal(expected);

            expect(Utils.isNumber([1, 2, 3])).to.equal(false);
            expect(Utils.isNumber(true)).to.equal(false);
            expect(Utils.isNumber(new Date)).to.equal(false);
            expect(Utils.isNumber(new Error)).to.equal(false);
            expect(Utils.isNumber(Utils)).to.equal(false);
            expect(Utils.isNumber(Array.prototype.slice)).to.equal(false);
            expect(Utils.isNumber({ 'a': 1 })).to.equal(false);
            expect(Utils.isNumber(/x/)).to.equal(false);
            expect(Utils.isNumber('a')).to.equal(false);
            expect(Utils.isNumber(Symbol('a'))).to.equal(false);
        });
    });


    describe('.isBoolean', () => {

        it('should return `true` for booleans', () => {

            expect(Utils.isBoolean(true)).to.equal(true);
            expect(Utils.isBoolean(false)).to.equal(true);
            expect(Utils.isBoolean(Object(true))).to.equal(true);
            expect(Utils.isBoolean(Object(false))).to.equal(true);
        });

        it('should return `false` for non-booleans', () => {

            const expected = falsey.map(value => {
                return value === false;
            });

            const actual = falsey.map(value => {
                return Utils.isBoolean(value);
            });

            expect(actual).to.deep.equal(expected);

            expect(Utils.isBoolean([1, 2, 3])).to.equal(false);
            expect(Utils.isBoolean(new Date)).to.equal(false);
            expect(Utils.isBoolean(new Error)).to.equal(false);
            expect(Utils.isBoolean(Utils)).to.equal(false);
            expect(Utils.isBoolean(Array.prototype.slice)).to.equal(false);
            expect(Utils.isBoolean({ 'a': 1 })).to.equal(false);
            expect(Utils.isBoolean(1)).to.equal(false);
            expect(Utils.isBoolean(/x/)).to.equal(false);
            expect(Utils.isBoolean('a')).to.equal(false);
            expect(Utils.isBoolean(Symbol('a'))).to.equal(false);
        });
    });


    describe('.isFunction', () => {

        it('should return `true` for functions', () => {

            expect(Utils.isFunction(Utils.isNumber)).to.equal(true);
            expect(Utils.isFunction(Array.isArray)).to.equal(true);
        });

        it('should return `true` for array view constructors', () => {

            const expected = arrayViews.map((type) => {
                return Object.prototype.toString
                    .call(root[type]) == '[object Function]';
            });

            const actual = arrayViews.map((type) => {
                return Utils.isFunction(root[type]);
            });

            expect(actual).to.deep.equal(expected);
        });

        it('should return `false` for non-functions', () => {

            const expected = falsey.map(() => { return false; });

            const actual = falsey.map(value => {
                return Utils.isFunction(value);
            });

            expect(actual).to.deep.equal(expected);

            expect(Utils.isFunction([1, 2, 3])).to.equal(false);
            expect(Utils.isFunction(true)).to.equal(false);
            expect(Utils.isFunction(new Date)).to.equal(false);
            expect(Utils.isFunction(new Error)).to.equal(false);
            expect(Utils.isFunction({ 'a': 1 })).to.equal(false);
            expect(Utils.isFunction(1)).to.equal(false);
            expect(Utils.isFunction(/x/)).to.equal(false);
            expect(Utils.isFunction('a')).to.equal(false);
            expect(Utils.isFunction(Symbol('a'))).to.equal(false);

            if (document) {
                expect(Utils.isFunction(document.getElementsByTagName('body')))
                    .to.equal(false);
            }
        });
    });


    describe('.isJsonLike', () => {

        it('should return `true` for JsonLike values', () => {

            expect(Utils.isJsonLike('{...}')).to.equal(true);
            expect(Utils.isJsonLike('[...]')).to.equal(true);
            expect(Utils.isJsonLike({ 'a': 1 })).to.equal(true);
        });

        it('should return `false` for non JsonLike values', () => {

            expect(Utils.isJsonLike('{{expr}}')).to.equal(false);
            expect(Utils.isJsonLike('{expr')).to.equal(false);
            expect(Utils.isJsonLike('[expr')).to.equal(false);
            expect(Utils.isJsonLike([1, 2, 3])).to.equal(false);
            expect(Utils.isJsonLike(true)).to.equal(false);
            expect(Utils.isJsonLike(new Date)).to.equal(false);
            expect(Utils.isJsonLike(new Error)).to.equal(false);
            expect(Utils.isJsonLike(Array.prototype.slice)).to.equal(false);
            expect(Utils.isJsonLike(1)).to.equal(false);
            expect(Utils.isJsonLike(/x/)).to.equal(false);
            expect(Utils.isJsonLike('a')).to.equal(false);
            expect(Utils.isJsonLike(Symbol('a'))).to.equal(false);
        });
    });


    describe('.isEmpty', () => {

        it('should return `true` for empty values', () => {

            const expected = empties.map(() => { return true; }),
                  actual = empties.map(Utils.isEmpty);

            expect(actual).to.deep.equal(expected);

            expect(Utils.isEmpty(true)).to.equal(true);
            expect(Utils.isEmpty(1)).to.equal(true);
            expect(Utils.isEmpty(NaN)).to.equal(true);
            expect(Utils.isEmpty(/x/)).to.equal(true);
            expect(Utils.isEmpty(Symbol('a'))).to.equal(true);
            expect(Utils.isEmpty(undefined)).to.equal(true);

            if (Buffer) {
                expect(Utils.isEmpty(new Buffer(0))).to.equal(true);
                expect(Utils.isEmpty(new Buffer(1))).to.equal(false);
            }
        });

        it('should return `false` for non-empty values', () => {

            expect(Utils.isEmpty([0])).to.equal(false);
            expect(Utils.isEmpty({ 'a': 0 })).to.equal(false);
            expect(Utils.isEmpty('a')).to.equal(false);
        });

        it('should work with an object that has a `length` property', () => {

            expect(Utils.isEmpty({ 'length': 0 })).to.equal(false);
        });

        it('should not treat objects with negative lengths as array-like', () => {

            function Foo (): void { return; }
            Foo.prototype.length = -1;

            expect(Utils.isEmpty(new (Foo as any))).to.equal(true);
        });

        it('should not treat objects with non-number lengths as array-like', () => {

            expect(Utils.isEmpty({ 'length': '0' })).to.equal(false);
        });
    });


    describe('.times', () => {

        it('should coerce non-finite `n` values to `0`', () => {

            [-Infinity, NaN, Infinity].forEach(n => {
                expect(Utils.times(n)).to.deep.equal([]);
            });
        });

        it('should coerce `n` to an integer', () => {

            const actual = Utils.times(2.6, n => n);
            expect(actual).to.be.deep.equal([0, 1]);
        });

        it('should provide the correct `iteratee` arguments', () => {

            const args: Array<number> = [];

            Utils.times(1, i => {
                args.push(i);
            });

            expect(args).to.be.deep.equal([0]);
        });

        it('should use `identity` when `iteratee` is nullish', () => {

            const values = [null, undefined],
                  expected = [0, 1, 2];

            const actual = values.map((value, index) => {
                return index ? Utils.times(3, value) : Utils.times(3);
            });

            expect(actual[0]).to.be.deep.equal(expected);
            expect(actual[1]).to.be.deep.equal(expected);
        });

        it('should return an array of the results of each `iteratee` execution',
            () => {

                expect(Utils.times(3, n => n * 2)).to.be.deep.equal([0, 2, 4]);
            });

        it('should return an empty array for falsey and negative `n` arguments',
            () => {

                const values = falsey.concat(-1, -Infinity),
                      expected = values.map(() => []);

                const actual = values.map(value => {
                    return Utils.times(value);
                });

                expect(actual).to.be.deep.equal(expected);
            });
    });


    describe('.isEqual', () => {

        const symbol1 = Symbol ? Symbol('a') : true,
              symbol2 = Symbol ? Symbol('b') : false;

        it('should compare primitives', () => {

            const pairs = [
                [1, 1, true], [1, '1', false], [1, 2, false],
                [-0, -0, true], [0, 0, true], [Object(0), Object(0), true],
                [-0, 0, true], [0, '0', false], [0, null, false], [NaN, NaN, true],
                [Object(NaN), Object(NaN), true], [NaN, 'a', false],
                [NaN, Infinity, false], ['a', 'a', true], ['a', 'b', false],
                ['a', ['a'], false], [true, true, true],
                [Object(true), Object(true), true], [true, 1, false],
                [true, 'a', false], [false, false, true],
                [Object(false), Object(false), true], [false, 0, false],
                [false, '', false], [symbol1, symbol1, true],
                [symbol1, Object(symbol1), false],
                [Object(symbol1), Object(symbol1), true], [symbol1, symbol2, false],
                [null, null, true], [null, undefined, false], [null, {}, false],
                [null, '', false], [undefined, undefined, true],
                [undefined, null, false], [undefined, '', false]
            ];

            const expected = pairs.map(pair => {
                return pair[2];
            });

            const actual = pairs.map(pair => {
                return Utils.isEqual(pair[0], pair[1]);
            });

            expect(actual).to.be.deep.equal(expected);
        });

        it('should compare arrays', () => {

            let array1: Array<any> = [true, null, 1, 'a', undefined],
                array2: Array<any> = [true, null, 1, 'a', undefined];

            expect(Utils.isEqual(array1, array2)).to.be.equal(true);

            array1 = [[1, 2, 3], new Date(2012, 4, 23), /x/, { 'e': 1 }];
            array2 = [[1, 2, 3], new Date(2012, 4, 23), /x/, { 'e': 1 }];

            expect(Utils.isEqual(array1, array2)).to.be.equal(true);

            array1 = [1];
            array1[2] = 3;

            array2 = [1];
            array2[1] = undefined;
            array2[2] = 3;

            expect(Utils.isEqual(array1, array2)).to.be.true;

            array1 = [1, 2, 3];
            array2 = [3, 2, 1];

            expect(Utils.isEqual(array1, array2)).to.be.false;

            array1 = [1, 2];
            array2 = [1, 2, 3];

            expect(Utils.isEqual(array1, array2)).to.be.false;
        });

        it('should treat arrays with identical values but different non-index properties as equal',
        () => {

            let array1: Array<any> | RegExpExecArray | null = [1, 2, 3],
                array2: Array<any> = [1, 2, 3];

            array1.forEach = array1.indexOf = array1.lastIndexOf =
                    array1.reduce = array1.reduceRight = (): any => { return 1; };

            array2.concat = array2.join = array2.pop =
                    array2.reverse = array2.shift = array2.slice =
                    array2.sort = array2.splice = array2.unshift = (): any => { return 1; };

            expect(Utils.isEqual(array1, array2)).to.be.true;

            array1 = /c/.exec('abcde');
            array2 = ['c'];

            expect(Utils.isEqual(array1, array2)).to.be.true;
        });

        it('should compare sparse arrays', () => {

            const array = Array(1);

            expect(Utils.isEqual(array, Array(1))).to.be.true;
            expect(Utils.isEqual(array, [undefined])).to.be.true;
            expect(Utils.isEqual(array, Array(2))).to.be.false;
        });

        it('should compare plain objects', () => {

            interface MyObj {
                a?: any; b?: any; c?: any; d?: any; e?: any; f?: any;
            }
            let object1: MyObj = { 'a': true, 'b': null, 'c': 1, 'd': 'a', 'e': undefined },
                object2: MyObj  = { 'a': true, 'b': null, 'c': 1, 'd': 'a', 'e': undefined };

            expect(Utils.isEqual(object1, object2)).to.be.true;

            object1 = {
                'a': [1, 2, 3], 'b': new Date(2012, 4, 23), 'c': /x/,
                'd': { 'e': 1 }
            };
            object2 = {
                'a': [1, 2, 3], 'b': new Date(2012, 4, 23), 'c': /x/,
                'd': { 'e': 1 }
            };

            expect(Utils.isEqual(object1, object2)).to.be.true;

            object1 = { 'a': 1, 'b': 2, 'c': 3 };
            object2 = { 'a': 3, 'b': 2, 'c': 1 };

            expect(Utils.isEqual(object1, object2)).to.be.false;

            object1 = { 'a': 1, 'b': 2, 'c': 3 };
            object2 = { 'd': 1, 'e': 2, 'f': 3 };

            expect(Utils.isEqual(object1, object2)).to.be.false;

            object1 = { 'a': 1, 'b': 2 };
            object2 = { 'a': 1, 'b': 2, 'c': 3 };

            expect(Utils.isEqual(object1, object2)).to.be.false;
        });

        it('should compare objects regardless of key order', () => {

            const object1 = { 'a': 1, 'b': 2, 'c': 3 },
                  object2 = { 'c': 3, 'a': 1, 'b': 2 };

            expect(Utils.isEqual(object1, object2)).to.be.true;
        });

        it('should compare nested objects', () => {

            const noop = (): void => { return; };

            const object1 = {
                'a': [1, 2, 3],
                'b': true,
                'c': 1,
                'd': 'a',
                'e': {
                    'f': ['a', 'b', 'c'],
                    'g': false,
                    'h': new Date(2012, 4, 23),
                    'i': noop,
                    'j': 'a'
                }
            };

            const object2 = {
                'a': [1, 2, 3],
                'b': true,
                'c': 1,
                'd': 'a',
                'e': {
                    'f': ['a', 'b', 'c'],
                    'g': false,
                    'h': new Date(2012, 4, 23),
                    'i': noop,
                    'j': 'a'
                }
            };

            expect(Utils.isEqual(object1, object2)).to.be.true;
        });

        it('should compare object instances', () => {

            class Foo {
                public a: number;
                constructor () {
                    this.a = 1;
                }
            }
            class Bar {
                public a: number;
                constructor () {
                    this.a = 2;
                }
            }

            expect(Utils.isEqual(new Foo(), new Foo())).to.be.true;
            expect(Utils.isEqual(new Foo(), new Bar())).to.be.false;
            expect(Utils.isEqual({ 'a': 1 }, new Foo())).to.be.false;
            expect(Utils.isEqual({ 'a': 2 }, new Bar())).to.be.false;
        });

        it('should compare objects with constructor properties', () => {

            expect(Utils.isEqual({ 'constructor': 1 }, { 'constructor': 1 }))
                .to.be.true;
            expect(Utils.isEqual({ 'constructor': 1 }, { 'constructor': '1' }))
                .to.be.false;
            expect(Utils.isEqual({ 'constructor': [1] }, { 'constructor': [1] }))
                .to.be.true;
            expect(Utils.isEqual({ 'constructor': [1] }, { 'constructor': ['1'] }))
                .to.be.false;
            expect(Utils.isEqual({ 'constructor': Object }, {})).to.be.false;
        });

        it('should compare arrays with circular references', () => {

            let array1: Array<any> = [],
                array2: Array<any> = [];

            array1.push(array1);
            array2.push(array2);

            expect(Utils.isEqual(array1, array2)).to.be.true;

            array1.push('b');
            array2.push('b');

            expect(Utils.isEqual(array1, array2)).to.be.true;

            array1.push('c');
            array2.push('d');

            expect(Utils.isEqual(array1, array2)).to.be.false;

            array1 = ['a', 'b', 'c'];
            array1[1] = array1;
            array2 = ['a', ['a', 'b', 'c'], 'c'];

            expect(Utils.isEqual(array1, array2)).to.be.false;
        });

        it('should compare objects with circular references', () => {

            let object1: any = {},
                object2: any = {};

            object1.a = object1;
            object2.a = object2;

            expect(Utils.isEqual(object1, object2)).to.be.true;

            object1 = { 'a': 1, 'b': 2, 'c': 3 };
            object1.b = object1;
            object2 = { 'a': 1, 'b': { 'a': 1, 'b': 2, 'c': 3 }, 'c': 3 };

            expect(Utils.isEqual(object1, object2)).to.be.false;
        });

        it('should compare objects with multiple circular references',
            () => {

                const array1: Array<any> = [{}],
                      array2: Array<any> = [{}];

                (array1[0].a = array1).push(array1);
                (array2[0].a = array2).push(array2);

                expect(Utils.isEqual(array1, array2)).to.be.true;
            });

        it('should compare objects with complex circular references', () => {

            const object1: any = {
                'foo': { 'b': { 'c': { 'd': {} } } },
                'bar': { 'a': 2 }
            };
            const object2: any = {
                'foo': { 'b': { 'c': { 'd': {} } } },
                'bar': { 'a': 2 }
            };

            object1.foo.b.c.d = object1;
            object1.bar.b = object1.foo.b;

            object2.foo.b.c.d = object2;
            object2.bar.b = object2.foo.b;

            expect(Utils.isEqual(object1, object2)).to.be.true;
        });

        it('should compare objects with shared property values', () => {

            const object1: any = {
                'a': [1, 2]
            };
            const object2: any = {
                'a': [1, 2],
                'b': [1, 2]
            };
            object1.b = object1.a;

            expect(Utils.isEqual(object1, object2)).to.be.true;
        });

        it('should return `false` for objects with custom `toString` methods',
            () => {

                let primitive: any;
                const object = { 'toString': (): any => { return primitive; } },
                      values = [true, null, 1, 'a', undefined],
                      expected = values.map(() => false);

                const actual = values.map(value => {
                    primitive = value;
                    return Utils.isEqual(object, value);
                });

                expect(actual).to.be.deep.equal(expected);
            });

        it('should avoid common type coercions', () => {

            expect(Utils.isEqual(true, Object(false))).to.be.false;
            expect(Utils.isEqual(Object(false), Object(0))).to.be.false;
            expect(Utils.isEqual(false, Object(''))).to.be.false;
            expect(Utils.isEqual(Object(36), Object('36'))).to.be.false;
            expect(Utils.isEqual(0, '')).to.be.false;
            expect(Utils.isEqual(1, true)).to.be.false;
            expect(Utils.isEqual(1337756400000, new Date(2012, 4, 23))).to.be.false;
            expect(Utils.isEqual('36', 36)).to.be.false;
            expect(Utils.isEqual(36, '36')).to.be.false;
        });

        it('should compare date objects', () => {

            const date = new Date(2012, 4, 23);

            expect(Utils.isEqual(date, new Date(2012, 4, 23))).to.be.true;
            expect(Utils.isEqual(date, new Date(2013, 3, 25))).to.be.false;
            expect(Utils.isEqual(date, { 'getTime': +date })).to.be.false;
            expect(Utils.isEqual(new Date('a'), new Date('a'))).to.be.false;
        });

        it('should compare error objects', () => {

            const pairs = [
                'Error',
                'EvalError',
                'RangeError',
                'ReferenceError',
                'SyntaxError',
                'TypeError',
                'URIError'
            ].map((type, index, errorTypes) => {
                const otherType = errorTypes[++index % errorTypes.length],
                      CtorA = root[type],
                      CtorB = root[otherType];

                return [
                    new CtorA('a'), new CtorA('a'),
                    new CtorB('a'), new CtorB('b')
                ];
            });

            const expected = pairs.map(() => [true, false, false]);

            const actual = pairs.map(pair => {
                return [
                    Utils.isEqual(pair[0], pair[1]),
                    Utils.isEqual(pair[0], pair[2]),
                    Utils.isEqual(pair[2], pair[3])
                ];
            });

            expect(actual).to.be.deep.equal(expected);
        });

        it('should compare functions', () => {

            function a (): number { return 1 + 2; }
            function b (): number { return 1 + 2; }

            expect(Utils.isEqual(a, a)).to.be.true;
            expect(Utils.isEqual(a, b)).to.be.false;
        });

        it('should compare regexes', () => {

            expect(Utils.isEqual(/x/gim, /x/gim)).to.be.true;
            expect(Utils.isEqual(/x/gim, /x/mgi)).to.be.true;
            expect(Utils.isEqual(/x/gi, /x/g)).to.be.false;
            expect(Utils.isEqual(/x/, /y/)).to.be.false;
            expect(Utils.isEqual(/x/g, {
                'global': true, 'ignoreCase': false,
                'multiline': false, 'source': 'x'
            })).to.be.false;
        });
    });


    describe('iteration method', () => {

        const methodsFunc: { [key: string]: Function } = {
            forEach: Utils['forEach'],
            forEachRight: Utils['forEachRight'],
            forOwn: Utils['forOwn'],
            some: Utils['some']
        };
        const methods = [
            'forEach',
            'forEachRight',
            'forOwn',
            'some'
        ];
        const collectionMethods = [
            'forEach',
            'forEachRight',
            'some'
        ];
        const iterationMethods = [
            'forEach',
            'forEachRight',
            'forOwn'
        ];
        const objectMethods = [
            'forOwn'
        ];
        const rightMethods = [
            'forEachRight'
        ];
        const exitMethods = [
            'forEach',
            'forEachRight',
            'forOwn'
        ];


        methods.forEach(methodName => {

            const array = [1, 2, 3],
                  func = methodsFunc[methodName];

            it(methodName + ' should provide the correct iteratee arguments',
                () => {

                    const args: Array<any> = [],
                          expected = [
                              [1, 0, true], [2, 1, true], [3, 2, true]
                          ];

                    func(array, (value: any, index: number, collection: Array<any>) => {
                        args.push([
                            value, index, collection == array
                        ]);
                    });

                    if (rightMethods.indexOf(methodName) > -1) {
                        const tmp = expected[0];
                        expected[0] = expected[2];
                        expected[2] = tmp;
                    }

                    expect(args).to.be.deep.equal(expected);
                });

            it(methodName + ' should treat sparse arrays as dense',
                () => {

                    const array = [1];
                    array[2] = 3;

                    const expected = objectMethods.indexOf(methodName) > -1 ?
                        [[1, '0', array], [undefined, '1', array], [3, '2', array]]
                        : [[1, 0, array], [undefined, 1, array], [3, 2, array]];

                    if (rightMethods.indexOf(methodName) > -1)
                        expected.reverse();

                    const argsList: Array<any> = [];
                    func(array, (value: any, index: number, collection: Array<any>) => {
                        argsList.push([value, index, collection]);
                    });

                    if (objectMethods.indexOf(methodName) === -1)
                        expect(argsList).to.be.deep.equal(expected);
                });
        });


        iterationMethods.forEach(methodName => {

            const array = [1, 2, 3],
                  func = methodsFunc[methodName];

            it(methodName + ' should return the collection', () => {

                expect(func(array, Boolean)).to.be.equal(array);
            });
        });

        methods.forEach(methodName => {

            const func = methodsFunc[methodName],
                  isSome = methodName == 'some';

            it(methodName + ' should ignore changes to `array.length`', () => {

                let count = 0;
                const array: Array<number> = [1];

                func(array, () => {
                    if (++count == 1)
                        array.push(2);
                    return !(isSome);
                }, null);

                expect(count).to.be.equal(1);
            });
        });

        methods.concat(collectionMethods).forEach(methodName => {

            const func = methodsFunc[methodName],
                  isSome = methodName == 'some';

            it(methodName + ' should ignore added `object` properties', () => {

                let count = 0;
                const object: any = { 'a': 1 };

                func(object, () => {
                    if (++count == 1)
                        object.b = 2;
                    return !(isSome);
                }, null);

                expect(count).to.be.equal(1);
            });
        });

        exitMethods.forEach(methodName => {

            const func = methodsFunc[methodName];

            it(methodName + ' can exit early when iterating arrays', () => {

                const array = [1, 2, 3],
                      values: Array<number> = [];

                func(array, (value: number, other: number) => {
                    values.push(Array.isArray(value) ? other : value);
                    return false;
                });

                expect(values)
                    .to.be.deep.equal([rightMethods.indexOf(methodName) > -1 ? 3 : 1]);
            });

            it(methodName + ' can exit early when iterating objects', () => {

                const object = { 'a': 1, 'b': 2, 'c': 3 },
                      values: Array<number> = [];

                func(object, (value: number, other: number) => {
                    values.push(Array.isArray(value) ? other : value);
                    return false;
                });

                expect(values.length).to.be.equal(1);
            });
        });
    });


    describe('extend', () => {

        let object: any;
        const source1 = { d: 'bar', e: 2 };
        const source2 = { f: 'lol' };
        const source3 = { b: false, g: 'mdr' };
        const source4 = { f: ':-)' };

        beforeEach(() => {

            object = { a: 1, b: 2, c: 'foo' };
        });

        it('assign source\'s properties to object', () => {

            const expected = {
                a: 1, b: 2, c: 'foo', d: 'bar', e: 2
            };

            expect(Utils.extend(object, source1)).to.be.deep.equal(expected);
        });

        it('assign multiple source\'s properties to object', () => {

            const expected = {
                a: 1, b: 2, c: 'foo', d: 'bar', e: 2, f: 'lol'
            };

            expect(Utils.extend(object, source1, source2))
                .to.be.deep.equal(expected);
        });

        it('a source ecrase object\'s equivalent properties', () => {

            const expected = {
                a: 1, b: false, c: 'foo', g: 'mdr'
            };

            expect(Utils.extend(object, source3))
                .to.be.deep.equal(expected);
        });

        it('a source ecrase previous source\' properties to object', () => {

            const expected = {
                a: 1, b: 2, c: 'foo', f: ':-)'
            };

            expect(Utils.extend(object, source2, source4))
                .to.be.deep.equal(expected);
        });

    });


    describe('.camelCase', () => {

        it('should work with numbers', () => {

            expect(Utils.camelCase('12 feet')).to.equal('12Feet');
            expect(Utils.camelCase('enable 6h format')).to.equal('enable6HFormat');
            expect(Utils.camelCase('enable 24H format')).to.equal('enable24HFormat');
            expect(Utils.camelCase('too legit 2 quit')).to.equal('tooLegit2Quit');
            expect(Utils.camelCase('walk 500 miles')).to.equal('walk500Miles');
            expect(Utils.camelCase('xhr2 request')).to.equal('xhr2Request');
        });

        it('should handle acronyms', () => {

            expect(true).to.be.true;
            ['safe HTML', 'safeHTML'].forEach(function (string) {
                expect(Utils.camelCase(string)).to.equal('safeHtml');
            });

            ['escape HTML entities', 'escapeHTMLEntities'].forEach(function (string) {
                expect(Utils.camelCase(string)).to.equal('escapeHtmlEntities');
            });

            ['XMLHttpRequest', 'XmlHTTPRequest'].forEach(function (string) {
                expect(Utils.camelCase(string)).to.equal('xmlHttpRequest');
            });
        });
    });


    describe('.kebabCase', () => {

        it('ne change pas un élément déjà en kebabCase', () => {

            expect(Utils.kebabCase('foo-bar')).to.be.equal('foo-bar');
        });

        it('transforme des éléments espacés', () => {

            expect(Utils.kebabCase('Foo bar')).to.be.equal('foo-bar');
            expect(Utils.kebabCase('Foo Bar')).to.be.equal('foo-bar');
        });

        it('transforme du camelCase', () => {

            expect(Utils.kebabCase('fooBar')).to.be.equal('foo-bar');
        });

        it('transforme de la notation constante `__FOO_BAR__`', () => {

            expect(Utils.kebabCase('__FOO_BAR__')).to.be.equal('foo-bar');
        });
    });

    describe('levenshtein', () => {

        it('renvoit la longueur de str2 si str1 vide', () => {

            expect(Utils.levenshtein('', 'foo')).to.be.equal(3);
        });

        it('renvoit la longueur de str1 si str2 vide', () => {

            expect(Utils.levenshtein('bar!', '')).to.be.equal(4);
        });

        it('Test avec des valeurs', () => {

            expect(Utils.levenshtein('niche', 'chien')).to.be.equal(4);
            expect(Utils.levenshtein('chat', 'chien')).to.be.equal(3);
            expect(Utils.levenshtein('chat', 'poule')).to.be.equal(5);
            expect(Utils.levenshtein('poue', 'poule')).to.be.equal(1);
        });
    });


    describe('clone methods', () => {

        const clonesFunc: { [key: string]: Function } = {
            clone: Utils['clone'],
            cloneDeep: Utils['cloneDeep']
        };

        class Foo {

            public a: any;
            public b: any;

            constructor () {

                this.a = 1;
                this.b = 1;
            }

            c (): void { return; }
        }
        Foo.prototype.b = 1;

        const objects = {
            'arrays': ['a', '', 1],
            'array-like-objects': { '0': 'a', '1': '', 'length': 3 },
            'booleans': false,
            'date objects': new Date,
            'objects': { 'a': 0, 'b': 1, 'c': 2 },
            'objects with object values': { 'a': /a/, 'b': ['B'], 'c': { 'C': 1 } },
            'null values': null,
            'numbers': 0,
            'regexes': /a/gim,
            'strings': 'a',
            'undefined values': undefined
        };

        objects.arrays.length = 3;


        it('.clone should perform a shallow clone', () => {

            const array = [{ 'a': 0 }, { 'b': 1 }],
                  actual = Utils.clone(array);

            expect(actual).to.be.deep.equal(array);
            expect(actual !== array && actual[0] === array[0]).to.be.true;
        });

        it('cloneDeep should deep clone objects with circular references',
            () => {

                const object: any = {
                    'foo': { 'b': { 'c': { 'd': {} } } },
                    'bar': {}
                };

                object.foo.b.c.d = object;
                object.bar.b = object.foo.b;

                const actual = Utils.cloneDeep(object);
                expect(actual.bar.b === actual.foo.b &&
                    actual === actual.foo.b.c.d &&
                    actual !== object).to.be.true;
            });

        it('cloneDeep should deep clone objects with lots of circular references',
            () => {

                const cyclical: any = {};
                Utils.times(200 + 1, index => {
                    cyclical['v' + index] =
                        [index ? cyclical['v' + (index - 1)] : cyclical];
                });

                const clone = Utils.cloneDeep(cyclical),
                      actual = clone['v' + 200][0];

                expect(actual).to.be.equal(clone['v' + (200 - 1)]);
                expect(actual).to.be.not.equal(cyclical['v' + (200 - 1)]);
            });

        ['clone', 'cloneDeep'].forEach(methodName => {
            const func = clonesFunc[methodName],
                  isDeep = methodName == 'cloneDeep';

            Utils.forOwn(objects, (object, key) => {

                it(methodName + ' should clone ' + key, () => {

                    const actual = func(object);

                    expect(actual).to.be.deep.equal(object);

                    if (Utils.isObject(object))
                        expect(actual).to.be.not.equal(object);
                    else
                        expect(actual).to.be.equal(object);
                });
            });

            it(methodName + ' should clone array buffers', () => {

                if (ArrayBuffer) {
                    const arrayBuffer = new ArrayBuffer(2);
                    const actual = func(arrayBuffer);
                    expect(actual.byteLength).to.be.equal(arrayBuffer.byteLength);
                    expect(actual).to.be.not.equal(arrayBuffer);
                }
            });

            it(methodName + ' should clone buffers', () => {

                if (Buffer) {
                    const buffer = new Buffer([1, 2]),
                          actual = func(buffer);

                    expect(actual.byteLength).to.be.equal(buffer.byteLength);
                    expect(actual).to.be.not.equal(buffer);

                    buffer[0] = 2;
                    expect(actual[0]).to.be.equal(isDeep ? 1 : 2);
                }
            });


            it(methodName + ' should clone `index` and `input` array properties', () => {

                const array = /c/.exec('abcde'),
                      actual = func(array);

                expect(actual.index).to.be.equal(2);
                expect(actual.input).to.be.equal('abcde');
            });

            it(methodName + ' should clone `lastIndex` regexp property', () => {

                const regexp = /c/g;
                regexp.exec('abcde');

                expect(func(regexp).lastIndex).to.be.equal(3);
            });

            it(methodName + ' should clone properties that shadow those on `Object.prototype`',
            () => {

                const object = {
                    'constructor': Object.prototype.constructor,
                    'hasOwnProperty': Object.prototype.hasOwnProperty,
                    'isPrototypeOf': Object.prototype.isPrototypeOf,
                    'propertyIsEnumerable': Object.prototype.propertyIsEnumerable,
                    'toLocaleString': Object.prototype.toLocaleString,
                    'toString': Object.prototype.toString,
                    'valueOf': Object.prototype.valueOf
                };

                const actual = func(object);

                expect(actual).to.be.deep.equal(object);
                expect(actual).to.be.not.equal(object);
            });

            it(methodName + ' should clone symbol objects', () => {

                const symbol = Symbol ? Symbol('a') : undefined;
                if (symbol) {
                    expect(func(symbol)).to.be.equal(symbol);

                    const object = Object(symbol),
                          actual = func(object);

                    expect(typeof actual).to.be.equal('object');
                    expect(actual).to.be.not.equal(object);
                }
            });

            it(methodName + ' should not clone symbol primitives', () => {

                const symbol = Symbol ? Symbol('a') : undefined;
                if (symbol)
                    expect(func(symbol)).to.be.equal(symbol);
            });

            it(methodName + ' should not error on DOM elements', () => {

                if (document) {
                    const element = document.createElement('div');

                    try {
                        expect(func(element)).to.be.deep.equal({});
                    } catch (e) {
                        expect(e.message).to.be.false;
                    }
                }
            });
        });
    });

    describe('.some', () => {

        it('should return `true` if `predicate` returns truthy for any element',
            () => {

                expect(Utils.some([false, 1, ''], value => { return value; }))
                    .to.be.equal(true);
                expect(Utils.some([null, 'a', 0], value => { return value; }))
                    .to.be.equal(true);
            });

        it('should return `false` for empty collections', () => {

            const expected = empties.map(() => { return false; }),
                  actual = empties.map(value => {
                      try {
                          return Utils.some(value, value => { return value; });
                      } catch (e) { e.message = ''; }
                  });

            expect(actual).to.be.deep.equal(expected);
        });

        it('should return `true` as soon as `predicate` returns truthy',
            () => {

                let count = 0;

                expect(Utils.some([null, true, null], value => {
                    count++;
                    return value;
                })).to.be.true;

                expect(count).to.be.equal(2);
            });

        it('should return `false` if `predicate` returns falsey for all elements',
            () => {

                expect(Utils.some([false, false, false], value => { return value; }))
                    .to.be.false;
                expect(Utils.some([null, 0, ''], value => { return value; }))
                    .to.be.false;
            });

        it('should use identity when `predicate` is null', () => {

            expect(Utils.some([0, 0], null)).to.be.equal(false);
            expect(Utils.some([0, 1], null)).to.be.deep.equal(true);
        });
    });



    describe('.ucfirst', () => {

        it('premier caractère en majuscule', () => {

            expect(Utils.ucfirst('foobar')).to.be.equal('Foobar');
        });

        it('Ok accents', () => {

            expect(Utils.ucfirst('école')).to.be.equal('École');
        });

        it('Ok si caractères autre que lettre', () => {

            expect(Utils.ucfirst('1foobar')).to.be.equal('1foobar');
            expect(Utils.ucfirst('_foobar')).to.be.equal('_foobar');
        });

        it('pas de pb avec autres types', () => {

            expect(Utils.ucfirst(2)).to.be.equal(2);
            expect(Utils.ucfirst(true)).to.be.equal(true);
            expect(Utils.ucfirst([1, 2])).to.be.deep.equal([1, 2]);
        });
    });


    describe('.uuidRegExp', () => {

        it('retourne une RegExp', () => {

            expect(Utils.uuidRegExp() instanceof RegExp).to.be.true;
        });

        it('retourne la RegExp pour valider un uuid v4', () => {

            const reg = RegExp(
                '^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$',
                'i'
            );
            expect(Utils.uuidRegExp()).to.be.deep.equal(reg);
        });
    });
});
