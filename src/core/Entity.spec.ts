/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Entity } from './Entity';
import { KernelAccess } from './KernelAccess';
import * as Utils from './Utils';

class TestEntity extends Entity {

    _setProperties (): void {

        this._setProperty('b', null, 'c');
        this._setProperty('d', { integer: { min: 1, max: 5 } });
        this._setProperty('e', null, undefined, 'foobar');
    }
}

describe('core/Entity', () => {

    it('KernalAccess', () => {

        expect(new TestEntity()).to.be.instanceof(KernelAccess);
    });

    it('isNew true par défaut', () => {

        const entity = new TestEntity();

        expect(entity.isNew()).to.be.true;
    });

    it('On peut lui transmettre des variables', () => {

        const entity = new TestEntity({ c: 1, d: 2 });

        expect(entity.b).to.be.equal(1);
        expect(entity.d).to.be.equal(2);
    });

    it('On peut récupérer toutes les propriétés', () => {

        const entity = new TestEntity({ c: 1, d: 2 });
        expect(entity.getValues()).to.be.deep.equal({ c: 1, d: 2, e: 'foobar' });
    });

    it('Si on lui transmet variables, isNew false', () => {

        const entity = new TestEntity({ c: 1, d: 2 });

        expect(entity.isNew()).to.be.false;
    });

    it('Si on lui transmet variables, on peut forcer isNew true', () => {

        const entity = new TestEntity({ c: 1, d: 2 }, true);

        expect(entity.isNew()).to.be.true;
    });

    it('hasModifications false par défaut', () => {

        const entity = new TestEntity();

        expect(entity.hasModifications()).to.be.false;
    });

    it('hasModifications true si modifs', () => {

        const entity = new TestEntity({ c: 1, d: 2 });

        entity.b = 2;

        expect(entity.hasModifications()).to.be.true;
    });

    it('hasModifications true si modifs (même si valeur ==)', () => {

        const entity = new TestEntity({ c: 1, d: 2 });

        entity.b = 1;

        expect(entity.hasModifications()).to.be.true;
    });

    it('saved met isNew false et hasModifications true', () => {

        const entity1 = new TestEntity({ c: 1, d: 2 });
        entity1.b = 1;
        entity1.saved();
        const entity2 = new TestEntity({ c: 1, d: 2 }, true);
        entity2.saved();

        expect(entity1.hasModifications()).to.be.false;
        expect(entity1.isNew()).to.be.false;
        expect(entity2.hasModifications()).to.be.false;
        expect(entity2.isNew()).to.be.false;
    });

    it('alias for properties', () => {

        const entity = new TestEntity({ c: 1 });

        expect(entity.b).to.be.equal(1);
    });

    it('properties enumerable', () => {

        const entity = new TestEntity({ c: 1, d: 2 });
        const properties: Array<string> = [];

        Utils.forEach(entity, (value, name) => {
            properties.push(name);
        });

        expect(properties).to.deep.equal([
            '_alias', '_isNew', '_hasModifications', '_properties', '_validators', 'b', 'd', 'e'
        ]);
    });

    it('lance une erreur si la valeur n\'est pas valide', () => {

        const entity = new TestEntity();

        expect(() => {
            entity.d = 6;
        }).throws(TypeError);
    });

    it('une propriété peut avoir une valeur par défaut', () => {

        const entity = new TestEntity();

        expect(entity.e).to.be.equal('foobar');
    });

    it('undefined si aucune valeur définie', () => {

        const entity = new TestEntity();

        expect(entity.b).to.be.undefined;
    });

    it('valeur par défaut utilisée si `set(undefined)`', () => {

        const entity = new TestEntity();
        entity.e = undefined;

        expect(entity.e).to.be.equal('foobar');
    });
});
