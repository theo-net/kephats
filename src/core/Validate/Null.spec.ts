/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { NullValidator } from './Null';
import { Validator } from './Validator';

describe('core/Validate/NullValidator', () => {

    it('est un `Validator', () => {

        expect(new NullValidator()).to.be.instanceOf(Validator);
    });

    it('retourne toujours `true`', () => {

        const validator = new NullValidator();

        expect(validator.isValid(0)).to.be.true;
        expect(validator.isValid(-1)).to.be.true;
        expect(validator.isValid(21)).to.be.true;
        expect(validator.isValid(NaN)).to.be.true;
        expect(validator.isValid(true)).to.be.true;
        expect(validator.isValid('foobar')).to.be.true;
        expect(validator.isValid(/a/)).to.be.true;
        expect(validator.isValid({ truc: 1 })).to.be.true;
        expect(validator.isValid([1, 2, 3])).to.be.true;
        expect(validator.isValid(-1.1)).to.be.true;
        expect(validator.isValid(2.1)).to.be.true;
        expect(validator.getOccurredErrors().length).to.be.equal(0);
    });
});
