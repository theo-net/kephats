/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Validator } from './Validator';

/**
 * Teste si la valeur avec une fonction
 *
 *    - `fct`:  Fonction de test fct (value)
 *    - `[message]`: Fonction générant le message `fct (value, testReturn)`
 */
export class FunctionValidator extends Validator {

    _setArguments (): void {

        this._arguments = {
            fct: 'Fonction de test',
            message: 'Fonction générant le message d\'erreur'
        };
        this._defaultArguments = {
            message: (): string => {
                return 'La valeur n\'est pas valide.';
            }
        };
    }


    _validation (value = null): boolean {

        let test = true;

        test = this._validatorArguments.fct(value);

        if (test !== true) {
            test = this._addOccurredError(
                this._validatorArguments.message(value, test)
            );
        }

        return test;
    }
}
