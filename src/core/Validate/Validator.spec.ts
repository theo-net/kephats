/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Validator } from './Validator';

describe('core/Validate/Validator', () => {

    class MyValidator extends Validator {
        _setArguments (): void {
            this._arguments = {
                min: 'Valeur minimum',
                max: 'Valeur maximum'
            };
            this._defaultArguments = { max: 3 };
        }
        _validation (value = 0): boolean {

            let test = true;

            if (value < this._validatorArguments.min || value > this._validatorArguments.max)
                test = this._addOccurredError('Pas bien');

            return test;
        }
        _addOccurredErrorSpy (message: string): boolean {
            return this._addOccurredError(message);
        }

    }
    let validator: MyValidator;
    const args = {
        min: 2,
        max: 5
    };

    beforeEach(() => {

        validator = new MyValidator(args);
    });

    it('sauve les arguments', () => {

        expect(validator.getArguments()).to.be.deep.equal(args);
    });

    it('prend, si besoin, les valeurs par défaut des arguments', () => {

        validator = new MyValidator({ min: 1 });
        expect(validator.getArguments()).to.be.deep.equal({
            min: 1, max: 3
        });
    });

    it('lance une erreur si un argument est manquant', () => {

        class MySecondValidator extends MyValidator {
            _setArguments (): void {
                this._arguments = {
                    min: 'Valeur minimum',
                    max: 'Valeur maximum'
                };
            }
        }

        expect(() => new MySecondValidator({ min: 0 }))
           .to.throw(TypeError, 'Needs parameters:\n  - max: Valeur maximum');
    });


    describe('_addOccurredError', function () {

        it('retourne `false`', function () {

            expect(validator._addOccurredErrorSpy('test')).to.be.equal(false);
        });

        it('ajoute le message', function () {

            validator._addOccurredErrorSpy('test');
            expect(validator.getOccurredErrors()).to.be.deep.equal(['test']);
        });
    });


    describe('validation', () => {

        it('isValid() renvoit `true` si valeur valide', () => {

            expect(validator.isValid(3)).to.be.true;
        });

        it('isValid() renvoit `false` si valeur invalide', () => {

            expect(validator.isValid(10)).to.be.false;
        });

        it('hasError() renvoit `false` si valeur valide', () => {

            validator.isValid(3);
            expect(validator.hasError()).to.be.false;
        });

        it('hasError() renvoit `true` si valeur invalide', () => {

            validator.isValid(10);
            expect(validator.hasError()).to.be.true;
        });

        it('Réinitialise les erreurs avant chaque test', () => {

            validator.isValid(10);
            validator.isValid(3);

            expect(validator.hasError()).to.be.false;
        });
    });


    describe('getOccurredErrors', () => {

        it('Renvoit un tableau vide si aucune erreur', function () {

            expect(validator.getOccurredErrors()).to.be.deep.equal([]);
        });

        it('Renvoit les messages d\'erreur', function () {

            validator.isValid(10);

            expect(validator.getOccurredErrors()).to.be.deep.equal([
                'Pas bien'
            ]);
        });
    });
});
