/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { FloatValidator } from './Float';

/**
 * Teste si la valeur est un entier et est comprise dans un interval
 *
 *    - `min = -Infinity`:  valeur minimum
 *    - `max = Infinity`: valeur maximum
 */
export class IntegerValidator extends FloatValidator {

    _validation (value = NaN): boolean {

        let test = true;

        if (!Number.isInteger(value))
            test = this._addOccurredError('Doit être un entier.');
        else
            test = super._validation(value);

        return test;
    }
}
