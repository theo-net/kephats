/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { NotEmptyValidator } from './NotEmpty';
import { Validator } from './Validator';

describe('core/Validate/NotEmptyValidator', () => {

    it('est un `Validator', () => {

        expect(new NotEmptyValidator()).to.be.instanceOf(Validator);
    });

    it('check if value is not empty', () => {

        const validator = new NotEmptyValidator();

        expect(validator.isValid(0)).to.be.false;
        expect(validator.isValid(-1)).to.be.false;
        expect(validator.isValid(21)).to.be.false;
        expect(validator.isValid('')).to.be.false;
        expect(validator.isValid(true)).to.be.false;
        expect(validator.isValid('foobar')).to.be.true;
        expect(validator.isValid(/a/)).to.be.false;
        expect(validator.isValid({ truc: 1 })).to.be.true;
        expect(validator.isValid([])).to.be.false;
        expect(validator.isValid({})).to.be.false;
        expect(validator.isValid()).to.be.false;
        expect(validator.isValid(2.1)).to.be.false;
        expect(validator.getOccurredErrors()[0])
            .to.be.equal('Ne doit pas être vide.');
        expect(validator.isValid([1, 2, 3])).to.be.true;
        expect(validator.getOccurredErrors().length).to.be.equal(0);
    });
});
