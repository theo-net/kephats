/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { BooleanValidator } from './Boolean';
import { Validator } from './Validator';

describe('core/Validate/BooleanValidator', () => {

    it('est un `Validator', () => {

        expect(new BooleanValidator()).to.be.instanceOf(Validator);
    });

    it('check if value is an boolean', () => {

        const validator = new BooleanValidator();

        expect(validator.isValid(0)).to.be.false;
        expect(validator.isValid(1)).to.be.false;
        expect(validator.isValid('1')).to.be.false;
        expect(validator.isValid(true)).to.be.true;
        expect(validator.isValid('foobar')).to.be.false;
        expect(validator.isValid(/a/)).to.be.false;
        expect(validator.isValid({ truc: 1 })).to.be.false;
        expect(validator.isValid([1, 2, 3])).to.be.false;
        expect(validator.isValid(-1.1)).to.be.false;
        expect(validator.isValid(2.1)).to.be.false;

        expect(validator.getOccurredErrors()[0])
            .to.be.equal('Doit être un boolean.');

        expect(validator.isValid(false)).to.be.true;
        expect(validator.getOccurredErrors().length).to.be.equal(0);
    });
});
