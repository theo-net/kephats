/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Validator } from './Validator';

/**
 * Teste si la valeur est une adresse mail
 */
export class MailValidator extends Validator {

    _setArguments (): void {

        this._arguments = {};
    }

    _validation (value = ''): boolean {

        let test = true;

        if (!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value)) // eslint-disable-line max-len
            test = this._addOccurredError('Doit être une adresse mail.');

        return test;
    }
}
