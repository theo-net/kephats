/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Validator } from './Validator';

/**
 * Teste si la valeur est un entier et est comprise dans un interval
 *
 *    - `min = -Infinity`:  valeur minimum
 *    - `max = Infinity`: valeur maximum
 */
export class FloatValidator extends Validator {

    _setArguments (): void {

        this._arguments = {
            min: 'Valeur minimum',
            max: 'Valeur maximum'
        };
        this._defaultArguments = {
            min: -Infinity,
            max: Infinity
        };
    }


    _validation (value = NaN): boolean {

        let test = true;

        if (typeof value != 'number' || isNaN(value))
            test = this._addOccurredError('Doit être un nombre flottant.');
        else {

            if (value < this._validatorArguments.min) {
                test = this._addOccurredError('Doit être supérieur à '
                    + this._validatorArguments.min + '.');
            }

            if (value > this._validatorArguments.max) {
                test = this._addOccurredError('Doit être inférieur à '
                    + this._validatorArguments.max + '.');
            }
        }

        return test;
    }
}
