/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as sinon from 'sinon';

import { IntegerValidator } from './Integer';
import { FloatValidator } from './Float';
import { FunctionValidator } from './Function';
import { ListValidator } from './List';
import { RegExpValidator } from './RegExp';
import { ValidateValidator } from './Validate';
import { Validator } from './Validator';
import { Validate as ValidateService } from '../Services/Validate';

describe('core/Validate/ValidateValidator', () => {

    let validate: ValidateValidator;

    beforeEach(() => {

        validate = new ValidateValidator();
    });

    it('est un `Validator', () => {

        expect(validate).to.be.instanceOf(Validator);
    });


    describe('addValidator', () => {

        it('retourne l\'objet', () => {

            expect(validate.addValidator(new IntegerValidator()))
                .to.be.equal(validate);
        });

        it('ajoute le validateur', () => {

            const validator1 = new IntegerValidator(),
                  validator2 = new IntegerValidator();

            validate.addValidator(validator1);
            validate.addValidator(validator2);

            expect(validate.getValidators()[0]).to.be.equal(validator1);
            expect(validate.getValidators()[1]).to.be.equal(validator2);
        });
    });


    describe('add', () => {

        it('returns self', () => {

            validate = new ValidateValidator({ service: new ValidateService });
            expect(validate.add('integer')).to.be.equal(validate);
        });

        it('si on donne une RegExp, construit un RegExpValidator', () => {

            validate.add(/a/, 'un message');

            const validator = validate.getValidators()[0];

            expect(validator).to.be.instanceOf(RegExpValidator);
            expect(validator.getArguments().regExp).to.be.deep.equal(/a/);
            expect(validator.getArguments().message())
                .to.be.equal('un message');
        });

        it('si on donne une fonction, construit un FunctionValidator', () => {

            const fct = (): boolean => { return true; };
            validate.add(fct, 'un message');

            const validator = validate.getValidators()[0];

            expect(validator).to.be.instanceOf(FunctionValidator);
            expect(validator.getArguments().fct).to.be.equal(fct);
            expect(validator.getArguments().message())
                .to.be.equal('un message');
        });

        it('on peut récupérer un validateur à partir du service', () => {

            validate = new ValidateValidator({ service: new ValidateService });
            validate.add('integer', { max: 5 });

            const validator = validate.getValidators()[0];

            expect(validator).to.be.instanceOf(IntegerValidator);
            expect(validator.getArguments().max).to.be.equal(5);
        });

        it('on récupère, grâce à un objet, le validateur depuis le service', () => {

            validate = new ValidateValidator({ service: new ValidateService });
            validate.add({ integer: { max: 5 } });

            const validator = validate.getValidators()[0];

            expect(validator).to.be.instanceOf(IntegerValidator);
            expect(validator.getArguments().max).to.be.equal(5);
        });

        it('avec un tableau, on peut ajouter plusieurs validateurs', () => {

            validate = new ValidateValidator({ service: new ValidateService });
            validate.add([
                { integer: { min: 0 } },
                { integer: { max: 5 } }
            ]);

            const validator1 = validate.getValidators()[0],
                  validator2 = validate.getValidators()[1];

            expect(validator1).to.be.instanceOf(IntegerValidator);
            expect(validator2).to.be.instanceOf(IntegerValidator);
            expect(validator1.getArguments().min).to.be.equal(0);
            expect(validator2.getArguments().max).to.be.equal(5);
        });

        it('avec un tableau, on créé un ListValidator', () => {

            validate = new ValidateValidator({ service: new ValidateService });
            validate.add(['a', 'b']);

            const  validator = validate.getValidators()[0];

            expect(validator).to.be.instanceOf(ListValidator);
            expect(validator.getArguments().list).to.be.deep.equal(['a', 'b']);
        });
    });


    describe('isValid', () => {

        it('exécute tous les validateurs', () => {

            const validator1 = new IntegerValidator(),
                  validator2 = new IntegerValidator();
            const spy1 = sinon.spy(validator1, 'isValid'),
                  spy2 = sinon.spy(validator2, 'isValid');

            validate.addValidator(validator1);
            validate.addValidator(validator2);
            validate.isValid(3);

            expect(spy1.calledWith(3)).to.be.true;
            expect(spy2.calledWith(3)).to.be.true;
        });

        it('exécute tous les validateurs, même si un renvoit `false`', () => {

            const validator1 = new IntegerValidator(),
                  validator2 = new FloatValidator();
            const spy1 = sinon.spy(validator1, 'isValid'),
                  spy2 = sinon.spy(validator2, 'isValid');

            validate.addValidator(validator1);
            validate.addValidator(validator2);
            validate.isValid(3.1);

            expect(spy1.calledWith(3.1)).to.be.true;
            expect(spy2.calledWith(3.1)).to.be.true;
        });

        it('renvoit `false` si un des validateur retourne `false`', () => {


            const validator1 = new IntegerValidator(),
                  validator2 = new IntegerValidator({ max: 3 });

            validate.addValidator(validator1);
            validate.addValidator(validator2);

            expect(validate.isValid(4)).to.be.equal(false);
        });

        it('renvoit `true` si tous les validateurs retournent `true`', () => {

            const validator1 = new IntegerValidator(),
                  validator2 = new IntegerValidator();

            validate.addValidator(validator1);
            validate.addValidator(validator2);

            expect(validate.isValid(4)).to.be.equal(true);
        });

        it('sauve les erreurs de tous les validateurs', () => {


            const validator1 = new IntegerValidator();
            const validator2 = new FloatValidator({ max: 3 });

            validate.addValidator(validator1);
            validate.addValidator(validator2);

            validate.isValid(3.5);

            expect(validate.getOccurredErrors()[0])
                .to.be.equal('Doit être un entier.');
            expect(validate.getOccurredErrors()[1])
                .to.be.equal('Doit être inférieur à 3.');
        });

        it('liste des erreurs remise à zéro avant validation', () => {


            const validator1 = new IntegerValidator();
            const validator2 = new FloatValidator({ max: 3 });

            validate.addValidator(validator1);
            validate.addValidator(validator2);

            validate.isValid(3.5);
            validate.isValid(2);

            expect(validate.getOccurredErrors().length).to.be.equal(0);
        });
    });

    describe('_getFromService', () => {

        it('retourne un validateur depuis le service', () => {

            validate = new ValidateValidator({ service: new ValidateService });
            validate.add('integer', { max: 5 });

            const validator = validate.getValidators()[0];

            expect(validator).to.be.instanceOf(IntegerValidator);
            expect(validator.getArguments().max).to.be.equal(5);
        });

        it('envoit une erreur si n\'est pas lié au service', () => {

            let error = false;
            validate = new ValidateValidator();

            try {
                validate.add('truc', { max: 5 });
            } catch (e) {
                error = e.message;
            }

            expect(error).to.be.equal(
                'Le validateur "truc" n\'est pas lié au service `validate` !'
            );
        });

        it('envoit une erreur si le validateur n\'existe pas dans le service', () => {

            let error = false;
            validate = new ValidateValidator({ service: new ValidateService });

            try {
                validate.add('truc', { max: 5 });
            } catch (e) {
                error = e.message;
            }

            expect(error).to.be.equal('Le service `validate` ne possède pas de validateur truc !');
        });
    });
});
