/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { ListValidator } from './List';
import { Validator } from './Validator';

describe('core/Validate/ListValidator', () => {

    it('est un `Validator', () => {

        expect(new ListValidator({ list: 'a' })).to.be.instanceOf(Validator);
    });

    it('check if value is in a list', () => {

        const validator = new ListValidator({ list: [1, 'lol', 2.2] });

        expect(validator.isValid('lol')).to.be.true;
        expect(validator.isValid(2.2)).to.be.true;
        expect(validator.isValid(3)).to.be.false;
        expect(validator.isValid('test')).to.be.false;
        expect(validator.isValid({ a: 1 })).to.be.false;
        expect(validator.isValid([])).to.be.false;
        expect(validator.getOccurredErrors()[0])
            .to.be.equal('Doit avoir une des valeurs suivantes: 1,lol,2.2.');
        expect(validator.isValid(1)).to.be.true;
        expect(validator.getOccurredErrors().length).to.be.equal(0);
    });

    it('fonctionne avec une liste fournie par un `String`', () => {

        const validator = new ListValidator({ list: 'a,b,c,lol' });

        expect(validator.isValid('a')).to.be.true;
        expect(validator.isValid('b')).to.be.true;
        expect(validator.isValid('c')).to.be.true;
        expect(validator.isValid('lol')).to.be.true;
        expect(validator.isValid(1)).to.be.false;
        expect(validator.getOccurredErrors()[0])
            .to.be.equal('Doit avoir une des valeurs suivantes: a,b,c,lol.');
    });
});
