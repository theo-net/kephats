/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as sinon from 'sinon';

import { FunctionValidator } from './Function';
import { Validator } from './Validator';

describe('core/Validate/FunctionValidator', () => {

    it('est un `Validator', () => {

        expect(new FunctionValidator({
            fct: (): boolean => { return true; }
        })).to.be.instanceOf(Validator);
    });

    it('execute the function to test the value', () => {

        const valid = sinon.spy();
        const validator = new FunctionValidator({ fct: valid });

        validator.isValid('a');

        expect(valid.calledWith('a')).to.be.true;
    });

    it('execute the function to generate the message', () => {

        const msg = sinon.spy();
        const validator = new FunctionValidator({
            fct: (): string => { return 'return'; }, message: msg
        });

        validator.isValid('a');

        expect(msg.calledWith('a', 'return')).to.be.true;
    });

    it('return `true` si la fonction retourne `true`', () => {

        const validator = new FunctionValidator({
            fct: (): boolean => { return true; }
        });

        expect(validator.isValid('a')).to.be.equal(true);
    });

    it('return `false` si la fonction retourne `false`', () => {

        const validator = new FunctionValidator({
            fct: (): boolean => { return false; }
        });

        expect(validator.isValid('a')).to.be.equal(false);
    });

    it('remise à zéro des erreurs', () => {

        const validator = new FunctionValidator({
            fct: (val: number): boolean => { return val > 1; }
        });

        validator.isValid(0);
        validator.isValid(2);
        expect(validator.getOccurredErrors().length).to.be.equal(0);
    });
});
