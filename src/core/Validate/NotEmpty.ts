/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Validator } from './Validator';
import * as Utils from '../Utils';

/**
 * Teste si la valeur est n'est pas vide
 */
export class NotEmptyValidator extends Validator {

    _setArguments (): void {

        this._arguments = {};
    }

    _validation (value = null): boolean {

        let test = true;

        if (Utils.isEmpty(value))
            test = this._addOccurredError('Ne doit pas être vide.');

        return test;
    }
}
