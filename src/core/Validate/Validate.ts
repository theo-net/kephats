/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ArgumentsConstructorType, Validator } from './Validator';
import { FunctionValidator } from './Function';
import { ListValidator } from './List';
import { RegExpValidator } from './RegExp';
import * as Utils from '../Utils';

/* eslint-disable @typescript-eslint/no-explicit-any */
export type tValidator = string|Function|RegExp|Record<string, ArgumentsConstructorType>;

/**
 * Créé une collection de validateurs qui seront tous testés.
 */
export class ValidateValidator extends Validator {

    protected _validators: Array<Validator> = [];

    _setArguments (): void {

        this._arguments = {
            service: 'Service validate'
        };
        this._defaultArguments = {
            service: null
        };
    }


    /**
     * Ajoute un validateur à la collection
     *
     * @param validator Validateur à ajouter
     * @returns this
     * @throws {TypeError}
     */
    addValidator (validator: Validator): ValidateValidator {

        this._validators.push(validator);

        return this;
    }


    /**
     * Ajoute un nouveau validateur en le construisant grâce au service `validate`.
     *
     * @param validator Validateur à ajouter. Si `validator` est un `string`, récupère le validateur
     *    correspondant depuis le service, s'il s'agit d'une fonction, construit un nouveau validateur,
     *    de même s'il s'agit d'une `RegExp`. S'il s'agit d'un objet, on récupère le service
     *    correspondant:
     *        { name: args } ex: {integer: {min: 0, max: 5}}
     *    S'il s'agit d'un tableau : soit on construit une liste (ListValidator), soit il s'agit
     *    d'une série de validateurs décrits par `tValidator` comme ci-dessus.
     *          `tValidator` = string|Function|RegExp|{[keys: string]: {[keys: string]: any}}
     * @param  args Arguments qui seront passés au validateur. S'il s'agit d'un `string`, ce sera le
     *    message d'erreur (utilisé si on construit le validateur à partir d'une fonction ou d'une
     *   `RegExp`).
     * @returns this
     * @throws {TypeError}
     */
    add (
        validator: tValidator|Array<tValidator>,
        args: ArgumentsConstructorType|string = {}
    ): ValidateValidator {

        // On initialise si besoin la liste des validators
        if (!Array.isArray(this._validators))
            this._validators = [];

        // Si args est en fait le message
        let message;
        if (Utils.isString(args)) {
            message = (): ArgumentsConstructorType|string => {
                return args;
            };
        }

        // Validator est une RegExp
        if (validator instanceof RegExp) {
            if (message !== undefined) {
                this._validators.push(new RegExpValidator({
                    regExp: validator, message: message
                }));
            }
            else
                this._validators.push(new RegExpValidator({ regExp: validator }));
        }
        // Validator est une Function
        else if (Utils.isFunction(validator)) {
            if (message !== undefined) {
                this._validators.push(new FunctionValidator({
                    fct: validator, message: message
                }));
            }
            else
                this._validators.push(new FunctionValidator({ fct: validator }));
        }
        // On construit le validateur à partir du service
        else if (Utils.isString(validator))
            this._validators.push(this._getFromService(validator.toString(), args));
        // Array
        else if (Array.isArray(validator)) {
            // Object
            if (Utils.isObject(validator[0]))
                validator.forEach(val => this.add(val));
            // List
            else
                this._validators.push(new ListValidator({ list: validator }));
        }
        // Object
        else if (Utils.isObject(validator)) {

            const valName = Object.getOwnPropertyNames(validator)[0];
            args = Object.values(validator)[0];
            this._validators.push(this._getFromService(valName, args));
        }
        else {
            throw new TypeError(
                'La validateur ajouté n\'est pas valide !');
        }

        return this;
    }


    /**
     * Retourne tous les validateurs
     */
    getValidators (): Array<Validator> {

        return this._validators;
    }


    /**
     * Exécute tous les validateurs. Renvoit `false` si un ne passe pas.
     * @param {*} value Valeur à tester
     * @returns {Boolean}
     */
    _validation (value = null): boolean {

        let test = true;

        this._validators.forEach(validator => {

            test = validator.isValid(value) && test;

            if (validator.hasError()) {
                this._occurredErrors =
                    this._occurredErrors.concat(validator.getOccurredErrors());
            }
        });

        return test;
    }


    /**
     * Récupère un validateur depuis le service validate
     *
     * @param name Nom du validateur
     * @param args Arguments
     */
    private _getFromService (name: string, args: ArgumentsConstructorType|string): Validator {

        // On vérifie que Validate est lié au service validate
        if (this._validatorArguments.service === null) {
            throw new Error(
                'Le validateur "' + name + '" n\'est pas lié au service `validate` !');
        }

        return this._validatorArguments.service.getWithName(name, args);
    }
}
