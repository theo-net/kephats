/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as sinon from 'sinon';

import { RegExpValidator } from './RegExp';
import { Validator } from './Validator';

describe('core/Validate/RegExpValidator', () => {

    it('est un `Validator', () => {

        expect(new RegExpValidator({ regExp: /.*/ })).to.be.instanceOf(Validator);
    });

    it('execute the function to generate the message', () => {

        const msg = sinon.spy();
        const validator = new RegExpValidator({
            regExp: /a/, message: msg
        });

        validator.isValid('b');

        expect(msg.calledWith('b')).to.be.true;
    });

    it('return `true` si le test retourne `true`', () => {

        const validator = new RegExpValidator({ regExp: /a/ });

        expect(validator.isValid('a')).to.be.equal(true);
    });

    it('return `false` si la fonction retourne `false`', () => {

        const validator = new RegExpValidator({ regExp: /a/ });

        expect(validator.isValid('b')).to.be.equal(false);
    });

    it('remise à zéro des erreurs', () => {

        const validator = new RegExpValidator({ regExp: /a/ });
        validator.isValid('b');
        validator.isValid('a');
        expect(validator.getOccurredErrors().length).to.be.equal(0);
    });
});
