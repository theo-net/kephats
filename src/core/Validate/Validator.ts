/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as Utils from '../Utils';

/* eslint-disable @typescript-eslint/no-explicit-any */

// Décrit les arguments
export type ArgumentsConstructorType = Record<string, any>;

/**
 * Objet permettant de valider des données. On ne doit pas l'instancier directement, mais les
 * différents Validator doivent étendre cette classe.
 *
 * Chaque validateur peut définir `this._arguments` avec les arguments nécessaires à la validation
 * des données :
 *
 *      this._arguments = {
 *          min: 'Valeur minimum',
 *          max: 'Valeur maximum'
 *      }
 */
export abstract class Validator {

    protected _arguments: { [key: string]: string} = {};
    protected _defaultArguments: ArgumentsConstructorType = {};
    protected _occurredErrors: Array<string> = [];
    protected _validatorArguments: ArgumentsConstructorType;

    /**
     * Construit le validateur
     * @param {Object} [args={}] Arguments nécessaires à la construction du
     *                           validateur
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    constructor (args: ArgumentsConstructorType = {}) {

        // Arguments du validateur
        this._setArguments();
        this._validatorArguments = Utils.extend(this._defaultArguments, args);
        this._checkArguments();
    }


    /**
     * Retourne les arguments du validateur
     */
    getArguments (): ArgumentsConstructorType {

        return this._validatorArguments;
    }


    /**
     * Vérifie si `value` est une valeur valide.
     * À chaque exécution de la méthode, la liste des erreurs est réinitialisée
     * @param value Valeur à tester
     */
    isValid (value: any = null): boolean {

        this.reset();

        return this._validation(value);
    }


    /**
     * Regarde si une valeur a été détectée
     */
    hasError (): boolean {

        return !Utils.isEmpty(this._occurredErrors);
    }


    /**
     * Renvoit la liste des erreurs si la validation ne passe pas, un tableau vide sinon.
     */
    getOccurredErrors (): Array<string> {

        return this._occurredErrors;
    }


    /**
     * Remet à zéro la liste d'erreurs
     */
    reset (): void {

        this._occurredErrors = [];
    }


    /**
     * Définit les arguments. C'est ici que l'on doit définir les arguments.
     *
     * Doit contenir :
     *
     *      this._arguments = {
     *          nom: 'description',
     *          ...
     *      } ;
     *      this._defaultArguments = {
     *          nom: valeur par défaut
     *          ...
     *      }
     */
    protected abstract _setArguments (): void;


    /**
     * Validation à implémenter
     * @param value Valeur à tester
     */
    protected abstract _validation (value: any): boolean;


    /**
     * Check que les arguments obligatoires soient bien transmis. Envoi une erreur sinon
     * @throws {BaseError}
     */
    private _checkArguments (): boolean {

        const needed: Array<string> = [];

        Object.keys(this._arguments).forEach(name => {
            if (!Object.prototype.hasOwnProperty.call(this._validatorArguments, name))
                needed.push(name + ': ' + this._arguments[name]);
        });

        if (Utils.isEmpty(needed))
            return true;

        let message = '';
        needed.forEach(msg => { message += '\n  - ' + msg; });

        throw new TypeError('Needs parameters:' + message);
        return false;
    }


    /**
     * Ajoute un message d'erreur. Renvoit toujours false pour faciliter l'écriture des tests.
     * @param message Message à ajouter
     */
    protected _addOccurredError (message: string): false {

        this._occurredErrors.push(message);
        return false;
    }
}
