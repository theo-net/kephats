/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Validator } from './Validator';
import * as Utils from '../Utils';

/**
 * Teste si la valeur est une chaîne de caractère dont la longueur est comprise
 * dans un interval
 *
 *    `- min = 0`:         taille minimum
 *    `- max = -Infinity`: taille maximum
 */
export class StringValidator extends Validator {

    _setArguments (): void {

        this._arguments = {
            min: 'Taille minimum de la chaîne de caractère',
            max: 'Taille maximum de la chaîne de caractère'
        };
        this._defaultArguments = {
            min: 0,
            max: Infinity
        };
    }


    _validation (value = ''): boolean {

        let test = true;

        if (!Utils.isString(value))
            test = this._addOccurredError('Doit être une chaîne de caractères.');
        else {

            if (value.length < this._validatorArguments.min) {
                test = this._addOccurredError('La valeur est trop courte, le minimum '
                    + 'est ' + this._validatorArguments.min + ' et vous avez entré '
                    + value.length + ' caractère(s).');
            }

            if (value.length > this._validatorArguments.max) {
                test = this._addOccurredError('La valeur est trop longue, le maximum '
                    + 'est ' + this._validatorArguments.max + ' et vous avez entré '
                    + value.length + ' caractère(s).');
            }
        }

        return test;
    }
}
