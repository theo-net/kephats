/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Validator } from './Validator';

/**
 * Teste si la valeur avec une expression régulière
 *
 *    - `regExp`:  Expression régulière
 *    - `[message]`: Fonction générant le message `fct (value)`
 */
export class RegExpValidator extends Validator {

    _setArguments (): void {

        this._arguments = {
            regExp: 'Expression régulière',
            message: 'Fonction générant le message d\'erreur'
        };
        this._defaultArguments = {
            message: (): string => {
                return 'La valeur n\'est pas valide.';
            }
        };
    }


    _validation (value = null): boolean {

        const test = this._validatorArguments.regExp.test(value);

        if (test !== true)
            this._addOccurredError(this._validatorArguments.message(value));

        return test;
    }
}
