/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Validator } from './Validator';
import * as Utils from '../Utils';

/**
 * Teste si la valeur est un élément de la liste
 *
 *    - `list`: Liste des valeurs possibles, peut-être un `Array` ou une chaîne de
 *          caractères, dans ce cas, elle sera découpée selon les `,`
 */
export class ListValidator extends Validator {

    _setArguments (): void {

        this._arguments = {
            list: 'Liste des valeurs'
        };
    }


    _validation (value = null): boolean {

        let test = true;

        if (Utils.isString(this._validatorArguments.list))
            this._validatorArguments.list = this._validatorArguments.list.split(',');

        if (this._validatorArguments.list.indexOf(value) === -1) {
            test = this._addOccurredError('Doit avoir une des valeurs suivantes: '
                + this._validatorArguments.list + '.');
        }

        return test;
    }
}
