/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Validator } from './Validator';

/**
 * Renvoit toujours `true`.
 */
export class NullValidator extends Validator {

    _setArguments (): void {

        this._arguments = {};
    }

    _validation (value = null): boolean {

        return !!value || true;
    }
}
