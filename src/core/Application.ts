/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Kernel } from "./Kernel";

/**
 * Chaque script/application réalisés avec le framework doit prendre comme racine cette classe
 * (même s'il passe par une autre dérivée, `Cli`, `Server`, ...)
 *
 * Celle-ci fournit un accès aux Kernel, s'enregistre comme service, et fournit une série d'outils
 * disponibles directement avec `Application`.
 */
export class Application {

    protected _name = '';
    protected _version = '';
    protected _kernel: Kernel;

    /**
     * Créé une nouvelle application. La configuration passée en paramètre sera chargée dans le
     * noyau. Il s'agit normalement de la configuration de l'environnement (voir `cli/` et
     * `browser/` pour comprendre la manière dont elle est chargée).
     * @param kernel Le Kernel
     * @param envConfig Configuration d'environnement
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    constructor (kernel: Kernel, envConfig?: Record<string, any>) {

        this._kernel = kernel;

        if (envConfig)
            kernel.get('$config').add(envConfig, true);
    }


    /**
     * Retourne le kernel
     */
    kernel (): Kernel {

        return this._kernel;
    }


    /**
     * Définit ou donne le nom de notre application
     *
     * @param name Nom de l'application
     * @returns Le nom, si aucun paramètre, `this` sinon
     */
    name (name?: string): string | this {

        if (name) {
            this._name = name;
            return this;
        }
        else
            return this._name;
    }


    /**
     * Définit ou donne la version de notre application
     *
     * @param version Version de l'application
     * @returns La version, si aucun paramètre, `this` sinon
     */
    version (version?: string): string | this {

        if (version) {
            this._version = version;
            return this;
        }
        else
            return this._version;
    }
}
