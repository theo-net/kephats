/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Application } from './Application';
import * as Framework from './Framework';
import { Entity } from './Entity';
import { Kernel } from './Kernel';

describe('core/Framework', () => {

    it('donne accès au Kernel', () => {

        expect(Framework.kernel()).to.be.instanceOf(Kernel);
    });

    it('donne accès à la class Entity', () => {

        expect(Framework.Entity).to.be.equal(Entity);
    });

    it('app() retourne une nouvelle instance d\'Application', () => {

        expect(Framework.app()).to.be.instanceOf(Application);
        expect(Framework.app()).to.be.not.equal(Framework.app());
    });

    it('app() transmet le kernel à l\'application', () => {

        expect(Framework.app().kernel()).to.be.equal(Framework.kernel());
    });

    it('app() enregistre l\'applocation comme service', () => {

        const app = Framework.app();
        expect(app.kernel().get('$application')).to.be.equal(app);
    });
});
