/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Kernel } from './Kernel';
import { Application } from './Application';

/**
 * Réunit tous les outils nécessaires
 */

export { Entity } from './Entity';

/**
 * Retourne le Kernel
 */
export function kernel (): Kernel {

    return new Kernel();
}

/**
 * Créé une nouvelle application
 */
export function app (): Application {

    const kernel = new Kernel();
    const app = new Application(kernel);
    kernel.container().register('$application', app);
    return app;
}
