/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Application } from './Application';
import { Kernel } from './Kernel';
import { Plugin } from './Plugin';

class MyPlugin extends Plugin {
    init (): void {
        return;
    }
}

describe('core/Plugin', () => {

    it('extends Application', () => {

        expect(new MyPlugin(new Kernel())).to.be.instanceOf(Application);
    });
});
