/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Kernel } from './Kernel';

/**
 * En étendant cette classe, on permet aux enfants d'avoir une méthode
 * `kernel()` retournant le Kernel de l'Application.
 */
export abstract class KernelAccess {

    /**
     * Retourne l'instance de Kernel, ce dernier étant un singleton
     */
    kernel (): Kernel {

        return new Kernel();
    }
}
