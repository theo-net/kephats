/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Application } from "./Application";

export abstract class Plugin extends Application {

    /**
     * Initialisation du plugin (sera exécuté lors de son chargement)
     * @param config Configuration du plugin
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    abstract init (config?: {[keys: string]: any}): void;
}
