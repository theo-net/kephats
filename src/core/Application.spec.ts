/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Application } from './Application';
import { Kernel } from "./Kernel";

describe('core/Application', () => {

    const kernel = new Kernel();

    it('enregistre le kernel', () => {

        const app = new Application(kernel);
        expect(app.kernel()).to.be.equal(kernel);
    });

    it('charge une configuration d\'enrionnement', () => {

        const app = new Application(kernel, { anEnvValue: 49 });
        expect(app.kernel().get('$config').get('anEnvValue')).to.be.equal(49);
    });


    describe('name', () => {

        const app = new Application(kernel);

        it('définit le nom de l\'application', () => {

            app.name('app');
            expect(app.name()).to.be.equal('app');
        });

        it('si un nom est défini, retourne l\'app', () => {

            expect(app.name('appCool')).to.be.equal(app);
        });
    });

    describe('version', () => {

        const app = new Application(kernel);

        it('définit la version de l\'application', () => {

            app.version('0.1.2');
            expect(app.version()).to.be.equal('0.1.2');
        });

        it('si une version est définit, retourne l\'app', () => {

            expect(app.version('0.2.3')).to.be.equal(app);
        });
    });
});

