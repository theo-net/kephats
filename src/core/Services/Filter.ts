/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { filterFilter } from './filterFilter';

/**
 * Gère les filtres disponibles dans notre application
 */
export class Filter {

    protected _filters: { [keys: string]: Function } = {};

    constructor () {

        // On enregistre le filtre filter
        this.register('filter', filterFilter);
    }


    /**
     * Indique si un filtre est enregistré
     * @param {String} name Nom du filtre
     * @returns {Boolean}
     */
    has (name: string): boolean {

        return Object.keys(this._filters).indexOf(name) > -1;
    }


    /**
     * Enregistre le filtre.
     *
     * ATTENTION : `filter` doit être de la forme `function () {}` et non de la forme `() => {}`
     *             pour que le parser puisse fonctionner.
     * @param name Nom du filtre
     * @param filter Filtre à enregistrer
     */
    register (name: string, filter: Function): void {

        this._filters[name] = filter;
    }


    /**
     * Retourne un filtre
     * @param name Nom du filtre
     */
    get (name: string): Function {

        return this._filters[name];
    }
}
