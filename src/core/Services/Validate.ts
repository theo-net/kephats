/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ValidateValidator } from '../Validate/Validate';
import { BooleanValidator } from '../Validate/Boolean';
import { FloatValidator } from '../Validate/Float';
import { FunctionValidator } from '../Validate/Function';
import { IntegerValidator } from '../Validate/Integer';
import { ListValidator } from '../Validate/List';
import { NotEmptyValidator } from '../Validate/NotEmpty';
import { NullValidator } from '../Validate/Null';
import { RegExpValidator } from '../Validate/RegExp';
import { StringValidator } from '../Validate/String';
import { MailValidator } from '../Validate/Mail';
import { ArgumentsConstructorType, Validator } from '../Validate/Validator';

/**
 * Créé un service permettant de générer des objets validant des données.
 */
export class Validate {

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected _validators: { [keys: string]: Record<string, any> } = {
        boolean: { object: new BooleanValidator() },
        float: { class: FloatValidator },
        function: { class: FunctionValidator },
        integer: { class: IntegerValidator },
        list: { class: ListValidator },
        notEmpty: { class: NotEmptyValidator },
        'null': { object: new NullValidator() },
        regExp: { class: RegExpValidator },
        string: { class: StringValidator },
        mail: { class: MailValidator }
    };


    /**
     * Construit un nouveau validateur
     *
     * @param validator Voir `ValidateValidator.add()`
     * @param args Voir `ValidateValidator.add()`
     */
    make (validator?: string | Function | RegExp, args: {} | string = {}): ValidateValidator {

        const val = new ValidateValidator({ service: this });

        validator = validator ? validator : 'null';

        return val.add(validator, args);
    }


    /**
     * Retourne un nouveau validateur à partir de son nom
     * @param name Nom du validateur
     * @param args Arguments
     */
    getWithName (name: string, args: ArgumentsConstructorType = {}): Validator {

        if (!Object.prototype.hasOwnProperty.call(this._validators, name)) {
            throw new Error('Le service `validate` ne possède pas de validateur '
                + name + ' !');
        }

        if (Object.prototype.hasOwnProperty.call(this._validators[name], 'object'))
            return this._validators[name].object;

        return new this._validators[name].class(args);
    }


    /**
     * Définit un nouveau validateur
     *
     *     validator = {
     *         class: require('..'),   // créer à partir d'une classe
     *         object: new *Validator  // on s'occupe nous-même de la création
     *     }
     *
     * @param name Nom du validateur
     * @param validator Description du validateur
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    setValidator (name: string, validator: Record<string, any>): void {

        this._validators[name] = validator;
    }
}
