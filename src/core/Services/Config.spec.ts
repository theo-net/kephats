/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Config } from './Config';

describe('core/Service/Config', () => {

    let config: Config;

    beforeEach(() => {

        config = new Config();
    });

    describe('set', () => {

        it('enregistre un élément de configuration', () => {

            config.set('test', 'aValue');
            expect(config.get('test')).to.be.equal('aValue');
        });

        it('n\'écrase pas, par défaut, un élément existant', () => {

            config.set('test', 'aValue');
            config.set('test', 'otherValue');
            expect(config.get('test')).to.be.equal('aValue');
        });

        it('possède un paramètre permettant d\'écraser un élément portant déja le même identifiant',
        () => {

            config.set('test', 'aValue');
            config.set('test', 'otherValue', true);
            expect(config.get('test')).to.be.equal('otherValue');
        });
    });


    describe('get', () => {

        it('retourne la valeur d\'un paramètre', () => {

            config.set('param', 'foobar');
            expect(config.get('param')).to.be.equal('foobar');
        });

        it('retourne une valeur par défaut si le param est inexistant', () => {

            config.set('param', 'foobar');
            expect(config.get('parami2', 'lol')).to.be.equal('lol');
        });

        it('la valeur par défaut peut être le résultat d`une fct', () => {

            config.set('param', 'foobar');
            expect(config.get('parami2', () => 'lol')).to.be.equal('lol');
        });

        it('remplace les constantes par leur valeur', () => {

            config.add({
                'param1': 'foo',
                'param2': 'bar',
                'param3': '%param1%',
                'param4': '%param1%?',
                'param5': '%param1%%param2%'
            });
            const expected = {
                'param1': 'foo',
                'param2': 'bar',
                'param3': 'foo',
                'param4': 'foo?',
                'param5': 'foobar'
            };

            expect(config.get('param3')).to.be.equal(expected.param3);
            expect(config.get('param4')).to.be.equal(expected.param4);
            expect(config.get('param5')).to.be.equal(expected.param5);
        });

        it('remplace les constantes par leur valeur sauf si format=false', () => {

            config.add({
                'param1': 'foo',
                'param2': 'bar',
                'param3': '%param1%',
                'param4': '%param1%?',
                'param5': '%param1%%param2%'
            });
            const expected = {
                'param1': 'foo',
                'param2': 'bar',
                'param3': '%param1%',
                'param4': '%param1%?',
                'param5': '%param1%%param2%'
            };

            expect(config.get('param3', null, false)).to.be.equal(expected.param3);
            expect(config.get('param4', null, false)).to.be.equal(expected.param4);
            expect(config.get('param5', null, false)).to.be.equal(expected.param5);
        });

        it('n\'essaye pas de formater les non string', () => {

            const obj = {};
            config.add({
                'param1': 'foo',
                'param2': '%param1%?',
                'param3': obj
            });
            const expected = {
                'param1': 'foo',
                'param2': 'foo?',
                'param3': obj
            };

            expect(config.get('param2')).to.be.equal(expected.param2);
            expect(config.get('param3')).to.be.equal(expected.param3);
        });

        it('on peut retourner une série de valeur avec foo.bar.*', () => {

            config.add({
                'foo.truc': 1,
                'foo.bar.a': 1,
                'foo.bar.b': 1,
                'foo.bar.c.d': 1,
                'foo.bar.c.e': 1,
                'foo.bar.c.f.g': 1
            });
            expect(config.get('foo.bar.*')).to.be.deep.equal({
                'a': 1, 'b': 1, 'c.d': 1, 'c.e': 1, 'c.f.g': 1
            });
        });

        it('dans ce cas, la valeur par défaut est mergée', () => {

            config.add({
                'foo.bar.a': 1,
                'foo.bar.c.d': 1,
                'foo.bar.c.e': 0,
                'foo.bar.c.f.g': 1
            });
            expect(config.get('foo.bar.*', { b: 1, 'c.e': 1 })).to.be.deep.equal({
                'a': 1, 'b': 1, 'c.d': 1, 'c.e': 0, 'c.f.g': 1
            });
        });
    });


    describe('has', () => {

        it('returns true if config have the key', () => {

            config.set('test', true);
            expect(config.has('test')).to.be.equal(true);
        });

        it('returns false if config haven\'t the key', () => {

            config.set('test', true);
            expect(config.has('test2')).to.be.equal(false);
        });
    });


    describe('delete', () => {

        it('delete a element', () => {

            config.set('test', true);
            expect(config.has('test')).to.be.equal(true);
            config.delete('test');
            expect(config.has('test')).to.be.equal(false);
        });

        it('delete element foo.bar.*', () => {

            config.add({
                'foo.truc': 1,
                'foo.bar.a': 1,
                'foo.bar.b': 1,
                'foo.bar.c.d': 1,
                'foo.bar.c.e': 1,
                'foo.bar.c.f.g': 1
            });
            config.delete('foo.bar.*');
            expect(config.has('foo.truc')).to.be.true;
            expect(config.has('foo.bar.a')).to.be.false;
            expect(config.has('foo.bar.b')).to.be.false;
            expect(config.has('foo.bar.c.d')).to.be.false;
            expect(config.has('foo.bar.c.e')).to.be.false;
            expect(config.has('foo.bar.c.f.g')).to.be.false;
        });
    });


    describe('add', () => {

        it('ajoute une série d\'éléments', () => {

            config.set('test', 1);
            config.add({ 'a': 2, 'b': 3 });
            expect(config.getAll()).to.be.deep.equal({ 'test': 1, 'a': 2, 'b': 3 });
        });
    });


    describe('getAll', () => {

        it('retourne tous les éléments', () => {

            config.add({ 'a': 2, 'b': 3 });
            expect(config.getAll()).to.be.deep.equal({ 'a': 2, 'b': 3 });
        });
    });


    describe('reset', () => {

        it('supprime tous les éléments de la config', () => {

            config.add({ 'a': 2, 'b': 3 });
            config.reset();
            expect(config.getAll()).to.be.deep.equal({});
        });
    });
});

