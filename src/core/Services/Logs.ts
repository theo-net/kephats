/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { KernelAccess} from '../KernelAccess';

/**
 * LogLevel
 */
export type LogLevel = 'normal' | 'warn' | 'debug';

/**
 * Type d'un message de log (associé ici au logLevel) :
 *
 *  - `normal` : `info`, `success`, `error`
 *  - `warn` : `normal` + `warning`
 *  - `debug` : `warn` + `debug` + `data`
 */
export enum LogType {
    info, success, error, warning, debug, data
}
type LogTypeString = keyof typeof LogType;

/**
 * Gestion des logs de l'application
 */
export class Logs extends KernelAccess {

    protected _fctTransform = {
        info: this._defaultFct,
        success: this._defaultFct,
        error: this._defaultFct,
        warning: this._defaultFct,
        debug: this._defaultFct,
        data: this._defaultFct
    };


    /**
     * Traite un message de log
     * @param message Message
     * @param type Type
     */
    log (message: string, type: LogTypeString = 'info'): void {

        const logLevel: LogLevel = this.getLogLevel();

        if (type == 'info')
            message = 'Info: ' + message;
        else if (type == 'warning')
            message = 'Warning: ' + message;

        if (type == 'info')
            console.log(this._fctTransform['info'](message));
        else if (type == 'success')
            console.log(this._fctTransform['success'](message));
        else if (type == 'error')
            console.error(this._fctTransform['error'](message));
        else if (type == 'warning' &&
            (logLevel == 'warn' || logLevel == 'debug'))
            console.log(this._fctTransform['warning'](message));
        else if (type == 'debug' && logLevel == 'debug')
            console.log(this._fctTransform['debug'](message));
        else if (type == 'data' && logLevel == 'debug')
            console.log(this._fctTransform['data'](message));
    }


    /**
     * Retourne le logLevel
     */
    getLogLevel (): LogLevel {

        return this.kernel().get('$config').get('logLevel');
    }


    /**
     * Personnalise un message lors de son affichage dans la console (utile pour un formatage en cli
     * par exemple)
     */
    setTransformMessage (type: LogTypeString , fct: (text: string) => string): void {

        this._fctTransform[type] = fct;
    }


    /**
     * Fonction de transformation par défaut (ne fait rien)
     */
    protected _defaultFct (text: string): string {

        return text;
    }
}
