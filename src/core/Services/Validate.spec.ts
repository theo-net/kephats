/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Validate } from './Validate';
import { ValidateValidator } from '../Validate/Validate';
import { BooleanValidator } from '../Validate/Boolean';
import { FloatValidator } from '../Validate/Float';
import { FunctionValidator } from '../Validate/Function';
import { IntegerValidator } from '../Validate/Integer';
import { ListValidator } from '../Validate/List';
import { NotEmptyValidator } from '../Validate/NotEmpty';
import { NullValidator } from '../Validate/Null';
import { RegExpValidator } from '../Validate/RegExp';
import { StringValidator } from '../Validate/String';
import { MailValidator } from '../Validate/Mail';

describe('core/Services/Validate', () => {

    let validate: Validate;

    describe('make', () => {

        it('Renvoit un `Validator`', () => {

            validate = new Validate();
            expect(validate.make('notEmpty')).to.be.instanceOf(ValidateValidator);
        });
    });


    describe('getWithName', () => {

        it('returns BooleanValidator', () => {

            expect(validate.getWithName('boolean'))
                .to.be.instanceOf(BooleanValidator);
        });

        it('returns FloatValidator', () => {

            expect(validate.getWithName('float'))
                .to.be.instanceOf(FloatValidator);
        });

        it('returns FunctionValidator', () => {

            expect(validate.getWithName('function', { fct: () => { return true; } }))
                .to.be.instanceOf(FunctionValidator);
        });

        it('returns IntegerValidator', () => {

            expect(validate.getWithName('integer'))
                .to.be.instanceOf(IntegerValidator);
        });

        it('returns ListValidator', () => {

            expect(validate.getWithName('list', { list: 'a,b' }))
                .to.be.instanceOf(ListValidator);
        });

        it('returns NotEmptyValidator', () => {

            expect(validate.getWithName('notEmpty'))
                .to.be.instanceOf(NotEmptyValidator);
        });

        it('returns NullValidator', () => {

            expect(validate.getWithName('null'))
                .to.be.instanceOf(NullValidator);
        });

        it('returns RegExpValidator', () => {

            expect(validate.getWithName('regExp', { regExp: /a/ }))
                .to.be.instanceOf(RegExpValidator);
        });

        it('returns StringValidator', () => {

            expect(validate.getWithName('string'))
                .to.be.instanceOf(StringValidator);
        });

        it('returns MailValidator', () => {

            expect(validate.getWithName('mail'))
                .to.be.instanceOf(MailValidator);
        });
    });

    describe('setValidator', () => {

        it('on définit un validator en fournissant sa class', () => {

            validate.setValidator('test', { class: IntegerValidator });
            expect(validate.getWithName('test'))
                .to.be.instanceOf(IntegerValidator);
        });

        it('on définit un validator en fournissant un objet', () => {

            const validator = new IntegerValidator();
            validate.setValidator('test', { object: validator });

            expect(validate.getWithName('test')).to.be.equal(validator);
        });
    });
});

