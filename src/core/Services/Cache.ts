/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as Utils from '../Utils';

/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * Service gérant le cache de l'application
 * Chaque élément peut être associé à une date d'expiration à partir de laquelle l'élément
 * sera effacé.
 */
export class Cache {

    protected _enabled = true;
    protected _exp: { [keys: string]: number|null } = {};
    protected _vars: Record<string, any> = {};


    /**
     * Indique si le cache est activé ou non
     */
    isEnabled (): boolean {

        return this._enabled;
    }


    /**
     * Active ou désactive le cache
     */
    enable (enabled: boolean): void {

        this._enabled = enabled;
    }


    /**
     * Définit une donnée à mettre en cache. Un valeur précédente sera toujours écrasée.
     * @param id Identifiant
     * @param value Valeur
     * @param exp Expiration (en seconde depuis maintenant), mettre `null` (valeur par défaut)
     *      pour aucune expiration.
     */
    set (id: string, value: any, exp: number|null = null): void {

        this._vars[id] = value;
        this._exp[id] = exp !== null ? Date.now() + exp * 1000 : null;
    }


    /**
     * Retourne la valeur contenue dans le cache. S'il est absent ou expiré, on retournera la
     * valeur par défaut qui sera mis en cache.
     *
     * @param id Identifiant
     * @param dflt Valeur par défaut
     * @param exp Expiration (en seconde depuis maintenant), mettre `null` (valeur par défaut)
     *      pour aucune expiration.
     */
    get (id: string, dflt: any = null, exp: number|null = null): any {

        if (/.+\.\*$/.test(id)) {

            const root = id.substr(0, id.length - 1);
            let results: Record<string, any> = {};

            Utils.forEach(this._vars, (value, id) => {

                if (id.substr(0, root.length) == root && this.has(id))
                    results[id.substr(root.length)] = this._get(id);
            });

            if (Utils.isObject(dflt))
                results = Utils.extend({}, dflt, results);

            return results;
        }
        else {
            if (this.has(id))
                return this._get(id, dflt);
            else if (dflt !== null) {
                this.set(id, Utils.isFunction(dflt) ? dflt() : dflt, exp);
                return this._get(id, null);
            }
            else
                return null;
        }
    }


    /**
     * Retourne l'expiration d'un élément du cache. Si l'élément n'existe pas, renvoit `undefined`
     * @param id Id de l'élément
     */
    getExp (id: string): number | null | undefined {

        if (this.has(id))
            return this._exp[id];
        else
            return undefined;
    }


    /**
     * Retourne tous les éléments du cache
     */
    getAll (): Record<string, any> {

        return this._vars;
    }


    /**
     * Indique si un élément est en cache ou non. Retournera également `false` si l'élément
     * à expiré.
     * @param id Identifiant de l'élément à tester
     */
    has (id: string): boolean {

        let has = false;

        if (this._enabled) {

            if (Object.keys(this._vars).indexOf(id) > -1)
                has = this._exp[id] === null ? true : (this._exp[id] as number) > Date.now();

            if (!has)
                this.delete(id);
        }

        return has;
    }


    /**
     * Supprime un élément
     * @param id Identifiant de l'élément à supprimer
     */
    delete (id: string): void {

        const fct = (elmt: string): void => {
            delete this._vars[elmt];
            delete this._exp[elmt];
        };

        if (/.+\.\*$/.test(id)) {

            const root = id.substr(0, id.length - 1);
            Utils.forEach(this._vars, (value, id) => {

                if (id.substr(0, root.length) == root)
                    (fct as Function)(id);
            });
        }
        else
            (fct as Function)(id);
    }


    /**
     * Remet à zéro toute la configuration
     */
    reset (): void {

        delete this._vars;
        this._vars = {};
        delete this._exp;
        this._exp = {};
    }


    /**
     * Voir `core/Services/Config.get()`
     */
    _get (id: string, dflt: any = null): any {

        if (/.+\.\*$/.test(id)) {

            const root = id.substr(0, id.length - 1);
            let results: {[keys: string]: any} = {};

            Utils.forEach(this._vars, (value, id) => {

                if (id.substr(0, root.length) == root)
                    results[id.substr(root.length)] = this.get(id);
            });

            if (Utils.isObject(dflt))
                results = Utils.extend({}, dflt, results);

            return results;
        }
        else
            return this.has(id) ? this._vars[id] : (Utils.isFunction(dflt) ? dflt() : dflt);
    }
}

