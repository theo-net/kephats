/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as Utils from '../Utils';
import { Kernel } from '../Kernel';

/* eslint-disable @typescript-eslint/no-explicit-any */

export interface WatchFn {
    (p?: any): any;
    inputs?: any;
    _watchDelegate?: Function;
}

/**
 * event {
 *   name: nom de l'évènement,
 *   targetScope: scope qui lance l'évènement,
 *   curentScope: le scope dont on parcours les listeners (vaut null à la fin)
 *   preventDefault(): defini une propriété defaultPrevented à true
 * }
 */


/**
 * Permet de s'assurer que les listener seront appelés au moins lors de la
 * première boucle de digest.
 */
function initWatchVal (): void { return; }


/**
 * retourne un boolean indique si `obj` est comme un `Array`
 * @param {*} obj élément à tester
 * @returns {Boolean}
 */
function isArrayLike (obj: any): boolean {

    if (obj === null || obj === undefined)
        return false;

    return obj.length === 0 ||
        (Utils.isNumber(obj.length) &&
            obj.length > 0 && (obj.length - 1) in obj);
}


/**
 * L'objet Scope auquel nous attachons des propriétés qui seront utilisées dans nos application.
 *
 * Il n'y a aucune restriction à ce que l'on peut attacher comme propriétés à l'objet.
 *
 *     const scope = new Scope() ;
 *     scope.prop1 = 'a string' ;
 *     scope.prop2 = 123 ;
 *     scope.prop3 = {
 *         an: 'other object'
 *     } ;
 *     scope.prop3 = function () {} ;
 *
 * C'est cet objet qui gère également le dirty-checking et le digest cycle.
 *
 * Le Scope intègre également un gestionnaire d'évènement. Il existe deux modes de propagation
 * pour les évènements : `emitting` et `broadcasting`
 *
 *  - `emitting`: les souscriveurs du scope courant et ses ancêtres sont notifiés
 *  - `broadcasting`: ceux du scope courant et ses enfants.
 */
export class Scope {

    [k: string]: any;

    // pour Time To Live
    protected _TTL = 10;
    // liste de tous les watchers attachés au scope
    protected _watchers: null | any[] = [];
    // sauve le dernier watchers où la valeur observée a changé
    protected _lastDirtyWatch = null;
    // Liste des $evalAsync jobs
    protected _asyncQueue: any[] = [];
    // Liste des $applyAsync jobs
    protected _applyAsyncQueue: Function[] = [];
    // Trace d'un applyAsync déjà lancé
    protected _applyAsyncId: number | null = null;
    // Liste des postDigest jobs
    protected _postDigestQueue: Function[] = [];
    // La racine
    protected $root = this;
    // Liste des enfants
    protected _children: Scope[] = [];
    // Liste des listeners d'event
    protected _listeners: Record<string, (Function | null)[]> = {};
    // Phase courante (null, $digest ou $apply)
    protected _phase: string | null = null;
    // Le parser
    protected _parse: Function;
    // Le kernel
    protected _kernel: Kernel;

    constructor (kernel: Kernel) {

        // Le Kernel
        this._kernel = kernel;
        // Le parser
        this._parse = kernel.get('$parser');
    }


    /**
     * Retourne la phase courante
     */
    $getPhase (): string | null {

        return this._phase;
    }


    /**
     * Retourne tous les watchers
     */
    $getWatchers (): any[] | null {

        return this._watchers;
    }


    /**
     * Retourne les enfants
     */
    $getChildren (): Scope[] {

        return this._children;
    }


    /**
     * Retourne les listeners
     */
    $getListeners (): Record<string, (Function | null)[]> {

        return this._listeners;
    }


    /**
     * Fixe à `value` le TTL. Ainsi, si `value` n'est pas un nombre ou s'il n'est pas passé en
     * paramètre, on aura la valeur courante de TTL. Par défaut, TTL vaut 10.
     * @param value Valeur du TTL
     * @returns Valeur courante du TTL
     */
    $digestTtl (value: number): number {

        if (Utils.isNumber(value))
            this._TTL = value;
        return this._TTL;
    }


    /**
     * Permet d'attacher les watchers aux scopes. Les watchers sont des fonctions qui sont notifiées
     * lorque quelque chose change dans le scope.
     *
     * L'ajout d'un watcher au cours d'une boucle de digest force à reparcourir tous les watchers
     * une nouvelle fois (réinitialisation de `_lastDirtyWatch`).
     *
     * La fonction retourne une fonction, qui lorsqu'elle est appellée, détruit le
     * watcher.
     *
     * @param watchFn Fonction surveillant le scope
     *
     * `watchFn (scope)` : `{Scope}` `scope` scope courant
     * @param listenerFn Fonction appelée lorsque le scope est modifié si on ne passe pas ce
     *          paramètre, alors on aura juste watchFn qui sera appelé à chaque boucle de digest
     *          quelque soit la valeur retournée. Cela permet d'avoir une fonction qui est notifiée
     *          à chaque fois que le scope est parcouru. Pour une meilleur optimisation, il est
     *          mieux de ne rien retourner (la valeur du watch sera ainsi toujours `undefined`)
     *
     * `listenerFn (newValue, oldValue, scope)` :
     *
     *   - `{any}` `newValue` nouvelle valeur
     *   - `{any}` `oldValue` ancienne valeur, égale à newValue lors de la première boucle
     *   - `{Scope}` `scope` scope courant
     * @param valueEq compare les valeurs (value-based dirty-checking) pour les tableaux ou les
     *          objets par exemple (`false` par défaut).
     * @returns Fonction détruisant le watcher lors de son appel.
     */
    $watch (watchFn: Function | string, listenerFn?: Function, valueEq = false): Function {

        const watchFnParsed = this._parse(watchFn);

        // Le parseur peut attacher un watchDelegate
        if (watchFnParsed._watchDelegate)
            return watchFnParsed._watchDelegate(this, listenerFn, valueEq, watchFnParsed);

        const watcher = {
            watchFn: watchFnParsed,
            listenerFn: listenerFn || function (): void { return; },
            valueEq: !!valueEq, // false par défaut
            last: initWatchVal
            // Ainsi, même si watchDn retourne undefined la première fois, le listener sera appelé
        };
        (this._watchers as any[]).unshift(watcher); // Ajoute le watcher au début de la liste
        this.$root._lastDirtyWatch = null;

        // Fonction de destruction
        return (): void => {

            const index = (this._watchers as any[]).indexOf(watcher);
            // Si index vaut -1 c'est que le watcher à déjà été supprimé
            if (index >= 0) {
                (this._watchers as any[]).splice(index, 1);
                // On supprime l'optimisation pour être sûr que tous les watchs
                // s'exécutent, même si celui a été supprimé par l'un d'entre eux.
                this.$root._lastDirtyWatch = null;
            }
        };
    }


    /**
     * Exécute `expr` en utilisant `$eval` et lance le cycle de digest en appellant `$digest`. En
     * utilisant cette fonction, on est sûr que si `expr` modifie le scope, `$digest` sera appelé et
     * donc les changements répercutés.
     *
     * Le digest lancé s'applique à toute la hiérachie du scope (parents inclus).
     * @param expr Expression a exécuter
     */
    $apply (expr: any): Function {

        try {
            this.$beginPhase('$apply');
            return this.$eval(expr);
        } finally {         // Quoiqu'il arrive, $digest est appellée
            this.$clearPhase();
            this.$root.$digest();
        }
    }


    /**
     * Termine une phase
     */
    $clearPhase (): void {

        this._phase = null;
    }


    /**
     * Lance des boucles de digest tant qu'une valeur est modifiée. Mais une expection est envoyée
     * s'il y a plus de 10 itérations (cf valeur TTL).
     */
    $digest (): void {

        let ttl = this._TTL;
        let dirty;
        this.$root._lastDirtyWatch = null;

        this.$beginPhase('$digest');

        // S'il y a des applyAsync en attente, on les lances de suite
        if (this.$root._applyAsyncId) {

            clearTimeout(this.$root._applyAsyncId);
            this._flushApplyAsync();
        }

        // Les différentes itérations de digestOnce
        do {

            // Exécution des $evalAsync
            while (this._asyncQueue.length) {
                try {
                    const asyncTask = this._asyncQueue.shift();
                    (asyncTask as any).scope.$eval((asyncTask as any).expression);
                } catch (e) {
                    console.error(e);
                }
            }

            // Dirty checking
            dirty = this._digestOnce();

            // Pas plus de TTL itérations
            if ((dirty || this._asyncQueue.length) && !(ttl--))
                throw this._TTL + ' digest iterations reached';

            // Toutes les async doivent être exécutées.
        } while (dirty || this._asyncQueue.length);

        this.$clearPhase();

        // postDigest
        while (this._postDigestQueue.length) {
            try {
                (this._postDigestQueue as any[]).shift()();
            } catch (e) {
                console.error(e);
            }
        }
    }


    /**
     * Définit la phase courante, envoit une exception si une phase est en cours
     * @param hase Nom de la phase
     */
    $beginPhase (phase: string): void {

        if (this.__phase)
            throw this._phase + ' already in progress.';

        this._phase = phase;
    }


    /**
     * Exécute la fonction `expr` en fournissant le scope comme argument, si `locals` est défini,
     * il sera passé comme argument.
     * @param expr Expression a évaluer
     * @param locals Valeurs locales
     */
    $eval (expr: any, locals?: any): Function {

        return this._parse(expr)(this, locals);
    }


    /**
     * Gère l'héritage : retourne un nouveau scope enfant
     *
     *  - si on cherche à accéder à une propriété de l'enfant qui n'existe pas, alors ce sera celle
     *    de son parent qui sera retournée.
     *  - un parent ne reçoit pas les propriétés de ses enfants
     *  - un enfant héritera des propriétés ajoutés, après sa création, à son parent
     *  - si l'on modifie une propriété partagée, tous ceux qui la partage voient la modification
     *  - lorsqu'on attribut une valeur à une propriété d'un enfant, celle-ci masque celle du parent.
     *
     *                 a
     *               /   \
     *              aa     ab
     *            /    \   |
     *          aaa   aab  abb
     *
     *          a.e = [1, 2, 3]   // aa.e === [1, 2, 3]
     *          aa.f = 3          // a.f === undefined && ab.f === undefined
     *          aaa.e.push(4)     // a.e === [1, 2, 3, 4]
     *          a.g = 'z'
     *          aa.g = 'y'        // a.g === 'z' && aa.g = 'y'
     *          a.h = {i: 1}
     *          aa.h.i = 2        // a.h.i === 2 && aa.h.i === 2
     *
     * On remarque qu'il est interressant de travailler avec des objets, ainsi on est sûr de continuer
     * de partager les données avec le parent.
     *
     * Lorsque l'on lance un digest, seul les watchs du scope courant et de ses enfants sont écoutés,
     * jamais ceux des parents.
     *
     * Si `isolated` vaut `true`, alors le nouveau scope n'accèdera pas aux propriétes de son parent.
     * Cependant, un parent lance un digest, il sera aussi appliqué aux enfants isolés. `$apply()`,
     * `$evalAsync()` et `$applyAsync()` remontent jusqu'à la racine.
     *
     * Si parent est précisé : le scope à partir duquel on appelle `$new()` fournira l'héritage des
     * propriété, mais la racine sera `parent`.
     *
     * @param isolated Isolé ou non
     * @param parent Racine pour le nouveau scope
     */
    $new (isolated?: boolean, parent?: Scope): Scope {

        let child;
        parent = parent || this;

        // L'enfant est isolé
        if (isolated) {
            child = new Scope(this._kernel);
            child.$root = parent.$root;
            child._asyncQueue = parent._asyncQueue;
            child._postDigestQueue = parent._postDigestQueue;
            child._applyAsyncQueue = parent._applyAsyncQueue;
        }
        // Sinon
        else
            child = Object.create(this);

        // On attache l'enfant au parent
        parent._children.push(child);

        // Chaque scope à sa propre pile de watchers
        child._watchers = [];
        // sa propre liste d'écouteurs d'évènements
        child._listeners = {};
        // et sa propre liste d'enfant
        child._children = [];
        // Le scope possède une référence à son parent direct
        child.$parent = parent;

        // On retourne le nouveau scope
        return child;
    }


    /**
     * Enregistre les watchers `watchFns` ayant tous le même listener : `listenerFn`
     * Cela permet d'enregistrer une même action pour un groupe de variables différentes à sauvegarder.
     * Le listener ne sera exécuté qu'une seule fois par digest.
     * La première fois, `oldValues` est le même tableau que `newValues`.
     * Si `watchFns` est vide, le listener sera quand même appelé une fois.
     * Enfin, la méthode retourne une fonction qui permet de désenregistrer le groupe de watcher, de
     * la même manière que `$watch()`.
     *
     * @param watchFns Tableau de watchers
     * @param listenerFn Listener
     */
    $watchGroup (watchFns: WatchFn[], listenerFn: Function): Function {

        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const self = this;
        const newValues = new Array(watchFns.length),
              oldValues = new Array(watchFns.length);
        let changeReactionScheduled = false;
        let firstRun = true;

        // Le listener est au moins exécuté une fois, sauf si entre temps le group
        // a été détruit
        if (watchFns.length === 0) {

            let shouldCall = true;
            this.$evalAsync(() => {
                if (shouldCall)
                    listenerFn(newValues, newValues, this);
            });
            // On retourne une fonction de destruction
            return function (): void { shouldCall = false; };
        }

        // Appel le listener
        function watchGroupListener (): void {

            if (firstRun) {
                firstRun = false;
                listenerFn(newValues, newValues, self);
            } else
                listenerFn(newValues, oldValues, self);
            changeReactionScheduled = false;
        }

        // Sauve les fonctions de désenregistration et enregistre les watchers
        const destroyFunctions = watchFns.map((watchFn, i) => {

            return this.$watch(watchFn, (newValue: any, oldValue: any) => {

                newValues[i] = newValue;
                oldValues[i] = oldValue;

                if (!changeReactionScheduled) {

                    changeReactionScheduled = true;
                    this.$evalAsync(watchGroupListener);
                }
            });
        });

        // Retourne la fonction de destruction
        return function (): void {
            destroyFunctions.forEach(function (destroyFunction) {
                destroyFunction();
            });
        };
    }


    /**
     * Enregistre `listener` pour qu'il écoute `eventName`.
     * @param eventName Nom de l'evènement
     * @param listener Listener
     */
    $on (eventName: string, listener: Function): Function {

        let listeners = this._listeners[eventName];

        // Initialiste la liste des listeners si eventName n'est pas encore
        // enregistré
        if (!listeners)
            this._listeners[eventName] = listeners = [];

        listeners.push(listener);

        // Renvoit une fonction de destruction
        return function (): void {
            const index = listeners.indexOf(listener);
            if (index >= 0)
                listeners[index] = null;
        };
    }


    /**
     * Détruit le scope. Les watchers sont supprimés et les listeners d'event également. C'est le
     * garbage collector du moteur JS qui effacera le scope s'il n'y a plus aucune référence à lui.
     *
     * Les hiérarchies étant rompues, les enfants seront également détruits.
     *
     * Un évènement `'$destroy'` est déclenché et propagé vers ses enfants.
     */
    $destroy (): void {

        this.$broadcast('$destroy');

        // On le désinscrit de son parent
        if (this.$parent) {

            const siblings = this.$parent._children;
            const indexOfThis = siblings.indexOf(this);
            if (indexOfThis >= 0)
                siblings.splice(indexOfThis, 1);
        }

        // On supprime les watchers et les listeners
        this._watchers = null;
        this._listeners = {};
    }


    /**
     * L'objet `event` transmis aux listeners contient en plus une méthode `stopPropagation()` qui
     * arrête la propagation vers les parents. Les autres listeners du scope courant seront biens
     * appelés.
     * @param eventName Nom de l'évenement
     * @returns L'évenement
     */
    $emit (eventName: string): Record<string, any> {

        let propagationStopped = false;
        // Objet passé au listener
        const event: Record<string, any> = {
            name: eventName,
            targetScope: this,
            stopPropagation: function (): void {
                propagationStopped = true;
            },
            preventDefault: function (): void {
                event.defaultPrevented = true;
            }
        };
        // eslint-disable-next-line prefer-rest-params
        const listenerArgs = [event].concat(Array.prototype.slice.call(arguments, 1));

        // eslint-disable-next-line @typescript-eslint/no-this-alias
        let scope = this;

        // On remonte la hiérachie
        do {
            event.currentScope = scope;
            scope._fireEventOnScope(eventName, listenerArgs);
            scope = scope.$parent;
        } while (scope && !propagationStopped);

        event.currentScope = null;
        return event;
    }


    /**
     * Broadcast un évènement
     * @param eventName Nom de l'évenement
     * @returns L'évenement
     */
    $broadcast (eventName: string): Record<string, any> {

        // Objet passé au listener
        const event: Record<string, any> = {
            name: eventName,
            targetScope: this,
            preventDefault: function (): void {
                event.defaultPrevented = true;
            }
        };
        // eslint-disable-next-line prefer-rest-params
        const listenerArgs = [event].concat(Array.prototype.slice.call(arguments, 1));

        // Parcours tous les enfants
        this._everyScope(function (scope: Scope): true {
            event.currentScope = scope;
            scope._fireEventOnScope(eventName, listenerArgs);
            return true;
        });

        event.currentScope = null;
        return event;
    }


    /**
     * Exécute `expr`, à l'aide d'`$eval()`, plus tard dans le même cycle de digest.
     *
     * Si aucune boucle de digest n'est en cours, on en lance une.
     *
     * Le digest porte sur toute la hiérachie (parents inclus).
     * @param expr Expression à évaluer
     */
    $evalAsync (expr: any): void {

        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const self = this;

        if (!self._phase && !self._asyncQueue.length) {
            setTimeout(function () {
                if (self._asyncQueue.length)
                    self.$root.$digest();
            }, 0);
        }

        this._asyncQueue.push({ scope: self, expression: expr });
    }


    /**
     * comme `$apply()`, mais permet de regrouper plusieurs appels et ainsi lancer un `$digest()` qu'une
     * seule fois, plutôt qu'après chaque `$apply()`. Mais elle n'est jamais exécutée dans le cycle
     * courant.
     * @param expr Expression à évaluer
     */
    $applyAsync (expr: any): void {

        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const self = this;
        self._applyAsyncQueue.push(function (): void {
            self.$eval(expr);
        });

        // Pour s'assurer qu'on ne lance pas successivement plusieurs boucles de
        // digest si l'on appelle plusieurs fois de suite la fonction.
        if (self.$root._applyAsyncId === null) {
            self.$root._applyAsyncId = setTimeout(function () {
                self.$apply(self._flushApplyAsync.bind(self));
            }, 0) as unknown as number;
        }
    }


    /**
     * Alors que `$watch()` observe les valeurs (référence ou alors les valeurs en détail de chaque
     * attributs et sous attributs de l'objet observé, `$watchCollection()` est optimisé pour observer
     * une collection et indiquer ainsi si un nouvel élément est ajouté, un autre supprimé, ou
     * modifié.
     *
     * Si on n'observe pas un tableau ou un objet, la fonction fonctionne de la même manière que
     * `$watch()`.
     *
     * @param watchFn watcher
     * @param istenerFn listener
     * @returns fonction de destruction.
     */
    $watchCollection (watchFn: Function | string, listenerFn: Function): Function {

        let newValue: any;
        let oldValue: any;
        let oldLength: number;
        let veryOldValue: any;
        const trackVeryOldValue = (listenerFn.length > 1); // si listenerFn requiert
        let changeCount = 0;                             // oldValue
        let firstRun = true;

        const watchFnParsed = this._parse(watchFn);

        // Watcher
        const internalWatchFn = (scope: Scope): number => {

            let newLength: number;
            newValue = watchFnParsed(scope);

            // C'est un objet, ainsi String ne sera pas traité (il aura été considéré
            // comme un Arraay car il possède un attribut length
            if (Utils.isObject(newValue)) {

                // C'est un tableau ou similaire
                if (isArrayLike(newValue)) {

                    // La valeur devient un tableau
                    if (!Array.isArray(oldValue)) {
                        changeCount++;
                        oldValue = [];
                    }

                    // Item(s) ajouté(s) ou supprimé(s)
                    if (newValue.length !== oldValue.length) {
                        changeCount++;
                        oldValue.length = newValue.length;
                    }

                    // Item(s) remplacé(s) ou réordonné(s)
                    // On utilise le forEach du Utils, car newValue peut être comme un
                    // Array, mais pas de ce type, donc sans la méthode .forEach
                    Utils.forEach(newValue, function (newItem, i) {
                        const bothNaN = Number.isNaN(newItem) && Number.isNaN(oldValue[i]);
                        if (!bothNaN && newItem !== oldValue[i]) {
                            changeCount++;
                            oldValue[i] = newItem;
                        }
                    });

                }

                // Il s'agit d'un objet classique
                else {

                    // La valeur observée est devenue un object
                    if (!Utils.isObject(oldValue) || isArrayLike(oldValue)) {
                        changeCount++;
                        oldValue = {};
                        oldLength = 0;
                    }

                    // On regarde si Une propriété a changée ou a été ajoutée. On
                    // s'arrange pour que NaN == NaN
                    newLength = 0;
                    Utils.forOwn(newValue, function (newVal, key) {

                        newLength++;

                        // La propriété existait déjà
                        if (Object.prototype.hasOwnProperty.call(oldValue, key)) {
                            const bothNaN = Number.isNaN(newVal) &&
                                Number.isNaN(oldValue[key]);
                            if (!bothNaN && oldValue[key] !== newVal) {
                                changeCount++;
                                oldValue[key] = newVal;
                            }
                        }
                        // La propriété a été ajoutée
                        else {
                            changeCount++;
                            oldLength++;
                            oldValue[key] = newVal;
                        }
                    });

                    // On regarde si un attribut a été supprimé
                    if (oldLength > newLength) {
                        changeCount++;
                        Utils.forOwn(oldValue, function (oldVal, key) {
                            if (!Object.prototype.hasOwnProperty.call(newValue, key)) {
                                oldLength--;
                                delete oldValue[key];
                            }
                        });
                    }
                }
            } else {

                // Si on observe pas une collection
                if (!this._areEqual(newValue, oldValue, false))
                    changeCount++;

                oldValue = newValue;
            }

            // On retourne le compteur, ainsi digest sait s'il y a eu des modifications
            return changeCount;
        };

        // Listener
        const internalListenerFn = (): void => {

            if (firstRun) {
                listenerFn(newValue, newValue, this);
                firstRun = false;
            } else
                listenerFn(newValue, veryOldValue, this);

            if (trackVeryOldValue)
                veryOldValue = Utils.clone(newValue);
        };

        // Enregistre le watcher et listener, et renvoit la fonction de destruction
        return this.$watch(internalWatchFn, internalListenerFn);
    }


    /**
     * Parcours tous les watchers attachés au scope et lance les listeners attachés si le scope a
     * été modifié. C'est cette fonction qui permet le dirty-checking.
     */
    private _digestOnce (): boolean | undefined {

        let dirty: boolean | undefined;

        // Lance le digest sur le scope et tous ses enfants
        this._everyScope(function (scope: Scope) {

            let newValue, oldValue;

            // every comme ForEach, mais s'arrête dès que le callback return false
            Utils.forEachRight(scope._watchers as any[], function (watcher: any) {

                try {
                    newValue = watcher.watchFn(scope);
                    oldValue = watcher.last;

                    // Appelle le listener si la valeur à changée
                    if (!scope._areEqual(newValue, oldValue, watcher.valueEq)) {

                        // valeur modifiée, on sauve le watcher
                        scope.$root._lastDirtyWatch = watcher;
                        watcher.last =
                            (watcher.valueEq ? Utils.cloneDeep(newValue) : newValue);
                        watcher.listenerFn(newValue,
                            (oldValue === initWatchVal ? newValue : oldValue),
                            scope);
                        dirty = true;
                    }
                    // On s'arrête s'il n'y a pas eu de changement depuis le dernier tour
                    else if (scope.$root._lastDirtyWatch === watcher) {
                        dirty = false;
                        return false;
                    }
                } catch (e) {
                    console.error(e);
                }
            });

            return dirty !== false;
        });

        return dirty;
    }


    /**
     * Exécute `fn` pour le scope courrant et pour chacun de ses enfants, récursivement.
     * `fn` doit retourner un boolean : `true` pour continuer de parcourir les enfants,
     * `false` pour arrêter.
     * @param fn Fonction à exécuter
     */
    private _everyScope (fn: Function): boolean {

        // Exécute fn et si cette dernière renvoit true, l'exécute sur les enfants
        if (fn(this)) {
            return this._children.every(function (child) {
                return child._everyScope(fn);
            });
        }
        // Sinon, arrête une boucle lancé par un _everyScope parent
        else
            return false;
    }


    /**
     * Détermine si `newValue` et `oldValue` sont égales ou non.
     * @param newValue Nouvelle valeur
     * @param oldValue Ancienne valeur
     * @param valueEq Comparaison pour les tableaux et objets (deep)
     */
    private _areEqual (newValue: any, oldValue: any, valueEq: boolean): boolean {

        if (valueEq)
            return Utils.isEqual(newValue, oldValue);
        else {
            return newValue === oldValue ||
                (typeof newValue === 'number' && typeof oldValue === 'number' &&
                    isNaN(newValue) && isNaN(oldValue));
        }
    }


    /**
     * Lance l'évènement `eventName` sur le scope courant
     * @param eventName Nom de l'évènement
     * @param listenerArgs Arguments du listener
     */
    private _fireEventOnScope (eventName: string, listenerArgs: any[]): void {

        // On appelle les listeners du scope courant
        const listeners = this._listeners[eventName] || [];
        let i = 0;
        while (i < listeners.length) {
            // Le listener a été supprimé
            if (listeners[i] === null)
                listeners.splice(i, 1);
            else {
                try {
                    // eslint-disable-next-line prefer-spread
                    (listeners[i] as Function).apply(null, listenerArgs);
                } catch (e) {
                    console.error(e);
                }
                i++;
            }
        }
    }


    /**
     * Exécute toutes les `applyAsync` enregistrées.
     */
    private _flushApplyAsync (): void {

        while (this._applyAsyncQueue.length) {
            try {
                (this._applyAsyncQueue.shift() as Function)();
            } catch (e) {
                console.error(e);
            }
        }
        this.$root._applyAsyncId = null;
    }


    /**
     * Enregistre une fonction `fn` à exécuter après la dernière boucle de digest. Elle ne sera
     * exécutée qu'une seule fois.
     * @param fn Fonction à enregistrer
     */
    _postDigest (fn: Function): void {

        this._postDigestQueue.push(fn);
    }
}

