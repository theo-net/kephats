/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Filter } from './Filter';
import { filterFilter } from './filterFilter';

describe('Filter', () => {

    let filter: Filter;
    const f1 = (): void => { return; },
          f2 = (): void => { return; };

    beforeEach(() => {

        filter = new Filter();
        filter.register('f1', f1);
    });

    it('le filtre `filter` est défini', () => {

        expect(filter.get('filter')).to.be.equal(filterFilter);
    });

    it('indique si un filtre est disponible', () => {

        expect(filter.has('f1')).to.be.equal(true);
        expect(filter.has('f2')).to.be.equal(false);
    });

    it('enregistre un filtre', () => {

        filter.register('test', f2);
        expect(filter.has('test')).to.be.true;
    });

    it('récupère un filtre', () => {

        filter.register('test', f2);
        expect(filter.get('test')).to.be.equal(f2);
    });
});
