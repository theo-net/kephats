/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as sinon from 'sinon';

import { Logs } from './Logs';

describe('core/Services/Logs', () => {

    let logs = new Logs();
    let consoleLog: sinon.SinonStub, consoleError: sinon.SinonStub;

    before(() => {

        consoleLog = sinon.stub(console, 'log');
        consoleError = sinon.stub(console, 'error');
    });

    after(() => {

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (console.log as any).restore();
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (console.error as any).restore();
    });

    beforeEach(() => {

        logs = new Logs();
    });

    it('_getLogLevel retourne le bon log level', () => {

        expect(logs.getLogLevel()).to.be.equal('normal');

        logs.kernel().get('$config').set('logLevel', 'warn', true);
        expect(logs.getLogLevel()).to.be.equal('warn');
    });


    it('log normal', () => {

        logs.kernel().get('$config').set('logLevel', 'normal', true);

        logs.log('msg info', 'info');
        expect(consoleLog.calledWith('Info: msg info')).to.be.true;

        logs.log('msg success', 'success');
        expect(consoleLog.calledWith('msg success')).to.be.true;

        logs.log('msg warning', 'warning');
        expect(consoleLog.calledWith('Warning: msg warning')).to.be.false;

        logs.log('msg error', 'error');
        expect(consoleError.calledWith('msg error')).to.be.true;

        logs.log('msg debug', 'debug');
        expect(consoleLog.calledWith('msg debug')).to.be.false;

        logs.log('msg data', 'data');
        expect(consoleLog.calledWith('msg data')).to.be.false;
    });

    it('log warn', () => {

        logs.kernel().get('$config').set('logLevel', 'warn', true);

        logs.log('msg info', 'info');
        expect(consoleLog.calledWith('Info: msg info')).to.be.true;

        logs.log('msg success', 'success');
        expect(consoleLog.calledWith('msg success')).to.be.true;

        logs.log('msg warning', 'warning');
        expect(consoleLog.calledWith('Warning: msg warning')).to.be.true;

        logs.log('msg error', 'error');
        expect(consoleError.calledWith('msg error')).to.be.true;

        logs.log('msg debug', 'debug');
        expect(consoleLog.calledWith('msg debug')).to.be.false;

        logs.log('msg data', 'data');
        expect(consoleLog.calledWith('msg data')).to.be.false;
    });

    it('log debug', () => {

        logs.kernel().get('$config').set('logLevel', 'debug', true);

        logs.log('msg info', 'info');
        expect(consoleLog.calledWith('Info: msg info')).to.be.true;

        logs.log('msg success', 'success');
        expect(consoleLog.calledWith('msg success')).to.be.true;

        logs.log('msg warning', 'warning');
        expect(consoleLog.calledWith('Warning: msg warning')).to.be.true;

        logs.log('msg error', 'error');
        expect(consoleError.calledWith('msg error')).to.be.true;

        logs.log('msg debug', 'debug');
        expect(consoleLog.calledWith('msg debug')).to.be.true;

        logs.log('msg data', 'data');
        expect(consoleLog.calledWith('msg data')).to.be.true;
    });

    it('on peut personnaliser l\'affichage des message', () => {

        logs.kernel().get('$config').set('logLevel', 'debug', true);
        logs.setTransformMessage('info', (text: string) => text + ' info');
        logs.setTransformMessage('success', (text: string) => text + ' success');
        logs.setTransformMessage('error', (text: string) => text + ' error');
        logs.setTransformMessage('warning', (text: string) => text + ' warning');
        logs.setTransformMessage('debug', (text: string) => text + ' debug');
        logs.setTransformMessage('data', (text: string) => text + ' data');

        logs.log('msg info', 'info');
        expect(consoleLog.calledWith('Info: msg info info')).to.be.true;

        logs.log('msg success', 'success');
        expect(consoleLog.calledWith('msg success success')).to.be.true;

        logs.log('msg warning', 'warning');
        expect(consoleLog.calledWith('Warning: msg warning warning')).to.be.true;

        logs.log('msg error', 'error');
        expect(consoleError.calledWith('msg error error')).to.be.true;

        logs.log('msg debug', 'debug');
        expect(consoleLog.calledWith('msg debug debug')).to.be.true;

        logs.log('msg data', 'data');
        expect(consoleLog.calledWith('msg data data')).to.be.true;
    });
});
