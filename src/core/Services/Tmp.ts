/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Config } from './Config';

/**
 * Service permettant de stocker des données temporaires (elles seront effacées si l'application
 * redémarre.
 *
 * Il comporte les mêmes méthodes que le service `$config`
 */
export class Tmp extends Config {
}
