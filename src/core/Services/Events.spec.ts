/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as sinon from 'sinon';

import { Event } from './Event';
import { Events } from './Events';

describe('core/Services/Events', () => {

    let events: Events;

    beforeEach(() => {

        events = new Events();
    });


    it('register() enregistre un nouveau canal d\'évènement', () => {

        events.register('foobar');

        expect(events.get('foobar')).to.be.instanceof(Event);
        expect((events.get('foobar') as Event).getId()).to.be.equal('foobar');
    });

    it('attach() attache un observer à un canal', () => {

        events.register('foobar');
        const observer = (): void => { return; };

        events.attach('foobar', 'lol', observer);
        expect((events.get('foobar') as Event).get('lol')).to.be.equal(observer);
    });

    it('notify notifie tous les observers d\'un canal et transmet les données', () => {

        events.register('foobar');
        events.register('lol');
        const spy1 = sinon.spy(),
              spy2 = sinon.spy(),
              spy3 = sinon.spy(),
              data = { a: 1, b: 2 };

        events.attach('foobar', 'spy1', spy1);
        events.attach('foobar', 'spy2', spy2);
        events.attach('lol', 'spy3', spy3);

        events.notify('foobar', data);

        expect(spy1.calledOnce).to.be.true;
        expect(spy1.calledWith(data)).to.be.true;
        expect(spy2.calledOnce).to.be.true;
        expect(spy2.calledWith(data)).to.be.true;
        expect(spy3.calledOnce).to.be.false;
    });

    it('has indique si un canal est enregistré', () => {

        events.register('foobar');

        expect(events.has('foobar')).to.be.equal(true);
        expect(events.has('test')).to.be.equal(false);
    });
});
