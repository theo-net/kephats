/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Cache } from './Cache';

describe('core/Services/Cache', () => {

    let cache: Cache;

    beforeEach(() => {

        cache = new Cache();
    });

    it('par défaut, cache activé', () => {

        expect(cache.isEnabled()).to.be.true;
    });

    it('active ou désactive le cache', () => {

        cache.enable(false);
        expect(cache.isEnabled()).to.be.false;
        cache.enable(true);
        expect(cache.isEnabled()).to.be.true;
    });

    describe('set', () => {

        it('une date d\'expiration peut être définie', () => {

            cache.set('foo', 'bar', 100);
            const exp = Date.now() + 100 * 1000;
            expect(cache.get('foo')).to.be.equal('bar');
            expect(cache.getExp('foo')).to.be.equal(exp);
        });

        it('par défaut, pas d\'expiration', () => {

            cache.set('foo', 'bar');
            expect(cache.get('foo')).to.be.equal('bar');
            expect(cache.getExp('foo')).to.be.equal(null);
        });

        it('écrasera toujours une donnée déjà en cache', () => {

            cache.set('foo', 'bar', 10);
            cache.set('foo', 'bar2', 20);
            const exp = Date.now() + 20 * 1000;
            expect(cache.get('foo')).to.be.equal('bar2');
            expect(cache.getExp('foo')).to.be.equal(exp);
        });
    });


    describe('has', () => {

        it('renverra false et supprimera l\'élément s\'il est expiré', () => {

            cache.set('foo', 'bar', 10);
            cache.set('foo2', 'bar2', -10);
            cache.set('foo3', 'bar3');
            expect(cache.has('foo')).to.be.true;
            expect(cache.has('foo2')).to.be.false;
            expect(cache.has('foo3')).to.be.true;
            expect(cache.get('foo2')).to.be.null;
            expect(cache.getExp('foo2')).to.be.undefined;
        });

        it('toujours `false` si cache désactivé', () => {

            cache.set('foo', 'bar');
            expect(cache.has('foo')).to.be.true;
            cache.enable(false);
            expect(cache.has('foo')).to.be.false;
        });
    });


    describe('get', () => {

        it('met en cache la valeur par défaut', () => {

            cache.set('foo', 'bar');
            expect(cache.has('bar')).to.be.false;
            expect(cache.get('bar', 'foo')).to.be.equal('foo');
            expect(cache.has('bar')).to.be.true;
            expect(cache.get('bar', 'truc')).to.be.equal('foo');
            expect(cache.get('truc')).to.be.equal(null);
            expect(cache.has('truc')).to.be.false;
        });

        it('date d\'expiration définie pour la valeur par défaut', () => {

            cache.set('foo', 'bar');
            cache.get('bar', 'foo');
            expect(cache.getExp('bar')).to.be.equal(null);
            cache.get('truc', 'muche', 10);
            const exp = Date.now() + 10 * 1000;
            expect(cache.getExp('truc')).to.be.equal(exp);
        });

        it('fonctionne avec les séries foo.bar.*', () => {

            cache.set('foo.truc', 1);
            cache.set('foo.bar.a', 1);
            cache.set('foo.bar.b', 1);
            cache.set('foo.bar.c.d', 1);
            cache.set('foo.bar.c.e', 1);
            cache.set('foo.bar.c.f.g', 1);

            expect(cache.get('foo.bar.*')).to.be.deep.equal({
                a: 1, b: 1, 'c.d': 1, 'c.e': 1, 'c.f.g': 1
            });
        });

        it('On veille à l\'expiration avec les séries', () => {

            cache.set('foo.truc', 1);
            cache.set('foo.bar.a', 1, 10);
            cache.set('foo.bar.b', 1, -10);
            cache.set('foo.bar.c.d', 1);
            cache.set('foo.bar.c.e', 1);
            cache.set('foo.bar.c.f.g', 1);

            expect(cache.get('foo.bar.*')).to.be.deep.equal({
                a: 1, 'c.d': 1, 'c.e': 1, 'c.f.g': 1
            });
            expect(cache.get('foo.bar.b')).to.be.null;
            expect(cache.getExp('foo.bar.b')).to.be.undefined;
        });

        it('pour série merge val par déf, mais pas de cache', () => {

            cache.set('foo.truc', 1);
            cache.set('foo.bar.a', 1);
            cache.set('foo.bar.c.d', 1);
            cache.set('foo.bar.c.e', 0);
            cache.set('foo.bar.c.f.g', 1);

            expect(cache.get('foo.bar.*', { b: 1, 'c.e': 1 })).to.be.deep.equal({
                a: 1, b: 1, 'c.d': 1, 'c.e': 0, 'c.f.g': 1
            });
            expect(cache.has('foo.bar.b')).to.be.false;
        });
    });


    describe('getExp', () => {

        it('return `null` if the element don\'t exist', () => {

            expect(cache.getExp('foobar')).to.be.undefined;
        });
    });


    describe('delete', () => {

        it('nettoie aussi les dates d\'expiration', () => {

            cache.set('foo', 'bar');
            cache.delete('foo');
            expect(cache.get('foo')).to.be.null;
            expect(cache.getExp('foo')).to.be.undefined;
        });

        it('fonctionne bien avec les séries foo.bar.*', () => {

            cache.set('foo.truc', 1);
            cache.set('foo.bar.a', 1);
            cache.set('foo.bar.b', 1);
            cache.set('foo.bar.c.d', 1);
            cache.set('foo.bar.c.e', 1);
            cache.set('foo.bar.c.f.g', 1);
            cache.delete('foo.bar.*');

            expect(cache.has('foo.truc')).to.be.true;
            expect(cache.has('foo.bar.a')).to.be.false;
            expect(cache.has('foo.bar.b')).to.be.false;
            expect(cache.has('foo.bar.c.d')).to.be.false;
            expect(cache.has('foo.bar.c.e')).to.be.false;
            expect(cache.has('foo.bar.c.f.g')).to.be.false;

            expect(cache.getExp('foo.truc')).to.be.not.undefined;
            expect(cache.getExp('foo.bar.a')).to.be.undefined;
            expect(cache.getExp('foo.bar.b')).to.be.undefined;
            expect(cache.getExp('foo.bar.c.d')).to.be.undefined;
            expect(cache.getExp('foo.bar.c.e')).to.be.undefined;
            expect(cache.getExp('foo.bar.c.f.g')).to.be.undefined;
        });
    });


    describe('getAll', () => {

        it('retourne tous les éléments', () => {

            cache.set('foo', 'bar', 100);
            cache.set('bar', 'foo', 100);
            expect(cache.getAll()).to.be.deep.equal({ 'foo': 'bar', 'bar': 'foo' });
        });
    });


    describe('reset', () => {

        it('supprime tous les éléments du cache', () => {

            cache.set('foo', 'bar', 100);
            cache.set('bar', 'foo', 100);
            cache.reset();
            expect(cache.getAll()).to.be.deep.equal({});
        });
    });
});

