/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Kernel } from '../Kernel';
import * as Utils from '../Utils';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type ServiceInstance = any;

/**
 * Permet l'implémentation de l'injection de dépendances.
 *
 * On parle de « service » pour tous les objets partagés dans l'application.
 *
 *      let services = new ServiceContainer() ;
 *      services.register('name', new AService()) ;
 *
 * Si le service est une fonction, celle-ci sera appelée à chaque `get` avec le Kernel comme
 * argument.
 */
export class ServiceContainer {

    protected _kernel: Kernel | undefined;
    protected _services: { [keys: string]: ServiceInstance} = {};

    /**
     * Initialise le service container.
     * @param kernel Kernel de l'application
     */
    constructor (kernel?: Kernel) {

        this._kernel = kernel;
    }


    /**
     * Enregistre un service
     *
     * @param name Nom du service
     * @param service Service à initialiser
     */
    register (name: string, service: ServiceInstance): void {

        this._services[name] = service;
    }


    /**
     * Retourne un service. Si le service est une fonction, l'exécute avec le Kernel comme argument
     * et retourne le résultat.
     *
     * @param name Nom du service
     */
    get (name: string): ServiceInstance {

        if (Utils.isFunction(this._services[name]))
            return this._services[name](this._kernel);

        return this._services[name];
    }


    /**
     * Indique si un service est défini
     *
     * @param name Nom du service
     */
    has (name: string): boolean {

        return Object.keys(this._services).indexOf(name) > -1;
    }


    /**
     * Retourne tous les services
     */
    getAll (): { [keys: string]: ServiceInstance} {

        return this._services;
    }
}
