/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as sinon from 'sinon';

import { Kernel } from '../Kernel';
import { Scope, WatchFn } from './Scope';
import * as Utils from '../Utils';

/* eslint-disable @typescript-eslint/no-explicit-any */

describe('core/Services/Scope', () => {

    let scope: Scope;

    beforeEach(() => {

        scope = new Scope(new Kernel());
    });

    it('has a _phase field whose value is the current digest phase', () => {

        scope.aValue = [1, 2, 3];
        scope.phaseInWatchFunction = undefined;
        scope.phaseInListenerFunction = undefined;
        scope.phaseInApplyFunction = undefined;

        scope.$watch(
            function (scope: Scope) {
                scope.phaseInWatchFunction = scope.$getPhase();
                return scope.aValue;
            },
            function (newValue: any, oldValue: any, scope: Scope) {
                scope.phaseInListenerFunction = scope.$getPhase();
            }
        );

        scope.$apply(function (scope: Scope) {
            scope.phaseInApplyFunction = scope.$getPhase();
        });

        expect(scope.phaseInWatchFunction).to.be.equal('$digest');
        expect(scope.phaseInListenerFunction).to.be.equal('$digest');
        expect(scope.phaseInApplyFunction).to.be.equal('$apply');
    });


    /**
     * Digest
     */
    describe('digest', () => {

        it('calls the listener function of a watch on first $digest', () => {

            const watchFn = function (): string { return 'wat'; };
            const listenerFn = sinon.spy();
            scope.$watch(watchFn, listenerFn);

            scope.$digest();

            expect(listenerFn.called).to.be.true;
        });

        it('calls the watch function with the scope as the argument', () => {

            const watchFn = sinon.spy();
            const listenerFn = function (): void { return; };
            scope.$watch(watchFn, listenerFn);

            scope.$digest();

            expect(watchFn.calledWithExactly(scope)).to.be.true;
        });

        it('calls the listener function when the watched value changes', () => {

            scope.someValue = 'a';
            scope.counter = 0;

            scope.$watch(
                function (scope: Scope) { return scope.someValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            expect(scope.counter).to.be.equal(0);

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.someValue = 'b';
            expect(scope.counter).to.be.equal(1);

            scope.$digest();
            expect(scope.counter).to.be.equal(2);
        });

        it('calls listener when watch value is first undefined', () => {

            scope.counter = 0;

            scope.$watch(
                function (scope: Scope) { return scope.someValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);
        });

        it('calls listener with new value as old value the first time', () => {

            scope.someValue = 123;
            let oldValueGiven;

            scope.$watch(
                function (scope: Scope) { return scope.someValue; },
                function (newValue: any, oldValue: any) { oldValueGiven = oldValue; }
            );

            scope.$digest();
            expect(oldValueGiven).to.be.equal(123);
        });

        it('may have watchers that omit the listener function', () => {

            const watchFn = sinon.spy();
            scope.$watch(watchFn as WatchFn);

            scope.$digest();

            expect(watchFn.returnValues.length).to.be.equal(2);
        });

        it('triggers chained watchers in the same digest', () => {

            scope.name = 'Pierre';

            scope.$watch(
                function (scope: Scope) { return scope.nameUpper; },
                function (newValue: any, oldValue: any, scope: Scope) {
                    if (newValue)
                        scope.initial = newValue.substring(0, 1) + '.';
                }
            );

            scope.$watch(
                function (scope: Scope) { return scope.name; },
                function (newValue: any, oldValue: any, scope: Scope) {
                    if (newValue)
                        scope.nameUpper = newValue.toUpperCase();
                }
            );

            scope.$digest();
            expect(scope.initial).to.be.equal('P.');

            scope.name = 'Jean';
            scope.$digest();
            expect(scope.initial).to.be.equal('J.');
        });

        it('gives up on the watches after 10 iterations.', () => {

            scope.counterA = 0;
            scope.counterB = 0;

            scope.$watch(
                function (scope: Scope) { return scope.counterA; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counterB++; }
            );

            scope.$watch(
                function (scope: Scope) { return scope.counterB; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counterA++; }
            );

            expect((function () { scope.$digest(); })).to.throw();
        });

        it('ends the digest when the last watch is clean', () => {

            // On rempli un tableau
            scope.array = [];
            Utils.times(100, function (i) { scope.array.push(i); });
            let watchExecutions = 0;

            Utils.times(100, function (i) {
                scope.$watch(
                    function (scope: Scope) {
                        watchExecutions++;
                        return scope.array[i];
                    },
                    function () { return; }
                );
            });

            scope.$digest();
            expect(watchExecutions).to.be.equal(200);

            scope.array[0] = 33;
            scope.$digest();
            expect(watchExecutions).to.be.equal(301);
        });

        it('does not end digest so that new watches are not run', () => {

            scope.aValue = 'abc';
            scope.counter = 0;

            scope.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) {

                    scope.$watch(
                        function (scope: Scope) { return scope.aValue; },
                        function (newValue: any, oldValue: any, scope: Scope) {
                            scope.counter++;
                        }
                    );
                }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);
        });

        it('compares based on value if enabled', () => {

            scope.aValue = [1, 2, 3];
            scope.counter = 0;

            scope.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; },
                true
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.aValue.push(4);
            scope.$digest();
            expect(scope.counter).to.be.equal(2);
        });

        it('correctly handles NaNs', () => {

            scope.number = 0 / 0; // NaN
            scope.counter = 0;

            scope.$watch(
                function (scope: Scope) { return scope.number; },
                function (newValue: any, oldValue: any, scope: Scope) {
                    scope.counter++;
                }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.$digest();
            expect(scope.counter).to.be.equal(1);
        });

        it('catches exceptions in watch functions and continues', () => {

            scope.aValue = 'abc';
            scope.counter = 0;

            scope.$watch(
                function () { throw 'Test in watchFn throw error OK'; },
                function () { return; }
            );
            scope.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);
        });

        it('catches exceptions in listener functions and continues', () => {

            scope.aValue = 'abc';
            scope.counter = 0;

            scope.$watch(
                function (scope: Scope) { return scope.aValue; },
                function () {
                    throw 'Test in listenerFn throw error OK';
                }
            );
            scope.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);
        });

        it('allows destroying a $watch with a removal function', () => {

            scope.aValue = 'abc';
            scope.counter = 0;

            const destroyWatch = scope.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.aValue = 'def';
            scope.$digest();
            expect(scope.counter).to.be.equal(2);

            scope.aValue = 'ghi';
            destroyWatch();
            scope.$digest();
            expect(scope.counter).to.be.equal(2);
        });

        it('allows destroying a $watch during digest', () => {

            scope.aValue = 'abc';

            const watchCalls: string[] = [];

            scope.$watch(
                function (scope: Scope) {
                    watchCalls.push('first');
                    return scope.aValue;
                }
            );

            const destroyWatch = scope.$watch(
                function () {
                    watchCalls.push('second');
                    destroyWatch();
                }
            );

            scope.$watch(
                function (scope: Scope) {
                    watchCalls.push('third');
                    return scope.aValue;
                }
            );

            scope.$digest();
            expect(watchCalls).to.be.deep.equal(['first', 'second', 'third', 'first', 'third']);
        });

        it('allows a $watch to destroy another during digest', () => {

            scope.aValue = 'abc';
            scope.counter = 0;

            const destroyWatch = scope.$watch(
                function () { return; },
                function () { return; }
            );

            scope.$watch(
                function (scope: Scope) { return scope.aValue; },
                function () { destroyWatch(); }
            );

            scope.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.deep.equal(1);
        });

        it('allows destroying several $watches during digest', () => {

            scope.aValue = 'abc';
            scope.counter = 0;

            const destroyWatch1 = scope.$watch(
                function () {
                    destroyWatch1();
                    // eslint-disable-next-line @typescript-eslint/no-use-before-define
                    destroyWatch2();
                }
            );
            const destroyWatch2 = scope.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.deep.equal(0);
        });


        /**
         * Le scope utilise les expressions
         */

        it('accepts expressions for watch functions', () => {

            let theValue;

            scope.aValue = 29;
            scope.$watch('aValue', function (newValue: any) {
                theValue = newValue;
            });
            scope.$digest();

            expect(theValue).to.be.equal(29);
        });

        it('removes constant watches after first invocation', () => {

            scope.$watch('[1, 2, 3]', function () { return; });
            scope.$digest();
            expect((scope.$getWatchers() as any[]).length).to.be.equal(0);
        });

        it('accepts one-time watches', () => {

            let theValue;

            scope.aValue = 29;
            scope.$watch('::aValue', function (newValue: any) {
                theValue = newValue;
            });
            scope.$digest();

            expect(theValue).to.be.equal(29);
        });

        it('removes one-time watches after first invocation', () => {

            scope.aValue = 29;
            scope.$watch('::aValue', function () { return; });
            scope.$digest();

            expect((scope.$getWatchers() as any[]).length).to.be.equal(0);
        });

        it('does not remove one-time-watches until value is defined', () => {

            scope.$watch('::aValue', function () { return; });

            scope.$digest();
            expect((scope.$getWatchers() as any[]).length).to.be.equal(1);

            scope.aValue = 29;
            scope.$digest();
            expect((scope.$getWatchers() as any[]).length).to.be.equal(0);
        });

        it('does not remove one-time-watches until value stays defined', () => {

            scope.aValue = 29;

            scope.$watch('::aValue', function () { return; });
            const unwatchDeleter = scope.$watch('aValue', function () {
                delete scope.aValue;
            });

            scope.$digest();
            expect((scope.$getWatchers() as any[]).length).to.be.equal(2);

            scope.aValue = 29;
            unwatchDeleter();
            scope.$digest();
            expect((scope.$getWatchers() as any[]).length).to.be.equal(0);
        });

        it('does not remove one-time-watches before all array items defined', () => {

            scope.$watch('::[1, 2, aValue]', function () { return; }, true);

            scope.$digest();
            expect((scope.$getWatchers() as any[]).length).to.be.equal(1);

            scope.aValue = 3;
            scope.$digest();
            expect((scope.$getWatchers() as any[]).length).to.be.equal(0);
        });

        it('does not remove one-time-watches before all object vals defined', () => {

            scope.$watch('::{a: 1, b: aValue}', function () { return; }, true);

            scope.$digest();
            expect((scope.$getWatchers() as any[]).length).to.be.equal(1);

            scope.aValue = 3;
            scope.$digest();
            expect((scope.$getWatchers() as any[]).length).to.be.equal(0);
        });

        it('does not re-evaluate an array if its contents do not change', () => {

            const values: any[] = [];
            scope.a = 1;
            scope.b = 2;
            scope.c = 3;

            scope.$watch('[a, b, c]', function (value: any) { values.push(value); });

            scope.$digest();
            expect(values.length).to.be.equal(1);
            expect(values[0]).to.be.deep.equal([1, 2, 3]);

            scope.$digest();
            expect(values.length).to.be.equal(1);

            scope.c = 4;
            scope.$digest();
            expect(values.length).to.be.equal(2);
            expect(values[1]).to.be.deep.equal([1, 2, 4]);
        });


        /**
         * eval
         */
        describe('eval', () => {

            it('executes $eval\'ed function and returns result', () => {

                scope.aValue = 42;

                const result = scope.$eval(function (scope: Scope) { return scope.aValue; });

                expect(result).to.be.equal(42);
            });

            it('passes the second $eval argument straight through', () => {

                scope.aValue = 42;

                const result = scope.$eval(function (scope: Scope, arg: any) {
                    return scope.aValue + arg;
                }, 2);

                expect(result).to.be.equal(44);
            });

            it('accepts expression in $eval', () => {

                expect(scope.$eval('29')).to.be.equal(29);
            });

        });


        /**
         * $apply
         */
        describe('apply', () => {

            it('executes $apply\'ed function and starts the digest', () => {

                scope.aValue = 'someValue';
                scope.counter = 0;

                scope.$watch(
                    function (scope: Scope) { return scope.aValue; },
                    function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
                );

                scope.$digest();
                expect(scope.counter).to.be.equal(1);

                scope.$apply(function (scope: Scope) { scope.aValue = 'someOtherValue'; });
                expect(scope.counter).to.be.equal(2);
            });

            it('accepts expressions in $apply', function () {

                scope.aFunction = function (): number { return 29; };
                expect(scope.$apply('aFunction()')).to.be.equal(29);
            });
        });


        /**
         * $evalAsync
         */
        describe('evalAsync', () => {

            it('executes $evalAsync\'ed function later in the same cycle', () => {

                scope.aValue = [1, 2, 3];
                scope.asyncEvaluated = false;
                scope.asyncEvaluatedImmediately = false;

                scope.$watch(
                    function (scope: Scope) { return scope.aValue; },
                    function (newValue: any, oldValue: any, scope: Scope) {
                        scope.$evalAsync(function (scope: Scope) {
                            scope.asyncEvaluated = true;
                        });
                        scope.asyncEvaluatedImmediately = scope.asyncEvaluated;
                    }
                );

                scope.$digest();
                expect(scope.asyncEvaluated).to.be.equal(true);
                expect(scope.asyncEvaluatedImmediately).to.be.equal(false);
            });

            it('executes $evalAsync\'ed functions added by watch functions', () => {

                scope.aValue = [1, 2, 3];
                scope.asyncEvaluated = false;

                scope.$watch(
                    function (scope: Scope) {
                        if (!scope.asyncEvaluated) {
                            scope.$evalAsync(function (scope: Scope) {
                                scope.asyncEvaluated = true;
                            });
                        }
                        return scope.aValue;
                    },
                    function () { return; }
                );

                scope.$digest();

                expect(scope.asyncEvaluated).to.be.equal(true);
            });

            it('executes $evalAsync\'ed functions even when not dirty', () => {

                scope.aValue = [1, 2, 3];
                scope.asyncEvaluatedTimes = 0;

                scope.$watch(
                    function (scope: Scope) {
                        if (scope.asyncEvaluatedTimes < 2) {
                            scope.$evalAsync(function (scope: Scope) {
                                scope.asyncEvaluatedTimes++;
                            });
                        }
                        return scope.aValue;
                    },
                    function () { return; }
                );

                scope.$digest();

                expect(scope.asyncEvaluatedTimes).to.be.equal(2);
            });

            it('eventually halts $evalAsync added by watches', () => {

                scope.aValue = [1, 2, 3];

                scope.$watch(
                    function (scope: Scope) {
                        scope.$evalAsync(function () { return; });
                        return scope.aValue;
                    },
                    function () { return; }
                );

                expect(function () { scope.$digest(); }).to.throw();
            });

            it('shedules a digest in $evalAsync', function (done) {

                scope.aValue = 'abc';
                scope.counter = 0;

                scope.$watch(
                    function (scope: Scope) { return scope.aValue; },
                    function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
                );

                scope.$evalAsync(function () { return; });

                expect(scope.counter).to.be.equal(0);
                setTimeout(function () {
                    expect(scope.counter).to.be.equal(1);
                    done();
                });
            });

            it('catches exceptions in $evalAsync', function (done) {

                scope.aValue = 'abc';
                scope.counter = 0;

                scope.$watch(
                    function (scope: Scope) { return scope.aValue; },
                    function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
                );

                scope.$evalAsync(function () {
                    throw 'Test in evalAsync throw error OK';
                });

                setTimeout(function () {
                    expect(scope.counter).to.be.equal(1);
                    done();
                }, 50);
            });

            it('accepts expressions in $evalAsync', function (done) {

                let called: boolean;
                scope.aFunction = function (): void { called = true; };
                scope.$evalAsync('aFunction()');
                scope._postDigest(function () {
                    expect(called).to.be.equal(true);
                    done();
                });
            });

        });


        /**
         * $applyAsync
         */
        describe('applyAsync', () => {

            it('allows async $apply with $applyAsync', function (done) {

                scope.counter = 0;

                scope.$watch(
                    function (scope: Scope) { return scope.aValue; },
                    function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
                );

                scope.$digest();
                expect(scope.counter).to.be.equal(1);

                scope.$applyAsync(function (scope: Scope) {
                    scope.aValue = 'abc';
                });
                expect(scope.counter).to.be.equal(1);

                setTimeout(function () {
                    expect(scope.counter).to.be.equal(2);
                    done();
                }, 50);
            });

            it('never executes $applyAsync\'ed function ine the same cycle', (done) => {

                scope.aValue = [1, 2, 3];
                scope.asyncApplied = false;

                scope.$watch(
                    function (scope: Scope) { return scope.aValue; },
                    function (newValue: any, oldValue: any, scope: Scope) {
                        scope.$applyAsync(function (scope: Scope) {
                            scope.asyncApplied = true;
                        });
                    }
                );

                scope.$digest();
                expect(scope.asyncApplied).to.be.equal(false);
                setTimeout(function () {
                    expect(scope.asyncApplied).to.be.equal(true);
                    done();
                }, 50);
            });

            it('coalesces many calls to $applyAsync', (done) => {

                scope.counter = 0;

                scope.$watch(
                    function (scope: Scope) {
                        scope.counter++;
                        return scope.aValue;
                    },
                    function () { return; }
                );

                scope.$applyAsync(function (scope: Scope) { scope.aValue = 'abc'; });
                scope.$applyAsync(function (scope: Scope) { scope.aValue = 'def'; });

                setTimeout(function () {
                    expect(scope.counter).to.be.equal(2);
                    done();
                }, 50);
            });

            it('cancels and flushes $applyAsync if digested first', (done) => {

                scope.counter = 0;

                scope.$watch(
                    function (scope: Scope) {
                        scope.counter++;
                        return scope.aValue;
                    },
                    function () { return; }
                );

                scope.$applyAsync(function (scope: Scope) { scope.aValue = 'abc'; });
                scope.$applyAsync(function (scope: Scope) { scope.aValue = 'def'; });

                scope.$digest();
                expect(scope.counter).to.be.equal(2);
                expect(scope.aValue).to.be.deep.equal('def');

                setTimeout(function () {
                    expect(scope.counter).to.be.equal(2);
                    done();
                }, 50);
            });

            it('catches exceptions in $applyAsync', (done) => {

                scope.$applyAsync(function () {
                    throw 'Test in applyAsync throw error Ok';
                });

                scope.$applyAsync(function () {
                    throw 'Test in applyAsync throw error Ok';
                });

                scope.$applyAsync(function (scope: Scope) {
                    scope.applied = true;
                });

                setTimeout(function () {
                    expect(scope.applied).to.be.equal(true);
                    done();
                }, 50);
            });
        });


        /**
         * _postDigest
         */
        describe('_postDigest', () => {

            it('runs a _postDigest function after each digest', () => {

                scope.counter = 0;

                scope._postDigest(function () { scope.counter++; });

                expect(scope.counter).to.be.equal(0);

                scope.$digest();
                expect(scope.counter).to.be.equal(1);

                scope.$digest();
                expect(scope.counter).to.be.equal(1);
            });

            it('does not include _postDigest in the digest', () => {

                scope.aValue = 'original value';

                scope._postDigest(function () {
                    scope.aValue = 'changed value';
                });
                scope.$watch(
                    function (scope: Scope) { return scope.aValue; },
                    function (newValue: any) {
                        scope.watchedValue = newValue;
                    }
                );

                scope.$digest();
                expect(scope.watchedValue).to.be.equal('original value');

                scope.$digest();
                expect(scope.watchedValue).to.be.equal('changed value');
            });

            it('catches exceptions in _postDigest', () => {

                let didRun = false;

                scope._postDigest(function () {
                    throw 'Test in postDigest throw error Ok';
                });

                scope._postDigest(function () {
                    didRun = true;
                });

                scope.$digest();
                expect(didRun).to.be.equal(true);
            });

        });
    });


    /**
     * $watchGroup
     */
    describe('watchGroup', () => {

        it('takes watches as an array and calls listener with arrays', () => {

            let gotNewValues, gotOldValues;

            scope.aValue = 1;
            scope.anotherValue = 2;

            scope.$watchGroup(
                [
                    function (scope: Scope): any { return scope.aValue; },
                    function (scope: Scope): any { return scope.anotherValue; }
                ],
                function (newValues: any, oldValues: any) {
                    gotNewValues = newValues;
                    gotOldValues = oldValues;
                }
            );

            scope.$digest();
            expect(gotNewValues).to.be.deep.equal([1, 2]);
            expect(gotOldValues).to.be.deep.equal([1, 2]);
        });

        it('only calls listener once per digest', () => {

            let counter = 0;

            scope.aValue = 1;
            scope.anotherValue = 2;

            scope.$watchGroup(
                [
                    function (scope: Scope): any { return scope.aValue; },
                    function (scope: Scope): any { return scope.anotherValue; }
                ],
                () => { counter++; }
            );

            scope.$digest();
            expect(counter).to.be.deep.equal(1);
        });

        it('uses the same array of old and new values on first run', () => {

            let gotNewValues, gotOldValues;

            scope.aValue = 1;
            scope.anotherValue = 2;

            scope.$watchGroup(
                [
                    function (scope: Scope): any { return scope.aValue; },
                    function (scope: Scope): any { return scope.anotherValue; }
                ],
                function (newValues: any, oldValues: any) {
                    gotNewValues = newValues;
                    gotOldValues = oldValues;
                }
            );

            scope.$digest();
            expect(gotNewValues).to.be.equal(gotOldValues);
        });

        it('uses different array of old and new values on subsequent run', () => {

            let gotNewValues, gotOldValues;

            scope.aValue = 1;
            scope.anotherValue = 2;

            scope.$watchGroup(
                [
                    function (scope: Scope): any { return scope.aValue; },
                    function (scope: Scope): any { return scope.anotherValue; }
                ],
                function (newValues: any, oldValues: any) {
                    gotNewValues = newValues;
                    gotOldValues = oldValues;
                }
            );

            scope.$digest();
            scope.anotherValue = 3;
            scope.$digest();

            expect(gotNewValues).to.be.deep.equal([1, 3]);
            expect(gotOldValues).to.be.deep.equal([1, 2]);
        });

        it('calls the listener once when the watch array is empty', () => {

            let gotNewValues, gotOldValues;

            scope.$watchGroup([],
                function (newValues: any, oldValues: any) {
                    gotNewValues = newValues;
                    gotOldValues = oldValues;
                }
            );

            scope.$digest();
            expect(gotNewValues).to.be.deep.equal([]);
            expect(gotOldValues).to.be.deep.equal([]);
        });

        it('can be deregistered', () => {

            let counter = 0;

            scope.aValue = 1;
            scope.anotherValue = 2;

            const destroyGroup = scope.$watchGroup(
                [
                    function (scope: Scope): any { return scope.aValue; },
                    function (scope: Scope): any { return scope.anotherValue; }
                ],
                () => { counter++; }
            );

            scope.$digest();
            scope.anotherValue = 3;
            destroyGroup();
            scope.$digest();

            expect(counter).to.be.deep.equal(1);
        });

        it('does not call the zero-watch listener when deregistered first', () => {

            let counter = 0;

            const destroyGroup = scope.$watchGroup([],
                () => { counter++; }
            );

            destroyGroup();
            scope.$digest();
            expect(counter).to.be.deep.equal(0);
        });

    });


    /**
     * $watchCollection
     */
    describe('watchCollection', () => {

        it('works like a normal watch for non-collections', () => {

            let valueProvided;

            scope.aValue = 42;
            scope.counter = 0;

            scope.$watchCollection(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) {
                    valueProvided = newValue;
                    scope.counter++;
                }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);
            expect(valueProvided).to.be.equal(scope.aValue);

            scope.aValue = 43;
            scope.$digest();
            expect(scope.counter).to.be.equal(2);

            scope.$digest();
            expect(scope.counter).to.be.equal(2);
        });

        it('works like a normal watch for NaNs', () => {

            scope.aValue = 0 / 0;
            scope.counter = 0;

            scope.$watchCollection(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.$digest();
            expect(scope.counter).to.be.equal(1);
        });

        it('notices when the value becomes an array', () => {

            scope.counter = 0;

            scope.$watchCollection(
                function (scope: Scope) { return scope.arr; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.arr = [1, 2, 3];
            scope.$digest();
            expect(scope.counter).to.be.equal(2);

            scope.$digest();
            expect(scope.counter).to.be.equal(2);
        });

        it('notices an item added to an array', () => {

            scope.arr = [1, 2, 3];
            scope.counter = 0;

            scope.$watchCollection(
                function (scope: Scope) { return scope.arr; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.arr.push(4);
            scope.$digest();
            expect(scope.counter).to.be.equal(2);

            scope.$digest();
            expect(scope.counter).to.be.equal(2);
        });

        it('notices an item removed from an array', () => {

            scope.arr = [1, 2, 3];
            scope.counter = 0;

            scope.$watchCollection(
                function (scope: Scope) { return scope.arr; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.arr.shift();
            scope.$digest();
            expect(scope.counter).to.be.equal(2);

            scope.$digest();
            expect(scope.counter).to.be.equal(2);
        });

        it('notices an item replaced in an array', () => {

            scope.arr = [1, 2, 3];
            scope.counter = 0;

            scope.$watchCollection(
                function (scope: Scope) { return scope.arr; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.arr[1] = 4;
            scope.$digest();
            expect(scope.counter).to.be.equal(2);

            scope.$digest();
            expect(scope.counter).to.be.equal(2);
        });

        it('notices items recorded in an array', () => {

            scope.arr = [2, 1, 3];
            scope.counter = 0;

            scope.$watchCollection(
                function (scope: Scope) { return scope.arr; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.arr.sort();
            scope.$digest();
            expect(scope.counter).to.be.equal(2);

            scope.$digest();
            expect(scope.counter).to.be.equal(2);
        });

        it('does not fail on NaNs in array', () => {

            scope.arr = [2, NaN, 3];
            scope.counter = 0;

            scope.$watchCollection(
                function (scope: Scope) { return scope.arr; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);
        });

        it('notices an item replaced in an arguments object', () => {

            (function (...args): void {
                scope.arrayLike = args;
            })(1, 2, 3);
            scope.counter = 0;

            scope.$watchCollection(
                function (scope: Scope) { return scope.arrayLike; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.arrayLike[1] = 4;
            scope.$digest();
            expect(scope.counter).to.be.equal(2);

            scope.$digest();
            expect(scope.counter).to.be.equal(2);
        });

        it('notices an item replaced in a NodeList object', () => {

            if ((global as any).document) {
                document.documentElement.appendChild(document.createElement('div'));
                scope.arrayLike = document.getElementsByTagName('div');
                scope.counter = 0;

                scope.$watchCollection(
                    function (scope: Scope) { return scope.arrayLike; },
                    function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
                );

                scope.$digest();
                expect(scope.counter).to.be.equal(1);

                document.documentElement.appendChild(document.createElement('div'));
                scope.$digest();
                expect(scope.counter).to.be.equal(2);

                scope.$digest();
                expect(scope.counter).to.be.equal(2);
            }
        });

        it('notices when the value becomes an object', () => {

            scope.counter = 0;

            scope.$watchCollection(
                function (scope: Scope) { return scope.obj; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.obj = { a: 1 };
            scope.$digest();
            expect(scope.counter).to.be.equal(2);

            scope.$digest();
            expect(scope.counter).to.be.equal(2);
        });

        it('notices when an attribute is added to an object', () => {

            scope.counter = 0;
            scope.obj = { a: 1 };

            scope.$watchCollection(
                function (scope: Scope) { return scope.obj; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.obj.b = 2;
            scope.$digest();
            expect(scope.counter).to.be.equal(2);

            scope.$digest();
            expect(scope.counter).to.be.equal(2);
        });

        it('notices when an attribute is changed an object', () => {

            scope.counter = 0;
            scope.obj = { a: 1 };

            scope.$watchCollection(
                function (scope: Scope) { return scope.obj; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            scope.obj.a = 2;
            scope.$digest();
            expect(scope.counter).to.be.equal(2);

            scope.$digest();
            expect(scope.counter).to.be.equal(2);
        });

        it('does not faim on NaN attributes in objects', () => {

            scope.counter = 0;
            scope.obj = { a: NaN };

            scope.$watchCollection(
                function (scope: Scope) { return scope.obj; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);
        });

        it('notices when an attribute is removed from an object', () => {

            scope.counter = 0;
            scope.obj = { a: 1 };

            scope.$watchCollection(
                function (scope: Scope) { return scope.obj; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            scope.$digest();
            expect(scope.counter).to.be.equal(1);

            delete scope.obj.a;
            scope.$digest();
            expect(scope.counter).to.be.equal(2);

            scope.$digest();
            expect(scope.counter).to.be.equal(2);
        });

        it('does not consider any object with a length property an array',
            function () {

                scope.counter = 0;
                scope.obj = { length: 42, otherKey: 'abc' };

                scope.$watchCollection(
                    function (scope: Scope) { return scope.obj; },
                    function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
                );

                scope.$digest();

                scope.obj.newKey = 'def';
                scope.$digest();

                expect(scope.counter).to.be.equal(2);
            });

        it('gives the old non-collection value to listeners', () => {

            scope.aValue = 42;
            let oldValueGiven;

            scope.$watchCollection(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any) { oldValueGiven = oldValue; }
            );

            scope.$digest();

            scope.aValue = 43;
            scope.$digest();

            expect(oldValueGiven).to.be.equal(42);
        });

        it('gives the old array value to listeners', () => {

            scope.aValue = [1, 2, 3];
            let oldValueGiven;

            scope.$watchCollection(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any) { oldValueGiven = oldValue; }
            );

            scope.$digest();

            scope.aValue.push(4);
            scope.$digest();

            expect(oldValueGiven).to.be.deep.equal([1, 2, 3]);
        });

        it('gives the old object value to listeners', () => {

            scope.aValue = { a: 1, b: 2 };
            let oldValueGiven;

            scope.$watchCollection(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any) { oldValueGiven = oldValue; }
            );

            scope.$digest();

            scope.aValue.c = 3;
            scope.$digest();

            expect(oldValueGiven).to.be.deep.equal({ a: 1, b: 2 });
        });

        it('uses the new value as the old value on first digest', () => {

            scope.aValue = { a: 1, b: 2 };
            let oldValueGiven;

            scope.$watchCollection(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any) { oldValueGiven = oldValue; }
            );

            scope.$digest();

            expect(oldValueGiven).to.be.deep.equal({ a: 1, b: 2 });
        });

        it('accepts expressions for watch functions', () => {

            let theValue;
            scope.aColl = [1, 2, 3];
            scope.$watchCollection('aColl', function (newValue: any) {
                theValue = newValue;
            });
            scope.$digest();

            expect(theValue).to.be.deep.equal([1, 2, 3]);
        });

    });


    /**
     * Inheritance
     */
    describe('inheritance', () => {

        let parent: Scope;

        beforeEach(() => {
            parent = new Scope(new Kernel());
        });

        it('inherits the parent\'s properties', () => {

            parent.aValue = [1, 2, 3];

            const child = parent.$new();

            expect(child.aValue).to.be.deep.equal([1, 2, 3]);
        });

        it('does not cause a parent inherit its properties', () => {

            const child = parent.$new();
            child.aValue = [1, 2, 3];

            expect(parent.aValue).to.be.undefined;
        });

        it('inherits the paren\'s properties whenever they are defined',
            () => {

                const child = parent.$new();

                parent.aValue = [1, 2, 3];

                expect(child.aValue).to.be.deep.equal([1, 2, 3]);
            });

        it('can manipulate a parent scope\'s property', () => {

            const child = parent.$new();
            parent.aValue = [1, 2, 3];

            child.aValue.push(4);

            expect(child.aValue).to.be.deep.equal([1, 2, 3, 4]);
            expect(parent.aValue).to.be.deep.equal([1, 2, 3, 4]);
        });

        it('can watch a property in the parent', () => {

            const child = parent.$new();
            parent.aValue = [1, 2, 3];
            child.counter = 0;

            child.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; },
                true
            );

            child.$digest();
            expect(child.counter).to.be.equal(1);

            parent.aValue.push(4);
            child.$digest();
            expect(child.counter).to.be.equal(2);
        });

        it('can be nested at any depth', () => {

            const a = parent,
                  aa = a.$new(),
                  aaa = aa.$new(),
                  aab = aa.$new(),
                  ab = a.$new(),
                  abb = ab.$new();

            a.value = 1;

            expect(aa.value).to.be.equal(1);
            expect(aaa.value).to.be.equal(1);
            expect(aab.value).to.be.equal(1);
            expect(ab.value).to.be.equal(1);
            expect(abb.value).to.be.equal(1);

            ab.anotherValue = 2;

            expect(abb.anotherValue).to.be.equal(2);
            expect(aa.anotherValue).to.be.undefined;
            expect(aaa.anotherValue).to.be.undefined;
        });

        it('shadows a parent\'s property with the same name', () => {

            const child = parent.$new();

            parent.name = 'Pierre';
            child.name = 'Paul';

            expect(child.name).to.be.equal('Paul');
            expect(parent.name).to.be.equal('Pierre');
        });

        it('does not shadow memeber of parent scope\'s attributes', () => {

            const child = parent.$new();

            parent.user = { name: 'Pierre' };
            child.user.name = 'Paul';

            expect(child.user.name).to.be.equal('Paul');
            expect(parent.user.name).to.be.equal('Paul');
        });

        it('does not digest its parent(s)', () => {

            const child = parent.$new();

            parent.aValue = 'abc';
            parent.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) {
                    scope.aValueWas = newValue;
                }
            );

            child.$digest();
            expect(child.aValueWas).to.be.undefined;
        });

        it('keeps a record of its children', () => {

            const child1 = parent.$new(),
                  child2 = parent.$new(),
                  // eslint-disable-next-line @typescript-eslint/camelcase
                  child2_1 = child2.$new();

            expect(parent.$getChildren().length).to.be.equal(2);
            expect(parent.$getChildren()[0]).to.be.equal(child1);
            expect(parent.$getChildren()[1]).to.be.equal(child2);

            expect(child1.$getChildren().length).to.be.equal(0);

            expect(child2.$getChildren().length).to.be.equal(1);
            expect(child2.$getChildren()[0]).to.be.equal(child2_1);
        });

        it('digests its children', () => {

            const child = parent.$new();

            parent.aValue = 'abc';
            child.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) {
                    scope.aValueWas = newValue;
                }
            );

            parent.$digest();
            expect(child.aValueWas).to.be.equal('abc');
        });

        it('digests from root an $apply', () => {

            const child = parent.$new(),
                  child2 = child.$new();

            parent.aValue = 'abc';
            parent.counter = 0;
            parent.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            child2.$apply(function () { return; });
            expect(parent.counter).to.be.equal(1);
        });

        it('schedules a digest from root on $evalAsync', function (done) {

            const child = parent.$new(),
                  child2 = child.$new();

            parent.aValue = 'abc';
            parent.counter = 0;
            parent.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            child2.$evalAsync(function () { return; });
            setTimeout(function () {
                expect(parent.counter).to.be.equal(1);
                done();
            }, 50);
        });

        it('does not have access to parent attributes when isolated', () => {

            const child = parent.$new(true);

            parent.aValue = 'abc';

            expect(child.aValue).to.be.undefined;
        });

        it('cannot watch parent attributes when isolated', () => {

            const child = parent.$new(true);

            parent.aValue = 'abc';
            child.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) {
                    scope.aValueWas = newValue;
                }
            );

            child.$digest();
            expect(child.aValueWas).to.be.undefined;
        });

        it('digests its isolated children', () => {

            const child = parent.$new(true);

            child.aValue = 'abc';
            child.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) {
                    scope.aValueWas = newValue;
                }
            );

            parent.$digest();
            expect(child.aValueWas).to.be.equal('abc');
        });

        it('digests from root on $apply when isolated', () => {

            const child = parent.$new(true),
                  child2 = child.$new();

            parent.aValue = 'abc';
            parent.counter = 0;
            parent.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            child2.$apply(function () { return; });

            expect(parent.counter).to.be.equal(1);
        });

        it('shedules a digest from root on $evalAsync when isolated', (done) => {

            const child = parent.$new(true),
                  child2 = child.$new();

            parent.aValue = 'abc';
            parent.counter = 0;
            parent.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; }
            );

            child2.$evalAsync(function () { return; });

            setTimeout(function () {
                expect(parent.counter).to.be.equal(1);
                done();
            }, 50);
        });

        it('executes $evalAsync functions on isolated scopes', (done) => {

            const child = parent.$new(true);

            child.$evalAsync(function (scope: Scope) {
                scope.didEvalAsync = true;
            });

            setTimeout(function () {
                expect(child.didEvalAsync).to.be.equal(true);
                done();
            }, 50);
        });

        it('executes _postDigest functions on isolated scopes', () => {

            const child = parent.$new(true);

            child._postDigest(() => {
                child.didPostDigest = true;
            });
            parent.$digest();

            expect(child.didPostDigest).to.be.equal(true);
        });

        it('can take some other scope as the parent', () => {

            const prototypeParent = parent.$new(),
                  hierarchyParent = parent.$new(),
                  child = prototypeParent.$new(false, hierarchyParent);

            prototypeParent.a = 1;
            expect(child.a).to.be.equal(1);

            child.counter = 0;
            child.$watch(function (scope: Scope) { scope.counter++; });

            prototypeParent.$digest();
            expect(child.counter).to.be.equal(0);

            hierarchyParent.$digest();
            expect(child.counter).to.be.equal(2);
        });

        it('is no longer digested when $destroy has been called', () => {

            const child = parent.$new();

            child.aValue = [1, 2, 3];
            child.counter = 0;
            child.$watch(
                function (scope: Scope) { return scope.aValue; },
                function (newValue: any, oldValue: any, scope: Scope) { scope.counter++; },
                true
            );

            parent.$digest();
            expect(child.counter).to.be.equal(1);

            child.aValue.push(4);
            parent.$digest();
            expect(child.counter).to.be.equal(2);

            child.$destroy();
            child.aValue.push(5);
            parent.$digest();
            expect(child.counter).to.be.equal(2);
        });
    });

    /**
     * Events
     */
    describe('Events', () => {

        let parent: Scope;
        let scope: Scope;
        let child: Scope;
        let isolatedChild: Scope;

        beforeEach(() => {

            parent = new Scope(new Kernel());
            scope = parent.$new();
            child = scope.$new();
            isolatedChild = scope.$new(true);
        });

        it('allows registering listeners', () => {

            const listener1 = function (): void { return; },
                  listener2 = function (): void { return; },
                  listener3 = function (): void { return; };

            scope.$on('someEvent', listener1);
            scope.$on('someEvent', listener2);
            scope.$on('someOtherEvent', listener3);

            expect(scope.$getListeners()).to.be.deep.equal({
                someEvent: [listener1, listener2],
                someOtherEvent: [listener3]
            });
        });

        it('registers different listeners for every scope', () => {

            const listener1 = function (): void { return; },
                  listener2 = function (): void { return; },
                  listener3 = function (): void { return; };

            scope.$on('someEvent', listener1);
            child.$on('someEvent', listener2);
            isolatedChild.$on('someEvent', listener3);

            expect(scope.$getListeners()).to.be.deep.equal({ someEvent: [listener1] });
            expect(child.$getListeners()).to.be.deep.equal({ someEvent: [listener2] });
            expect(isolatedChild.$getListeners())
                .to.be.deep.equal({ someEvent: [listener3] });
        });

        ['$emit', '$broadcast'].forEach((method) => {

            it('calls the listeners of the matching event on ' + method, () => {

                const listener1 = sinon.spy(),
                      listener2 = sinon.spy();
                scope.$on('someEvent', listener1);
                scope.$on('someOtherEvent', listener2);

                scope[method]('someEvent');

                expect(listener1.called).to.be.true;
                expect(listener2.notCalled).to.be.true;
            });

            it('passes an event object with a name to listeners on ' + method, () => {

                const listener = sinon.spy();
                scope.$on('someEvent', listener);

                scope[method]('someEvent');

                expect(listener.called).to.be.true;
                expect(listener.getCall(0).args[0].name).to.be.deep.equal('someEvent');
            });

            it('passes the same event object to each listener on ' + method, () => {

                const listener1 = sinon.spy(),
                      listener2 = sinon.spy();
                scope.$on('someEvent', listener1);
                scope.$on('someEvent', listener2);

                scope[method]('someEvent');

                const event1 = listener1.getCall(0).args[0],
                      event2 = listener2.getCall(0).args[0];

                expect(event1).to.be.equal(event2);
            });

            it('passes additional arguments to listener on ' + method, () => {

                const listener = sinon.spy();
                scope.$on('someEvent', listener);

                scope[method]('someEvent', 'and', ['additional', 'arguments'], '...');

                expect(listener.getCall(0).args[1]).to.be.deep.equal('and');
                expect(listener.getCall(0).args[2]).to.be.deep.equal(
                    ['additional', 'arguments']);
                expect(listener.getCall(0).args[3]).to.be.deep.equal('...');
            });

            it('returns the event object on ' + method, () => {

                const returnedEvent = scope[method]('someEvent');

                expect(returnedEvent).to.be.not.undefined;
                expect(returnedEvent.name).to.be.deep.equal('someEvent');
            });

            it('can be deregistered ' + method, () => {

                const listener = sinon.spy(),
                      deregister = scope.$on('someEvent', listener);

                deregister();

                scope[method]('someEvent');

                expect(listener.notCalled).to.be.true;
            });

            it('does not skip the next listener when removed on ' + method, () => {

                let deregister: Function = () => { return; };

                const listener = function (): void { deregister(); },
                      nextListener = sinon.spy();

                deregister = scope.$on('someEvent', listener);
                scope.$on('someEvent', nextListener);

                scope[method]('someEvent');

                expect(nextListener.called).to.be.true;
            });

            it('sets currentScope to null after propagation on ' + method, () => {

                let event: Record<string, any> = {};
                const scopeListener = function (evt: Record<string, any>): void { event = evt; };
                scope.$on('someEvent', scopeListener);

                scope[method]('someEvent');

                expect(event.currentScope).to.be.equal(null);
            });

            it('is sets defaultPrevented when preventDefault called on ' + method, () => {

                const listener = function (event: Record<string, any>): void {
                    event.preventDefault();
                };
                scope.$on('someEvent', listener);

                const event = scope[method]('someEvent');

                expect(event.defaultPrevented).to.be.equal(true);
            });

            it('does not stop on exceptions on ' + method, () => {

                const listener1 = function (): void {
                    throw 'listener1 throwing an exception';
                };
                const listener2 = sinon.spy();
                scope.$on('someEvent', listener1);
                scope.$on('someEvent', listener2);

                scope[method]('someEvent');

                expect(listener2.called).to.be.true;
            });
        });

        it('propagates up the scope hierarchy on $emit', () => {

            const parentListener = sinon.spy(),
                  scopeListener = sinon.spy();

            parent.$on('someEvent', parentListener);
            scope.$on('someEvent', scopeListener);

            scope.$emit('someEvent');

            expect(scopeListener.called).to.be.true;
            expect(parentListener.called).to.be.true;
        });

        it('propagates the same event up on $emit', () => {

            const parentListener = sinon.spy(),
                  scopeListener = sinon.spy();

            parent.$on('someEvent', parentListener);
            scope.$on('someEvent', scopeListener);

            scope.$emit('someEvent');

            const scopeEvent = scopeListener.getCall(0).args[0],
                  parentEvent = scopeListener.getCall(0).args[0];
            expect(scopeEvent).to.be.equal(parentEvent);
        });

        it('propagates down the scope hierarchy on $broacast', () => {

            const scopeListener = sinon.spy(),
                  childListener = sinon.spy(),
                  isolatedChildListener = sinon.spy();

            scope.$on('someEvent', scopeListener);
            child.$on('someEvent', childListener);
            isolatedChild.$on('someEvent', isolatedChildListener);

            scope.$broadcast('someEvent');

            expect(scopeListener.called).to.be.true;
            expect(childListener.called).to.be.true;
            expect(isolatedChildListener.called).to.be.true;
        });

        it('propagates the same event down on $broadcast', () => {

            const scopeListener = sinon.spy(),
                  childListener = sinon.spy();

            scope.$on('someEvent', scopeListener);
            child.$on('someEvent', childListener);

            scope.$broadcast('someEvent');

            const scopeEvent = scopeListener.getCall(0).args[0],
                  childEvent = scopeListener.getCall(0).args[0];
            expect(scopeEvent).to.be.equal(childEvent);
        });

        it('attaches targetScope on $emit', () => {

            const scopeListener = sinon.spy(),
                  parentListener = sinon.spy();

            scope.$on('someEvent', scopeListener);
            parent.$on('someEvent', parentListener);

            scope.$emit('someEvent');

            expect(scopeListener.getCall(0).args[0].targetScope)
                .to.be.equal(scope);
            expect(parentListener.getCall(0).args[0].targetScope)
                .to.be.equal(scope);
        });

        it('attaches targetScope on $broadcast', () => {

            const scopeListener = sinon.spy(),
                  childListener = sinon.spy();

            scope.$on('someEvent', scopeListener);
            child.$on('someEvent', childListener);

            scope.$broadcast('someEvent');

            expect(scopeListener.getCall(0).args[0].targetScope)
                .to.be.equal(scope);
            expect(childListener.getCall(0).args[0].targetScope)
                .to.be.equal(scope);
        });

        it('attaches currentScope on $emit', () => {

            let currentScopeOnScope, currentScopeOnParent;
            const scopeListener = function (event: Record<string, any>): void {
                currentScopeOnScope = event.currentScope;
            };
            const parentListener = function (event: Record<string, any>): void {
                currentScopeOnParent = event.currentScope;
            };

            scope.$on('someEvent', scopeListener);
            parent.$on('someEvent', parentListener);

            scope.$emit('someEvent');

            expect(currentScopeOnScope).to.be.equal(scope);
            expect(currentScopeOnParent).to.be.equal(parent);
        });

        it('attaches currentScope on $broadcast', () => {

            let currentScopeOnScope, currentScopeOnChild;
            const scopeListener = function (event: Record<string, any>): void {
                currentScopeOnScope = event.currentScope;
            };
            const childListener = function (event: Record<string, any>): void {
                currentScopeOnChild = event.currentScope;
            };

            scope.$on('someEvent', scopeListener);
            child.$on('someEvent', childListener);

            scope.$broadcast('someEvent');

            expect(currentScopeOnScope).to.be.equal(scope);
            expect(currentScopeOnChild).to.be.equal(child);
        });

        it('does not propagate to parents when stopped', () => {

            const scopeListener = function (event: Record<string, any>): void {
                event.stopPropagation();
            };
            const parentListener = sinon.spy();

            scope.$on('someEvent', scopeListener);
            parent.$on('someEvent', parentListener);

            scope.$emit('someEvent');

            expect(parentListener.notCalled).to.be.true;
        });

        it('is received by listeners on current scope after being stopped', () => {

            const listener1 = function (event: Record<string, any>): void {
                event.stopPropagation();
            };
            const listener2 = sinon.spy();

            scope.$on('someEvent', listener1);
            scope.$on('someEvent', listener2);

            scope.$emit('someEvent');

            expect(listener2.called).to.be.true;
        });

        it('fires $destroy when destroyed', () => {

            const listener = sinon.spy();
            scope.$on('$destroy', listener);

            scope.$destroy();

            expect(listener.called).to.be.true;
        });

        it('fires $destroy on children destroyed', () => {

            const listener = sinon.spy();
            child.$on('$destroy', listener);

            scope.$destroy();

            expect(listener.called).to.be.true;
        });

        it('no longers calls listeners after destroyed', () => {

            const listener = sinon.spy();
            scope.$on('myEvent', listener);

            scope.$destroy();

            scope.$emit('myEvent');
            expect(listener.notCalled).to.be.true;
        });

    });


    /**
     * TTL configurability
     */
    describe('TTL configurability', () => {

        it('allows configuring a shorter TTL', () => {

            scope = new Scope(new Kernel());
            scope.$digestTtl(5);

            scope.counterA = 0;
            scope.counterB = 0;

            scope.$watch(
                function (scope: Scope) { return scope.counterA; },
                function (newValue: any, oldValue: any, scope: Scope) {
                    if (scope.counterB < 5)
                        scope.counterB++;
                }
            );
            scope.$watch(
                function (scope: Scope) { return scope.counterB; },
                function (newValue: any, oldValue: any, scope: Scope) {
                    scope.counterA++;
                }
            );

            expect(function () { scope.$digest(); }).to.throw();
        });
    });
});
