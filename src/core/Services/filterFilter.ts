/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as Utils from '../Utils';

/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * Est la fonction exportée. Elle retourne le filtre `filter` qui filtre un tableau.
 *    `filter:filterExpr:comparator`
 * @param array Tableau à filtrer
 * @param filterExpr expression qui sert de filtre
 * @param comparator fonction de comparaison
 */
export function filterFilter (
    array: any[], filterExpr: string | Function, comparator: Function
): any[] {


    // deepCompare
    function deepCompare (
        actual: any, expected: any,
        comparator: Function,
        matchAnyProperty?: boolean, inWildcard?: boolean
    ): boolean {

        // "!aString"
        if (Utils.isString(expected) && expected.startsWith('!'))
            return !deepCompare(actual, expected.substring(1), comparator, matchAnyProperty);
        // Actual est un tableau
        if (Array.isArray(actual)) {
            return actual.some(function (actualItem) {
                return deepCompare(actualItem, expected, comparator, matchAnyProperty);
            });
        }
        // Actual est un objet
        if (Utils.isObject(actual)) {
            // Si expected est un objet
            if (Utils.isObject(expected) && !inWildcard) {
                return Object.getOwnPropertyNames(expected).every(
                    function (property) {
                        if (expected[property] === undefined)
                            return true;
                        const isWildcard = (property === '$');
                        const actualVal = isWildcard ? actual : actual[property];
                        return deepCompare(
                            actualVal, expected[property], comparator, isWildcard, isWildcard
                        );
                    }
                );
            }
            // Sinon
            else if (matchAnyProperty) {
                return Object.getOwnPropertyNames(actual).some(function (key) {
                    return deepCompare(actual[key], expected, comparator, matchAnyProperty);
                });
            }
            else
                return comparator(actual, expected);
        }
        // Sinon (string, number, boolean, ...)
        else
            return comparator(actual, expected);
    }


    /**
     * createPredicateFn (expression, comparator)
     *
     * Créé la fonction de comparaison qui est appliqué au tableau.
     * Si comparator vaut `true`, alors la fonction utilisée sera `Utils.isEqual`,
     * si comparator est une fonction, alors on utilisera celle-ci.
     * @param expression Expression
     * @param comparator Si vaut `true`, on utilise `Utils.isEqal`
     */
    function createPredicateFn (
        expression: string | Record<string, any>, comparator: boolean | Function
    ): Function {

        const shouldMatchPrimitives =
              Utils.isObject(expression) && ('$' in (expression as Record<string, any>));

        // Si comparator vaut true
        if (comparator === true)
            comparator = Utils.isEqual.bind(Utils); // les this internes fonctionnent
        // On a fourni une fonction de comparaison
        else if (!Utils.isFunction(comparator)) {
            comparator = function (actual: any, expected: any): boolean {

                // Cas d'undefined
                if (actual === undefined)
                    return false;
                // Cas du null
                if (actual === null || expected === null)
                    return actual === expected;
                // Sinon
                actual = ('' + actual).toLowerCase();
                expected = ('' + expected).toLowerCase();
                return actual.indexOf(expected) !== -1;
            };
        }

        return function predicateFn (item: any): boolean {

            if (shouldMatchPrimitives && !Utils.isObject(item)) {
                return deepCompare(
                    item, (expression as Record<string, any>).$, comparator as Function
                );
            }
            return deepCompare(item, expression, comparator as Function, true);
        };
    }


    // Filter

    let predicateFn: Function;

    if (Utils.isFunction(filterExpr))
        predicateFn = filterExpr as Function;
    else if (Utils.isString(filterExpr) ||
        Utils.isNumber(filterExpr) ||
        Utils.isBoolean(filterExpr) ||
        filterExpr === null ||
        Utils.isObject(filterExpr))
        predicateFn = createPredicateFn(filterExpr, comparator);
    else
        return array;

    return array.filter(predicateFn as any);
}
