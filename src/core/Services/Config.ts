/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as Utils from '../Utils';

/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * Service permettant d'accéder à la configuration de l'application
 *
 *     const config = new Config();
 *     config.add({
 *         'param1': 'bar',
 *         'param2': 'foo%param1%
 *      }) ;
 *      let param = config.get('param2') ;
 *      console.log(param2) ; // foobar
 */
export class Config {

    protected _vars: Record<string, any> = {};


    /**
     * Définit un paramètre
     *
     * @param id Identifiant du paramètre
     * @param value Valeur du paramètre
     * @param ecrase Écrase ou non un paramètre portant le même identifiant (vaut `false` par défaut).
     */
    set (id: string, value: any, ecrase = false): void {

        if (!ecrase && this._vars[id] !== undefined)
            return;

        this._vars[id] = value;
    }


    /**
     * Retourne un paramètre ou la valeur par défaut si celle-ci est spécifiée et si le paramètre
     * n'est pas initialisé.
     * En formatant le retour, on remplace une constante par sa valeur.
     *
     * @param id Identifiant
     * @param dflt Valeur par défaut
     * @param format Doit-on formater la sortie (`false` par défaut)
     */
    get (id: string, dflt: any = null, format = true): any {

        if (/.+\.\*$/.test(id)) {

            const root = id.substr(0, id.length - 1);
            let results: {[keys: string]: any} = {};

            Utils.forEach(this._vars, (value, id) => {

                if (id.substr(0, root.length) == root)
                    results[id.substr(root.length)] = this.get(id);
            });

            if (Utils.isObject(dflt))
                results = Utils.extend({}, dflt, results);

            return results;
        }
        else {
            return this.has(id) ? (
                format && Utils.isString(this._vars[id]) ?
                    this._vars[id].replace(
                        /%([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]+)%/g,
                        (match: string, p1: string) => { return this._vars[p1]; }
                    ) : this._vars[id]
            ) : (Utils.isFunction(dflt) ? dflt() : dflt);
        }
    }


    /**
     * Indique si un élément est présent ou non
     * @param id Identifiant de l'élément à tester
     */
    has (id: string): boolean {

        return Object.keys(this._vars).indexOf(id) > -1;
    }


    /**
     * Supprime un élément. On peut supprimer une série avec `foo.bar.*` comme valeur d'identifiant.
     * @param id Identifiant de l'élément à supprimer
     * @param fct Fonction de suppression (on a pas à l'utiliser)
     */
    delete (id: string, fct?: Function): void {

        if (!Utils.isFunction(fct))
            fct = (elmt: string): void => { delete this._vars[elmt]; };

        if (/.+\.\*$/.test(id)) {

            const root = id.substr(0, id.length - 1);
            Utils.forEach(this._vars, (value, id) => {

                if (id.substr(0, root.length) == root)
                    (fct as Function)(id);
            });
        }
        else
            (fct as Function)(id);
    }


    /**
     * Initialise une série de paramètres
     * @param parameters Tableau de paramètres (les clés sont l'id)
     * @param ecrase Écrase ou non un élément précédent (`false` par défaut)
     */
    add (parameters: Record<string, any>, ecrase = false): void {

        Utils.forEach(parameters, (value, id) => {
            this.set(id, value, ecrase);
        });
    }


    /**
     * Retourne tous les éléments de la configuration
     */
    getAll (): Record<string, any> {

        return this._vars;
    }


    /**
     * Remet à zéro toute la configuration
     */
    reset (): void {

        delete this._vars;
        this._vars = {};
    }
}
