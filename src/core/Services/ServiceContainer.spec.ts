/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as sinon from 'sinon';

import { Kernel } from '../Kernel';
import { ServiceContainer } from './ServiceContainer';

describe('core/Services/ServiceContainer', () => {

    // On définit des faux services
    class A { }
    class B { }
    class C { }

    describe('register & get', () => {

        it('defini un service', () => {

            const services = new ServiceContainer();
            services.register('test', new A());

            expect(services.get('test')).to.be.instanceOf(A);
        });

        it('Retourne un service', () => {

            const a = new A();
            const aBis = new A();
            const services = new ServiceContainer();
            services.register('test', a);

            expect(services.get('test')).to.equal(a);
            expect(services.get('test')).to.not.equal(aBis);
            expect(services.get('test2')).to.equal(undefined);
        });

        it('si le service est une fonction, l\'exécute avec le Kernel', () => {

            const kernel = new Kernel();
            const spy = sinon.spy();
            const services = new ServiceContainer(kernel);
            services.register('test', spy);
            services.get('test');

            expect(spy.calledOnce).to.equal(true);
            expect(spy.calledWithExactly(kernel)).to.be.true;
        });

        it('si le service est une fonction, retourne le résultat', () => {

            const services = new ServiceContainer();
            services.register('test', () => 'foobar');

            expect(services.get('test')).to.be.equal('foobar');
        });
    });


    describe('has', () => {

        it('indique si un service est defini', () => {

            const services = new ServiceContainer();
            services.register('test', new A());

            expect(services.has('test')).to.equal(true);
            expect(services.has('test2')).to.equal(false);
        });
    });


    describe('getAll', () => {

        it('retourne tous les services', () => {

            const services = new ServiceContainer();
            const a = new A(),
                  b = new B(),
                  c = new C();
            services.register('serviceA', a);
            services.register('serviceB', b);
            services.register('serviceC', c);

            expect(services.getAll()).to.deep.equal({
                serviceA: a,
                serviceB: b,
                serviceC: c
            });
        });
    });
});

