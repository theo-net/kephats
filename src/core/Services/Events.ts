/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Event } from './Event';

/**
 * Gestion des évènements
 *
 * Il s'agit d'un canal publique : n'importe qui peut y déclencher un évènement et écouter les
 * autres évènements.
 *
 * Il s'agit du service donnant l'accès aux différents canaux d'évènements.
 */
export class Events {

    protected _events: { [keys: string]: Event } = {};

    /**
     * Enregistre un nouveau canal d'évènement. Si l'id est déjà défini, on effacera l'accès au
     * canal précédent.
     * @param id Identifiant du canal
     */
    register (id: string): void {

        this._events[id] = new Event(id);
    }


    /**
     * Attache un callback à un canal
     * @param canal Identifiant du canal
     * @param id Identifiant de l'observer canal
     * @param callback Le callback
     */
    attach (canal: string, id: string, callback: Function): void {

        if (this._events[canal])
            this._events[canal].attach(id, callback);
    }


    /**
     * Retourne un canal d'évènement
     */
    get (canal: string): Event | undefined {

        return this._events[canal];
    }


    /**
     * Notifie un évènement
     * @param canal Identifiant du canal à notifier
     * @param data Données à transmettre
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    notify (canal: string, data: any): void {

        if (this._events[canal])
            this._events[canal].notify(data);
    }


    /**
     * Indique si un canal est enregistré
     * @param canal Identidiant du canal à tester
     */
    has (canal: string): boolean {

        return Object.keys(this._events).indexOf(canal) > -1;
    }
}
