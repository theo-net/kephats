/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as sinon from 'sinon';

import { Event } from './Event';

describe('core/Services/Event', () => {

    it('Sauve son identifiant', () => {

        const event = new Event('foobar');
        expect(event.getId()).to.be.equal('foobar');
    });

    it('Sait attacher un observer', () => {

        const event = new Event('foobar'),
              callback = (): void => { return; };

        event.attach('test', callback);
        expect(event.get('test')).to.be.equal(callback);
    });

    it('get() returns `undefinned` if the observer don\'t exists', () => {

        const event = new Event('foobar');
        expect(event.get('test')).to.be.undefined;
    });

    it('Sait détacher un observer', () => {

        const event = new Event('foobar'),
              callback = (): void => { return; };

        event.attach('test', callback);
        event.detach('test');
        expect(event.get('test')).to.be.undefined;
    });

    it('Notifie tous les observers et transmet les données', () => {

        const event = new Event('foobar'),
              spy1 = sinon.spy(),
              spy2 = sinon.spy(),
              spy3 = sinon.spy(),
              data = { a: 1, b: 2 };

        event.attach('spy1', spy1);
        event.attach('spy2', spy2);
        event.attach('spy3', spy3);
        event.detach('spy3');

        event.notify(data);

        expect(spy1.calledOnce).to.be.true;
        expect(spy1.calledWith(data)).to.be.true;
        expect(spy2.calledOnce).to.be.true;
        expect(spy2.calledWith(data)).to.be.true;
        expect(spy3.calledOnce).to.be.false;
    });
});

