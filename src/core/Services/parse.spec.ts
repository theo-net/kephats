/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


import { expect } from 'chai';

import { createParser } from './parse';
import { Kernel } from '../Kernel';

/* eslint-disable @typescript-eslint/no-explicit-any */

describe('core/Services/parse', () => {

    const kernel = new Kernel();
    const parse = createParser(kernel);

    it('returns the function itself when given on', () => {

        const fn = (): void => { return; };
        expect(parse(fn)).to.be.equal(fn);
    });

    it('still returns a function when given no argument', () => {

        expect(typeof parse()).to.be.equal('function');
    });

    /**
     * Parsing Integers
     */

    it('can parse an integer', () => {

        const fn = parse('29');
        expect(fn).to.not.be.undefined;
        expect(fn()).to.be.equal(29);
    });

    /**
     * Parsing Floating Point Numbers
     */

    it('can parse a floating point number', () => {

        const fn = parse('3.14');
        expect(fn()).to.be.equal(3.14);
    });

    it('can parse a floating point number without an integer part', () => {

        const fn = parse('.29');
        expect(fn()).to.be.equal(0.29);
    });

    /**
     * Parsing Scientific Notation
     */

    it('can parse a number in scientific notation', () => {

        const fn = parse('29e3');
        expect(fn()).to.be.equal(29000);
    });

    it('can parse scientific notation with a float coefficient', () => {

        const fn = parse('.29e2');
        expect(fn()).to.be.equal(29);
    });

    it('can parse scientific notation with negative exponents', () => {

        const fn = parse('2900e-2');
        expect(fn()).to.be.equal(29);
    });

    it('can parse scientific notation with the + sign', () => {

        const fn = parse('.29e+2');
        expect(fn()).to.be.equal(29);
    });

    it('can parse upper case scientific notation', () => {

        const fn = parse('.29E2');
        expect(fn()).to.be.equal(29);
    });

    it('will not parse invalid scientific notation', () => {

        expect(function () { parse('42e-'); }).to.throw();
        expect(function () { parse('42e-'); }).to.throw();
    });

    /**
     * Parsing String
     */

    it('can parse a string in single quotes', () => {

        const fn = parse("'abc'");
        expect(fn()).to.be.deep.equal('abc');
    });

    it('can parse a string in double quotes', () => {

        const fn = parse('"abc"');
        expect(fn()).to.be.deep.equal('abc');
    });

    it('will not parse a string with mismatching quotes', () => {

        expect(function () { parse('"abc\''); }).to.throw();
    });

    it('can parse a string with single quotes inside', () => {

        const fn = parse("'a\\'b'");
        expect(fn()).to.be.deep.equal('a\'b');
    });

    it('can parse a string with double quotes inside', () => {

        const fn = parse('"a\\"b"');
        expect(fn()).to.be.deep.equal('a"b');
    });

    it('will parse a string with unicode escapes', () => {

        const fn = parse('"\\u00A0"');
        expect(fn()).to.be.deep.equal('\u00A0');
    });

    it('will not parse a string with invilid unicode escapes', () => {

        expect(function () { parse('"\\u00T0"'); }).to.throw();
    });

    /**
     * null, true, false
     */

    it('will parse null', () => {

        const fn = parse('null');
        expect(fn()).to.be.equal(null);
    });

    it('will parse true', () => {

        const fn = parse('true');
        expect(fn()).to.be.equal(true);
    });

    it('will parse false', () => {

        const fn = parse('false');
        expect(fn()).to.be.equal(false);
    });

    /**
     * Whitespace
     */

    it('ignores whitespace', () => {

        const fn = parse(' \n42 ');
        expect(fn()).to.be.deep.equal(42);
    });


    /**
     * Arrays
     */

    it('will parse an empty array', () => {

        const fn = parse('[]');
        expect(fn()).to.be.deep.equal([]);
    });

    it('will parse a non-empty array', () => {

        const fn = parse('[1, "two", [3], true]');
        expect(fn()).to.be.deep.equal([1, 'two', [3], true]);
    });

    it('will parse an array with trailling commas', () => {

        const fn = parse('[1, 2, 3, ]');
        expect(fn()).to.be.deep.equal([1, 2, 3]);
    });


    /**
     * Objects
     */
    it('will parse an empty object', () => {

        const fn = parse('{}');
        expect(fn()).to.be.deep.equal({});
    });

    it('will parse a non-empty object', () => {

        const fn = parse('{"a key": 1, \'another-key\': 2}');
        expect(fn()).to.be.deep.equal({ 'a key': 1, 'another-key': 2 });
    });

    it('will parse an object with identifier keys', () => {

        const fn = parse('{a: 1, b: [2, 3], c: {d: 4}}');
        expect(fn()).to.be.deep.equal({ a: 1, b: [2, 3], c: { d: 4 } });
    });


    /**
     * Simple Attribute Lookup
     */

    it('looks up an attribute from the scope', () => {

        const fn = parse('aKey');
        expect(fn({ aKey: 42 })).to.be.equal(42);
        expect(fn({})).to.be.undefined;
    });

    it('returns undefined when looking up attribute from undefined', () => {

        const fn = parse('aKey');
        expect(fn()).to.be.undefined;
    });


    /**
     * this
     */

    it('will parse this', () => {

        const fn = parse('this');
        const scope = {};
        expect(fn(scope)).to.be.equal(scope);
        expect(fn()).to.be.undefined;
    });


    /**
     * Non-computed Attribute Lookup
     */

    it('looks up a 2-part identifier path from the scope', () => {

        const fn = parse('aKey.anotherKey');
        expect(fn({ aKey: { anotherKey: 29 } })).to.be.equal(29);
        expect(fn({ aKey: {} })).to.be.undefined;
        expect(fn({})).to.be.undefined;
    });

    it('looks up a member from an object', () => {

        const fn = parse('{aKey: 29}.aKey');
        expect(fn()).to.be.equal(29);
    });

    it('looks up a 4-part identifier path from the scope', () => {

        const fn = parse('aKey.secondKey.thirdKey.fourthKey');
        expect(fn({ aKey: { secondKey: { thirdKey: { fourthKey: 29 } } } })).to.be.equal(29);
        expect(fn({ aKey: { secondKey: { thirdKey: {} } } })).to.be.undefined;
        expect(fn({ aKey: {} })).to.be.undefined;
        expect(fn()).to.be.undefined;
    });

    /**
     * Locals
     */

    it('uses locals instead of scope when there is a matching key', () => {

        const fn = parse('aKey'),
              scope = { aKey: 26 },
              locals = { aKey: 3 };
        expect(fn(scope, locals)).to.be.equal(3);
    });

    it('does not use locals instead of scope when no matching key', () => {

        const fn = parse('aKey'),
              scope = { aKey: 29 },
              locals = { otherKey: 3 };
        expect(fn(scope, locals)).to.be.equal(29);
    });

    it('uses locals instead of scope when the first part matches', () => {

        const fn = parse('aKey.anotherKey'),
              scope = { aKey: { anotherKey: 29 } },
              locals = { aKey: {} };
        expect(fn(scope, locals)).to.be.undefined;
    });


    /**
     * Computed Attribute Lookup
     */

    it('parses a simple computed property access', () => {

        const fn = parse('aKey["anotherKey"]');
        expect(fn({ aKey: { anotherKey: 29 } })).to.be.equal(29);
    });

    it('parses a computed numeric array access', () => {

        const fn = parse('anArray[1]');
        expect(fn({ anArray: [1, 2, 3] })).to.be.equal(2);
    });

    it('parses a computed access with another key as property', () => {

        const fn = parse('lock[key]');
        expect(fn({ key: 'theKey', lock: { theKey: 29 } })).to.be.equal(29);
    });

    it('parses computed access with another access as property', () => {

        const fn = parse('lock[keys["aKey"]]');
        expect(fn({ keys: { aKey: 'theKey' }, lock: { theKey: 29 } })).to.be.equal(29);
    });


    /**
     * Function Calls
     */

    it('parses a function call', () => {

        const fn = parse('aFunction()');
        expect(fn({ aFunction: function () { return 29; } })).to.be.equal(29);
    });

    it('parses a function call with a single number argument', () => {

        const fn = parse('aFunction(29)');
        expect(fn({ aFunction: function (n: number) { return n; } })).to.be.equal(29);
    });

    it('parses a function call with a single identifier argument', () => {

        const fn = parse('aFunction(n)');
        expect(fn({ n: 29, aFunction: function (arg: number) { return arg; } }))
            .to.be.equal(29);
    });

    it('parses a function call with a single function call argument', () => {

        const fn = parse('aFunction(argFn())');
        expect(fn({
            argFn: function () { return 29; },
            aFunction: function (arg: number) { return arg; }
        })).to.be.equal(29);
    });

    it('parses a function call with multiple arguments', () => {

        const fn = parse('aFunction(3, n, argFn())');
        expect(fn({
            n: 26,
            argFn: function () { return 29; },
            aFunction: function (a1: number, a2: number, a3: number) { return a1 + a2 + a3; }
        })).to.be.equal(58);
    });


    /**
     * Method Call
     */

    it('calls methods accessed as computed properties', () => {

        const scope = {
            anObject: {
                aMember: 29,
                aFunction: function (): number {
                    return this.aMember;
                }
            }
        };
        const fn = parse('anObject["aFunction"]()');

        expect(fn(scope)).to.be.equal(29);
    });

    it('calls methods accessed as non-computed properties', () => {

        const scope = {
            anObject: {
                aMember: 29,
                aFunction: function (): number {
                    return this.aMember;
                }
            }
        };
        const fn = parse('anObject.aFunction()');
        expect(fn(scope)).to.be.equal(29);
    });

    it('binds bare functions to the scope', () => {

        const scope = {
            aFunction: function (): Record<string, any> {
                return this;
            }
        };
        const fn = parse('aFunction()');
        expect(fn(scope)).to.be.equal(scope);
    });

    it('binds bare functions to the locals', () => {

        const scope = {},
              locals = {
                  aFunction: function (): Record<string, any> {
                      return this;
                  }
              };
        const fn = parse('aFunction()');
        expect(fn(scope, locals)).to.be.equal(locals);
    });


    /**
     * Assigning Values
     */

    it('parses a simple attribute assignment', () => {

        const fn = parse('anAttribute = 29');
        const scope: Record<string, any> = {};
        fn(scope);
        expect(scope.anAttribute).to.be.equal(29);
    });

    it('can assign any primary expression', () => {

        const fn = parse('anAttribute = aFunction()');
        const scope: Record<string, any> = { aFunction: function (): number { return 29; } };
        fn(scope);
        expect(scope.anAttribute).to.be.equal(29);
    });

    it('can assign a computed object property', () => {

        const fn = parse('anObject["anAttribute"] = 29');
        const scope: Record<string, any> = { anObject: {} };
        fn(scope);
        expect(scope.anObject.anAttribute).to.be.equal(29);
    });

    it('can assign a non-computed object property', () => {

        const fn = parse('anObject.anAttribute = 29');
        const scope: Record<string, any> = { anObject: {} };
        fn(scope);
        expect(scope.anObject.anAttribute).to.be.equal(29);
    });

    it('can assign a nested object property', () => {

        const fn = parse('anArray[0].anAttribute = 29');
        const scope: Record<string, any> = { anArray: [{}] };
        fn(scope);
        expect(scope.anArray[0].anAttribute).to.be.equal(29);
    });

    it('creates the objects in the assignment path that do not exist', () => {

        const fn = parse('some["nested"].property.path = 29');
        const scope: Record<string, any> = {};
        fn(scope);
        expect(scope.some.nested.property.path).to.be.equal(29);
    });


    /**
     * Ensuring Safety In Member Access
     */

    it('does not allow calling the function constructor', () => {

        expect(function () {
            const fn = parse('aFunction.constructor("return window ;")()');
            fn({ aFunction: function () { return; } });
        }).to.throw();
    });

    it('does not allow accessing __proto__', () => {

        expect(function () {
            const fn = parse('obj.__proto__');
            fn({ obj: {} });
        }).to.throw();
    });


    it('does not allow calling __defineGetter__', () => {

        expect(function () {
            const fn = parse('obj.__defineGetter__("evil", fn)');
            fn({ obj: {}, fn: function () { return; } });
        }).to.throw();
    });


    it('does not allow calling __defineSetter__', () => {

        expect(function () {
            const fn = parse('obj.__defineSetter__("evil", fn');
            fn({ obj: {}, fn: function () { return; } });
        }).to.throw();
    });


    it('does not allow calling __lookupGetter__', () => {

        expect(function () {
            const fn = parse('obj.__lookupGetter__("evil")');
            fn({ obj: {} });
        }).to.throw();
    });


    it('does not allow calling __lookupSetter__', () => {

        expect(function () {
            const fn = parse('obj.__lookupSetter__("evil")');
            fn({ obj: {} });
        }).to.throw();
    });


    /**
     * Ensuring Safe Objects
     */

    it('does not allow accessing window as computed property', () => {

        const fn = parse('anObject["wnd"]');
        expect(function () { fn({ anObject: { wnd: window } }); }).to.throw();
    });

    it('does not allow accessing window as non-computed property', () => {

        const fn = parse('anObject.wnd');
        expect(function () { fn({ anObject: { wnd: window } }); }).to.throw();
    });

    it('does not allow passing window as function argument', () => {

        const fn = parse('aFunction(wnd)');
        expect(function () {
            fn({ aFunction: function () { return; }, wnd: window });
        }).to.throw();
    });

    it('does not allow calling method on window', () => {

        const fn = parse('wnd.scroolTo(0)');
        expect(function () {
            fn({ wnd: window });
        }).to.throw();
    });

    it('does not allow function to return window', () => {

        const fn = parse('getWnd()');
        expect(function () {
            fn({ getWnd: function () { return window; } });
        }).to.throw();
    });

    it('does not allow assigning window', () => {

        const fn = parse('wnd = anObject');
        expect(function () {
            fn({ anObject: window });
        }).to.throw();
    });

    it('does not allow referencing window', () => {

        const fn = parse('wnd');
        expect(function () {
            fn({ wnd: window });
        }).to.throw();
    });

    it('does not allow calling functions on DOM elements', () => {

        const fn = parse('el.setAttributes("evil", "true")');
        expect(function () { fn({ el: document.documentElement }); }).to.throw();
    });

    it('does not allow calling the aliased function constructor', () => {

        const fn = parse('fnConstructor("return window ;")');
        expect(function () {
            fn({ fnConstructor: (function (): void { return; }).constructor });
        }).to.throw();
    });

    it('does not allow calling functions on Object', () => {

        const fn = parse('obj.create({})');
        expect(function () {
            fn({ obj: Object });
        }).to.throw();
    });


    /**
     * Ensurign Safe Function
     */

    it('does not allow calling call', () => {

        const fn = parse('fun.call(obj)');
        expect(function () { fn({ fun: function () { return; }, obj: {} }); }).to.throw();
    });

    it('does not allow calling aplly', () => {

        const fn = parse('fun.call(apply)');
        expect(function () { fn({ fun: function () { return; }, obj: {} }); }).to.throw();
    });


    /**
     * Unary Operators
     */

    it('parses a unary +', () => {

        expect(parse('+29')()).to.be.equal(29);
        expect(parse('+a')({ a: 29 })).to.be.equal(29);
    });

    it('replaces undefined with zero for unary+', () => {

        expect(parse('+a')({})).to.be.equal(0);
    });

    it('parses a unary !', () => {

        expect(parse('!true')()).to.be.equal(false);
        expect(parse('!29')()).to.be.equal(false);
        expect(parse('!a')({ a: false })).to.be.equal(true);
        expect(parse('!!a')({ a: false })).to.be.equal(false);
    });

    it('parses a unary -', () => {

        expect(parse('-29')()).to.be.equal(-29);
        expect(parse('-a')({ a: -29 })).to.be.equal(29);
        expect(parse('--a')({ a: -29 })).to.be.equal(-29);
        expect(parse('-a')({})).to.be.equal(0);
    });

    it('parses a unary (+, -, !) in a string', () => {

        expect(parse('"+"')()).to.be.equal('+');
        expect(parse('"-"')()).to.be.equal('-');
        expect(parse('"!"')()).to.be.equal('!');
    });


    /**
     * Multiplicative Operators
     */

    it('parses a multiplication', () => {

        expect(parse('14.5 * 2')()).to.be.equal(29);
    });

    it('parses a division', () => {

        expect(parse('58 / 2')()).to.be.equal(29);
    });

    it('parses a remainder', () => {

        expect(parse('3 % 2')()).to.be.equal(1);
    });

    it('parses several multiplicatives', () => {

        expect(parse('36 *2 % 5')()).to.be.equal(2);
    });


    /**
     * Additive Operators
     */

    it('parses an addition', () => {

        expect(parse('10 + 19')()).to.be.equal(29);
    });

    it('parses a subtraction', () => {

        expect(parse('29 - 19')()).to.be.equal(10);
    });

    it('parses multiplicatives on a higher precedence than additives',
        () => {

            expect(parse('2 + 3 * 5')()).to.be.equal(17);
            expect(parse('2 + 3 * 2 + 3')()).to.be.equal(11);
        });

    it('substitutes undefined with zero in addition', () => {

        expect(parse('a + 29')()).to.be.equal(29);
        expect(parse('10 + a')()).to.be.equal(10);
    });

    it('substitutes undefined with zero in substraction', () => {

        expect(parse('a - 29')()).to.be.equal(-29);
        expect(parse('10 - a')()).to.be.equal(10);
    });


    /**
     * Relational And Equality Operators
     */

    it('parses relational operators', () => {

        expect(parse('1 < 2')()).to.be.equal(true);
        expect(parse('1 > 2')()).to.be.equal(false);
        expect(parse('1 <= 2')()).to.be.equal(true);
        expect(parse('2 <= 2')()).to.be.equal(true);
        expect(parse('1 >= 2')()).to.be.equal(false);
        expect(parse('2 >= 2')()).to.be.equal(true);
    });

    it('parses equality operators', () => {

        expect(parse('29 == 29')()).to.be.equal(true);
        expect(parse('29 == "29"')()).to.be.equal(true);
        expect(parse('29 != 29')()).to.be.equal(false);
        expect(parse('29 === 29')()).to.be.equal(true);
        expect(parse('29 === "29"')()).to.be.equal(false);
        expect(parse('29 !== 29')()).to.be.equal(false);
    });

    it('parses relationals on a higher precedence than equality', () => {

        expect(parse('2 == "2" > 2 === "2"')()).to.be.equal(false);

        /* 2 == "2" > 2 === "2"  et non 2 == "2" > 2 === "2"
           2 ==  false  === "2"           true   >  false
              false     === "2"               1  >  0
                     false                      true
         */
    });

    it('parses additives on a higher precedence than relationals', () => {

        expect(parse('2 + 3 < 6 - 2')()).to.be.equal(false);

        /* 2 + 3 < 6 - 2     2 + 3 < 6 - 2
             5   <   4       2 +  true - 2
               false         2 +   1   - 2  =  1
         */
    });


    /**
     * Logical Operators AND and OR
     */

    it('parses logical AND', () => {

        expect(parse('true && true')()).to.be.equal(true);
        expect(parse('true && false')()).to.be.equal(false);
    });

    it('parses logical OR', () => {

        expect(parse('true || true')()).to.be.equal(true);
        expect(parse('true || false')()).to.be.equal(true);
        expect(parse('false || false')()).to.be.equal(false);
    });

    it('parses multiple ANDs', () => {

        expect(parse('true && true && true')()).to.be.equal(true);
        expect(parse('true && true && false')()).to.be.equal(false);
    });

    it('parses multiple ORs', () => {

        expect(parse('true || true || true')()).to.be.equal(true);
        expect(parse('true || true || false')()).to.be.equal(true);
        expect(parse('false || false || true')()).to.be.equal(true);
        expect(parse('false || false || false')()).to.be.equal(false);
    });

    it('short-circuits AND', () => {

        let invoked: boolean | undefined;
        const scope = { fn: function (): void { invoked = true; } };

        parse('false && fn()')(scope);

        expect(invoked).to.be.undefined;
    });

    it('short-circuits OR', () => {

        let invoked: boolean | undefined;
        const scope = { fn: function (): void { invoked = true; } };

        parse('true || fn()')(scope);

        expect(invoked).to.be.undefined;
    });

    it('parses AND with a higher precedence than OR', () => {

        expect(parse('false && true || true')()).to.be.equal(true);
    });

    it('parses OR with a lower precedence than equality', () => {

        expect(parse('1 === 2 || 2 === 2')()).to.be.true;
    });


    /**
     * The Ternary Operator
     */

    it('parses the ternary expression', () => {

        expect(parse('a === 29 ? true : false')({ a: 29 })).to.be.equal(true);
        expect(parse('a === 29 ? true : false')({ a: 28 })).to.be.equal(false);
    });

    it('parses OR with a higher precedence than ternary', () => {

        expect(parse('0 || 1 ? 0 || 2 : 0 || 3')()).to.be.equal(2);
    });

    it('parses nested ternaries', () => {

        expect(
            parse('a === 29 ? b === 29 ? "a and b" : "a" : c === 29 ? "c": "none"')({
                a: 31,
                b: 30,
                c: 29
            })).to.be.deep.equal('c');
    });


    /**
     * Altering The Precedence Order with Parentheses
     */

    it('parses parentheses altering precedence order', () => {

        expect(parse('21 * (3 - 1)')()).to.be.equal(42);
        expect(parse('false && (true || true)')()).to.be.equal(false);
        expect(parse('-((a % 2) === 0 ? 1 : 2)')({ a: 42 })).to.be.equal(-1);
    });


    /**
     * Statement
     */

    it('parses several statements', () => {

        const fn = parse('a = 1 ; b = 2 ; c = 3');
        const scope = {};
        fn(scope);
        expect(scope).to.be.deep.equal({ a: 1, b: 2, c: 3 });
    });

    it('returns the value of the last statement', () => {

        expect(parse('a = 1 ; b = 2 ; a + b')({})).to.be.equal(3);
    });


    /**
     * Filter Expressions
     */

    it('can parse filter expressions', () => {

        kernel.get('$filter').register('upcase', function (str: string) {
            return str.toUpperCase();
        });
        const fn = parse('aString | upcase');
        expect(fn({ aString: 'Hello' })).to.be.deep.equal('HELLO');
    });

    it('can parse filter chain expressions', () => {

        kernel.get('$filter').register('exclamate', function (s: string) {
            return s + '!';
        });
        const fn = parse('aString | upcase | exclamate');
        expect(fn({ aString: 'Hello' })).to.be.deep.equal('HELLO!');
    });

    it('can pass an addtional argument to filters', () => {

        kernel.get('$filter').register('repeat', function (s: string, times: number) {
            let str = '';
            for (let i = 0; i < times; i++)
                str += s;
            return str;
        });
        const fn = parse('"hello" | repeat:3');
        expect(fn()).to.be.deep.equal('hellohellohello');
    });

    it('can pass several addtional arguments to filters', () => {

        kernel.get('$filter').register(
            'surround',
            function (s: string, left: string, right: string) {
                return left + s + right;
            }
        );
        const fn = parse('"hello" | surround:"*":"!"');
        expect(fn()).to.be.deep.equal('*hello!');
    });

    it('marks integers literal', () => {

        const fn = parse('29');
        expect(fn.literal).to.be.equal(true);
    });

    it('marks strings literal', () => {

        const fn = parse('"abc"');
        expect(fn.literal).to.be.equal(true);
    });

    it('marks booleans literal', () => {

        const fn = parse('true');
        expect(fn.literal).to.be.equal(true);
    });

    it('marks arrays literal', () => {

        const fn = parse('[1, 2, aVariable]');
        expect(fn.literal).to.be.equal(true);
    });

    it('marks objects literal', () => {

        const fn = parse('{a: 1, b: aVariable}');
        expect(fn.literal).to.be.equal(true);
    });

    it('marks unary expressions non-literal', () => {

        const fn = parse('!false');
        expect(fn.literal).to.be.equal(false);
    });

    it('marks binary expressions non-literal', () => {

        const fn = parse('1 + 2');
        expect(fn.literal).to.be.equal(false);
    });

    it('marks integers constant', () => {

        const fn = parse('29');
        expect(fn.constant).to.be.equal(true);
    });

    it('marks strings constant', () => {

        const fn = parse('"abc"');
        expect(fn.constant).to.be.equal(true);
    });

    it('marks booleans constant', () => {

        const fn = parse('true');
        expect(fn.constant).to.be.equal(true);
    });

    it('marks identifiers non-constant', () => {

        const fn = parse('a');
        expect(fn.constant).to.be.equal(false);
    });

    it('marks arrays constant when elements are constant', () => {

        expect(parse('[1, 2, 3]').constant).to.be.equal(true);
        expect(parse('[1, [2, [3]]]').constant).to.be.equal(true);
        expect(parse('[1, 2, a]').constant).to.be.equal(false);
        expect(parse('[1, [2, [a]]]').constant).to.be.equal(false);
    });

    it('marks objects constant when values are constant', () => {

        expect(parse('{a: 1, b: 2}').constant).to.be.equal(true);
        expect(parse('{a: 1, b: {c: 3}}').constant).to.be.equal(true);
        expect(parse('{a: 1, b: something}').constant).to.be.equal(false);
        expect(parse('{a: 1, b: {c: something}}').constant).to.be.equal(false);
    });

    it('marks this as non-constant', () => {

        expect(parse('this').constant).to.be.equal(false);
    });

    it('marks non-computed lookup constant when object is constant', () => {

        expect(parse('{a: 1}.a').constant).to.be.equal(true);
        expect(parse('obj.a').constant).to.be.equal(false);
    });

    it('marks computed lookup constant when object and key are', () => {

        expect(parse('{a: 1}["a"]').constant).to.be.equal(true);
        expect(parse('obj["a"]').constant).to.be.equal(false);
        expect(parse('{a: 1}[something]').constant).to.be.equal(false);
        expect(parse('obj[something]').constant).to.be.equal(false);
    });

    it('marks function calls non-constant', () => {

        expect(parse('aFunction()').constant).to.be.equal(false);
    });

    it('marks filters constant if arguments are', () => {

        kernel.get('$filter').register('aFilter', function (value: any) {
            console.log(value);
            return value;
        });

        expect(parse('[1, 2, 3] | aFilter').constant).to.be.equal(true);
        expect(parse('[1, 2, a] | aFilter').constant).to.be.equal(false);
        expect(parse('[1, 2, 3] | aFilter:29').constant).to.be.equal(true);
        expect(parse('[1, 2, 3] | aFilter:a').constant).to.be.equal(false);
    });

    it('marks assignments constant when both sides are', () => {

        expect(parse('1 = 2').constant).to.be.equal(true);
        expect(parse('a = 2').constant).to.be.equal(false);
        expect(parse('1 = b').constant).to.be.equal(false);
        expect(parse('a = b').constant).to.be.equal(false);
    });

    it('marks unaries constant when arguments are constant', () => {

        expect(parse('+29').constant).to.be.equal(true);
        expect(parse('+a').constant).to.be.equal(false);
    });

    it('marks binaries constant when both arguments are constant', () => {

        expect(parse('1 + 2').constant).to.be.equal(true);
        expect(parse('1 + 2').literal).to.be.equal(false);
        expect(parse('1 + a').constant).to.be.equal(false);
        expect(parse('a + 2').constant).to.be.equal(false);
        expect(parse('a + b').constant).to.be.equal(false);
    });

    it('marks logicals constant when arguments are constant', () => {

        expect(parse('true && false').constant).to.be.equal(true);
        expect(parse('true && false').literal).to.be.equal(false);
        expect(parse('true && a').constant).to.be.equal(false);
        expect(parse('a && false').constant).to.be.equal(false);
        expect(parse('a && b').constant).to.be.equal(false);
    });

    it('marks ternaries constant when all arguments are', () => {

        expect(parse('true ? 1 : 2').constant).to.be.equal(true);
        expect(parse('a ? 1 : 2').constant).to.be.equal(false);
        expect(parse('true ? b : 2').constant).to.be.equal(false);
        expect(parse('true ? 1 : c').constant).to.be.equal(false);
        expect(parse('a ? b : c').constant).to.be.equal(false);
    });


    /**
     * External Assignment
     */

    it('allows calling assign on identifier expression', () => {

        const fn = parse('anAttribute');
        expect(fn.assign).to.be.not.undefined;

        const scope: Record<string, any> = {};
        fn.assign(scope, 25);
        expect(scope.anAttribute).to.be.equal(25);
    });

    it('allows calling assign on member expressions', () => {

        const fn = parse('anObject.anAttribute');
        expect(fn.assign).to.be.not.undefined;

        const scope: Record<string, any> = {};
        fn.assign(scope, 25);
        expect(scope.anObject).to.be.deep.equal({ anAttribute: 25 });
    });
});
