/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Filter } from './Filter';
import { Kernel } from '../Kernel';
import { Scope, WatchFn } from './Scope';
import * as Utils from '../Utils';

/* eslint-disable @typescript-eslint/no-explicit-any */

type LexerToken = { [keys: string]: string | boolean | number};
type ASTElement = Record<string, any>;

interface ParseFn {
    (): any;
    _watchDelegate?: Function;
    literal?: boolean;
    constant?: boolean;
    inputs?: any[];
}


/**
 * Quelques constantes...
 */
// Les caractère d'echappement
const ESCAPES: { [keys: string]: string } = {
    'n': '\n', 'f': '\f', 'r': '\r',
    't': '\t', 'v': '\v', '\'': '\'',
    '"': '"'
};

const AST = {
    'Program': 'Program',
    'Literal': 'Literal',
    'ArrayExpression': 'ArrayExpression',
    'ObjectExpression': 'ObjectExpression',
    'Property': 'Property',
    'Identifier': 'Identifier',
    'ThisExpression': 'ThisExpression',
    'MemberExpression': 'MemberExpression',
    'CallExpression': 'CallExpression',
    'AssignmentExpression': 'AssignmentExpression',
    'UnaryExpression': 'UnaryExpression',
    'BinaryExpression': 'BinaryExpression',
    'LogicalExpression': 'LogicalExpression',
    'ConditionalExpression': 'ConditionalExpression',
    'KJSValueParameter': 'KJSValueParameter',
    constants: ({
        'null': { type: 'Literal', value: null },
        'true': { type: 'Literal', value: true },
        'false': { type: 'Literal', value: false },
        'this': { type: 'ThisExpression' }
    } as any)
};

const CALL = Function.prototype.call,
      APPLY = Function.prototype.apply,
      BIND = Function.prototype.bind;

const OPERATORS: Record<string, boolean> = {
    '+': true, '!': true, '-': true,
    '*': true, '/': true, '%': true,
    '=': true,
    '==': true, '!=': true,
    '===': true, '!==': true,
    '<': true, '>': true,
    '<=': true, '>=': true,
    '&&': true, '||': true,
    '|': true
};


/**
 * Un programme vide est `literal`, un programme non-vide est `literal` s'il a juste une expression
 * de type `literal`, `array` ou un objet.
 * @param ast AST à tester
 */
function isLiteral (ast: ASTElement): boolean {

    return ast.body.length === 0 || // programme vide
        ast.body.length === 1 && // une seule instruction
        (ast.body[0].type === AST.Literal ||
            ast.body[0].type === AST.ArrayExpression ||
            ast.body[0].type === AST.ObjectExpression);
}


/**
 * Parcours l'AST et détermine si le resultat sera une constante ou pas.
 * @param ast AST
 * @param filter Filtres
 */
function markConstantAndWatchExpressions (ast: ASTElement, filter: Filter): void {

    let allConstants: boolean;
    let argsToWatch: ASTElement[];

    switch (ast.type) {

        case AST.Program:
            allConstants = true;
            ast.body.forEach(function (expr: ASTElement) {
                markConstantAndWatchExpressions(expr, filter);
                allConstants = allConstants && expr.constant;
            });
            ast.constant = allConstants;
            break;

        case AST.Literal:
            ast.constant = true;
            ast.toWatch = [];
            break;

        case AST.Identifier:
            ast.constant = false;
            ast.toWatch = [ast];
            break;

        case AST.ArrayExpression:

            allConstants = true;
            argsToWatch = [];
            ast.elements.forEach(function (element: ASTElement) {
                markConstantAndWatchExpressions(element, filter);
                allConstants = allConstants && element.constant;
                if (!element.constant)
                    argsToWatch.push(...element.toWatch);
            });
            ast.constant = allConstants;
            ast.toWatch = argsToWatch;
            break;

        case AST.ObjectExpression:
            allConstants = true;
            argsToWatch = [];
            ast.properties.forEach(function (property: ASTElement) {
                markConstantAndWatchExpressions(property.value, filter);
                allConstants = allConstants && property.value.constant;
                if (!property.value.constant)
                    argsToWatch.push(...property.value.toWatch);
            });
            ast.constant = allConstants;
            ast.toWatch = argsToWatch;
            break;

        case AST.ThisExpression:
            ast.constant = false;
            ast.toWatch = [];
            break;

        case AST.MemberExpression:
            markConstantAndWatchExpressions(ast.object, filter);
            if (ast.computed)
                markConstantAndWatchExpressions(ast.property, filter);
            ast.constant = ast.object.constant &&
                (!ast.computed || ast.property.constant);
            ast.toWatch = [ast];
            break;

        case AST.CallExpression: {

            const stateless = ast.filter && !(filter.get(ast.callee.name) as any).$stateful;
            allConstants = stateless ? true : false;
            argsToWatch = [];
            ast.arguments.forEach(function (arg: ASTElement) {
                markConstantAndWatchExpressions(arg, filter);
                allConstants = allConstants && arg.constant;
                if (!arg.constant)

                    argsToWatch.push(...argsToWatch, arg.toWatch);
            });
            ast.constant = allConstants;
            ast.toWatch = stateless ? argsToWatch : [ast];
            break;
        }

        case AST.AssignmentExpression:
            markConstantAndWatchExpressions(ast.left, filter);
            markConstantAndWatchExpressions(ast.right, filter);
            ast.constant = ast.left.constant && ast.right.constant;
            ast.toWatch = [ast];
            break;

        case AST.UnaryExpression:
            markConstantAndWatchExpressions(ast.argument, filter);
            ast.constant = ast.argument.constant;
            ast.toWatch = ast.argument.toWatch;
            break;

        case AST.BinaryExpression:
            markConstantAndWatchExpressions(ast.left, filter);
            markConstantAndWatchExpressions(ast.right, filter);
            ast.constant = ast.left.constant && ast.right.constant;
            ast.toWatch = ast.left.toWatch.concat(ast.right.toWatch);
            break;

        case AST.LogicalExpression:
            markConstantAndWatchExpressions(ast.left, filter);
            markConstantAndWatchExpressions(ast.right, filter);
            ast.constant = ast.left.constant && ast.right.constant;
            ast.toWatch = [ast];
            break;

        case AST.ConditionalExpression:
            markConstantAndWatchExpressions(ast.test, filter);
            markConstantAndWatchExpressions(ast.consequent, filter);
            markConstantAndWatchExpressions(ast.alternate, filter);
            ast.constant = ast.test.constant && ast.consequent.constant &&
                ast.alternate.constant;
            ast.toWatch = [ast];
            break;
    }
}


/**
 * getInputs
 * @param ast AST
 */
function getInputs (ast: ASTElement): ASTElement[] | undefined {

    if (ast.length !== 1)
        return;
    const candidate = ast[0].toWatch;
    if (candidate.length !== 1 || candidate[0] !== ast[0])
        return candidate;
}


/**
 * C'est un watcher classique, sauf qu'il se supprime dès qu'il est exécuté
 * @param scope le scope
 * @param listenerFn le listener
 * @param valueEq ?
 * @param watchFn le watcher
 */
function constantWatchDelegate (
    scope: Record<string, any>,
    listenerFn: Function,
    valueEq: any,
    watchFn: Function
): Function {

    const unwatch = scope.$watch(
        function () {
            return watchFn(scope);
        },
        function (newValue: any, oldValue: any, scope: Record<string, any>) {
            if (Utils.isFunction(listenerFn))
                listenerFn.apply(scope, [newValue, oldValue, scope]);
            unwatch();
        },
        valueEq
    );
    return unwatch;
}


/**
 * C'est un watcher classique, sauf qu'il se supprime dès qu'il est exécuté
 * @param scope le scope
 * @param listenerFn le listener
 * @param valueEq ?
 * @param watchFn le watcher
 */
function oneTimeWatchDelegate (
    scope: Record<string, any>,
    listenerFn: Function,
    valueEq: any,
    watchFn: Function
): Function {

    let lastValue: any;

    const unwatch = scope.$watch(
        function () {
            return watchFn(scope);
        },
        function (newValue: any, oldValue: any, scope: Scope) {

            lastValue = newValue;

            // On appel le listener si défini
            if (Utils.isFunction(listenerFn))
                listenerFn.apply(scope, [newValue, oldValue, scope]);
            // On ne détruit le watcher que s'il a retourné une valeur et que
            // celle-ci est toujours définie à la fin du digest.
            if (newValue !== undefined) {
                scope._postDigest(function () {
                    if (lastValue !== undefined)
                        unwatch();
                });
            }
        },
        valueEq
    );
    return unwatch;
}

/**
 * Pas pour tous les `literal`, justes pour les objets et les tableaux.
 *
 * Les nombres, chaînes de caractères et booleans sont constants et utilisent donc
 * `constantWatchDelegate`
 * @param scope le scope
 * @param listenerFn le listener
 * @param valueEq ?
 * @param watchFn le watcher
 */
function oneTimeLiteralWatchDelegate (
    scope: Record<string, any>,
    listenerFn: Function,
    valueEq: any,
    watchFn: Function
): Function {

    function isAllDefined (val: any): boolean {

        // val est un array
        if (Array.isArray(val))
            return !val.some((e: any): boolean => { return e === undefined; });
        else {
            return Object.getOwnPropertyNames(val).every(function (property) {
                return val[property] !== undefined;
            });
        }
    }

    const unwatch = scope.$watch(
        function () {
            return watchFn(scope);
        },
        function (newValue: any, oldValue: any, scope: Record<string, any>) {

            // On appel le listener si défini
            if (Utils.isFunction(listenerFn))
                listenerFn.apply(scope, [newValue, oldValue, scope]);
            // On ne détruit le watcher que s'il a retourné une valeur et que
            // celle-ci est toujours définie à la fn du digest.
            if (isAllDefined(newValue)) {
                scope._postDigest(function () {
                    if (isAllDefined(newValue))
                        unwatch();
                });
            }
        },
        valueEq
    );
    return unwatch;
}

function expressionInputDirtyCheck (newValue: any, oldValue: any): boolean {

    return newValue === oldValue ||
        (typeof newValue === 'number' && typeof oldValue === 'number' &&
            Number.isNaN(newValue) && Number.isNaN(oldValue));
}


/**
 * @param scope le scope
 * @param listenerFn le listener
 * @param valueEq ?
 * @param watchFn le watcher
 * @returns {*}
 */
function inputsWatchDelegate (
    scope: Record<string, any>,
    listenerFn: Function,
    valueEq: any,
    watchFn: WatchFn
): Function {

    const inputExpressions = watchFn.inputs;

    // On initialise un tableau contenant les anciennes valeurs
    const oldValues: any[] = [];
    for (let i = 0; i < inputExpressions.length; i++)
        oldValues.push(function () { return function (): void { return; }; });

    let lastResult: any;

    return scope.$watch(function () {

        let changed = false;
        inputExpressions.forEach(function (inputExpr: Function, i: any) {
            const newValue = inputExpr(scope);
            if (changed || !expressionInputDirtyCheck(newValue, oldValues[i])) {
                changed = true;
                oldValues[i] = newValue;
            }
        });
        if (changed)
            lastResult = watchFn(scope);
        return lastResult;
    }, listenerFn, valueEq);
}




/**
 * Des helpers...
 */

// ensureSafeMemberName (name)
function ensureSafeMemberName (name: string): void {

    if (name === 'constructor' || name === '__proto__' ||
        name === '__defineGetter__' || name === '__defineSetter__' ||
        name === '__lookupGetter__' || name === '__lookupSetter__')
        throw 'Attempting to access a disallowed field in KephaJS expressions!';
}

// ensureSafeObject (obj)
function ensureSafeObject (obj: Record<string, any>): Record<string, any>  {

    if (obj) {
        if (obj.window === obj)
            throw 'Referencing window in KephaJS expressions is disallowed!';
        else if (obj.children &&
            (obj.nodeName || (obj.prop && obj.attr && obj.find)))
            throw 'Referencing DOM nodes in KephaJS expressions is disallowed!';
        else if (obj.constructor === obj)
            throw 'Referencing Function in KephaJS expressions is disallowed!';
        else if (obj === Object)
            throw 'Referencing Object in KephaJS expressions is disallowed!';
    }

    return obj;
}

// ensureSafeFunction (obj)
function ensureSafeFunction (obj: Record<string, any>): Record<string, any> {

    if (obj) {
        if (obj.constructor === obj)
            throw 'Referencing Function in KephaJS expressions is desallowed!';
        else if (obj === CALL || obj === APPLY || obj === BIND) {
            throw 'Referencing call, apply, or bind in KephaJS expressions is '
            + 'disallowed!';
        }
    }

    return obj;
}

// ifDefined (value, defaultValue)
function ifDefined (value: any, defaultValue: any): boolean {

    return typeof value === 'undefined' ? defaultValue : value;
}



/**
 * isAssignable (ast)
 * @param {Object} ast AST a tester
 * @returns {Boolean}
 */
function isAssignable (ast: ASTElement): boolean {

    return ast.type === AST.Identifier || ast.type == AST.MemberExpression;
}


/**
 * Retourne un AST assignable
 * @param {Object} ast AST à transformer en assignable
 * @returns {Object} AST assignable
 */
function assignableAST (ast: ASTElement): ASTElement | undefined {

    if (ast.body.length == 1 && isAssignable(ast.body[0])) {
        return {
            type: AST.AssignmentExpression,
            left: ast.body[0],
            right: { type: AST.KJSValueParameter }
        };
    }
}



/*
 * Parse l'expression originale et renvoit un tableau des tokens parsés.
 *
 *         'a + b'
 *      -> [
 *             {text: 'a', identifier: true},
 *             {text: '+'},
 *             {text: 'a', identifier: true}
 *         ]
 */
class Lexer {

    // La chaîne de caractères originale
    private _text = '';
    // L'index du caractère courant
    private _index = 0;
    // Le caractère courant
    private _ch: string | undefined = undefined;
    // Le résultat
    private _tokens: LexerToken[] = [];

    /**
     * Tokenise l'expression text.
     * @param {String} text Expression à tokeniser
     * @returns {Array} tokens
     */
    lex (text: string): LexerToken[] {

        // La chaîne de caractères originale
        this._text = text;
        // L'index du caractère courant
        this._index = 0;
        // Le caractère courant
        this._ch = undefined;
        // Le résultat
        this._tokens = [];

        // On parcourt toute la chaîne
        while (this._index < this._text.length) {

            this._ch = this._text.charAt(this._index);

            // Il s'agit d'un nombre
            if (this._isNumber(this._ch) ||
                (this._is('.') && this._isNumber(this._peek() as string)))
                this._readNumber();
            // il s'agit d'une chaîne de caractères
            else if (this._is('\'"'))
                this._readString(this._ch);
            // il s'agit d'un tableau, d'un objet, d'une fonction, d'un opérateur ternaire ou de la
            // fin d'une instruction
            else if (this._is('[],{}:.()?;')) {
                this._tokens.push({
                    text: this._ch
                });
                this._index++;
            }
            // il s'agit d'un identifier
            else if (this._isIdent(this._ch))
                this._readIdent();
            // on ignore les whitespaces
            else if (this._isWhitespace(this._ch))
                this._index++;
            // Sinon
            else {

                const ch = this._ch,
                      ch2 = this._ch + this._peek(),
                      ch3 = this._ch + this._peek() + this._peek(2),
                      op = OPERATORS[ch],
                      op2 = OPERATORS[ch2],
                      op3 = OPERATORS[ch3];
                // Si c'est un opérateur
                if (op || op2 || op3) {
                    const token = op3 ? ch3 : (op2 ? ch2 : ch);
                    this._tokens.push({ text: token });
                    this._index += token.length;
                }
                // sinon
                else {
                    throw new Error(
                        '$parse Error: Unexpected next character: "'
                        + this._ch + '" in "' + text + '"'
                    );
                }
            }
        }

        // On renvoit les tokens
        return this._tokens;
    }


    /**
     * Renvoit true si le ch courant (this._ch) est un des caractères de chs
     * @param chs Liste de caractères
     */
    private _is (chs: string): boolean {

        return chs.indexOf(this._ch as string) >= 0;
    }


    /**
     * ch est-il un nombre ?
     * @param ch Caractère à tester
     */
    private _isNumber (ch: string): boolean {

        return '0' <= ch && ch <= '9';
    }


    /**
     * Ce que peut-être ce qui suit 'e' dans une notation scientifique
     * @param ch Caractère à tester
     */
    private _isExpOperator (ch: string): boolean {

        return ch === '-' || ch === '+' || this._isNumber(ch);
    }


    /**
     * Est un identifieur (variable, mot clé, ...). Commence par une lettre (majuscule ou minuscule)
     * ou par _ ou $
     * @param ch Caractère à tester
     */
    private _isIdent (ch: string): boolean {

        return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch === '_' || ch === '$';
    }


    /**
     * Whitespace
     * @param ch Caractère à tester
     */
    private _isWhitespace (ch: string): boolean {

        return ch === ' ' || ch === '\r' || ch === '\t' || ch === '\n' || ch === '\v' ||
               ch === '\u00A0';
    }


    /**
     * Retourne le caractère à la position (`this._index + n`) sans changer l'index. S'il n'y a pas
     * de caractère à cette position, la méthode retournera `false`.
     * n vaut 1 par défaut.
     * @param n Position à partir de l'index (vaut 1 par défaut)
     */
    private _peek (n = 1): string | boolean {

        return this._index + n < this._text.length ?
            this._text.charAt(this._index + n)
            : false;
    }


    /**
     * Lit un nombre à partir de l'index courant
     */
    private _readNumber (): void {

        let number = '';

        // On parcourt la chaîne
        while (this._index < this._text.length) {

            const ch = this._text.charAt(this._index).toLowerCase(); // ainsi E -> e

            // Si c'est un nombre, on l'ajoute
            // (on accepte le point pour les flottants)
            if (ch === '.' || this._isNumber(ch))
                number += ch;
            // sinon, ...
            else {

                const nextCh = this._peek();
                const prevCh = number.charAt(number.length - 1);
                // Début de notation scientifique
                if (ch === 'e' && this._isExpOperator(nextCh as string))
                    number += ch;
                // Suit le 'e' et fait partie de la notation scientifique
                else if (this._isExpOperator(ch) && prevCh === 'e' &&
                    nextCh && this._isNumber(nextCh as string))
                    number += ch;
                // Suit le 'e', mais n'est pas valide
                else if (this._isExpOperator(ch) && prevCh === 'e' &&
                    (!nextCh || !this._isNumber(nextCh as string)))
                    throw 'Invalid exponent';
                // Sinon c'est qu'on a atteint la fin du nombre
                else
                    break;
            }
            this._index++;
        }

        // On sauve le token
        this._tokens.push({
            text: number,
            value: Number(number)
        });
    }


    /**
     * Lit une chaîne de caractères
     * @param quote `'` ou `"` selon ce qui a été utilisé pour ouvrir la chaîne
     */
    private _readString (quote: string): void {

        this._index++;
        let string = '',       // string lue
            rawString = quote, // source
            escape = false;

        // On parcours la chaîne à analyser
        while (this._index < this._text.length) {

            const ch = this._text.charAt(this._index);
            rawString += ch;

            // On est dans l'"escape mode"
            if (escape) {

                // unicode escape sequences
                if (ch === 'u') {

                    const hex = this._text.substring(this._index + 1, this._index + 5);
                    // On vérifie que la séquence est valide
                    if (!hex.match(/[\da-f]{4}/i))
                        throw 'Invalid unicode escape';
                    this._index += 4;
                    string += String.fromCharCode(parseInt(hex, 16));
                }
                // \n, \t, \v, ...
                else {
                    const replacement = ESCAPES[ch];
                    if (replacement)
                        string += replacement;
                    else
                        string += ch;
                }
                escape = false;
            }
            // On s'arrête si on atteint la fin de la chaîne
            else if (ch === quote) {

                this._index++;
                this._tokens.push({
                    text: rawString,
                    value: string
                });
                return;
            }
            // On passe en "escape mode"
            else if (ch === '\\')
                escape = true;
            // Sinon on ajoute le caractère au string
            else
                string += ch;

            this._index++;
        }

        throw 'Unmatched quote';
    }


    /**
     * Lit un identifier
     */
    private _readIdent (): void {

        let text = '';

        // On parcourt la chaîne de l'expression à parser
        while (this._index < this._text.length) {

            const ch = this._text.charAt(this._index);
            if (this._isIdent(ch) || this._isNumber(ch))
                text += ch;
            else
                break;
            this._index++;
        }

        const token = {
            text: text,
            identifier: true
        };

        this._tokens.push(token);
    }
}


/**
 * Constuit un Abstract Syntax Tree (AST arbre syntaxique abstrait) à partir d'un lexer.
 *
 *         [
 *             {text: 'a', identifier: true},
 *             {text: '+'},
 *             {text: 'a', identifier: true}
 *         ]
 *
 *      -> {
 *             type: AST.BinaryExpression,
 *             operator: '+',
 *             left: {
 *                 type: AST.Identifier,
 *                 name: 'a'
 *             },
 *             right: {
 *                 type: AST.Identifier,
 *                 name: 'b'
 *             }
 *         }
 */
class ASTBuilder {

    private _lexer: Lexer;
    private _tokens: LexerToken[] = [];

    /**
     * Enregistrer le lexer à transformer
     * @param lexer Le lexer
     */
    constructor (lexer: Lexer) {

        this._lexer = lexer;
    }

    /**
     * Construit un AST à partir d'une expression text
     * @param {String} text Expression dont on veut l'AST
     * @returns {Object} AST
     */
    ast (text: string): ASTElement {

        // On génère les tokens avec le lexer
        this._tokens = this._lexer.lex(text);

        return this._program() as ASTElement;
    }

    /**
     * Retourne la racine d'un AST, mais surtout la suite des instructions
     * @returns {Object}
      */
    private _program (): ASTElement | undefined {

        const body = [],
              test = true;

        while (test) {

            // Tant qu'il y a des tokens, on les ajoute au body
            if (this._tokens.length)
                body.push(this._filter());
            // S'il n'y a pas de ;, on retourne l'AST
            if (!this._expect(';'))
                return { type: AST.Program, body: body };
        }
    }


    /**
     * Détecte si l'on a affaire à une affectation et génère l'arbre correpsondant. Retournera juste
     * l'instruction s'il n'y a pas d'affection
     */
    private _assignment (): ASTElement {

        const left = this._ternary();

        if (this._expect('=')) {

            const right = this._ternary();
            return { type: AST.AssignmentExpression, left: left, right: right };
        }

        return left;
    }


    /**
     * AST.primary ()
     *
     * Créé l'arbre d'une opération élémentaire (une instruction).
     */
    private _primary (): ASTElement {

        let primary;

        // Précéance des parenthèses
        if (this._expect('(')) {
            primary = this._filter();
            this._consume(')');
        }
        // Si c'est un tableau
        else if (this._expect('['))
            primary = this._arrayDeclaration();
        // si c'est un objet
        else if (this._expect('{'))
            primary = this._object();
        // sinon si le token correspond à une des constantes
        else if (Object.prototype.hasOwnProperty.call(
            AST.constants, this._tokens[0].text as string
        ))
            primary = AST.constants[this._consume().text];
        // sinon s'il s'agit d'un identifier
        else if ((this._peek() as LexerToken).identifier)
            primary = this._identifier();
        // sion on génère une bonne branche AST.literal avec une valeur constante
        else
            primary = this._constant();

        // membres d'un objet ou arguments d'une fonction
        let next;
        while ((next = this._expect('.', '[', '('))) {

            // membres computed
            if (next.text === '[') {
                primary = {
                    type: AST.MemberExpression,
                    object: primary,
                    property: this._primary(),
                    computed: true
                };
                this._consume(']');
            }
            // membres non computed
            else if (next.text === '.') {
                primary = {
                    type: AST.MemberExpression,
                    object: primary,
                    property: this._identifier(),
                    computed: false
                };
            }
            // arguments d'une fonction
            else if (next.text === '(') {
                primary = {
                    type: AST.CallExpression,
                    callee: primary,
                    arguments: this._parseArguments()
                };
                this._consume(')');
            }
        }

        return primary;
    }


    /**
     * Analyse un tableau
     */
    private _arrayDeclaration (): ASTElement {

        const elements: ASTElement[] = [];

        // On regarde si le tableau est vide ou s'il contient des éléments que l'on
        // devra stocker
        if (!this._peek(']')) {
            do {
                // On rencontre ] après , : on s'arrête (ex: [1, 2, 3, ])
                if (this._peek(']'))
                    break;
                // On ajoute l'élément
                elements.push(this._assignment());
            } while (this._expect(','));
        }

        // Le ] est obligatoire
        this._consume(']');
        return { type: AST.ArrayExpression, elements: elements };
    }


    /**
     * Analyse un objet
     */
    private _object (): ASTElement {

        const properties: ASTElement[] = [];

        // On regarde si l'objet contient des propriétés que l'on devra stocker
        if (!this._peek('}')) {
            do {
                const property: ASTElement = { type: AST.Property };
                if ((this._peek() as LexerToken).identifier)
                    property.key = this._identifier();
                else
                    property.key = this._constant();
                this._consume(':');
                property.value = this._assignment();
                properties.push(property);
            } while (this._expect(','));
        }

        // Le } est obligatoire
        this._consume('}');
        return { type: AST.ObjectExpression, properties: properties };
    }


    /**
     * Parse les arguments d'une fonction
     */
    private _parseArguments (): ASTElement {

        const args: ASTElement[] = [];

        // S'il y a des arguments
        if (!this._peek(')')) {
            do
                args.push(this._assignment());
            while (this._expect(','));
        }

        return args;
    }


    /**
     * Identité à consommer (variable, propriété, ...)
     */
    private _identifier (): ASTElement {

        return { type: AST.Identifier, name: this._consume().text };
    }


    /**
     * Correspond aux nombres, flottants ou string
     */
    private _constant (): ASTElement {

        return { type: AST.Literal, value: this._consume().value };
    }


    /**
     * Opérateurs n'ayant qu'une opérande
     * @returns {Object}
     */
    private _unary (): ASTElement {

        let token;
        if ((token = this._expect('+', '!', '-'))) {
            return {
                type: AST.UnaryExpression,
                operator: token.text as string,
                argument: this._unary()
            };
        }
        else
            return this._primary();
    }


    /**
     * Opérateurs ayant deux opérandes. `*`, `/` et `%` on la même précéance, ils sont donc appliqués
     * dans l'ordre
     */
    private _multiplicative (): ASTElement {

        let left = this._unary();

        let token;
        while ((token = this._expect('*', '/', '%'))) {
            left = {
                type: AST.BinaryExpression,
                left: left,
                operator: token.text,
                right: this._unary()
            };
        }

        return left;
    }


    /**
     * Opérateurs ayant deux opérandes. `+` et `-` on la même précéance, ils sont donc appliqués dans
     * l'ordre. Mais il le seront après les opérateurs de multiplication.
     */
    private _additive (): ASTElement {

        let left = this._multiplicative();

        let token;
        while ((token = this._expect('+', '-'))) {
            left = {
                type: AST.BinaryExpression,
                left: left,
                operator: token.text,
                right: this._multiplicative()
            };
        }

        return left;
    }


    /**
     * Ont une préséance plus faible que les opérateurs `+` et `-`
     */
    private _relational (): ASTElement {

        let left = this._additive();

        let token;
        while ((token = this._expect('<', '>', '<=', '>='))) {
            left = {
                type: AST.BinaryExpression,
                left: left,
                operator: token.text,
                right: this._additive()
            };
        }

        return left;
    }


    /**
     * Ont une précéance plus faible que les operateurs relationnels
     */
    private _equality (): ASTElement {

        let left = this._relational();

        let token;
        while ((token = this._expect('==', '!=', '===', '!=='))) {
            left = {
                type: AST.BinaryExpression,
                left: left,
                operator: token.text,
                right: this._relational()
            };
        }

        return left;
    }


    /**
     * A une précéance plus faible que l'égalité
     */
    private _logicalAND (): ASTElement {

        let left = this._equality();

        let token;
        while ((token = this._expect('&&'))) {
            left = {
                type: AST.LogicalExpression,
                left: left,
                operator: token.text,
                right: this._equality()
            };
        }

        return left;
    }


    /**
     * À une précéance plus faible que AND et l'égalité
     */
    private _logicalOR (): ASTElement {

        let left = this._logicalAND();

        let token;
        while ((token = this._expect('||'))) {
            left = {
                type: AST.LogicalExpression,
                left: left,
                operator: token.text,
                right: this._logicalAND()
            };
        }

        return left;
    }


    /**
     * L'opérateur ternaire est en bas de l'échelle des précéance
     */
    private _ternary (): ASTElement {

        // Sert de fallback, s'il n'y a pas d'opérateur ternaire
        const test = this._logicalOR();

        if (this._expect('?')) {

            const consequent = this._assignment();

            // L'opérateur doit être complet
            if (this._consume(':')) {

                const alternate = this._assignment();
                return {
                    type: AST.ConditionalExpression,
                    test: test,
                    consequent: consequent,
                    alternate: alternate
                };
            }
        }

        return test;
    }


    /**
     * Filter
     */
    private _filter (): ASTElement {

        let left = this._assignment();

        // On parcours les filtres
        while (this._expect('|')) {

            const args = [left];
            left = {
                type: AST.CallExpression,
                callee: this._identifier(),
                arguments: args,
                filter: true
            };
            while (this._expect(':'))
                args.push(this._assignment());
        }

        return left;
    }


    /**
     * Si le premier token de la liste correspond à `e`, on le retourne et on le retire de la liste.
     * @param e1 expr
     * @param e2 expr
     * @param e3 expr
     * @param e4 expr
     * @returns {Object}
     */
    _expect (e1?: string, e2?: string, e3?: string, e4?: string): LexerToken | undefined {

        const token = this._peek(e1, e2, e3, e4);
        if (token)
            return this._tokens.shift();
    }


    /**
     * Fait comme this._expect, mais lève une exception si `e` n'est pas trouvé, ainsi si `e` est
     * `undefined`, consume renvera le premier token de liste (ie le prochain à analyser).
     * @param e expr
     */
    private _consume (e?: string): ASTElement {

        const token = this._expect(e);

        if (!token)
            throw 'Unexpected. Expecting: ' + e;

        return token;
    }


    /**
     * Retourne le premier token s'il correspond à `e` (ou si `e` vaut false)
     * @param e1 expr
     * @param e2 expr
     * @param e3 expr
     * @param e4 expr
     */
    private _peek (e1?: string, e2?: string, e3?: string, e4?: string): LexerToken | undefined {

        if (this._tokens.length > 0) {

            const text = this._tokens[0].text;

            if (text === e1 || text === e2 || text === e3 || text === e4 ||
                (!e1 && !e2 && !e3 && !e4))
                return this._tokens[0];
        }
    }
}


/**
 * Compile un AST en une fonction Javascript.
 *
 *         {
 *             type: AST.BinaryExpression,
 *             operator: '+',
 *             left: {
 *                 type: AST.Identifier,
 *                 name: 'a'
 *             },
 *             right: {
 *                 type: AST.Identifier,
 *                 name: 'b'
 *             }
 *         }
 *
 *      -> function (scope) {
 *             return scope.a + scope.b ;
 *         }
 */
class ASTCompiler {

    private _astBuilder: ASTBuilder;
    private _filter: Filter;
    private _stage = '';
    private _state: { [keys: string]: any} = {};
    private _stringEscapeRegex: RegExp;

    /**
     * Initialise le compiler
     * @param astBuilder AST
     * @param filter Service filter
     */
    constructor (astBuilder: ASTBuilder, filter: Filter) {

        this._astBuilder = astBuilder;
        this._filter = filter;

        this._stringEscapeRegex = /[^ a-zA-Z0-9]/g;
    }


    /**
     * Compile l'expression text
     * @param text Expression à compiler
     * @returns {Function}
     */
    compile (text: string): ParseFn {

        let extra = '';

        // Récupère l'AST avec l'AST Builder
        const ast = this._astBuilder.ast(text);

        markConstantAndWatchExpressions(ast, this._filter);

        // Contient les éléments de la fonctions que l'on génère
        this._state = {
            nextId: 0,    // Permet de créer des variables uniques
            fn: {
                body: [],   // Le corps de la fonction
                vars: [],   // Variables à déclarer au début de la fonction
            },
            filters: {},  // Filtres appelés par l'expression
            assign: { body: [], vars: [] },
            inputs: []
        };

        // Compilation des inputs fonctions
        this._stage = 'inputs';
        const inputs = getInputs(ast.body);
        if (Array.isArray(inputs)) {
            inputs.forEach((input, idx) => {

                const inputKey = 'fn' + idx;
                this._state[inputKey] = { body: [], vars: [] };
                this._state.computing = inputKey;
                this._state[inputKey]
                    .body.push('return ' + this._recurse(input) + ';');
                this._state.inputs.push(inputKey);
            });
        }

        // Fonction assign()
        this._stage = 'assign';
        const assignable = assignableAST(ast);
        if (assignable) {

            this._state.computing = 'assign';
            this._state.assign.body.push(this._recurse(assignable));
            extra = 'fn.assign=function(s,v,l){'
                + (this._state.assign.vars.length ?
                    'let ' + this._state.assign.vars.join(',') + ';'
                    : ''
                ) + this._state.assign.body.join('')
                + '};';
        }

        // On génère le corps de la fonction
        this._stage = 'main';
        this._state.computing = 'fn';
        this._recurse(ast);

        // On génère la fonction
        const fnString = this._filterPrefix()
            + 'let fn=function(s,l){'
            + (this._state.fn.vars.length ? // On déclare les variables
                'let ' + this._state.fn.vars.join(',') + ';'
                : ''
            ) + this._state.fn.body.join('')
            + '};'
            + this._watchFns()
            + extra
            + ' return fn;';

        const fn = new Function(
            'ensureSafeMemberName',
            'ensureSafeObject',
            'ensureSafeFunction',
            'ifDefined',
            'filter',
            'Utils',
            fnString
        )(
            ensureSafeMemberName,
            ensureSafeObject,
            ensureSafeFunction,
            ifDefined,
            this._filter,
            Utils
        );

        fn.literal = isLiteral(ast);
        fn.constant = ast.constant;
        return fn;
    }


    /**
     * Génère les instructions à partir d'`ast`. `Context` est le contexte courant et si `create`
     * vaut `true`, alors on créra les propriétés si l'expression cherche à utiliser des propriétés
     * qui n'existent pas.
     * @param ast AST à parcourir
     * @param context Contexte courant
     * @param create Force la création des propriétés
     */
    private _recurse (ast: ASTElement, context?: Record<string, any>, create?: boolean): string {

        let intoId;

        switch (ast.type) {

            // Racine du programme
            case AST.Program: {

                // On extrait la dernière expression
                const last = ast.body.slice(-1)[0];
                // On parcours toutes les expressions, sauf la dernière
                ast.body.slice(0, -1).forEach((stmt: ASTElement) => {
                    this._state[this._state.computing]
                        .body.push(this._recurse(stmt), ';');
                }, this);
                this._state[this._state.computing].body.push(
                    'return ', this._recurse(last), ';');
                break;
            }

            // Entier, flottant, string, ...
            case AST.Literal:
                return this._escape(ast.value);

            // Tableau
            case AST.ArrayExpression: {
                const elements = ast.elements.map((element: ASTElement) => {
                    return this._recurse(element);
                });
                return '[' + elements.join(',') + ']';
            }

            // Objet
            case AST.ObjectExpression: {

                const properties = ast.properties.map((property: ASTElement) => {
                    // On regarde le type de propriété (xyz: ou 'xyz':)
                    const key = property.key.type === AST.Identifier ?  // xyz: ?
                        property.key.name
                        : this._escape(property.key.value);
                    const value = this._recurse(property.value);
                    return key + ':' + value;
                });
                return '{' + properties.join(',') + '}';
            }

            // Identificateur
            case AST.Identifier: {

                ensureSafeMemberName(ast.name);
                // on créé une variable intermédiaire : si le scope n'est pas passé, on retournera undefined
                intoId = this._nextId();
                // Doit-on utiliser locals ?
                let localsCheck;
                if (this._stage === 'inputs')
                    localsCheck = 'false';
                else
                    localsCheck = this._getHasOwnProperty('l', ast.name);
                // Proprité dans locals
                this._if(localsCheck,
                    this._assign(intoId, this._nonComputedMember('l', ast.name))
                );
                // On créé la propriété si elle n'existe pas (si demandé)
                if (create) {
                    this._if(this._not(localsCheck)
                        + ' && s && '
                        + this._not(this._getHasOwnProperty('s', ast.name)),
                        this._assign(this._nonComputedMember('s', ast.name), '{}'));
                }
                // On la prend alors dans le scope
                this._if(this._not(localsCheck) + ' && s',
                    this._assign(intoId, this._nonComputedMember('s', ast.name))
                );
                // Contexte transmis
                if (context) {
                    context.context = localsCheck + '?l:s';
                    context.name = ast.name;
                    context.computed = false;
                }
                this._addEnsureSafeObject(intoId);
                return intoId;
            }

            // This : se réfère explicitement au scope
            case AST.ThisExpression:
                return 's';

            // Membres d'un objet
            case AST.MemberExpression: {

                intoId = this._nextId();
                const left = this._recurse(ast.object, undefined, create);

                if (context)
                    context.context = left;
                // computed
                if (ast.computed) {
                    const right = this._recurse(ast.property);
                    this._addEnsureSafeMemberName(right);
                    // Créé le membre si besoin et si demandé
                    if (create) {
                        this._if(this._not(this._computedMember(left, right)),
                            this._assign(this._computedMember(left, right), '{}'));
                    }
                    this._if(left,
                        this._assign(intoId,
                            'ensureSafeObject(' + this._computedMember(left, right) + ')')
                    );
                    if (context) {
                        context.name = right;
                        context.computed = true;
                    }
                }
                // Non-computed
                else {
                    ensureSafeMemberName(ast.property.name);
                    if (create) {
                        this._if(
                            this._not(this._nonComputedMember(left, ast.property.name)),
                            this._assign(this._nonComputedMember(left, ast.property.name),
                                '{}')
                        );
                    }
                    this._if(left,
                        this._assign(intoId,
                            'ensureSafeObject('
                            + this._nonComputedMember(left, ast.property.name) + ')')
                    );
                    if (context) {
                        context.name = ast.property.name;
                        context.computed = false;
                    }
                }
                return intoId;
            }

            // Appel d'une fonction, d'une méthode ou d'un filtre
            case AST.CallExpression: {
                let callContext: Record<string, any>, callee, args: string[];

                // C'est un filtre
                if (ast.filter) {

                    callee = this._filter.get(ast.callee.name);
                    args = ast.arguments.map((arg: Record<string, any>) => {
                        return this._recurse(arg);
                    });
                    return callee + '(' + args + ')';
                }
                // Sinon
                else {

                    callContext = {};
                    callee = this._recurse(ast.callee, callContext);
                    args = ast.arguments.map((arg: Record<string, any>) => {
                        return 'ensureSafeObject(' + this._recurse(arg) + ')';
                    });

                    if (callContext.name) {

                        this._addEnsureSafeObject(callContext.context);
                        if (callContext.computed)
                            callee = this._computedMember(callContext.context, callContext.name);
                        else
                            callee = this._nonComputedMember(callContext.context, callContext.name);
                    }
                    this._addEnsureSafeFunction(callee);
                    return callee + '&&ensureSafeObject(' + callee + '(' + args.join(',') + '))';
                }
            }

            // Affectation
            case AST.AssignmentExpression: {

                const leftContext: Record<string, any> = {};
                this._recurse(ast.left, leftContext, true);
                let leftExpr;
                if (leftContext.computed) {
                    leftExpr = this._computedMember(leftContext.context,
                        leftContext.name);
                }
                else {
                    leftExpr = this._nonComputedMember(leftContext.context,
                        leftContext.name);
                }
                return this._assign(leftExpr,
                    'ensureSafeObject(' + this._recurse(ast.right) + ')');
            }

            // Opérateur à une opérande
            case AST.UnaryExpression:
                return ast.operator
                    + '(' + this._ifDefined(this._recurse(ast.argument), 0) + ')';

            // Expressions logiques (AND et OR)
            case AST.LogicalExpression:
                intoId = this._nextId();
                this._state[this._state.computing].body.push(
                    this._assign(intoId, this._recurse(ast.left)));
                // On n'évalue right que si left == true (pour &&)
                // ou left == false (pour ||)
                this._if(ast.operator == '&&' ? intoId : this._not(intoId),
                    this._assign(intoId, this._recurse(ast.right)));
                return intoId;

            // Opérateur ternaire
            case AST.ConditionalExpression: {

                intoId = this._nextId();
                const testId = this._nextId();
                this._state[this._state.computing].body.push(
                    this._assign(testId, this._recurse(ast.test)));
                this._if(testId,
                    this._assign(intoId, this._recurse(ast.consequent)));
                this._if(this._not(testId),
                    this._assign(intoId, this._recurse(ast.alternate)));
                return intoId;
            }

            // Pour assign()
            case AST.KJSValueParameter:
                return 'v';

            // Opérateur à deux opérande
            case AST.BinaryExpression:
                // Pour un addition ou une soustraction, on remplace par 0 une valeur
                // non définie
                if (ast.operator === '+' || ast.operator === '-') {
                    return '(' + this._ifDefined(this._recurse(ast.left), 0) + ')'
                        + ast.operator
                        + '(' + this._ifDefined(this._recurse(ast.right), 0) + ')';
                }
                // Pour *, /, %
                else {
                    return '(' + this._recurse(ast.left) + ')'
                        + ast.operator + '(' + this._recurse(ast.right) + ')';
                }
        }
        return '';
    }


    /**
     * Pour le watcher
     * @returns {String}
     * @private
     */
    _watchFns (): string {

        const result = [];

        this._state.inputs.forEach((inputName: string) => {

            result.push('let ', inputName, '=function(s){',
                (this._state[inputName].vars.length ?
                    'let ' + this._state[inputName].vars.join(',') + ';'
                    : ''
                ),
                this._state[inputName].body.join(''),
                '};');
        });

        if (result.length)
            result.push('fn.inputs=[', this._state.inputs.join(','), '];');

        return result.join('');
    }


    /**
     * Génère une nouvelle variable et veille à sa déclaration. Si `skip` vaut `true`, alors on ne
     * déclarera pas la variables avec les autres.
     * @param skip Évite-t-on la déclaration de la variable ?
     */
    private _nextId (skip?: boolean): string {

        const id = 'v' + (this._state.nextId++);
        if (!skip)
            this._state[this._state.computing].vars.push(id);
        return id;
    }


    /**
     * Échape une valeur
     * @param value Valeur à échaper
     */
    private _escape (value: any): string {

        if (Utils.isString(value)) {
            return '\''
                + value.replace(this._stringEscapeRegex, this._stringEscapeFn) + '\'';
        }
        else if (value === null)
            return 'null';
        else
            return value;
    }


    /**
     * nonComputedMember: `(left).right`
     * @param left  Membre de gauche
     * @param right Membre de droite
     */
    private _nonComputedMember (left: string, right: string): string {

        return '(' + left + ').' + right;
    }


    /**
     * computedMember (left)[right]
     * @param left  Membre de gauche
     * @param right Membre de droite
     */
    private _computedMember (left: string, right: string): string {

        return '(' + left + ')[' + right + ']';
    }


    /**
     * Échape un caractère
     * @param ch caractère à échaper
     */
    private _stringEscapeFn (ch: string): string {

        return '\\u' + ('0000' + ch.charCodeAt(0).toString(16)).slice(-4);
    }


    /**
     * Créé une condition
     * @param test condition
     * @param consequent expression à exécuter si condition remplie
     */
    private _if (test: string, consequent: string): void {

        this._state[this._state.computing].body.push(
            'if(', test, '){', consequent, '}');
    }


    /**
     * Inverse l'expression
     * @param e expression
     */
    private _not (e: string): string {

        return '!(' + e + ')';
    }


    /**
     * getHasOwnProperty
     * @param object Objet à tester
     * @param property Propriété dont on veut déterminer l'existence
     */
    private _getHasOwnProperty (object: string, property: string): string {

        return object + '&&(' + this._escape(property) + ' in ' + object + ')';
    }


    /**
     * assign
     * @param id Identifiant de la variable
     * @param value Valeur à assigner
     */
    private _assign (id: string, value: string): string {

        return id + '=' + value + ';';
    }


    /**
     * addEnsureSafeMemberName
     * @param expr expression
     */
    private _addEnsureSafeMemberName (expr: string): void {

        this._state[this._state.computing].body.push(
            'ensureSafeMemberName(' + expr + ');');
    }


    /**
     * addEnsureSafeObject
     * @param expr expression
     */
    private _addEnsureSafeObject (expr: string): void {

        this._state[this._state.computing].body.push(
            'ensureSafeObject(' + expr + ');');
    }


    /**
     * addEnsureSafeFunction
     * @param expr expression
     */
    private _addEnsureSafeFunction (expr: string): void {

        this._state[this._state.computing].body.push(
            'ensureSafeFunction(' + expr + ');');
    }


    /**
     * ifDefined
     * @param value Valeur à tester
     * @param defaultValue Valeur par défaut
     */
    private _ifDefined (value: string, defaultValue: any): string {

        return 'ifDefined(' + value + ',' + this._escape(defaultValue) + ')';
    }


    /**
     * filter
     * @param name Nom du filtre
     */
    /*private _filter (name: string): string {

        if (!Object.prototype.hasOwnProperty.call(this._state.filters, 'name'))
            this._state.filters[name] = this._nextId(true);
        return this._state.filters[name];
    }


    /**
     * _filterPrefix
     * @returns {String}
     * @private
     */
    private _filterPrefix (): string {

        if (Utils.isEmpty(this._state.filters))
            return '';
        else {

            const parts = Object.getOwnPropertyNames(this._state.filters).map(
                name => {
                    return this._state.filters[name]
                        + '=filter(' + this._escape(name) + ')';
                });
            return 'let ' + parts.join(',') + ';';
        }
    }
}


/**
 * Parser
 *
 * Combine le Lexer, l'AST Builder et l'AST Compiler.
 */
class Parser {

    private _lexer = new Lexer();
    private _ast: ASTBuilder;
    private _astCompiler: ASTCompiler;

    /**
     * Initialise le parser
     * @param filter Service filter
     */
    constructor (filter: Filter) {

        this._ast = new ASTBuilder(this._lexer);
        this._astCompiler = new ASTCompiler(this._ast, filter);
    }


    /**
     * Parse l'expression text et renvoit une fonction
     * @param text Texte à parser
     */
    parse (text: string): ParseFn {

        return this._astCompiler.compile(text);
    }
}

/**
 * Fonction publique (celle qui est retournée lorsque l'on appelle le module) qui s'occupe de parser
 * et de retourner l'expression.
 *
 * L'expression produit une fonction acceptant deux paramètres : `scope` et `locals`. Ainsi toutes
 * les identificateurs se réfèreront à une propriété de `locals` (ou de `scope` si la propriété
 * n'existe pas dans locals)
 *
 * Précéance des opérateurs :
 *   1. Primary expressions: lookups, appels de fonctions, ou de méthodes
 *   2. Unary expressions: `+a`, `-a`, `!a`
 *   3. Multiplicative arithmetic expressions: `a * b`, `a / b`, et `a % b`
 *   4. Additive arithmetic expressions: `a + b` et `a - b`
 *   5. Relational expressions: `a < b`, `a > b`, `a <= b`, et `a >= b`
 *   6. Equality testing expressions: `a == b`, `a != b`, `a === b`, et `a !== b`
 *   7. Logical AND expressions: `a && b`
 *   8. Logical OR expressions: `a || b`
 *   9. Ternary expressions: `a ? b : c`
 *   10. Assignments: `a = b`
 *   11. Filters: `a | aFilter`
 *
 * Si `expr` est une fonction, `parse` retournera celle-ci, sinon, si ce n'est pas une chaîne de
 * caractères, elle retournera une fonction quelconque.
 *
 * Si `expr` commence par `::`, alors le watcher associé ne sera appelé qu'une seule fois, de la
 * même manière que si l'`expr` retourne une valeur constante.
 *
 * Voici les différents éléments reconnus :
 *
 *   - Literals
 *     - Integer             `42`
 *     - Floating point      `4.2`
 *     - Scientific notation `42E5` `42e5` `42e-5`
 *     - Single quoted string `'wat'`
 *     - Double-quoted string `"wat"`
 *     - Character escapes    `"\n\f\r\t\v\'\"\\"`
 *     - Unicode escapes      `"\u2665"`
 *     - Boleans   `true` `false`
 *     - null      `null`
 *     - undefined `undefined`
 *     - Arrays  `[1, 2, 3]` `[1, [2, 'three']]`
 *     - Objects `{a: 1, b: 2}` `{'a': 1, "b": "two"}`
 *   - Statements
 *     - Semicolon-separated  `expr; expr; expr`
 *     - Last one is returned `a = 1; a +b`
 *   -Parentheses
 *     - Alter precedence order `2 * (a + b)` `(a || b) && c`
 *   - Member Access
 *     - Field lookup    `aKey`
 *     - neated objects  `aKey.otherKey.key3`
 *     - Property lookup `aKey['otherKey']` `aKey[keyVar]` `aKey['other'].key3`
 *     - Array lookup    `anArray[42]`
 *   - Function Calls
 *     - Function calls `aFunction()` `aFunction(42, 'abc')`
 *     - Method calls   `anObject.aFunction()` `anObject[fnVar]()`
 *   - Operators (in order of precedence)
 *     - Unary          `-a` `+a` `!done`
 *     - Multiplicative `a * b` `a / b` `a % b`
 *     - Additive       `a + b` `a - b`
 *     - Comparison     `a < b` `a > b` `a <= b` `a >= b`
 *     - Equality       `a == b` `a != b` `a === b` `a !== b`
 *     - Logical And    `a && b`
 *     - Logical Or     `a || b`
 *     - Ternary        `a ? b : c`
 *     - Assignment     `aKey = val` `anObject.aKey = val` `anArray[42] = val`
 *     - Filters        `a | filter` `a | filter1 | filter2`
 *                      `a | filter:arg1:arg2`
 *
 * @param kernel Kernel de l'application
 * @returns `(expr: string) => string` avec `expr` l'expression à parser
 */
export function createParser (kernel: Kernel): Function {

    return function (expr: string | Function): Function {

        switch (typeof expr) {

            case 'string': {

                const parser = new Parser(kernel.get('$filter'));
                let oneTime = false;
                // One-Time expression
                if (expr.charAt(0) == ':' && expr.charAt(1) === ':') {
                    oneTime = true;
                    expr = expr.substring(2);
                }
                // On parse l'expression
                const parseFn = parser.parse(expr);
                // Si l'expression retourne une valeur constante
                if (parseFn.constant)
                    parseFn._watchDelegate = constantWatchDelegate;
                // One-Time expression
                else if (oneTime) {
                    parseFn._watchDelegate = parseFn.literal ?
                        oneTimeLiteralWatchDelegate
                        : oneTimeWatchDelegate;
                }
                // input expression functions
                else if (parseFn.inputs)
                    parseFn._watchDelegate = inputsWatchDelegate;
                return parseFn;
            }

            case 'function':
                return expr as Function;

            default:
                return (): void => { return; };
        }
    };
}
