/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as Utils from '../Utils';

/**
 * Canal d'évènement
 */
export class Event {

    protected _id: string;
    protected _observers: { [keys: string]: Function | undefined } = {};
    /**
     * Initialise le canal
     * @param id Identifiant du canal
     */
    constructor (id: string) {

        this._id = id;
    }


    /**
     * Retourne l'id du canal
     */
    getId (): string {

        return this._id;
    }


    /**
     * Attache un observer, écrasera un précédent portant le même nom.
     * @param id Identifiant, nécessaire pour désenregister l'observer
     * @param callback Fonction de callback à exécuter
     */
    attach (id: string, callback: Function): void {

        this._observers[id] = callback;
    }


    /**
     * Retourne un observer
     */
    get (id: string): Function | undefined {

        return this._observers[id];
    }


    /**
     * Détache un observer
     * @param id Identifiant de l'observer à détacher
     */
    detach (id: string): void {

        this._observers[id] = undefined;
    }


    /**
     * Notifie tous les observers
     * @param data Données à transmettre
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    notify (data: any): void {

        Utils.forEach(this._observers, observer => {
            if (Utils.isFunction(observer))
                observer(data);
        });
    }
}
