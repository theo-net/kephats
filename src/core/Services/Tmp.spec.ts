/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Config } from './Config';
import { Tmp } from './Tmp';

describe('core/Service/Tmp', () => {

    it('étend Config', () => {

        expect(new Tmp()).to.be.instanceOf(Config);
    });
});
