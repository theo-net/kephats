/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Kernel } from './Kernel';
import { KernelAccess } from './KernelAccess';

class MyClass extends KernelAccess {}

describe('core/KernelAccess', () => {

    it('retourne une instance de Kernel', () => {

        const kernelAccess = new MyClass();
        expect(kernelAccess.kernel()).to.be.instanceof(Kernel);
    });

    it('retourne toujours la même instance', () => {

        const kernelAccess = new MyClass(),
              kernel1 = kernelAccess.kernel(),
              kernel2 = kernelAccess.kernel();

        expect(kernel1).to.be.equal(kernel2);
    });

    it('plusieurs KernelAccess retournent toujours la même instance', () => {

        const kernelAccess = new MyClass(),
              kernelAccess2 = new MyClass(),
              kernel1 = kernelAccess.kernel(),
              kernel2 = kernelAccess2.kernel();

        expect(kernel1).to.be.equal(kernel2);
    });
});

