/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

let _instance: Kernel | null = null;

import { Cache } from './Services/Cache';
import { Config } from './Services/Config';
import { createInterpolate } from './Services/interpolate';
import { createParser } from './Services/parse';
import { Events } from './Services/Events';
import { Filter } from './Services/Filter';
import { Logs } from './Services/Logs';
import { Plugin } from './Plugin';
import { Scope } from './Services/Scope';
import { ServiceContainer } from './Services/ServiceContainer';
import { Validate } from './Services/Validate';
import { version as versionKts } from '../version';
import { Tmp } from './Services/Tmp';

/**
 * Cœur de l'application
 * Initialise les services et l'environnement. Il s'agit d'un singleton
 */
export class Kernel {

    protected _container: ServiceContainer = new ServiceContainer(this);

    /**
     * Initialisation du Kernel
     *
     * @param force Force la création d'une nouvelle instance
     */
    constructor (force = false) {

        // On n'initialise qu'une fois le Kernel
        if (!force && _instance !== null)
            return _instance;

        _instance = this;

        // Initialisation des services de base
        this._container.register('$plugins', new ServiceContainer(this));
        this._container.register('$config', new Config());

        // Initialisation Logs
        this.get('$config').set('logLevel', 'normal');
        this._container.register('$logs', new Logs());

        // Autres services
        this._container.register('$events', new Events());
        this._container.register('$cache', new Cache());
        this._container.register('$tmp', new Tmp());
        this._container.register('$validate', new Validate());
        this._container.register('$parser', createParser);
        this._container.register('$filter', new Filter());
        this._container.register('$interpolate', createInterpolate);
        this._container.register('$rootScope', new Scope(this));

        return _instance;
    }


    /**
     * Retourne le service-container
     */
    container (): ServiceContainer {

        return this._container;
    }


    /**
     * Retourne le service `$event`
     */
    events (): Events {

        return this.get('$events');
    }


    /**
     * Alias pour `this.container().get()`
     *
     * @param name Nom du service à récupérer
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    get (name: string): any {

        return this._container.get(name);
    }


    /**
     * Retourne la version du Framework
     */
    version (): string {

        return versionKts;
    }


    /**
     * Charge un plugin
     * @param plugin Plugin à charger
     */
    loadPlugin (plugin: typeof Plugin): Kernel {

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const pluginInstance = new (plugin as any)(this);
        pluginInstance.init();
        this._container.get('$plugins').register(pluginInstance.name(), pluginInstance);
        return this;
    }
}
