/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/* eslint-disable @typescript-eslint/no-explicit-any */

export class BaseError extends Error {

    public name: string;
    public originalMessage: any;
    public meta: any;

    constructor (message: string, meta?: any, app?: string, original?: any) {

        super(message);

        original = original ? original : message;

        this.name = this.constructor.name;
        this.originalMessage = original;
        this.meta = meta;
    }
}
