/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/*
 * Différents outils
 * Certaines méthodes sont inspirées de Lodash (Licence MIT) et leurs tests également. Nous ne
 * reprenons pas la bibliothèque en entier, car de nombreuses méthodes ne nous sont pas utiles. La
 * compatibilité n'est pas assurée, car n'étant pas un pro, j'ai supprimé des tests si je
 * n'arrivais pas à reproduire certains comportements.
 */

// On enregistre des tags
const TAG = {
    'args': '[object Arguments]',
    'array': '[object Array]',
    'bool': '[object Boolean]',
    'date': '[object Date]',
    'error': '[object Error]',
    'func': '[object Function]',
    'map': '[object Map]',
    'number': '[object Number]',
    'object': '[object Object]',
    'regExp': '[object RegExp]',
    'set': '[object Set]',
    'string': '[object String]',
    'weakMap': '[object WeakMap]',
    'arrayBuffer': '[object ArrayBuffer]',
    'float32': '[object Float32Array]',
    'float64': '[object Float64Array]',
    'int8': '[object Int8Array]',
    'int16': '[object Int16Array]',
    'int32': '[object Int32Array]',
    'uint8': '[object Uint8Array]',
    'uint8Clamped': '[object Uint8ClampedArray]',
    'uint16': '[object Uint16Array]',
    'uint32': '[object Uint32Array]'
};
const CLONEABLE_TAG: { [key: string]: boolean } = {};
CLONEABLE_TAG[TAG.args] = CLONEABLE_TAG[TAG.array] =
    CLONEABLE_TAG[TAG.arrayBuffer] = CLONEABLE_TAG[TAG.bool] =
    CLONEABLE_TAG[TAG.date] = CLONEABLE_TAG[TAG.float32] =
    CLONEABLE_TAG[TAG.float64] = CLONEABLE_TAG[TAG.int8] =
    CLONEABLE_TAG[TAG.int16] = CLONEABLE_TAG[TAG.int32] =
    CLONEABLE_TAG[TAG.number] = CLONEABLE_TAG[TAG.object] =
    CLONEABLE_TAG[TAG.regExp] = CLONEABLE_TAG[TAG.string] =
    CLONEABLE_TAG[TAG.uint8] = CLONEABLE_TAG[TAG.uint8] =
    CLONEABLE_TAG[TAG.uint16] = CLONEABLE_TAG[TAG.uint32] = true;
CLONEABLE_TAG[TAG.error] = CLONEABLE_TAG[TAG.func] =
    CLONEABLE_TAG[TAG.map] = CLONEABLE_TAG[TAG.set] =
    CLONEABLE_TAG[TAG.weakMap] = false;

/* eslint-disable @typescript-eslint/no-explicit-any */


/**
 * Test si `value` est considérable comme un objet (non `null` et le
 * `typeof` est `object`).
 * Exemple :
 *
 *      Util.isObjectLike({}) ;
 *          // => true
 *      Util.isObjectLike([1, 2, 3]) ;
 *          // => true
 *      Util.isObjectLike(() => {})) ;
 *          // => false
 *      Util.isObjectLike(null) ;
 *          // => false
 *
 * @param value valeur à tester
 * @author Lodash
 */
export function isObjectLike (value: any): boolean {

    return !!value && typeof value == 'object';
}


/**
 * Test si `value` est une chaine de caractères
 * Exemple :
 *
 *      Utils.isString('abc') ;
 *          // => true
 *      Utils.isString(1)
 *          // => false
 *
 * @param value valeur à tester
 * @author Lodash
 */
export function isString (value: any): boolean {

    return typeof value === 'string' ||
        (isObjectLike(value) &&
            Object.prototype.toString.call(value) == TAG.string
        );
}


/**
 * Test si `value` est un objet
 * @param value valeur à tester
 * @author Lodash
 */
export function isObject (value: any): boolean {

    const type = typeof value;
    return !!value && (type == 'object' || type == 'function');
}


/**
 * `value` est-il un nombre ?
 *
 * @param value valeur à tester
 * @param convert Si `true` `'123'` sera un nombre
 * @author Lodash
 */
export function isNumber (value: any, convert = false): boolean {

    return typeof value === 'number' ||
        (isObjectLike(value) &&
            Object.prototype.toString.call(value) == TAG.number
        ) || (convert &&
            (/^0x[0-9a-f]+$/i.test(value) ||
                /^[-+]?(?:\d+(?:\.\d*)?|\.\d+)(e[-+]?\d+)?$/.test(value)));
}


/**
 * `value` est-elle un `boolean` ?
 *
 * @param value valeur à tester
 * @author Lodash
 */
export function isBoolean (value: any): boolean {

    return value === true || value === false ||
        (isObjectLike(value) &&
            Object.prototype.toString.call(value) == TAG.bool
        );
}


/**
 * `value` est-elle une fonction ?
 * @param value valeur à tester
 * @author Lodash
 */
export function isFunction (value: any): boolean {

    return isObject(value) &&
        Object.prototype.toString.call(value) == TAG.func;
}


/**
 * `value` est-elle considérable à un JSON à parser. Retournera  `true` si `value` est un
 * objet simple (ex: `{a: 1}`)
 *
 * @param value valeur à tester
 */
export function isJsonLike (value: any): boolean {

    if (typeof value == 'symbol')
        return false;
    if (/^\{(?!\{)/.test(value))
        return /\}$/.test(value);
    else if (/^\[/.test(value))
        return /\]$/.test(value);
    else
        return false;
}


/**
 * `value` est-elle vide ?
 *
 * @param value valeur à tester
 * @author Lodash
 */
export function isEmpty (value: any): boolean {

    if (value === null)
        return true;
    else if (Array.isArray(value) || isString(value))
        return value.length === 0;
    else if (value instanceof RegExp)
        return true;
    else if (isObject(value))
        return !Object.getOwnPropertyNames(value).length;
    else
        return true;
}


/**
 * Invoque `n` fois `callback`
 *
 * @param n nombre de fois que la fonction callback doit être exécutée
 * @param callback fonction appelée avec i qui varie de 0 à n-1, si n'est pas définie
 *        ou n'est pas une fonction, remplace par une fonction renvoyant le paramètre (identity)
 * @param thisArg this binding
 * @returns Tableau des résultats
 * @author Lodash
 */
export function times (
    n: number,
    callback?: ((i: number) => any) | undefined | null,
    thisArg?: object
): Array < any > {

    const result = [];

    if(!n || n < 0 || n === Infinity)
        return [];

    if (callback === undefined || callback === null) {
        for (let i = 0; i < Math.floor(n); i++)
            result[i] = i;
        return result;
    }
    else {

        for (let i = 0; i < Math.floor(n); i++) {
            if (thisArg)
                result[i] = callback.call(thisArg, i);
            else
                result[i] = callback(i);
        }

        return result;
    }
}


/**
 * Compare `value` et `other` pour déterminer s'ils sont équivalents. Fonctionne avec des `array`,
 * `boolean`, `Date`, `number`, `Object`, `RegExp` et `string`. Pour cette fonction, `NaN` est égal
 * à lui-même.
 *
 * @param value valeur à comparer
 * @param other l'autre valeur à comparer
 * @param customizer fonction de comparaison
 * @param thisArg binding pour customizer
 * @param stackA Pour détecter la récursivité
 * @param stackB Pour détecter la récursivité
 * @author Lodash
 */
export function isEqual (
    value: any, other: any,
    customizer?: (value: any, other: any) => boolean,
    thisArg?: object,
    stackA: Array<any> = [], stackB: Array<any> = []
): boolean {

    // Si customizer est définie
    if (customizer !== undefined) {

        if (thisArg)
            return customizer.call(thisArg, value, other);
        else
            return customizer(value, other);
    }

    // Pour booleans, nombres, string
    if (value === other)
        return true;

    // null
    if (value == null || other == null ||
        (!isObject(value) && !isObjectLike(other)))
        return value !== value && other !== other;

    const valTag = Object.prototype.toString.call(value),
          othTag = Object.prototype.toString.call(other);
    if (valTag === othTag) {
        switch (Object.prototype.toString.call(value)) {

            case TAG.bool:
            case '[object Date]':
                return +value == +other;
            case '[object Error]':
                return (value.name == other.name && value.message == other.message);
            case TAG.number: // Treat `NaN`
                return (value != +value) ? other != +other : value == +other;
            case TAG.regExp:
            case TAG.string:
                return value == (other + '');
        }
    }


    // Tests pour la récursivité
    let length = stackA.length;
    while (length--) {
        if (stackA[length] == value)
            return stackB[length] == other;
    }
    stackA.push(value);
    stackB.push(other);


    let result = false;


    // Tableaux
    if (Array.isArray(value) && Array.isArray(other)) {

        // Si les tailles sont différentes, alors il y a un problème
        if (value.length !== other.length)
            return false;

        // On igore les propriétés qui ne sont pas des index
        for (let i = 0; i < value.length; i++) {

            if (!(value[i] === other[i] ||
                isEqual(value[i], other[i], undefined, undefined, stackA, stackB)
            ))
                return false;
        }

        result = true;
    }


    // Objets divers
    else if (typeof value === 'object' && typeof other === 'object' &&
        other !== null) {

        if ((Object.getOwnPropertyNames(value).length ==
            Object.getOwnPropertyNames(other).length) &&
            Object.getPrototypeOf(value) === Object.getPrototypeOf(other)) {

            result = Object.getOwnPropertyNames(value).every(property => {

                if (!Object.prototype.hasOwnProperty.call(other, property))
                    return false;
                return isEqual(value[property], other[property],
                    undefined, undefined, stackA, stackB);
            });
        } else
            return false;
    }

    stackA.pop();
    stackB.pop();

    return result;
}


/**
 * Appelle fn sur chacun des éléments de la collection. Si `fn` renvoit `false`, stop le `forEach`
 *
 * @param collection Collection
 * @param fn Fonction appelée fn(value, index, collection)
 * @param thisArg Contexte
 * @returns Retourne `collection`
 * @author Lodash
 */
export function forEach (
    collection: Array<any>| object,
    fn: (value: any, index: any, collection: Array<any> | object) => void| boolean,
    thisArg?: object
): Array<any> | object {

    if(typeof fn !== 'function')
        throw new TypeError(fn + ' is not a function');

    let k = -1;

    if (Array.isArray(collection)) {

        // Défini en amont pour ne pas recalculer à chaque fois la longueur
        const length = collection.length;

        while (++k < length) {
            if (fn.call(thisArg, collection[k], k, collection) === false)
                break;
        }
    }
    else if (collection !== undefined) {

        const iterable = isObject(collection) ? collection : Object(collection);
        const props = Object.keys(collection),
              length = props.length;

        while (++k < length) {

            const key = props[k];
            if (fn(iterable[key], key, iterable) === false)
                break;
        }
    }

    return collection;
}


/**
 * Appelle fn sur chacun des éléments de la collection, mais en la parcourant à partir de la
 * fin. Si fn renvoit false, stop le forEach
 *
 * @param collection Collection
 * @param fn Fontion appellée
 * @returns Retourne `collection`
 * @author Lodash
 */
export function forEachRight (
    collection: Array<any>| object,
    fn: (value: any, index: any, collection: Array<any> | object) => void| boolean,
    thisArg?: object
): Array<any> | object {

    if(typeof fn !== 'function')
        throw new TypeError(fn + ' is not a function');

    if (Array.isArray(collection)) {

        let k = collection.length;
        while (k) {

            k--;
            if (fn.call(thisArg, collection[k], k, collection) === false)
                break;
        }
    }
    else if (collection !== undefined) {

        const iterable = isObject(collection) ? collection : Object(collection);
        const props = Object.keys(collection),
              length = props.length;
        let index = length;

        while (index--) {

            const key = props[index];
            if (fn(iterable[key], key, iterable) === false)
                break;
        }
    }

    return collection;
}


/**
 * Assigne les propriétés énumérables des objets sources à `object`. Si la propriété existe
 * déjà, elle sera écrasée. De même, une source écrase les propriétés d'une source précédente.
 *
 * @param object Objet qui sera étendus
 * @param sources Les sources
 */
export function extend (
    object: Record<string, any>,
    ...sources: Array<Record<string, any>>
): object {

    sources.forEach(source => {
        if (isObject(source)) {
            Object.getOwnPropertyNames(source).forEach(property => {
                object[property] = source[property];
            });
        }
    });

    return object;
}


/**
 * Converti `string` en camelCase
 *
 * @param string Chaîne de caractère à convertir
 * @author Lodash
 */
export function camelCase (string: string): string {

    // On force string à être un String
    string = string === null ? '' : string + '';

    // On convertit les caractères latin-1 supplémentaires en latin classique
    // et on retire les caractères diacriques.
    const reComboMark = /[\u0300-\u036f\ufe20-\ufe23]/g;
    const reLatin1 = /[\xc0-\xd6\xd8-\xde\xdf-\xf6\xf8-\xff]/g;

    function deburrLetter (letter: string): string {

        // Utilisé pour mapper les caractères latin-1 supplementary en
        // caractères basic latin.
        const deburredLetters: { [key: string]: string } = {
            '\xc0': 'A', '\xc1': 'A', '\xc2': 'A', '\xc3': 'A', '\xc4': 'A',
            '\xc5': 'A',
            '\xe0': 'a', '\xe1': 'a', '\xe2': 'a', '\xe3': 'a', '\xe4': 'a',
            '\xe5': 'a',
            '\xc7': 'C', '\xe7': 'c',
            '\xd0': 'D', '\xf0': 'd',
            '\xc8': 'E', '\xc9': 'E', '\xca': 'E', '\xcb': 'E',
            '\xe8': 'e', '\xe9': 'e', '\xea': 'e', '\xeb': 'e',
            '\xcC': 'I', '\xcd': 'I', '\xce': 'I', '\xcf': 'I',
            '\xeC': 'i', '\xed': 'i', '\xee': 'i', '\xef': 'i',
            '\xd1': 'N', '\xf1': 'n',
            '\xd2': 'O', '\xd3': 'O', '\xd4': 'O', '\xd5': 'O', '\xd6': 'O',
            '\xd8': 'O',
            '\xf2': 'o', '\xf3': 'o', '\xf4': 'o', '\xf5': 'o', '\xf6': 'o',
            '\xf8': 'o',
            '\xd9': 'U', '\xda': 'U', '\xdb': 'U', '\xdc': 'U',
            '\xf9': 'u', '\xfa': 'u', '\xfb': 'u', '\xfc': 'u',
            '\xdd': 'Y', '\xfd': 'y', '\xff': 'y',
            '\xc6': 'Ae', '\xe6': 'ae',
            '\xde': 'Th', '\xfe': 'th',
            '\xdf': 'ss'
        };

        return deburredLetters[letter];
    }
    string = string.replace(reLatin1, deburrLetter).replace(reComboMark, '');

    // On génère un tableau de mots
    const reWords = (function (): RegExp {
        const upper = '[A-Z\\xc0-\\xd6\\xd8-\\xde]',
              lower = '[a-z\\xdf-\\xf6\\xf8-\\xff]+';
        return new RegExp(upper + '+(?=' + upper + lower + ')|' + upper + '?'
            + lower + '|' + upper + '+|[0-9]+', 'g');
    }());
    const array = string.match(reWords) || [];

    // On parcours la liste des mots
    let result = '';
    array.forEach(function (word, index) {

        // On met tout le mot en minuscule
        word = word.toLowerCase();
        // On met la première lettre en majuscule si ce n'est pas le premier mot
        result += index ? (word.charAt(0).toUpperCase() + word.slice(1)) : word;
    });

    return result;
}


/**
 * Converti `string` en kebab-case
 *
 *   - `Foo Bar      foo-bar`
 *   - `fooBar       foo-bar`
 *   - `__FOO_BAR__  foo-bar`
 * @param string Chaîne à convertir
 * @author Lodash
 */
export function kebabCase (string: string): string {

    const upper = '[A-Z\\xc0-\\xd6\\xd8-\\xde]',
          lower = '[a-z\\xdf-\\xf6\\xf8-\\xff]+';
    let index = -1;
    const array = string.match(RegExp(upper + '+(?=' + upper + lower + ')|'
        + upper + '?' + lower + '|' + upper + '+|[0-9]+', 'g')) || [];
    const length = array.length;
    let result = '';

    while (++index < length)
        result += (index ? '-' : '') + array[index].toLowerCase();

    return result;
}


/**
 * Calcul la distance de Levenshtein donnant une mesure de similarité entre deux chaînes de
 * caractères.
 *
 * @param str1 Première chaîne à tester
 * @param str2 Deuxième chaîne à tester
 * @param collator Utilise `IntlCollator` pour une comparaison locale-sensitive
 */
export function levenshtein (
    str1: string, str2: string, collator: Intl.Collator | null = null
): number {

    const str1Len = str1.length,
          str2Len = str2.length;

    // Cas basiques
    if (str1Len === 0) return str2Len;
    if (str2Len === 0) return str1Len;

    // Initialisation du collator
    if (collator) {
        try {
            collator = (typeof Intl !== 'undefined' &&
                typeof Intl.Collator !== 'undefined') ?
                Intl.Collator('generic', { sensitivity: 'base' }) : null;
        } catch (err) {
            console.log('Collator could not be initialized and wouldn\'t be used');
        }
    }


    // Variables à déclarer
    const prevRow = [],
          str2Char = [];
    let curCol, i, j, tmp, strCmp;
    let nextCol = 0;

    // Initialisation de prevRow
    for (i = 0; i < str2Len; i++) {

        prevRow[i] = i;
        str2Char[i] = str2.charCodeAt(i);
    }
    prevRow[str2Len] = str2Len;

    // Calcule de la distance entre la ligne courante avec la précédente
    for (i = 0; i < str1Len; i++) {

        nextCol = i + 1;

        for (j = 0; j < str2Len; j++) {

            curCol = nextCol;

            // substitution
            if (!collator)
                strCmp = str1.charCodeAt(i) === str2Char[j];
            else {
                strCmp = 0 === collator.compare(
                    str1.charAt(i), String.fromCharCode(str2Char[j]));
            }
            nextCol = prevRow[j] + (strCmp ? 0 : 1);

            // insertion
            tmp = curCol + 1;
            if (nextCol > tmp)
                nextCol = tmp;

            // deletion
            tmp = prevRow[j + 1] + 1;
            if (nextCol > tmp)
                nextCol = tmp;

            // Préparation pour la prochaine itération
            prevRow[j] = curCol;
        }

        // Préparation pour la prochaine itération
        prevRow[j] = nextCol;
    }

    return nextCol;
}


/**
 * Parcours les propriétés de l'objet. Si le callback retourne `false`, la boucle sera interrompue.
 *
 * @param obj Objet à parcourir
 * @param fn Function callback
 * @param thisArg Context
 * @returns Retourne `obj`
 * @author Lodash
 */
export function forOwn (
    obj: object,
    fn: (value: any, key: string | number, obj: object) => void| boolean,
    thisArg?: object
): object {

    const iterable = isObject(obj) ? obj : Object(obj),
          props = Object.keys(obj),
          length = props.length;
    let k = -1;

    while (++k < length) {

        let key: any = props[k];
        if (Array.isArray(obj)) key = Number(key);
        if (fn.call(thisArg, iterable[key], key, iterable) === false)
            break;
    }

    return obj;
}


/**
 * Créé un clone de `value` et le retourne. Si `isDeep` vaut `true`, les propriétés ayant pour
 * valeur un objet sont également clonées et non passées par référence. Les propriétés
 * d'`arguments` ou les objets créés par un constructeur autre que `Object` sont clonés comme
 * des objets `Objects`. Un objet vide est retourné pour les valeurs non clonables (comme les
 * fonctions, nœud DOM, ...)
 *
 * @param value Valeur à cloner
 * @param isDeep Deep clone
 * @param key The key of `value`
 * @param object The object `value ` belongs to.
 * @param stackA Tracks traversed source objects.
 * @param stackB Associates clones with source counterparts
 * @author Lodash
 */
export function clone (
    value: any,
    isDeep = false, key?: string, object?: object,
    stackA?: Array<any>, stackB?: Array<any>
): any {

    let result: any;

    // Value n'est pas un objet, on la retourne directement
    if(!isObject(value))
        return value;

    const isArr = Array.isArray(value);

    // Value est un tableau
    if (isArr) {

        result = new value.constructor(value.length);

        // Add array properties assigned by `RegExp#exec`
        if (value.length && typeof value[0] == 'string' &&
            Object.prototype.hasOwnProperty.call(value, 'index')) {
            result.index = value.index;
            result.input = value.input;
        }

        if (!isDeep) {
            value.forEach((val: any, ind: number) => {
                result[ind] = val;
            });
            return result;
        }
    }
    // Value est un objet
    else {

        const tag = Object.prototype.toString.call(value),
              isFunc = tag == TAG.func;

        if (tag == TAG.object || tag == TAG.args || (isFunc && !object)) {

            let Ctor = (isFunc ? {} : value).constructor;
            if (!(typeof Ctor == 'function' && Ctor instanceof Ctor))
                Ctor = Object;
            result = new Ctor;

            if (!isDeep) {
                if (value == null)
                    return result;

                Object.keys(value).forEach(key => {
                    result[key] = value[key];
                });
                return result;
            }
        }
        else {
            if (CLONEABLE_TAG[tag]) {

                const Ctor = value.constructor;

                switch (tag) {

                    case TAG.arrayBuffer: {

                        result = new ArrayBuffer(value.byteLength);
                        const view = new Uint8Array(result);
                        view.set(new Uint8Array(value));
                        return result;
                    }

                    case TAG.bool:
                    case TAG.date:
                        return new Ctor(+value);

                    case TAG.float32: case TAG.float64: case TAG.int8: case TAG.int16:
                    case TAG.int32: case TAG.uint8: case TAG.uint8Clamped:
                    case TAG.uint16: case TAG.uint32: {

                        const buffer = value.buffer;
                        result = new ArrayBuffer(buffer.byteLength);
                        const v = new Uint8Array(result);
                        v.set(new Uint8Array(buffer));
                        return new Ctor(isDeep ? result : buffer,
                        value.byteOffset, value.length);
                    }

                    case TAG.number:
                    case TAG.string:
                        return new Ctor(value);

                    case TAG.regExp:
                        result = new Ctor(value.source, /\w*$/.exec(value));
                        result.lastIndex = value.lastIndex;
                }
                return result;
            }
            else
                return (object ? value : {});
        }
    }

    // Check for circular references and return its corresponding clone.
    stackA || (stackA = []);
    stackB || (stackB = []);

    let length = stackA.length;
    while (length--) {
        if (stackA[length] == value)
            return stackB[length];
    }
    stackA.push(value);
    stackB.push(result);

    (isArr ? forEach : forOwn)(value, (subValue: any, key: any): void => {
        result[key] = clone(subValue, isDeep, key, value, stackA, stackB);
    });

    return result;
}


/**
 * Retourne un nouvel objet, copie de objet. Les propriétés ayant pour valeur un objet sont
 * également clonée et non passées par référence. Les propriétés d'`arguments` ou les objets
 * créés par un constructeur autre que `Object` sont clonés comme des objets `Objects`. Un
 * objet vide est retourné pour les valeurs non clonables (comme les fonctions, nœud DOM, ...)
 *
 * @param value L'objet à copier
 * @author Lodash
 */
export function cloneDeep (value: any): any {

    return clone(value, true);
}


/**
 * Exécute `callback` sur chacun des éléments. Retourne true, si chaque appel de callback a
 * retourné true, false sinon.
 *
 * @param collection Collection
 * @param predicate Function appelée sur chaque élément. Si `null`, remplacée par : `value => { return value ; }`
 * @param thisArg this binding
 * @author Lodash
 */
export function some (
    collection: Array<any>| object,
    predicate:
    ((currentValue: any, index: number, collection: Array<any> | object) => boolean) | null,
    thisArg?: object
): boolean {

    const predicateNull = (value: any): any => { return value; };
    if(predicate === null)
        predicate = predicateNull;

    // Fonction interne pour les Array
    if (Array.isArray(collection)) {

        let index = -1;
        const length = collection.length;

        while (++index < length) {
            if (predicate.call(thisArg, collection[index], index, collection))
                return true;
        }

        return false;
    }
    // Pour les objets
    else if (isObject(collection)) {

        let result;

        // On teste chaque propriété
        forEach(collection, (value, key, collection) => {

            result = predicate !== null ?
                predicate.call(thisArg, value, key, collection)
                : predicateNull;
            // Si result == false, on a pas besoin de tester les autres valeurs
            return !result;
        });

        // On s'assure d'avoir un boolean
        return !!result;
    }

    return false;
}


/**
 * Met la première lettre en majuscule
 * @param str String à transformer
 * @returns Si `str` n'est pas une chaîne, renverra `str` sans transformation
 */
export function ucfirst (str: any): any {

    if (isString(str))
        return str.charAt(0).toUpperCase() + str.substr(1);
    else
        return str;
}


/**
 * Retourne la RegExp permettant de valider un UUID v4
 */
export function uuidRegExp (): RegExp {

    return RegExp(
        '^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$',
        'i'
    );
}

/* eslint-enable @typescript-eslint/no-explicit-any */
