/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { assert, expect } from 'chai';

import { Cli } from './cli/Cli';
import { Controller } from './server/Controller';
import { Server } from './server/Server';
import * as CliFramework from './cli/Framework';
import { Field } from './server/Form/Field';
import { FormBuilder } from './server/Form/FormBuilder';
import * as Framework from './index';

// Pour les tests
process.setMaxListeners(0);

describe('index', () => {

    it('extends `cli/Framework`', () => {

        assert.containsAllKeys(Framework, CliFramework);
    });

    describe('cli()', () => {

        it('returns a new instance of Cli', () => {

            expect(Framework.cli()).to.be.instanceOf(Cli);
            expect(Framework.cli()).to.be.not.equal(Framework.cli());
        });

        it('transmet le kernel à l\'application', () => {

            expect(Framework.cli().kernel()).to.be.equal(Framework.kernel());
        });

        it('enregistre l\'applocation comme service', () => {

            const cli = Framework.cli();
            expect(cli.kernel().get('$application')).to.be.equal(cli);
        });
    });

    describe('server', () => {

        it('returns a new instance of Server', () => {

            expect(Framework.server()).instanceOf(Server);
        });

        it('set a default static path', () => {

            const server = Framework.server();
            server.run(['start']);
            server.kernel().get('$config').get('$server').close();
            expect(server.get('$config').get('staticDir')).to.be.equal('./');
        });

        it('returns FormBuilder constructor', () => {

            expect(Framework.FormBuilder).to.be.equal(FormBuilder);
        });

        it('returns Field constructor', () => {

            expect(Framework.Field).to.be.equal(Field);
        });

        it('returns Controller constructor', () => {

            expect(Framework.Controller).to.be.equal(Controller);
        });
    });
});
