/**
 * This file is part of KephaTs.
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Cli } from './cli/Cli';
import { Kernel } from './core/Kernel';
import { Server } from './server/Server';

/**
 * Point d'entrée du framework
 *
 * Fournit le framework, ainsi que des méthode pour créer une app CLI ou un serveur HTTP.
 */

export * from './cli/Framework';

/**
 * Créé une nouvelle application CLI
 */
export function cli (): Cli {

    const kernel = new Kernel();
    const app = new Cli(kernel);
    kernel.container().register('$application', app);
    return app;
}


/**
 * Créé un nouveau serveur
 * @param config Configuration, voir `server/Server`
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function server (config: Record<string, any> = { staticDir: './' }): Server {

    const kernel = new Kernel();
    const app = new Server(kernel, config);
    kernel.container().register('$application', app);
    return app;
}

/**
 * Pour créer nos formulaires
 */
export { FormBuilder } from './server/Form/FormBuilder';
/**
 * Pour créer nos fields
 */
export { Field } from './server/Form/Field';
/**
 * Pour créer nos contrôleurs
 */
export { Controller } from './server/Controller';
